

dials(3N)                                               dials(3N)

SSyynnooppssiiss
       cc [options] file -lnsl #include <dial.h>

       int dials(calls_t *call, calls_t **ret_call, dial_status_t
       *dstatus);

       void undials(int fd, calls_t *ret_call);

DDeessccrriippttiioonn
       dials returns a file descriptor for a terminal  line  that
       is  open for reading or writing.  The argument to dials is
       a calls_t structure.  The calls_t structure is defined  in
       the header file.

       When it is finished with a terminal line, the calling pro-
       gram should call undials to add HUPCL to  the  c_flags  of
       the termios structure for that line.

       The arguments to dials are defined as follows: The defini-
       tion of calls_t in the  dial.h  header  file  is:  typedef
       struct {      long flags;           /* reserved for future
       use */      struct termios *attr;    /* pointer to termios
       attribute  struct  */       int  speed;           /* speed
       used for the link */      char *device_name;    /*  device
       name  for  outgoing  line */      char *telno;          /*
       telephone number to dial */       char *caller_telno;   /*
       the   calling   telephone  number  */       char *sysname;
       /*  system  name  to  use  from  the   Systems   file   */
            unsigned  short  function; /* bit field for switching
       on required functions */       char *class;             /*
       the    device   to   search   for   in   Type   field   of
                                the  /etc/uccp/Devices  file   */
            char *protocol;       /* protocol string as specified
       in   Type    field    of                               the
       /etc/uccp/Systems  file  */       int  pinfo_len;       /*
       size  of  pinfo   (in   bytes)   */        void *pinfo;
       /*  pointer to protocol-specific information */ } calls_t;
       The elements of the calls_t structure are  defined  below:
       This  field  must be 0, and is reserved for future use.  A
       pointer  to  a  termios  structure,  as  defined  in   the
       termios.h header file.  A NULL value for this pointer ele-
       ment may be passed to the dials function, but  if  such  a
       structure  is  included, the elements specified in it will
       be set for the outgoing terminal line before  the  connec-
       tion  is established.  This setting is often important for
       certain attributes such as parity and baud rate.  Intended
       only  for  use  with  an  outgoing dialed call.  Its value
       should be either 300 or 1200 to identify the  113A  modem,
       or the high- or low-speed setting on the 212A modem.  Note
       that the 113A modem or the low-speed setting of  the  212A
       modem will transmit at any rate between 0 and 300 bits per
       second.  However, the high-speed setting of the 212A modem
       transmits and receives at 1200 bits per second only.

                                                                1

dials(3N)                                               dials(3N)

       A  value of -1 specifies any speed.  If the requested ter-
       minal line is a direct line, this field should  contain  a
       string  pointer to its device name.  Legal values for such
       terminal device names are kept in the file.  In this case,
       the  value of the speed element should be set to -1.  This
       value will cause dials to determine the correct value from
       the  Devices file.  A pointer to a character string repre-
       senting the telephone  number  of  a  system  name  to  be
       dialed.   Such  numbers  may consist only of these charac-
       ters: dial 0-9 dial * dial # wait for secondary dial  tone
       delay  for approximately 4 seconds This field is typically
       set for ISDN devices so that isdndialer can map the  tele-
       phone  number to the B channels on the ISDN adapters. This
       allows the application to select a B channel  for  placing
       outgoing  calls.   The  name  of the system to call.  This
       field is filled in by the  application  and  is  optional.
       This is a bit mask that represents functions to turn on or
       off.  This field defaults to 0, which is equivalent to the
       service.  The  following  bit masks are defined in dial.h:
       FUNC_NULL 0x00 /*   cu    functionality    (default)    */
       FUNC_CHAT 0x80 /*  allows  chat  scripts to be executed */
       FUNC_TX        0x40 /* sets the open line modes to  trans-
       fer  files  */  To enable functionality, set this field to
       FUNC_CHAT | FUNC_TX.  This supplies dials with  the  class
       parameter for the dialup connection.  The default class is
       NULL.  See the -c option of  for  the  meaning  of  class.
       This points to an area of static storage that contains the
       processed protocol field for the device used for the  con-
       nection.   The  protocol  string  is  reported back to the
       application  via  the  Connection  Server  interface.  The
       default  protocol  string  is NULL.  The Connection Server
       returns this data to the calling application.  The Connec-
       tion Server returns this data.

       The  definition  of  dial_status_t  is:  typedef  struct {
            int  status_code;        /* value defined  in  dial.h
       */       dial_service_t service_type;  /* ennumerated ser-
       vices available */ } dial_status_t;

FFiilleess
RReettuurrnn vvaalluueess
       On failure, a negative value indicating the reason for the
       failure  will  be  returned.  Mnemonics for these negative
       indexes are defined in the header file: INTRPT      -1  /*
       interrupt  occurred */ D_HUNG     -2  /* dialer failed (no
       return from write) */ NO_ANS     -3  /* no answer  (caller
       script  failed)  */ ILL_BD     -4  /* illegal baud rate */
       A_PROB      -5 /*  acu   problem   (open()   failure)   */
       L_PROB       -6   /*  line  problem  (open()  failure)  */
       NO_Ldv     -7 /* can't open  Devices  file  */  DV_NT_A
       -8  /* requested device not available */ DV_NT_K    -9  /*
       requested device not known */ NO_BD_A   -10  /* no  device
       available at requested baud */ NO_BD_K   -11  /* no device
       known at requested  baud  */  DV_NT_E   -12  /*  requested

                                                                2

dials(3N)                                               dials(3N)

       speed  does  not  match */ BAD_SYS   -13  /* system not in
       Systems  file  */  CS_PROB   -14  /*   connection   server
       related  error */ DV_W_TM   -15  /* not allowed to call at
       this time */

RReeffeerreenncceess
NNoottiicceess
       Including the dial.h header  file  automatically  includes
       the termios.h header file.

       An  system  call  for  3600  seconds  is made (and caught)
       within the dials module for the purpose  of  touching  the
       LCK.. file and constitutes the device allocation semaphore
       for the terminal device.  Otherwise, may simply delete the
       LCK..  entry  on its 90 minute clean up rounds.  The alarm
       may go off while the user program is in a or system  call,
       causing  an  apparent  error  return.  If the user program
       expects to be around for an hour or  more,  error  returns
       from  reads  should  be  checked for errno==EINTR, and the
       read possibly reissued.

       dials is an interface call to the Connection  Server  dae-
       mon.  The Connection Server establishes the connection and
       passes back the file descriptor to  dials.   See  and  for
       more information.

       The   lock  files  that  act  as  semaphores  for  devices
       (var/spool/locks/LC...)  are created and maintained by the
       Connection  Server.   They  do not have to (and should not
       be) removed by the user.

                                                                3

