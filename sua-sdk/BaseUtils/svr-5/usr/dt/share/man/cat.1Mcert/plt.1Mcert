

plt(1Mcert)                                           plt(1Mcert)

SSyynnooppssiiss
       $PLT_HOME/plt  $PLT_HOME  is the top of the directory tree
       where the executable and log files reside.   Its  location
       is:

       &geminiREL; &everest;, and Linux AIX 5L

DDeessccrriippttiioonn
       plt  is  the interface to a suite of tests that exercise a
       computer system under a heavy load.  These tests are  used
       to  qualify computer systems with the operating system and
       can also be used to test kernel sanity  after  new  device
       drivers and other kernel-level code have been added.

UUssaaggee
       The plt tests deliver output to xterm windows as they run,
       assuming that plt can successfully run an X program.  Each
       xterm  window  has  the name of the test in the title bar.
       All the test output is also written to logfiles  that  can
       be analyzed later; if plt cannot run an X program, it sup-
       presses the screen output and merely logs the  results  to
       the files.

       To run the plt tests: Open an xterm window and execute the
       $PLT_HOME/plt command.  The tests display notices  on  the
       screen,  ask  for  user input, and allow you to select the
       tests that will be run.  At the menu screen,  execute  the
       Balanced  load  tests  option  to  run  the tests that are
       required for certification.

       You can instead select individual tests to run when  test-
       ing  during  the development phase.  The network tests are
       optional on all systems; the graphics tests  are  part  of
       the  Balanced  load tests on AIX 5L but optional on &gemi-
       niREL;, &everest;, and Linux systems.  You can also select
       individual  graphics  tests (ICO, x11perf, or Maze) rather
       than running the full graphics suite.

       When you select the Balanced load tests option, the system
       runs  with  a  consistent but not onerous load.  Selecting
       individual tests causes them to run unbounded.   This  may
       exhaust resources and cause spurious failures.

       You will see the description string for the selected tests
       change to READY TO RUN.  Enter r to run  the  tests.   You
       will  be  prompted  for  additional parameters that may be
       required for individual tests such as network  and  graph-
       ics.   Specify  how long to run the tests in hours or min-
       utes, or just press  to  run  the  tests  without  a  time
       limit.

       For  certification, these tests should run for 36-48 hours
       without errors to give reasonable confidence of system and
       driver  sanity; specific requirements will be specified in

&geminiREL;, &everest;, AIX 5L, Linux-32, and Linux-64          1

plt(1Mcert)                                           plt(1Mcert)

       the certification agreement.  Shorter runs may  be  useful
       for  testing  during  the  development  process.  Insert a
       scratch floppy disk and an unmounted CD-ROM in the  appro-
       priate drives and press .  plt displays the actual command
       with options to be run and begins the tests.

       If you do not insert the media before starting the  tests,
       you  will  be  prompted to insert them and can do so while
       the test is running; the test suites will  recognize  when
       they  are available.  As the tests are running, the output
       from each test is displayed in xterm windows that  contain
       the  name of the test in the title bar.  Error information
       is written to files in the log directory.  Press  to  ter-
       minate  test  execution  before  the  time slice specified
       expires.  It may take several minutes for  the  system  to
       respond;  please  be patient.  The pltanalyze command that
       checks the test results summary is called at  the  end  of
       the  test  run.   It  may, however, report spurious errors
       because tests have not yet written the  final  information
       into  their  log files.  pltanalyze can be run manually: #
       cd $PLT_HOME # ./pltanalyze

       cdrom test:         SUCCESS  cpu  test:      SUCCESS  disk
       test:          SUCCESS  floppy test:        SUCCESS memory
       test:        SUCCESS network test:       NOT TESTED graph-
       ics test:          SUCCESS    # If any of the tests report
       FAIL, view the details in  the  appropriate  file  in  the
       $PLT_HOME/log  directory.   After you correct the problem,
       rerun the entire Balanced load tests suite, not  just  the
       test  that  failed.   Tests that were not part of the test
       run are marked NOT TESTED.

       The log files may contain  status  messages  that  do  not
       indicate failures in the tests.

       The  tests  write  a timestamp to the $PLT_HOME/log/sleepy
       file once every five minutes.  Should the test suite fail,
       or  should the system hang or panic, subtracting the first
       timestamp from the last timestamp will provide the elapsed
       time  the  tests  ran.   The  last  timestamp in this file
       determines the date and time at which there was a failure.

   TTeesstt ggrroouuppss
       These  test suites contain several tests groups, described
       below.  A log file is created for each test group  in  the
       $PLT_HOME/log directory.  Creates a very large working set
       of memory pages and randomly accesses them.   These  tests
       autoscale  to handle large and small memory configurations
       appropriately.  Tests the floating point  calculations  by
       using  the  and  routines.   The test suite determines the
       number of configured CPUs and forks at least one test  per
       CPU, but relies on the operating system to force execution
       on all configured CPUs.  Writes large and small  files  in
       different  modes,  creates  and  deletes  files and links,

&geminiREL;, &everest;, AIX 5L, Linux-32, and Linux-64          2

plt(1Mcert)                                           plt(1Mcert)

       executes various filesystem  operations  to  exercise  the
       hardware.   The  individual tests that are executed are in
       the $PLT_HOME/disktests directory.

       For &geminiREL;, additional disk tests are provided in the
       test  suites.   For  &everest;,  additional disk tests are
       provided in the test suites.  No additional disk tests are
       available  for  AIX  5L.   A continuous format and loop is
       executed to the floppy disk.  Recursive and  commands  are
       executed to exercise the CD-ROM subsystem.

       For  &geminiREL;,  additional CD-ROM tests are provided in
       the test suites.  For &everest;, additional  CD-ROM  tests
       are  provided  in  the  test suites.  No additional CD-ROM
       tests are available for AIX 5L.  Three graphics tests  are
       provided,  which  can be run individually: Displays a wire
       frame rotating icosahdron.   The  test  ensures  that  the
       client  refreshes  its contents and redraws the image cor-
       rectly under a variety of conditions.  Runs the standard X
       Window  performance  benchmarks  and reports statistics on
       server performance.

       Note that the x11perf test does  not  currently  run  well
       under  &geminiREL;  and  &everest;,  although  it does not
       cause any harm.  The test is best run under the test  har-
       ness  on  &geminiREL; and &everest;.  These and other test
       suites for the graphical subsystem are provided as part of
       the  xtests package for &geminiREL; and &everest;; we rec-
       ommend running these tests directly rather than as part of
       the  plt  tests on these platforms.  An automated X11 demo
       that repeatedly creates and solves a random maze.

       The graphics tests are part of the AIX  5L  Balanced  Load
       Tests  but  must  be manually selected for &geminiREL; and
       &everest;.  Continuously executes ftp commands.  The indi-
       vidual  tests  that  are executed are in the $PLT_HOME/net
       directory.

       For &geminiREL;, additional network card  tests  are  pro-
       vided  in the test suites.  For &everest;, additional net-
       work card tests are provided in the test suites.  No addi-
       tional network card tests are available for AIX 5L.

       Note that the network test suite is not currently included
       in the set of required tests executed  for  Balanced  load
       tests,  so  you  will need to request it separately.  Runs
       sar reports and logs the results.   This  along  with  the
       sleepy script are watchdogs that track activity and can be
       used to identify when a test failed.

   VVeerrssiioonn aapppplliiccaabbiilliittyy
       These tests are supported for all releases of &geminiREL;,
       AIX  5L,  and  &everest;.   They  have also been ported to
       Linux-32  and  Linux-64  although   they   are   not   yet

&geminiREL;, &everest;, AIX 5L, Linux-32, and Linux-64          3

plt(1Mcert)                                           plt(1Mcert)

       productized for Linux platforms.

       plt uses common source for all platforms.

   DDiiffffeerreenncceess bbeettwweeeenn vveerrssiioonnss
       Earlier versions of the plt suite were provided for &ever-
       est; as and for &geminiREL; as Those  versions  are  obso-
       leted  for  all releases of all  operating systems by this
       plt.

       The major new features for the plt suites are: Ability  to
       specify how long to run the tests, rather than having them
       run  continuously  until  they   are   manually   stopped.
       Autoscaling  to  handle  large  memory and disk configura-
       tions.  The test output is written to xterm windows rather
       than to virtual terminals or multiscreens as used for ear-
       lier versions of the tests.  Addition  of  tests  for  the
       graphics  subsystem.   Common source code for tests on all
       platforms.  Logging of  sar  data  during  the  test  run.
       Watchdog  processes  run  every  five  minutes rather than
       every minute as in earlier versions.  The pltanalyze  com-
       mand produces a summary of the each test run at the end of
       the run or can be run manually.  This  quickly  identifies
       any  test  failures, which can then be analyzed by studing
       the appropriate log files.  All log and summary files from
       a  plt run are moved to a logs.<n> directory when a subse-
       quent plt run is executed.  This enables  you  to  compare
       successive runs of the test suites.

FFiilleess
       The  files and directories listed with a relative pathname
       are created under the plt's home directory, referred to as
       $PLT_HOME:

       &geminiREL;, &everest;, and Linux AIX 5L

       Note  that  there is no actual plt user.  The test script.
       This can be run only as root.  Script to print summary  of
       test  results.   This script gives a quick view of whether
       the tests succeeded or failed.  Contains timestamps.   The
       data and time is written to this file once every five min-
       utes while the tests are running.  Bulletin that  must  be
       completed  and sent to your authorized  or IBM representa-
       tive to begin the process of getting your system or driver
       listed  in  the Compatible Hardware Web Pages (CHWP).  Log
       files generated by the most recent run of the  plt  tests.
       Log  files  and  TEST_RESULT files from previous plt runs.
       <n> is an incrementing number.  CD-ROM test scripts.   CPU
       tests.   Disk  subsystem  tests.   This  is  the front-end
       script that runs the individual  tests  in  the  disktests
       directory.  Individual tests run to exercise the disk sub-
       system.  Floppy tests.  Individual tests run  to  exercise
       the graphics subsystem.  Memory subsystem tests.  Individ-
       ual  tests  run  to  exercise  the  networking  subsystem.

&geminiREL;, &everest;, AIX 5L, Linux-32, and Linux-64          4

plt(1Mcert)                                           plt(1Mcert)

       Nework subsystem tests.  This is the front-end script that
       runs the individual tests in the net directory.   Watchdog
       script  that  writes  the  sar  output every five minutes.
       Watchdog script that writes the time and  datestamp  every
       five  minutes.  Contains information about the most recent
       execution of the plt tests.  Information recorded includes
       the time the test run started and stopped, the duration of
       the run, the tests that were run, and the pltanalyze  out-
       put.

       Each  successive  plt run moves this file and the contents
       of the log directory into a new logs.<n> directory so that
       each  test  run  begins with empty directories and yet the
       results of previous test runs is available for comparison.

EExxaammpplleess
       The  output  of the most recent pltanalyze execution along
       with other information about the test run is stored in the
       $PLT_HOME/TEST_RESULTS  file.  An example of this file for
       &everest; follows; similar output is  provided  on  &gemi-
       niREL; and AIX 5L.  # cat TEST_RESULTS

       SCO  Platform Load Tests 8.8 on OSR5 started at Tue Jan 23
       15:01:50 PST 2001

       SCO_SV rafiki 3.2 5.0.6 i386

       Starting Disk  Load  Tests:     /home/plt/tests/disk  OSR5
       gentle            Starting           Floppy           Load
       Tests:   /home/plt/tests/floppy  OSR5  Starting  CPU  Load
       Test  1:  /home/plt/tests/cpu  gentle Starting CD-ROM Load
       Tests:   /home/plt/tests/cdrom OSR5 Starting  Memory  Load
       Tests:   /home/plt/tests/memory -a 30 -m 133697536

       SCO  Platform  Load Tests ended at Tue Jan 23 15:02:50 PST
       2001

       Test duration was 60 seconds

       LOG FILE ANALYSIS

       cdrom test:         SUCCESS  cpu  test:      SUCCESS  disk
       test:          SUCCESS  floppy test:        SUCCESS memory
       test:        SUCCESS network test:       NOT TESTED graph-
       ics test:          NOT TESTED

       PLT was SUCCESSFUL.

       If  you  ran this test for SCO Certification, fill out the
       /home/plt/doc/bulletin file, and email it to your SCO rep-
       resentative,  along  with the /home/plt/send_me_to_sco.uue
       file.

&geminiREL;, &everest;, AIX 5L, Linux-32, and Linux-64          5

