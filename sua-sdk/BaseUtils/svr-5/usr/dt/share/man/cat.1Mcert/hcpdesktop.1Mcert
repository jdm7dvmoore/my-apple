

hcpdesktop(1Mcert)                             hcpdesktop(1Mcert)

DDeessccrriippttiioonn
       The hcpdesktop test and certification suites are run after
       the &geminiREL; or have run successfully, to  ensure  that
       the  graphics and networking subsystems behave properly on
       the system being tested.

UUssaaggee
                 Before beginning the hcpdesktop(ES) test make sure that the
                 the following components are properly installed and are
                 functional:

                 1.  Video subsystem and monitor
                 2.  A pointing device
                 3.  A Network Interface Card (NIC)
                 4.  The TCP/IP protocol stack

                 You are now ready to proceed to the "Desktop Test Recipe"
                 section. Note that there are separate sections for
                 Openserver and UnixWare.

            DESKTOP TEST RECIPE - for OpenServer 5 and OpenServer 3
                 These recipes are intended to assure a base level of
                 graphics and networking functionality by exercising
                 multiscreen(M), X11, and X(X), functionality and creating
                 network traffic.  A graphics display program, ico(X), is
                 used to verify proper graphics behavior.

               Local Testing
                 1.   Boot the system to the multi-user state.  On
                      multiscreen one (MS1), log in as root and create the
                      user "hcpuser".  The default settings are sufficient
                      for this test.  Exit the root logon session.  Log in as
                      hcpuser to test the new logon.  Leave hcpuser logged in
                      on MS1.

                 2.   Switch to multiscreen two (MS2), the default screen for
                      scologin(X).  Again, log in as hcpuser.  The X desktop
                      will display after a few moments.  Confirm that the
                      video subsystem, monitor, and mouse are all interacting
                      properly with the X server by visually inspecting the
                      screen for obvious problems (such as multiple images,
                      etc) and by confirming that the mouse is active and
                      behaving correctly.

                 3.   Start a scoterm(X) session by double-clicking the left
                      button on the "UNIX" icon.

                 4.   From the scoterm(X) session enter:

                          ico -faces &

                      Position the new window in the upper left-section of
                      the screen.

&geminiREL; and &everest;                                       1

hcpdesktop(1Mcert)                             hcpdesktop(1Mcert)

                 5.   Select the window displaying the scoterm(X) session and
                      enter:
                          ico -faces &
                      Position the new window in the upper right-hand section
                      of your display.  Verify proper behavior while both
                      ico(X) sessions are running.

                 6.   Allow both ico(X) sessions to continue executing.
                      Switch to MS1.  Enter the command:
                          /usr/hcptest/bin/cattermcap
                      The contents of /etc/termcap will be displayed on the
                      screen.

                 7.   While /etc/termcap is scrolling on the screen, switch
                      back to MS2.  Confirm that the ico(X) sessions are
                      displayed correctly.

                 8.   Switch back to MS1.  Confirm that the /etc/termcap
                      contents are still being scrolled to the screen.  If
                      the text is scrambled upon redisplay of MS1 then an
                      error condition has been discovered.

                 9.   To end these tests press <Del> while in MS1, terminate
                      the ico(X) sessions in MS2, and terminate the window in
                      which termcap(S) is scrolling.

               Remote Testing
                 This recipe requires that two systems are configured and
                 operational on a network.  In this recipe they will be
                 called "sys1" and "sys2".

                 1.   On sys2, login(M) as hcpuser on MS2.

                 2.   In MS2 on sys2, enter the following command from a
                      scoterm(X) session:
                          xhost +sys1

                 3.   On sys1, login(M) as hcpuser on MS2.

                 4.   In MS2 on sys1, start a scoterm(X) session and
                      rlogin(C) into sys2 by entering:
                          rlogin sys2

                 5.   Start an ico(X) process on sys2 with process output
                      being displayed on sys1 by entering:
                          ico -display sys1:0 -faces &

                 6.   The remotely executed ico(X) client should now be
                      displayed on the sys1 X(X) session just as it was in
                      the Local Testing recipe.

            FILES
                 /usr/hcptest/bin/cattermcap
                      A script that displays /etc/termcap in an enless loop.

&geminiREL; and &everest;                                       2

hcpdesktop(1Mcert)                             hcpdesktop(1Mcert)

            THE NEXT STEP
                 If this testing is completed successfully complete the test
                 bulletin and submit to your SCO representative.

            DESKTOP TEST RECIPE - for UnixWare
                 These recipes are intended to assure a base level of
                 graphics and networking functionality by exercising virtual
                 terminal, X11, and X(X), functionality and creating network
                 traffic.  A graphics display program, ico(X), is used to
                 verify proper graphics behavior.

               Local Testing
                 1.   Boot the system to the multi-user state.  On the
                      Graphical Screen ("GS"), log into the system
                      administrator account and create the user "hcpuser".
                      The default settings are sufficient for this test.  Log
                      out and relog in as hcpuser to test the new login
                      account.

                 2.   Switch to the console screen ("CS") by typing the
                      Alt+SysRq key sequence followed by the "h" key.  Again,
                      log in as hcpuser. When prompted for "Display Desktop
                      (y/n)?", type "n" to remain in character mode. Switch
                      back to GS by typing the Alt+SysRq key sequence
                      followed by the "n" key. Confirm that the video
                      subsystem, monitor, and mouse are all interacting
                      properly with the X server by visually inspecting the
                      screen for obvious problems (such as multiple images,
                      etc) and by confirming that the mouse is active and
                      behaving correctly.

                 3.   Start an xterm(X) session by double-clicking the left
                      button on the "Applications" icon, followed by double-
                      clicking the "Terminal" icon.

                 4.   From the xterm(X) session enter:
                          ico -faces &
                      Position the new window in the upper left-section of
                      the screen.

                 5.   Select the window displaying the xterm(X) session and
                      enter:
                          ico -faces &
                      Position the new window in the upper right-hand section
                      of your display.  Verify proper behavior while both
                      ico(X) sessions are running.

                 6.   Allow both ico(X) sessions to continue executing.
                      Switch to CS.  Enter the command:
                          /usr/hcptest/bin/cattermcap
                      The contents of /etc/termcap will be displayed on the
                      screen.

                 7.   While /etc/termcap is scrolling on the screen, switch

&geminiREL; and &everest;                                       3

hcpdesktop(1Mcert)                             hcpdesktop(1Mcert)

                      back to GS.  Confirm that the ico(X) sessions are
                      displayed correctly.

                 8.   Switch back to CS.  Confirm that the /etc/termcap
                      contents are still being scrolled to the screen.  If
                      the text is scrambled upon redisplay of CS, then an
                      error condition has been discovered.

                 9.   To end these tests press <Del> while in CS, terminate
                      the ico(X) sessions in GS, and terminate the window in
                      which termcap(S) is scrolling.

               Remote Testing
                 This recipe requires that two systems are configured and
                 operational on a network.  In this recipe they will be
                 called "sys1" and "sys2".

                 1.   On sys2, login(M) as hcpuser on GS.

                 2.   In GS on sys2, enter the following command from a
                      xterm(X) session:
                          xhost +sys1

                 3.   On sys1, login(M) as hcpuser on GS.

                 4.   In GS on sys1, start a xterm(X) session and rlogin(C)
                      into sys2 by entering:
                          rlogin sys2

                 5.   Start an ico(X) process on sys2 with process output
                      being displayed on sys1 by entering:
                          ico -display sys1:0 -faces &

                 6.   The remotely executed ico(X) client should now be
                      displayed on the sys1 X(X) session just as it was in
                      the Local Testing recipe.

            FILES
                 /usr/hcptest/bin/cattermcap
                      A script that displays /etc/termcap in an enless loop.

            THE NEXT STEP
                 If this testing is completed successfully complete the test
                 bulletin and submit to your SCO representative.

   VVeerrssiioonn aapppplliiccaabbiilliittyy
       7, 7mp, 8, 8mp on &geminiREL;"
       3, 3mp, 4, 4mp, 5, 5mp on &everest;

FFiilleess
RReeffeerreenncceess

&geminiREL; and &everest;                                       4

