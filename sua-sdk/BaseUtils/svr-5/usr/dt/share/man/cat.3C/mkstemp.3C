

mkstemp(3C)                                           mkstemp(3C)

SSyynnooppssiiss
       #include <stdlib.h>

       int mkstemp(char *template);

DDeessccrriippttiioonn
       The  mkstemp  function replaces the contents of the string
       pointed to by template by a unique file name, and  returns
       a  file descriptor for the file open for reading and writ-
       ing.  The function thus prevents any possible race  condi-
       tion  between  testing whether the file exists and opening
       it for use.

       The string in template should look like a file  name  with
       six  trailing X characters (XXXXXX); mkstemp replaces each
       X with a character from the portable file  name  character
       set.   The  characters  are chosen such that the resulting
       name does not duplicate the name of an existing file.

   RReettuurrnn vvaalluueess
       Upon successful completion, mkstemp returns an  open  file
       descriptor.   Otherwise -1 is returned if no suitable file
       could be created.

UUssaaggee
       It is possible to run out of letters.

       The mkstemp function does not check to  determine  whether
       the  file name part of template exceeds the maximum allow-
       able file name length.

       mkstemp actually changes the  template  string  which  you
       pass;  this  means  that  you cannot use the same template
       string more than once -- you need  a  fresh  template  for
       every unique file you want to open.

       When  mkstemp  is creating a new unique filename it checks
       for the prior existence of a file with  that  name.   This
       means  that if you are creating more than one unique file-
       name, it is bad practice to use the same root template for
       multiple invocations of mkstemp.

CCoommppaattiibbiilliittyy
       For portability with previous versions of the UNIX(R) sys-
       tem, is preferred over this function.

RReeffeerreenncceess
SSttaannddaarrddss ccoonnffoorrmmaannccee
       This routine conforms  to  X/Open  System  Interfaces  and
       Headers, Issue 4, Version 2.

                                                                1

