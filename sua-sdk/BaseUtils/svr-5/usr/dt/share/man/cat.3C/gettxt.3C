

gettxt(3C)                                             gettxt(3C)

SSyynnooppssiiss
       #include <unistd.h>

       char *gettxt (const char *msgid, const char *dflt_str);

DDeessccrriippttiioonn
       gettxt  retrieves  a text string from a message file.  The
       arguments to the function  are  a  message  identification
       msgid  and  a  default  string  dflt_str to be used if the
       retrieval fails.

       The text strings are in files created by the mkmsgs  util-
       ity    [see    and    installed    in    directories    in
       /usr/lib/locale/locale/LC_MESSAGES.

       The directory locale can be  viewed  as  the  language  in
       which  the text strings are written.  This is specified by
       the LC_MESSAGES category of setlocale [see which is  C  by
       default.

       The user can change the language in which the messages are
       displayed by invoking  the  setlocale  function  with  the
       appropriate arguments.

       The  user can also request that messages be displayed in a
       specific language by setting  environment  variables  (but
       only  if  a  call  to  setlocale(LC_MESSAGES,"") or setlo-
       cale(LC_ALL,"") is made from the  calling  program).   The
       first  of  the  following  environment  variables  with  a
       nonempty value is used: LC_ALL, LC_MESSAGES, and LANG.

       If the locale is explicitly changed (via  setlocale),  the
       pointers returned by gettxt may no longer be valid.

       The following depicts the acceptable syntax of msgid for a
       call to gettxt: [msgfilename]:msgnumber

       msgfilename indicates the message database  that  contains
       the  localized  version  of  the text string.  msgfilename
       must be limited to 14 characters.  These  characters  must
       be selected from a set of all characters values, excluding
       \0 (null) and the ASCII codes for / (slash) and : (colon).

       msgnum  must be a positive number that indicates the index
       of the string in the message database.

       If msgfilename does not exist in the locale (specified  by
       the last call to setlocale using the LC_ALL or LC_MESSAGES
       categories), or if the message number is  out  of  bounds,
       gettxt attempts to retrieve the message from the C locale.
       If this second retrieval fails, gettxt uses dflt_str.

       If msgfilename is omitted, gettxt attempts to retrieve the
       string from the default catalog specified by the last call

                                                                1

gettxt(3C)                                             gettxt(3C)

       to

       gettxt outputs: Message not found!!

       if: msgfilename is not a valid  catalog  name  as  defined
       above  no  catalog  is specified (either explicitly or via
       setcat) msgnumber is not  a  positive  number  no  message
       could be retrieved and dflt_str was omitted

   FFiilleess
       The following files are created by mkmsgs: default message
       files message files for language specified by locale

UUssaaggee
       In the following code fragment, test is the  name  of  the
       file that contains the messages and 10 is the message num-
       ber.     gettxt("test:10",    "hello    world\n");    get-
       txt("test:10",  "");  setcat("test"); gettxt(":10", "hello
       world\n");

   NNootteess
       gettxt overwrites  the  string  buffer  each  time  it  is
       called.  Applications should copy the data to preserve it.

RReeffeerreenncceess

                                                                2

