

dhcpc(1Mtcp)                                         dhcpc(1Mtcp)

SSyynnooppssiiss
       in.dhcpc  [  -D ] [-d debug_level] [-i interface_name] [-l
       lease_time] [-r request_address] [-t  select_timeout]  [-u
       user_id] [-v vendor_id]

       dcc dump | inform | reload | restart | start | stop

DDeessccrriippttiioonn
       dhcpc (/usr/sbin/in.dhcpc) is the DHCP client program that
       a system can use to obtain  its  configuration  parameters
       from  a Dynamic Host Configuration Protocol (DHCP) server.
       It is generally invoked to run as a daemon by an entry  in
       the /etc/inet/config file (see

       dhcpc  understands the following options: Do not daemonize
       the DHCP client. This option  is  intended  for  debugging
       purposes.  Control the amount of debugging messages gener-
       ated.  For example, -d 1 will set the debugging  level  to
       1.  Recognized values are 0, 1, 2, 3, and 4.  A value of 0
       generates no messages  and  1  to  4  generate  increasing
       amounts  of messages. Specifying an integer greater than 4
       has the same result as specifying 4. The default value  is
       1 which causes dhcpc to report startup messages.

       This  option overrides the value set for debug in the DHCP
       client configuration file.  Specify the name of  the  net-
       work  interface  on  which  to  broadcast  a DHCP discover
       request (for example, net0).  This option is required  for
       systems  which  have  more  than  one network interface. A
       received IP address offer will be applied to  this  inter-
       face.   Specify  the lease time in seconds to be requested
       from the DHCP server.

       This option overrides the value set for lease_time in  the
       DHCP client configuration file.  Specify the IP address to
       be requested from the DHCP server.

       This option overrides the value set for request_address in
       the  DHCP  client  configuration  file  and  the  value of
       CLIENT_IP in /var/adm/dhcpc.info.   Specify  the  time  in
       seconds that the client will wait for an offer from a DHCP
       server before  rebroadcasting  a  discover  message.   The
       default timeout period is 60 seconds.

       This  option overrides the value set for select_timeout in
       the DHCP client configuration file.  Specify a set of DHCP
       user class options to be requested from the DHCP server.

       This  option  overrides  the  value set for user_id in the
       DHCP client configuration file.  Specify  a  set  of  DHCP
       vendor class options to be requested from the DHCP server.

       This option overrides the value set for vendor_id  in  the
       DHCP  client  configuration  file.  The format of the DHCP

                                                                1

dhcpc(1Mtcp)                                         dhcpc(1Mtcp)

       client  configuration   file,   /etc/inet/dhcpc.conf,   is
       described in

       dhcpc  operates as follows: If a network interface has not
       been specified using the -i  option,  dhcpc  broadcasts  a
       DHCP  discover  message  on  the  default  (first) network
       interface. It then waits for an offer from a DHCP  server.
       If  the configured selection timeout period expires before
       it has received an offer, dhcpc  rebroadcasts  a  discover
       message.   On  receiving  an  offer,  dhcpc configures the
       local  host  directly  with  the  IP  address,   broadcast
       address,  and  network mask which it has obtained from the
       DHCP server.  dhcpc writes DHCP options  that  it  obtains
       from  the  DHCP server to the file /var/adm/dhcpc.opt with
       the  exception  of  encapsulated  vendor-specific  options
       which  are  written as raw data to /var/adm/dhcpc.eopt. It
       writes other received information to  /var/adm/dhcpc.info.
       dhcpc  uses  vendor  entries  in the configuration file to
       format this data and appends them  to  /var/adm/dhcpc.opt.
       The  DHCP  options  in  /var/adm/dhcpc.opt  are written as
       shell variable definitions.  dhcpc runs  the  commands  in
       /etc/inet/dhcpc.conf  in  the order that they are defined.
       These commands source the shell  variable  definitions  in
       /var/adm/dhcpc.opt  and  use  these  values selectively to
       configure the host.  If the -D option was  not  specified,
       dhcpc  converts  itself into a daemon running in the back-
       ground.  Depending on the entries that are defined in  the
       configuration  file, the DHCP options may include the fol-
       lowing values: The network mask  applied  to  the  network
       interface.  Specifies the offset of the client's subnet in
       seconds from Coordinated Universal Time (UTC).  A positive
       offset  indicates  a location east of the Greenwich merid-
       ian, and a negative offset indicates a  location  west  of
       the  Greenwich meridian.  The lease period in seconds of a
       dynamic IP address that has been  assigned  to  a  client.
       The type of the last DHCP message that was received.  This
       should normally  be  5  for  an  acknowledgment.   The  IP
       address of the DHCP server.  The rebind time is the period
       in seconds after the client obtains its lease at which the
       DHCP  server  suggests  the  client  broadcast  its  first
       request to renew its lease.  The rebind time is the period
       in seconds after the client obtains its lease at which the
       DHCP server  suggests  the  client  broadcast  its  second
       request  to  renew its lease.  dhcpc writes other informa-
       tion that it obtains to /var/adm/dhcpc.info: The date  (in
       seconds  since  the  epoch  (00:00  on January 1 1970)) at
       which the system was booted.  The IP address of  the  DHCP
       client.   The  date  (in seconds since the epoch (00:00 on
       January 1 1970)) at which the  lease  of  the  current  IP
       address  will  expire.  The IP address of the DHCP (BOOTP)
       gateway.  The IP address of  the  DHCP  server.   When  it
       starts,  dhcpc  requests  the  IP address specified by the
       value of CLIENT_IP in this file unless this is  overridden
       by   the   value   of  request_address  specified  in  the

                                                                2

dhcpc(1Mtcp)                                         dhcpc(1Mtcp)

       configuration file or by the argument to the -r option  on
       the command line.

   CClliieenntt IIPP aaddddrreessss rreenneewwaall
       If  the  DHCP  client  acquires  a dynamically assigned IP
       address, it  must  renew  the  address  before  its  lease
       expires.   Typically, the client is configured to run as a
       daemon so that it can renew the lease when 50% of  it  has
       run.

       If  the address is not available for renewal, or the lease
       expires altogether, the client suspends networking (losing
       open  connections),  obtains  a  new  IP  address and DHCP
       options from the server, reinitializes the host's  network
       interface,  and then re-runs the configuration commands in
       /etc/inet/dhcpc.conf with the newly acquired values.

   DDHHCCPP cclliieenntt ddaaeemmoonn ccoonnttrrooll
       The DHCP client daemon control program, dcc, allows you to
       control the operation of dhcpc from the command line.  The
       following operations are available: Dump the options  cur-
       rently   configured   for   the   client   to   the   file
       /var/tmp/dhcpc.dump.  This is equivalent  to  sending  the
       signal  SIGUSR2  to  dhcpc.   Send  a DHCP request for the
       options    defined    in    the     configuration     file
       /etc/inet/dhcpc.conf.   The values in /var/dhcpc/dhcpc.opt
       are updated and the commands defined in the  configuration
       file are re-run.  This is equivalent to sending the signal
       SIGUSR1 to dhcpc.  Reinitialize the client by running  the
       commands     defined    in    the    configuration    file
       /etc/inet/dhcpc.conf.  This is equivalent to  sending  the
       signal SIGHUP to dhcpc.  Note that this does not cause the
       lease of the IP address to be renewed.  Stop  and  restart
       dhcpc.   Start  dhcpc.   Stop dhcpc. This is equivalent to
       sending the signal SIGTERM to dhcpc.

FFiilleess
       list of DHCP  global  options  DHCP  client  configuration
       parameters   DHCP  server  configuration  parameters  DHCP
       client process ID configuration commands that may  be  run
       by  dhcpc contains an entry which can be enabled to invoke
       dhcpc as a daemon at boot time  raw  vendor-specific  DHCP
       options  obtained  from  the  server DHCP options obtained
       from the server in a form suitable for shell scripts  con-
       tains other information used by dhcpc

UUssaaggee
       The  is  used  to  configure  a system as a DHCP client by
       selecting this option in the settings of a LAN  interface.

       If  you  need  to  specify  any  options to dhcpc, add the
       options to  the  line  in  /etc/inet/config  that  invokes
       dhcpc.  For example, the following entry specifies the use
       of  interface  net1  and   a   debugging   level   of   3:

                                                                3

dhcpc(1Mtcp)                                         dhcpc(1Mtcp)

       3a:/usr/sbin/in.dhcpc::y:/etc/inet/dhcpc.conf:-i  net1  -d
       3: Shut down and reboot the system for the changes to take
       effect.

RReeffeerreenncceess
       RFC 1534, RFC 1542, RFC 2131, RFC 2132

                                                                4

