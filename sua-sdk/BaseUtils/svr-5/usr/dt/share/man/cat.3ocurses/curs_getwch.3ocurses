

curs_getwch(3ocurses)                       curs_getwch(3ocurses)

SSyynnooppssiiss
       cc [flag . . .] file -locurses [library . . .]

       #include <ocurses.h>

       int    getwch(void);   int   wgetwch(WINDOW   *win);   int
       mvgetwch(int y, int x); int mvwgetwch(WINDOW *win, int  y,
       int x); int ungetwch(int wch);

DDeessccrriippttiioonn
       The getwch, wgetwch, mvgetwch, and mvwgetwch routines read
       an EUC character from the  terminal  associated  with  the
       window,  transform it into a wchar_t character, and return
       a wchar_t character.  In no-delay mode,  if  no  input  is
       waiting,  the  value  ERR is returned.  In delay mode, the
       program waits until the system passes text through to  the
       program.   Depending  on  the  setting  of cbreak, this is
       after one character (cbreak mode), or after the first new-
       line  (nocbreak  mode).   In  half-delay mode, the program
       waits until a character is typed or the specified  timeout
       has been reached.  Unless noecho has been set, the charac-
       ter will also be echoed into the designated window.

       If the window is not a pad, and it has been moved or modi-
       fied  since  the  last  call to wrefresh, wrefresh will be
       called before another character is read.

       If keypad is TRUE, and a  function  key  is  pressed,  the
       token for that function key is returned instead of the raw
       characters.   Possible  function  keys  are   defined   in
       ocurses.h  with  integers beginning with 0401, whose names
       begin with KEY_.  If a character that could be the  begin-
       ning  of  a  function  key  (such  as escape) is received,
       curses sets a timer.  If the  remainder  of  the  sequence
       does not come in within the designated time, the character
       is passed through; otherwise, the function  key  value  is
       returned.   For  this  reason, many terminals experience a
       delay between the time a user presses the escape  key  and
       the escape is returned to the program.

       The  ungetwch routine places wch back onto the input queue
       to be returned by the next call to wgetwch.

   FFuunnccttiioonn kkeeyyss
       The following function keys, defined in  ocurses.h,  might
       be  returned  by  getwch if keypad has been enabled.  Note
       that not all of these may be  supported  on  a  particular
       terminal  if  the terminal does not transmit a unique code
       when the key is pressed or if the definition for  the  key
       is not present in the terminfo database.

                                                                1

curs_getwch(3ocurses)                       curs_getwch(3ocurses)

       -------------------------------------------------------------------
       -------------------------------------------------------------------
       Name                 Key name
       -------------------------------------------------------------------

              _KK_EE_YY____BB_RR_EE_AA_KK            Break key
       _KK_EE_YY____DD_OO_WW_NN             The four arrow keys ...
       _KK_EE_YY____UU_PP
       _KK_EE_YY____LL_EE_FF_TT
       _KK_EE_YY____RR_II_GG_HH_TT
       _KK_EE_YY____HH_OO_MM_EE             Home key (upward+left arrow)
       _KK_EE_YY____BB_AA_CC_KK_SS_PP_AA_CC_EE        Backspace
       _KK_EE_YY____FF_00               Function keys; space for 64 keys is reserved.
       _KK_EE_YY____FF_((_nn_))             For 0 <= n <= 63
       _KK_EE_YY____DD_LL               Delete line
       _KK_EE_YY____II_LL               Insert line
       _KK_EE_YY____DD_CC               Delete character
       _KK_EE_YY____II_CC               Insert char or enter insert mode
       _KK_EE_YY____EE_II_CC              Exit insert char mode
       _KK_EE_YY____CC_LL_EE_AA_RR            Clear screen
       _KK_EE_YY____EE_OO_SS              Clear to end of screen
       _KK_EE_YY____EE_OO_LL              Clear to end of line
       _KK_EE_YY____SS_FF               Scroll 1 line forward
       _KK_EE_YY____SS_RR               Scroll 1 line backward (reverse)
       _KK_EE_YY____NN_PP_AA_GG_EE            Next page
       _KK_EE_YY____PP_PP_AA_GG_EE            Previous page
       _KK_EE_YY____SS_TT_AA_BB             Set tab
       _KK_EE_YY____CC_TT_AA_BB             Clear tab
       _KK_EE_YY____CC_AA_TT_AA_BB            Clear all tabs
       _KK_EE_YY____EE_NN_TT_EE_RR            Enter or send
       _KK_EE_YY____SS_RR_EE_SS_EE_TT           Soft (partial) reset
       _KK_EE_YY____RR_EE_SS_EE_TT            Reset or hard reset
       _KK_EE_YY____PP_RR_II_NN_TT            Print or copy
       _KK_EE_YY____LL_LL               Home  down or bottom (lower left).  Keypad is
                            arranged like this:
                                 A1    up    A3
                                left   B2   right
                                 C1   down   C3
       _KK_EE_YY____AA_11               Upper left of keypad
       _KK_EE_YY____AA_33               Upper right of keypad
       _KK_EE_YY____BB_22               Center of keypad
       _KK_EE_YY____CC_11               Lower left of keypad
       _KK_EE_YY____CC_33               Lower right of keypad
       _KK_EE_YY____BB_TT_AA_BB             Back tab key
       _KK_EE_YY____BB_EE_GG              Beg(inning) key
       _KK_EE_YY____CC_AA_NN_CC_EE_LL           Cancel key
       _KK_EE_YY____CC_LL_OO_SS_EE            Close key
       _KK_EE_YY____CC_OO_MM_MM_AA_NN_DD          Cmd (command) key
       _KK_EE_YY____CC_OO_PP_YY             Copy key
       _KK_EE_YY____CC_RR_EE_AA_TT_EE           Create key
       _KK_EE_YY____EE_NN_DD              End key
       _KK_EE_YY____EE_XX_II_TT             Exit key
       _KK_EE_YY____FF_II_NN_DD             Find key

                                                                2

()                                                             ()

       _KK_EE_YY____HH_EE_LL_PP             Help key
       _KK_EE_YY____MM_AA_RR_KK             Mark key
       _KK_EE_YY____MM_EE_SS_SS_AA_GG_EE          Message key
       _KK_EE_YY____MM_OO_VV_EE             Move key
       _KK_EE_YY____NN_EE_XX_TT             Next object key
       _KK_EE_YY____OO_PP_EE_NN             Open key
       _KK_EE_YY____OO_PP_TT_II_OO_NN_SS          Options key
       _KK_EE_YY____PP_RR_EE_VV_II_OO_UU_SS         Previous object key
       _KK_EE_YY____RR_EE_DD_OO             Redo key
       _KK_EE_YY____RR_EE_FF_EE_RR_EE_NN_CC_EE        Ref(erence) key
       _KK_EE_YY____RR_EE_FF_RR_EE_SS_HH          Refresh key
       _KK_EE_YY____RR_EE_PP_LL_AA_CC_EE          Replace key
       _KK_EE_YY____RR_EE_SS_TT_AA_RR_TT          Restart key
       _KK_EE_YY____RR_EE_SS_UU_MM_EE           Resume key
       _KK_EE_YY____SS_AA_VV_EE             Save key
       _KK_EE_YY____SS_BB_EE_GG             Shifted beginning key
       _KK_EE_YY____SS_CC_AA_NN_CC_EE_LL          Shifted cancel key
       _KK_EE_YY____SS_CC_OO_MM_MM_AA_NN_DD         Shifted command key
       _KK_EE_YY____SS_CC_OO_PP_YY            Shifted copy key
       _KK_EE_YY____SS_CC_RR_EE_AA_TT_EE          Shifted create key
       _KK_EE_YY____SS_DD_CC              Shifted delete char key
       _KK_EE_YY____SS_DD_LL              Shifted delete line key
       _KK_EE_YY____SS_EE_LL_EE_CC_TT           Select key
       _KK_EE_YY____SS_EE_NN_DD             Shifted end key
       _KK_EE_YY____SS_EE_OO_LL             Shifted clear line key
       _KK_EE_YY____SS_EE_XX_II_TT            Shifted exit key
       _KK_EE_YY____SS_FF_II_NN_DD            Shifted find key
       _KK_EE_YY____SS_HH_EE_LL_PP            Shifted help key
       _KK_EE_YY____SS_HH_OO_MM_EE            Shifted home key
       _KK_EE_YY____SS_II_CC              Shifted input key
       _KK_EE_YY____SS_LL_EE_FF_TT            Shifted left arrow key
       _KK_EE_YY____SS_MM_EE_SS_SS_AA_GG_EE         Shifted message key
       _KK_EE_YY____SS_MM_OO_VV_EE            Shifted move key
       _KK_EE_YY____SS_NN_EE_XX_TT            Shifted next key
       _KK_EE_YY____SS_OO_PP_TT_II_OO_NN_SS         Shifted options key
       _KK_EE_YY____SS_PP_RR_EE_VV_II_OO_UU_SS        Shifted prev key
       _KK_EE_YY____SS_PP_RR_II_NN_TT           Shifted print key
       _KK_EE_YY____SS_RR_EE_DD_OO            Shifted redo key
       _KK_EE_YY____SS_RR_EE_PP_LL_AA_CC_EE         Shifted replace key
       _KK_EE_YY____SS_RR_II_GG_HH_TT           Shifted right arrow
       _KK_EE_YY____SS_RR_SS_UU_MM_EE           Shifted resume key
       _KK_EE_YY____SS_SS_AA_VV_EE            Shifted save key
       _KK_EE_YY____SS_SS_UU_SS_PP_EE_NN_DD         Shifted suspend key
       _KK_EE_YY____SS_UU_NN_DD_OO            Shifted undo key
       _KK_EE_YY____SS_UU_SS_PP_EE_NN_DD          Suspend key
       _KK_EE_YY____UU_NN_DD_OO             Undo key
       All routines return the integer ERR upon  failure  and  an
       integer value other than ERR upon successful completion.

RReeffeerreenncceess
NNoottiicceess
       The  header  file  ocurses.h  automatically  includes  the
       header files stdio.h and unctrl.h.

                                                                3

()                                                             ()

       Use of the escape key by a programmer for a single charac-
       ter function is discouraged.

       When   using  getwch,  wgetwch,  mvgetwch,  or  mvwgetwch,
       nocbreak mode and echo mode should not be used at the same
       time.   Depending on the state of the tty driver when each
       character is typed, the program  may  produce  undesirable
       results.

       Note that getwch, mvgetwch, and mvwgetwch may be macros.

                                                                4

