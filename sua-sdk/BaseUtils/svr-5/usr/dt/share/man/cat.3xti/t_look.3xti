

t_look(3xti)                                         t_look(3xti)

SSyynnooppssiiss
       cc [options] file -lnsl #include <xti.h>

       int t_look(int fd);

DDeessccrriippttiioonn
       This  function is an TLI/XTI local management routine used
       to return the current asynchronous event on the  transport
       endpoint  specified  by  fd.  The event indicated reflects
       the  service  type  of  the  transport  provider.   t_look
       enables  a  transport provider to notify a transport user,
       when the user is issuing functions in synchronous mode, if
       an  asynchronous  event has occurred on the specified end-
       point.

       Certain events require immediate notification of the  user
       and  are indicated by a specific error, TLOOK, on the cur-
       rent or next function to be executed.

       This function also enables a  transport  user  to  poll  a
       transport endpoint periodically for asynchronous events.

       Values returned by t_look include the following: A connect
       indication has arrived at the transport endpoint.  A  con-
       nect  confirmation  has arrived at the transport endpoint.
       (When the server accepts a connect request, the  confirma-
       tion  is  generated.)  User data has arrived at the trans-
       port endpoint.          Expedited user data has arrived at
       the transport endpoint.    A notification that the connec-
       tion was aborted or that the server did not accept a  con-
       nect  request  (disconnect  indication) has arrived at the
       transport endpoint.  Notification that  a  datagram  error
       occurred  (unitdata  error  indication) has arrived at the
       transport endpoint.  A request for the orderly release  of
       a  connection  (orderly release indication) has arrived at
       the transport endpoint.  Notification  that  it  is  again
       possible  to  send  user data has arrived at the transport
       endpoint.  Notification that it is again possible to  send
       expedited user data has arrived at the transport endpoint.

   PPaarraammeetteerrss
       the file descriptor for the local transport endpoint asso-
       ciated with the current event.

   SSttaattee ttrraannssiittiioonnss
       t_look  may be issued from any valid state except T_UNINIT
       and has no effect on the state.

FFiilleess
       X/Open Transport Interface Library (shared object) Network
       Services Library (shared object)

RReettuurrnn vvaalluueess
       On  success,  t_look  returns  0 if no event exists or the

                                                                1

t_look(3xti)                                         t_look(3xti)

       value that indicates which event exists.  On  failure,  -1
       is returned and t_errno is set to indicate the error.

   EErrrroorrss
       On  failure,  t_errno  may be set to one of the following:
       The specified file descriptor does not refer to  a  trans-
       port  endpoint.  A system error has occurred during execu-
       tion of this function.  A communication problem  has  been
       detected with the transport provider and there is no other
       value of t_errno to describe the error condition.

RReeffeerreenncceess

                                                                2

