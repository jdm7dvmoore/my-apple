

crypt(3G)                                               crypt(3G)

SSyynnooppssiiss
       cc [flag . . . ] file . . . -lgen [library] . . .

       #include <unistd.h>

       char *crypt(const char *key, const char *salt);

       void setkey(const char *key);

       void encrypt(char *block, int edflag);

DDeessccrriippttiioonn
       crypt is the password encryption function.  It is based on
       a one-way encryption algorithm  with  variations  intended
       (among  other  things) to frustrate use of hardware imple-
       mentations of a key search.

       key is the input string to encrypt, for instance, a user's
       typed password.  Only the first eight characters are used;
       the rest are ignored.  salt is a two-character string cho-
       sen  from  the  set  [a-zA-Z0-9./]; this string is used to
       perturb the hashing algorithm in  one  of  4096  different
       ways,  after  which the input string is used as the key to
       encrypt repeatedly a constant string.  The returned  value
       points to the encrypted input string.  The first two char-
       acters of the return value are the salt itself.

       The setkey and encrypt functions  provide  access  to  the
       hashing  algorithm.  The argument of setkey is a character
       array of length 64 containing  only  the  characters  with
       numerical  value  0  and  1.   This string is divided into
       groups of 8, the low-order bit in each group  is  ignored;
       this  gives  a  56-bit  key  that is set into the machine.
       This is the key that will be used with the  hashing  algo-
       rithm  to  encrypt the string block with the encrypt func-
       tion.

       The block argument of encrypt  is  a  character  array  of
       length  64  containing  only the characters with numerical
       value 0 and 1.  The argument array is modified in place to
       a  similar  array  representing  the  bits of the argument
       after having been subjected to the hashing algorithm using
       the  key  set  by setkey.  The argument edflag, indicating
       decryption rather than encryption, is ignored; use encrypt
       in libcrypt [see for decryption.

   EErrrroorrss
       If  edflag  is set to anything other than zero, errno will
       be set to ENOSYS.

RReeffeerreenncceess
NNoottiicceess
       The return value for crypt points to static data that  are
       overwritten by each call.

                                                                1

