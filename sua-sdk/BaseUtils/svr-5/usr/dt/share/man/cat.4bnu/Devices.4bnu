

Devices(4bnu)                                       Devices(4bnu)

DDeessccrriippttiioonn
       The  /etc/uucp/Devices file contains information about the
       devices that may be used to establish a link to  a  remote
       computer using the Basic Networking Utilities (BNU).  Sev-
       eral types of devices can be defined in the Devices  file,
       including  automatic  call units, direct links and network
       connections.

       All entries must contain at least five fields.  If a field
       is  left  blank, the field must contain a dash (-).  Blank
       lines, and lines that begin with white space, a tab, or  a
       hash sign (#) are ignored.

       The  fields  in the Devices file are defined, in order, as
       follows: Specifies the device name.  The device  name  can
       be  a name of your choosing, but it must match the name in
       the third field of the file.  The two names Direct and ACU
       are  reserved.   Generally the Type field is the name of a
       computer directly linked to  the  computer  on  which  the
       Devices  file  in question is located (to differentiate it
       from other directly linked computers) or  the  name  of  a
       network  accessible  to  the computer on which the Devices
       file is located (to differentiate it from other accessible
       networks).  Indicates a direct link to another computer or
       a switch.  Indicates that the link to a remote computer is
       made  through  an  Automatic  Call  Unit  (automatic  dial
       modem).  This modem may be connected  either  directly  to
       your  computer  or indirectly through a local area network
       (LAN) switch.  Specifies the name of the  LAN  or  switch.
       For  instance,  TCP  could be the name for the TCP/IP net-
       work.  Specifies a direct link to the  computer  Sys_name.
       This  naming scheme conveys the fact that the line associ-
       ated with this Devices entry is for a computer defined  in
       the  file.   Indicates  a  TLI  LAN-type  connection  with
       authentication scheme invocation.  Indicates a  sychronous
       ISDN  connection.   Indicates an asynchronous ISDN connec-
       tion.

       The keyword used in the Type field of the Devices file  is
       matched  against  the third field of Systems file entries,
       as shown in the following sample files:  #Systems  file  #
       eagle  Any ACU 1200 3251 ogin: nuucp ssword: password sys1
       Any CS -    sys1,uucico sys2  Any CS  -  sys3   Any  CS  -
       sys3,login  in:--in nuucp word:xx sys4  Any DK   9600 sys4
       INVOKE "cr1 -r" sys5  Any DK    9600  sys5         in:--in
       nuucp word:xx sys6  Any LAN - network_address

       #Devices file # ACU term/01m - 2400 att4024 Direct term/00
       - Any direct sysb term/03 - Any  uudirect  CS  -  -  -  CS
       LAN,eg tcp - - TLI \D The protocol to use for a device can
       be designated within the Type field.  Specifies the device
       name of the line (port) associated with the Devices entry.
       For instance, if the automatic dial modem for a particular
       entry  is  attached  to  the  /dev/term/01m line, the name

                                                                1

Devices(4bnu)                                       Devices(4bnu)

       specified in the Line field  is  term/01m.   The  optional
       modem  control  flag M indicates that the device should be
       opened without waiting for a carrier.  The  modem  control
       flag  is  separated  from the device name by a comma.  For
       example, term/01m,M.

       CS type entries must contain a dash (-) in the Line field.
       If  the  keyword ACU is used in the Type field and the ACU
       is an 801-type dialer, Line2 specifies the device name  of
       the  801  dialer.   Since  801-type  ACUs do not contain a
       modem, separate modems are required and are connected to a
       different  line, as defined in the Line field.  This means
       that one line is allocated to the modem and another to the
       dialer.  Non-801 dialers do not normally use this configu-
       ration.  Although non-801  dialers  therefore  ignore  the
       Line2 field, the field must contain a dash (-) as a place-
       holder.  CS entries must also contain a dash in the  Line2
       field.  If the ACU or Direct keywords are used in the Type
       field, the Class field needs to specify only the speed  of
       the  device.  The Class field may, however, specify a let-
       ter and a speed, for example, C1200  (Centrex),  or  D1200
       (Dimension  PBX).   This  allows  large organizations that
       have more than one type of telephone network to  differen-
       tiate between classes of dialers.  One network may be ded-
       icated to serving only internal office communications, for
       example,  while  another  handles external communications.
       In this case, it is necessary to distinguish which line(s)
       are used for internal communication and which are used for
       external communication.

       The keyword used in the Class field of the Devices file is
       matched  against  the fourth field of Systems file entries
       as shown in the following Devices and Systems file  lines.
       #Devices file # ACU tty11 - D1200 penril

       #Systems  file  #  eagle  Any  ACU  D1200 3251 ogin: nuucp
       ssword: password Some devices can be used  at  any  speed.
       In  this  case,  the  keyword  Any may be specified in the
       Class field.  If Any is specified, the line will match any
       speed  requested in a Systems file entry.  However, if the
       Devices file Class field specifies  Any  and  the  Systems
       file  Class  field  specifies  Any,  the speed defaults to
       1200bps.  Contains  pairs  of  dialers  and  tokens.   The
       dialer portion of a dialer-token pair may specify the name
       of an automatic dial modem, a LAN switch, or it may  spec-
       ify  direct  or  uudirect  for  a Direct Link device.  The
       token portion of a  dialer-token  pair  may  be  specified
       immediately following the dialer portion, or, if it is not
       present, it will be taken from  a  related  entry  in  the
       file.  There may be any number of dialer-token pairs.

       This  field  has  the  format: dialer token [dialer token]
       where the last pair may or may not be  present,  depending
       on  the  associated  device  (dialer).  In most cases, the

                                                                2

Devices(4bnu)                                       Devices(4bnu)

       last pair contains only a dialer  portion  and  the  token
       portion  is  retrieved from the Phone field of the Systems
       file entry.

       A valid entry for the dialer portion may be defined in the
       file or may be one of several special dialer types.  These
       special dialer types are compiled into  the  software  and
       are  therefore  available  without having to be entered in
       the Dialers file.  A request for a TLI network.  The  spe-
       cific network is chosen from the networks available to the
       system on which the requested service is available.   Gen-
       erally,  the  returned  network  connection  supports  and
       semantics, but may be modified using the  Devconfig  file.
       Bell  801  auto  dialer Transport Level Interface network.
       Generally, the network connection  returned  supports  and
       semantics  but  may  be  modified to support and using the
       Devconfig file.

       The Dialer-Token-Pairs field  may  be  structured  differ-
       ently,  depending on the device associated with the entry.

       If an automatic dial modem is connected directly to a port
       on  your  computer,  the  Dialer-Token-Pairs  field of the
       associated Devices file entry should only have  one  pair.
       This  pair  will  normally  be the name of the modem.  The
       dialer configuration files are found in /etc/uucp/default.
       If /etc/uucp/dialer exists, then that file is used to con-
       figure the dialer.  Otherwise, the /etc/uucp/Dialers  file
       is  checked  for  a corresponding entry.  For example, the
       28-8_Data_Fax_Modem has  a  configuration  file:  #Devices
       file # ACU term/01m,M - 38400 28-8_Data_Fax_Modem

       #/etc/uucp/default/28-8_Data_Fax_Modem file # MDM_SETUP=AT
       &F E0 V1 \V1 S0=0&K3\N6 MDM_SPEAKER=ATM0  MDM_DIALCMD=ATDT
       MDM_HANGUP=ATH  The  att2212c  device  is  defined  in the
       Dialers file:  #Devices  file  #  ACU  term/01m,M  -  1200
       att2212c

       #/etc/uucp/Dialers     file    #    att2212c    =+-,    ""
       atzod,o12=y,o4=n\r\c  06 atT\T\r\c ed Notice that only the
       dialer  portion (att2212c) is present in the Dialer-Token-
       Pairs field of the Devices file entry.   This  means  that
       the  token  to  be  passed to the dialer (in this case the
       phone number) is taken from the Phone field of  a  Systems
       file entry.

       If a direct link is to be established to a given computer,
       the  Dialer-Token-Pairs  field  of  the  associated  entry
       should  contain  the  keyword direct or uudirect.  This is
       true for both types of direct  link  entries,  Direct  and
       Sys_name (see the discussion of the Type field).

       If  you want to communicate with a computer that is on the
       same local network switch as your computer, your  computer

                                                                3

Devices(4bnu)                                       Devices(4bnu)

       must  first access the switch, and the switch can make the
       connection to the other computer.  In this type of  entry,
       there  is  only  one  pair.  The dialer portion is used to
       match a Dialers file entry as shown here: #Devices file  #
       Datakit term/03 - 9600 datakit

       #Dialers  file  # datakit ""      "" \d TION: - - :TION \D
       In the example, the token portion  is  left  blank.   This
       indicates that it is retrieved from the Systems file.  The
       Systems file entry for this particular computer will  con-
       tain  the  token  in  the  Phone  field, which is normally
       reserved for the telephone number of the computer (see the
       discussion of the Phone field in This type of dialer-token
       pair contains an escape character (\D) to ensure that  the
       contents  of  the Phone field will not be interpreted as a
       valid entry in the Dialcodes file.

       If an automatic dial modem is connected to a switch,  your
       computer  must first access the switch and the switch will
       make the connection to the  automatic  dial  modem.   This
       type of entry requires two dialer-token pairs.  The dialer
       portion of each pair (the fifth and seventh fields of  the
       entry)  will be used to match entries in the Dialers file.
       #Devices file # ACU term/04 - 1200 datakit dial att2212c

       #Dialers file # datakit ""      "" \d TION: - -  :TION  \D
       att2212c =+-, "" atzod,o12=y,o4=n\r\c \006 atT\T\r\c ed In
       the first pair, datakit is the  dialer  and  dial  is  the
       token  that  is  passed  to  the Datakit switch to tell it
       which device (automatic dial modem)  to  connect  to  your
       computer.   This  token will be unique for each LAN switch
       since each switch may be set  up  differently.   Once  the
       modem  has been connected, the second dialer-token pair is
       accessed.  The second dialer is  att2212c;  the  token  is
       retrieved from the Systems file.

       There  are  two  escape  characters  that  may appear in a
       Dialer-Token-Pairs field: Specifies that the Phone (token)
       field should be translated using the Dialcodes file.  This
       escape character is normally placed in  the  Dialers  file
       for  each  caller script associated with an automatic dial
       modem.  The translation  will  not  therefore  take  place
       until  the  caller script is accessed.  Indicates that the
       Phone (token) field should not  be  translated  using  the
       Dialcodes  file.   A  \D  is also used in the Dialers file
       with entries associated with  network  switches  (develcon
       and micom).

       If  the  dialer  is an internal dialer, \T is the default.
       Otherwise, \D is the default.

FFiilleess
UUssaaggee
       To use BNU, the system administrator must manually  create

                                                                4

Devices(4bnu)                                       Devices(4bnu)

       the  entries  in  the  Devices  file:  the system-supplied
       Devices file contains only  comment  lines.   The  Devices
       file works closely with the and files.  Note that a change
       to an entry in one file may require a change to a  related
       entry in another file.

   PPrroottooccoollss
       You can choose the protocol to use with each device.  Typ-
       ically, specifying a protocol is not necessary, since  you
       can  use the default.  If you do specify the protocol, you
       must do so in the form: type, protocol [(windows,  packet-
       size)]  Available  protocols are: Generic packet protocol.
       This protocol provides error detection and  retransmission
       over  potentially noisy lines.  By its nature, it is rela-
       tively slow.

       Two parameters characterize the g  protocol:  windows  and
       packetsize.   windows indicates the number of packets that
       may be transmitted without waiting for an  acknowledgement
       from  the remote host.  packetsize indicates the number of
       data bytes in each packet.  By default, windows is set  to
       7 packets and packetsize is set to 64 bytes.  Identical to
       the g protocol in that it provides the same  error  detec-
       tion  and  retransmission;  however, the G protocol allows
       the number of windows and the packet size to be varied  to
       match  the  characteristics  of  the  transmission medium.
       When properly configured, performance can be significantly
       better than the g protocol.

       windows  may  range  from 1 to 7, and packetsize may range
       from 32 to 4096 bytes, in powers of 2.  Assumes error-free
       transmission and performs no error checking or retransmis-
       sion.  The e protocol is the fastest of the  three  proto-
       cols.  Use it for reliable local area networks only.

       There are no parameters to be tuned within the e protocol.
       Makes the same assumptions as the  e  protocol  (that  is,
       that  the underlying network provides an error-free commu-
       nications channel that  transfers  the  data  in  sequence
       without duplication), but uses Datakit-specific ioctls.

       There are no parameters to be tuned within the d protocol.
       By default, protocol is set to g(7,64)G(7,64)ed.

       The following example uses the e protocol  over  a  TCP/IP
       local area network.  If the e protocol is not available, g
       will be used instead.  TCP,eg tcp - -  TLIS  \D  The  next
       example  uses  the  G protocol on a high-speed modem.  The
       number of windows is set to 7, and the packet size is  512
       bytes.   If  the  G protocol is unavailable, the generic g
       protocol will  be  used.   ACU,G(7,512)g  term/11  -  9600
       att2296a  Presumably,  seven windows with a packet size of
       512 bytes will provide optimum throughput for  the  speci-
       fied device.

                                                                5

Devices(4bnu)                                       Devices(4bnu)

       For  incoming connections, the preferred protocol priority
       and parameters may be specified in the  Config  file  (see
       using the protocol parameter.

RReeffeerreenncceess

                                                                6

