

DmiNew(3dmi)                                         DmiNew(3dmi)

SSyynnooppssiiss
       #include <dmi2mem.h>

       DmiTimestamp_t   *   DmiNewTimestamp   (       DmiMemDsc_t
       memDsc,      DmiErrorStatus_t * status);

       DmiString_t  *  DmiNewString   (        char   *   src   ,
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiOctetString_t  *  DmiNewOctetString (      size_t size,
            char * src  ,       DmiMemDsc_t  memDsc,       DmiEr-
       rorStatus_t * status);

       DmiDataUnion_t   *   DmiNewDataUnion   (       DmiMemDsc_t
       memDsc,      DmiErrorStatus_t * status);

       DmiEnumInfo_t * DmiNewEnumInfo (      DmiMemDsc_t  memDsc,
            DmiErrorStatus_t * status);

       DmiAttributeInfo_t       *      DmiNewAttributeInfo      (
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiAttributeData_t      *      DmiNewAttributeData       (
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiGroupInfo_t * DmiNewGroupInfo(      DmiMemDsc_t memDsc,
            DmiErrorStatus_t * status);

       DmiComponentInfo_t      *      DmiNewComponentInfo       (
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiFileDataInfo_t  * DmiNewFileDataInfo (      DmiMemDsc_t
       memDsc,      DmiErrorStatus_t * status);

       DmiClassNameInfo_t      *      DmiNewClassNameInfo       (
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiRowRequest_t   *  DmiNewRowRequest  (       DmiMemDsc_t
       memDsc,      DmiErrorStatus_t * status);

       DmiRowData_t * DmiNewRowData  (       DmiMemDsc_t  memDsc,
            DmiErrorStatus_t * status);

       DmiAttributeIds_t * DmiNewAttributeIds (      size_t size,
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiAttributeValues_t * DmiNewAttributeValues (      size_t
       size,       DmiMemDsc_t  memDsc,       DmiErrorStatus_t  *
       status);

       DmiEnumList_t  *  DmiNewEnumList   (        size_t   size,
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiAttributeList_t  *  DmiNewAttributeList  (       size_t

                                                                1

DmiNew(3dmi)                                         DmiNew(3dmi)

       size,       DmiMemDsc_t  memDsc,       DmiErrorStatus_t  *
       status);

       DmiGroupList_t   *  DmiNewGroupList  (       size_t  size,
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiComponentList_t  *  DmiNewComponentList  (       size_t
       size,       DmiMemDsc_t  memDsc,       DmiErrorStatus_t  *
       status);

       DmiFileDataList_t * DmiNewFileDataList (      size_t size,
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiClassNameList_t  *  DmiNewClassNameList  (       size_t
       size,       DmiMemDsc_t  memDsc,       DmiErrorStatus_t  *
       status);

       DmiStringList_t  *  DmiNewStringList  (       size_t size,
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiFileTypeList_t * DmiNewFileTypeList (      size_t size,
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiMultiRowRequest_t * DmiNewMultiRowRequest (      size_t
       size,       DmiMemDsc_t  memDsc,       DmiErrorStatus_t  *
       status);

       DmiMultiRowData_t * DmiNewMultiRowData (      size_t size,
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiString_t FAR * DmiNewString (      const  char  *  src-
       String  ,      DmiMemDsc_t memDsc,      DmiErrorStatus_t *
       status);

       DmiString_t FAR * DmiNewUnicodeString (      const wchar_t
       *  srcUnicodeString,       DmiMemDsc_t memDsc,      DmiEr-
       rorStatus_t * status);

       DmiOctetString_t FAR  *  DmiNewOctetString  (       size_t
       sizeOctet,          const     char    *    srcOctetString,
            DmiMemDsc_t memDsc,      DmiErrorStatus_t * status);

       DmiNodeAddress_t * DmiNewNodeAddress (      const  char  *
       pAddress,       const  char  *  pRpc,       const  char  *
       pTransport,      DmiMemDsc_t memDsc,      DmiErrorStatus_t
       FAR * status);

DDeessccrriippttiioonn
       The  DmiNew  functions create the DMI data type structures
       in a memory model specified by the memDsc parameter.  They
       create  data  of  the  type  specified by the postfix. For
       example, DmiNewTimeStamp creates a  new,  zero-initialized
       DmiTimeStamp.

                                                                2

DmiNew(3dmi)                                         DmiNew(3dmi)

       The  contents of the newly-created data depend on the data
       type and the function's input parameters.  If  a  function
       does  not have default input parameters other than memDsc,
       then only zero-initialized, top-level DMI data type struc-
       tures  are  created.  For  example, the function creates a
       DmiRowData structure with all the members  initialized  to
       zero.

       Functions  that  create  DMI list data types (for example,
       DmiAttributeList and DmiAttributeIds allocate  a  list  of
       specified  size.  These  functions allocate both top-level
       structures ({size, list}) and zero-initialized  arrays  of
       corresponding  elements.  Top-level structures contain the
       specified list size and a  pointer  to  the  newly-created
       list.

       Some  functions let you specify the entire contents of the
       new data structure. The  DmiNewString  and  DmiNewUnicode-
       String  functions  create new DmiString for specified ANSI
       or UNICODE strings, correspondingly.  DmiString  size  and
       contents are determined by the input string.

       The  DmiNewOctetString  function  creates  a new DmiOctet-
       String of specified size and content.

       The DmiNewNodeAddress function  creates  a  DmiNodeAddress
       structure   with  specified  address,  rpc  and  transport
       fields.

   PPaarraammeetteerrss
       (Input) Memory model descriptor. If memDsc is  NULL,  then
       the  default memory model is used.  (Input) Number of ele-
       ments in the data_type list.  (Input) Null-terminated ANSI
       string.   (Input) Null-terminated UNICODE string.  In UNI-
       CODE, a null character is 2 octets.   (Input)  Pointer  to
       array   of  octets.   (Input)  Number  of  octets  in  the
       srcOctetString.   (Input)  NULL-terminated   string   that
       defines  the  address body of the DmiNodeAddress.  (Input)
       NULL-terminated string that defines the rpc  body  of  the
       DmiNodeAddress.    (Input)  NULL-terminated  string  which
       defines the transport body of the  DmiNodeAddress.   (Out-
       put)  Pointer  to  an  address  in memory where the DmiEr-
       rorStatus_t code is returned or  NULL.   When  the  status
       pointer  is NULL, the error status is not returned. Possi-
       ble error codes are:

RReettuurrnn vvaalluueess
       If successful, the DmiNew functions return a pointer to  a
       newly  allocated  DMI  data_type.   Otherwise, they return
       NULL.

NNoottiicceess
       Portions of this page are derived from material for  which
       the  copyright  owner  is  Intel  Corporation,  Inc.   The

                                                                3

DmiNew(3dmi)                                         DmiNew(3dmi)

       material is reprinted with permission.  See copyright page
       for a full statement of rights and permissions.

                                                                4

