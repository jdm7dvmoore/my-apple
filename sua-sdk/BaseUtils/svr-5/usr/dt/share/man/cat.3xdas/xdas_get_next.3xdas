

xdas_get_next(3xdas)                         xdas_get_next(3xdas)

SSyynnooppssiiss
       cc  [flag  ...]  file  ...   -Kthread -leels [library] ...
       #include <sys/xdas.h>

       OM_uint32  xdas_get_next(       OM_uint32   *minor_status,
            const     xdas_audit_ref_t    *das_ref,         const
       xdas_audit_stream_t     *audit_stream_ref,           const
       OM_uint32     *max_records,         const    xdas_buffer_t
       *audit_record_buffer,      OM_uint32 *no_of_records);

DDeessccrriippttiioonn
       The xdas_get_next(3xdas) function is a member of the Basic
       XDAS Conformance class.

       xdas_get_next(3xdas)  copies  up  to  max-records complete
       records from the audit stream accessed by das_ref into the
       buffer,  audit_record_buffer,  previously allocated by the
       caller.  The actual number of  records  retrieved  by  the
       function is returned in no_of_records.

       The caller must possess the XDAS_AUDIT_READ authority.

       If  the  function  successfully  reads one or more records
       from the audit stream,  the  cursor  associated  with  the
       audit  stream (referred to by das_ref) will be advanced to
       the next unread record in the stream.

       If the call is unsuccessful, the position of the cursor is
       not changed.

       If   there   are   no   more   available   audit  records,
       no_of_records  is  set  to  0  and  the  function  returns
       XDAS_S_END.

       If  the size of the buffer, audit_record_buffer, allocated
       by the caller is too small to hold a single audit  record,
       no_of_records  is  set  to  0  and  the  function  returns
       XDAS_S_BUFF_TOO_SMALL.

       On success, xdas_get_next(3xdas) returns  XDAS_S_COMPLETE.

   AArrgguummeennttss
       (Output) Provides additional information when the function
       returns XDAS_S_FAILURE.  (Input)  A  handle  to  the  XDAS
       server,  obtained from a previous call to (Input) A handle
       to the XDAS audit stream, obtained from a previous call to
       (Input)  The  maximum  number of records to be returned by
       the function in any one  call.   (Input)  Pointer  to  the
       buffer to which the audit records are to be copied.  (Out-
       put)  The  number  of   records   actually   copied   into
       audit_record_buffer.

RReettuurrnn vvaalluueess
       The  caller  does  not possess the correct authority.  The

                                                                1

xdas_get_next(3xdas)                         xdas_get_next(3xdas)

       buffer allocated by the caller is too small to hold a sin-
       gle  audit record.  Successful completion.  The end of the
       audit stream has been reached.  An implementation-specific
       error  or  failure  has occurred, such as missing required
       parameters, or a malloc failure.  The specified audit ser-
       vice handle does not point to the audit daemon.  The spec-
       ified audit stream handle is invalid.

RReeffeerreenncceess

                                                                2

