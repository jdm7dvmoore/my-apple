

xdas_put_event_info(3xdas)             xdas_put_event_info(3xdas)

SSyynnooppssiiss
       cc  [flag  ...]  file  ...   -Kthread -leels [library] ...
       #include <sys/xdas.h>

       OM_uint32 xdas_put_event_info(      OM_uint32  *minor_sta-
       tus,        const  xdas_audit_ref_t  *das_ref,       const
       xdas_audit_desc_t   *audit_record_descriptor,        const
       OM_unit32   event_number,       const  OM_unit32  outcome,
            const      xdas_buffer_t      *initiator_information,
            const  xdas_buffer_t  *target_information,      const
       xdas_buffer_t *event_information);

DDeessccrriippttiioonn
       The xdas_put_event_info(3xdas) function is a member of the
       XDAS Event Submission API Option Conformance class.

       xdas_put_event_info(3xdas)  adds  event  information to an
       audit record or overwrites existing  information.  If  the
       combination  of  information submitted and already present
       in the audit record referred to by audit_record_descriptor
       is  insufficient to evaluate applicable pre-selection cri-
       teria, the function returns XDAS_S_NO_DECISION_YET to  the
       caller.  If there is sufficient information for evaluation
       of applicable pre-selection checks the XDAS_S_COMPLETE  or
       XDAS_S_NO_AUDIT  are  returned  to  the  caller.  Multiple
       calls to xdas_put_event_info(3xdas) may be made.  For  any
       individual  parameter,  information  supplied in this call
       will overwrite any previous information supplied.

       Although several parameters are optional in this  call,  a
       caller  must  have populated all the parameters, even when
       empty,  in  one  or  more  sequences  of  calls   to   and
       xdas_put_event_info(3xdas)  before  a  call to can be suc-
       cessful.

       The caller must have the XDAS_AUDIT_SUBMIT authority.

       If  successful,  the  function  returns   XDAS_S_COMPLETE,
       XDAS_S_NO_DECISION_YET     or     XDAS_S_NO_AUDIT.      If
       XDAS_S_NO_AUDIT is returned, audit_record_descriptor is no
       longer a valid reference to an audit record.

       If  XDAS_S_NO_DECISION_YET  is returned, the caller should
       continue to construct the audit record by subsequent calls
       to xdas_put_event_info(3xdas).

   AArrgguummeennttss
       (Output)  An  implementation  specific  return status that
       provides additional  information  when  XDAS_S_FAILURE  is
       returned  by the function.  (Input) The handle to the XDAS
       server, obtained from a previous call to (Input) The  han-
       dle  to the audit record, obtained from a previous call to
       (Optional input) The event number of the  detected  event.
       If  specified  as  a  NULL  value  then  the  event number

                                                                1

xdas_put_event_info(3xdas)             xdas_put_event_info(3xdas)

       currently  associated  with   audit_record_descriptor   is
       unchanged.  Otherwise, event_number overwrites the current
       value.   Only  registered  event  numbers  configured  are
       valid.  Any  other  event  number results in the return of
       XDAS_S_INVALID_EVENT_NO.  (Optional input) The outcome  of
       the event determined by the caller. If specified as a NULL
       value then the  outcome  code  currently  associated  with
       audit_record_descriptor  is unchanged by this call. Other-
       wise outcome_code overwrites the current value.  Only  the
       outcome  codes  listed in are valid.  (Optional input) The
       information  describing  the  initiator  in   the   format
       required  by the XDAS common audit format. If specified as
       XDAS_C_NO_BUFFER the current initiator information associ-
       ated   with   the   audit_record_descriptor   supplied  is
       unchanged by this call. Otherwise the contents of  initia-
       tor_information  overwrite  the  current  value associated
       with audit_record_descriptor.  (Optional input) The infor-
       mation  on  the target of the event in the format required
       by  the  XDAS  common  audit  format.   If  specified   as
       XDAS_C_NO_BUFFER the current target information associated
       with the audit_record_descriptor supplied is unchanged  by
       this  call.  Otherwise  the contents of target_information
       overwrite    the    current    value    associated    with
       audit_record_descriptor.   (Optional input) The event spe-
       cific information that is to be added to the audit  record
       specified  by  audit_record_descriptor.   If  specified as
       XDAS_C_NO_BUFFER the current  event  specific  information
       associated  with  the  audit_record_descriptor supplied is
       unchanged  by  this  call.  Otherwise  the   contents   of
       event_information  overwrite  the current value associated
       with audit_record_descriptor.

RReettuurrnn vvaalluueess
       The caller does not possess the required authority.   Suc-
       cessful  completion.   An implementation specific error or
       failure has occurred, such as missing required parameters,
       or  a  malloc  failure.  The audit service handle supplied
       does not point to the audit service.  The specified  audit
       event  information is invalid.  The specified event number
       is invalid.  The initiator information supplied has a syn-
       tax error.  The specified outcome is not valid.  The spec-
       ified audit record descriptor is invalid.   The  specified
       target  information  has a syntax error.  The event speci-
       fied does not need to be audited.  The audit  service  has
       insufficient  information  to decide if the event requires
       auditing.

RReeffeerreenncceess

                                                                2

