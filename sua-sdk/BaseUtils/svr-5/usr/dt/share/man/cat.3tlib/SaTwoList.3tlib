

SaTwoList(3tlib)                                 SaTwoList(3tlib)

SSyynnooppssiiss
       SaTwoList  name  label1  items1 label2 items2 buttonlabel1
       buttonlabel2  [options]  SaTwoListAddItems  twolist   list
       items  SaTwoListDeleteItems twolist list SaTwoListGetItems
       twolist list SaTwoListSelectItems twolist list items  SaT-
       woListGetTabList twolist SaTwoListSetFocus twolist [list]

DDeessccrriippttiioonn
       These  interfaces  implement  a commonly used complex user
       interface widget, the two list. It  consists  of  adjacent
       lists  where items from one list can be moved or copied to
       the other.  Two lists apply to any situation where a  user
       must  divide  a  set of items into two disjoint sets (e.g.
       allowed users, denied users) or where one list  represents
       the  set  of  all  possible  values  and  the other list a
       selected subset. Common usage arranges the  two  lists  so
       that  users will tend to ``build'' their selection by mov-
       ing from left to right.

       SaTwoList creates a form widget which includes two  lists,
       each  with  a  title,  an  initial  list  of items, and an
       optional search  field.   Push  buttons  and  double-click
       actions  cause items to migrate between the lists. Initial
       focus can be set to either list and the two  list  widgets
       can be included in a larger tab list in the event that the
       two list is embedded in a larger dialog.

       In the documentation  that  follows,  the  two  individual
       lists are referred to as ``1'' (leftmost) and ``2''.  cre-
       ates the two  list  form.  SaTwoList  returns  the  widget
       string of the new form.

       A  rich  set  of  options to SaTwoList can be used to cus-
       tomize the behavior of the two list widget in a variety of
       ways  (See  below).   adds  items to one of the two lists.
       deletes items from one of the  two  lists.   retrieve  the
       items  from  one  of  the  two lists.  selects one or more
       items in one of the two lists.  returns an ordered list of
       widgets  in  the  two  list  form  representing  a logical
       sequence of traversal using the  key. This  list  must  be
       added  to  a tab and/or focus list for the form containing
       the two list in the event that it provides additional wid-
       gets.   sets the focus to one of the two lists. List ``1''
       is the default.

   AArrgguummeennttss
       &VTcl; widget name for the new form.  title label for list
       1.   label  name for the 1 to 2 button.  list of items for
       list 1.  title label for list 2.  label name for the 2  to
       1  button.   list  of  items for list 2.  Tcl list of 0 or
       more options where each option is two item list giving the
       option  keyword  and a value: specifies the height of each
       list in rows. The default is 7.  specifies  the  width  of
       each  list in columns.  Normally this is set automatically

                                                                1

SaTwoList(3tlib)                                 SaTwoList(3tlib)

       to the widest  element  or  list  title.   This  indicates
       whether  the  list  should  be single or multi-select. The
       default is multi.  optional text fields can  be  displayed
       which allow the user to search for an entry in the list by
       typing in the entry and pressing return. This  causes  the
       entry  to  be selected and all previously selected entries
       to be un-selected.  The default is not  to  display  these
       text fields.  They can be turned on for each list individ-
       ually or for both lists.  normally the lists are sorted in
       ascending localized order. Specifying 'none' will turn off
       sorting. One can also specify different  sorting  instruc-
       tions  as  long  as they are valid lsort options (validity
       isn't checked up front but by lsort when  it  is  called).
       by  default,  when  ever  an  entry is transferred between
       lists the entry is moved; that is the entry  is  added  to
       the destination list and removed from the orginating list.
       Entries can instead be copied from one list to  the  other
       which  means  that the entry in the orginating list is not
       removed. The copy behavior can be set for either  transfer
       direction  or  for  both  (always).  indicates whether the
       selected items after a transfer operation (either the but-
       tons  or  double click) are the items just transfered into
       the list (follow) or the next available item in the origi-
       nating  list  (next).  The default is next.  widget string
       for a specific two list. Returned by SaTwoList.  specifies
       one of the two lists, ``1'' or ``2''. Where optional, list
       defaults to ``1''.  list of 0 or more list items.

RReeffeerreenncceess

                                                                2

