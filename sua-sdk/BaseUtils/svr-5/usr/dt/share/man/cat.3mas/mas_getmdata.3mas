

mas_get_metadata(3mas)                     mas_get_metadata(3mas)

SSyynnooppssiiss
       cc [options] file -lmas

       #include   <mas.h>  name_t       mas_get_met_name(int  md,
       metid_t id); units_t    *mas_get_met_units(int md, metid_t
       id);  name_t      mas_get_met_unitsnm(int md, metid_t id);
       uint32     *mas_get_met_status(int md, metid_t id); uint32
       *mas_get_met_objsz(int    md,    metid_t    id);    uint32
       *mas_get_met_nobj(int    md,    metid_t    id);     uint32
       *mas_get_met_nlocs(int   md,   metid_t   id);   resource_t
       *mas_get_met_resources(int md, metid_t id);

DDeessccrriippttiioonn
       The mas_get_metadata functions  return  information  about
       individual  metrics  that  have been registered.  There is
       one function call for each field in  the  metric's  metric
       registration  table  entry.  For a complete description of
       each field, see the mas_register_met manual page.

       In the following functions, the md argument  is  a  metric
       descriptor  returned from mas_open, and the id argument is
       the metric ID number of the metric whose information is to
       be returned.

       The  function  mas_get_met_name  returns  a pointer to the
       metric name string.

       The function mas_get_met_units returns a  pointer  to  the
       units field for the metric.

       The  function  mas_get_met_unitsnm  returns a pointer to a
       string describing the units of the metric.

       The function mas_get_met_status returns a pointer  to  the
       metric  status  word.   The  metric  status  can be either
       MAS_AVAILABLE, indicating the metric is  fully  registered
       and in use, or MAS_UPDATE, indicating the metric is in the
       process of being registered.

       The function mas_get_met_objsz returns a  pointer  to  the
       elementary object size of the metric.

       The  function  mas_get_met_nobj  returns  a pointer to the
       number of objects, the array size, of the metric.

       The function mas_get_met_nlocs returns a  pointer  to  the
       total  number  of  possible instances of the metric.  This
       number is the (multiplicative) product of all of the  met-
       ric's resource's values.

       The  function mas_get_met_resources returns a pointer to a
       null terminated list of resource ID numbers.

                                                                1

mas_get_metadata(3mas)                     mas_get_metadata(3mas)

   RReettuurrnn vvaalluueess
       On success, these functions return a pointer to  the  data
       field in the caller's address space.

       On  failure, these functions return NULL and set mas_errno
       to identify the error.

   EErrrroorrss
       The requested metric ID id is not registered in the metric
       registration  file referred to by md.  The metric descrip-
       tor, md does not correspond to a metric registration  file
       previously opened by mas_open.

RReeffeerreenncceess

                                                                2

