

G2++(4C++)                                             G2++(4C++)

DDeessccrriippttiioonn
       G2++  records  are sequences of ASCII characters on a byte
       stream.  They "look" like tab-indented outlines  and  con-
       tain only printable ASCII characters (a formal syntax def-
       inition will be given at the end of  this  manual  entry).
       These  records  can  be  read  and written by C++ programs
       using the facilities described in

       Simple records have the form name ht value nl

       where ht and nl are the ASCII tab and newline  characters,
       respectively.  For example, the G2++ record id        Bob

       is  said  to be a record "of type id"; it carries a single
       field named id with the value Bob.

       Hierarchically-structured  values  may  be  created  using
       additional  tabs  and newlines to represent the hierarchy:
       person
            id        Bob
            age        11

       Here, a record of type person contains two  fields,  named
       id and age, along with their associated values.

       Array-valued  fields  are  also  supported.  The following
       example illustrates the syntax for arrays: person
            id        Bob
            age        11
            hobbies
                    0        video games
                    1        soccer
                    2        baseball

       Here, the field named hobbies is an array with three  ele-
       ments,  each  consisting of an index and a value.  Indices
       must be non-negative integers.

       Record structure is recursive,  allowing  nesting  to  any
       level.   For  example,  array  values  may  have their own
       "inner structure."  person
            id        Bob
            age        11
            hobbies
                    0        video games
                    1        soccer
                    2        baseball
            friends
                    0
                            id        Fred
                            age        10
                    1
                            id        Jane
                            age        12

                                                                1

G2++(4C++)                                             G2++(4C++)

       Field names (like person and age) are formed according  to
       the  rules for C identifiers.  Elementary values (like Bob
       and soccer) are formed from the alphabet  defined  by  the
       ASCII  function (note in particular that elementary values
       may contain blanks, but not tabs or newlines).   Tabs  are
       used  for indentation (one per level) and also to separate
       names from elementary values.

       Comments may be appended to any  line  of  a  G2++  record
       except  for the final newline, which must stand alone (see
       the formal syntax definition below).  Comments must appear
       at  the  end  of  the  line and must be separated from the
       rightmost field by one or more tabs.  By convention,  com-
       ments begin with a sharp (#).

       Here  is  the  complete syntax definition for G2++ records
       (alternatives are listed on separate lines): record     =>
       group nl group      => name value
                     name  value nl indent group value      => ht
       string nl
                     nl indent group
                     nl indent array array      => index value
                     index value nl indent array

       where:

       ht is an ASCII tab character (011)

       nl is an ASCII newline character (012)

       name is any valid C identifier

       index is any non-negative integer

       string is any sequence of printable ASCII  characters,  as
       defined by the ASCII function.

       indent  is one or more tabs, the exact number depending on
       the depth of recursion in the definition.

NNootteess
       According to the syntax definition, two  consecutive  new-
       lines always terminate a G2++ record.

       The  highest  level name in a G2++ record is also known as
       the record's type.

       Indices do not have  to  occur  in  any  particular  order
       within an array.

       The  G2++  data  language is identical to the G2 data lan-
       guage.

       that define G2++ record types (see obey the syntax of G2++

                                                                2

G2++(4C++)                                             G2++(4C++)

       records  given  here, but impose additional constraints on
       form of elementary values.

RReeffeerreenncceess

                                                                3

