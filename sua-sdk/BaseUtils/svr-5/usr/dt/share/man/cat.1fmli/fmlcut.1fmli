

fmlcut(1fmli)                                       fmlcut(1fmli)

SSyynnooppssiiss
       fmlcut -clist [file . . .]

       fmlcut -flist [-dchar] [-s] [file . . .]

DDeessccrriippttiioonn
       The  fmlcut  function  cuts  out  columns  from a table or
       fields from each line in file; in  database  parlance,  it
       implements  the  projection  of a relation.  fmlcut can be
       used as a filter; if file is not specified or  is  -,  the
       standard  input  is read.  list specifies the fields to be
       selected.  Fields can be  fixed  length  (character  posi-
       tions)  or variable length (separated by a field delimiter
       character), depending on whether -c or -f is specified.

       Note that either the -c or the -f option  must  be  speci-
       fied.

       The meanings of the options are:
       A  comma-separated  list  of  integer  field  numbers  (in
       increasing order), with optional - to indicate ranges  For
       example:  1,4,7;  1-3,8;  -5,10  (short for 1-5,10); or 3-
       (short for third through last field).  If -c is specified,
       list  specifies  character  positions (for example, -c1-72
       would pass the first 72 characters of  each  line).   Note
       that  no  space  intervenes between -c and list.  If -f is
       specified, list is a list of fields assumed  to  be  sepa-
       rated in the file by the default delimiter character, TAB,
       or by char if the -d option is  specified.   For  example,
       -f1,7 copies the first and seventh field only.  Lines with
       no delimiter characters are passed through intact  (useful
       for table subheadings), unless -s is specified.  Note that
       no space intervenes between -f and  list.   The  following
       options  can  be  used if you have specified -f.  If -d is
       specified, char is the field delimiter.   Space  or  other
       characters  with  special  meaning to FMLI must be quoted.
       Note that no space intervenes between -d  and  char.   The
       default  field delimiter is TAB.  Suppresses lines with no
       delimiter characters.  If -s is not specified, lines  with
       no delimiters will be passed through untouched.

EExxaammpplleess
       fmlcut -d: -f1,5 /etc/passwd  gets login IDs and names

       `who am i | fmlcut -f1 -d" "` gets the current login name

DDiiaaggnnoossttiiccss
       fmlcut returns the following exit values:

       0   when the selected field is successfully cut out
       2   on syntax errors

       The  following error messages may be displayed on the FMLI
       message line: A line has  more  than  1023  characters  or

                                                                1

fmlcut(1fmli)                                       fmlcut(1fmli)

       fields,  or there is no new-line character.  Missing -c or
       -f option or incorrectly specified list.  No error  occurs
       if  a  line has fewer fields than the list calls for.  The
       list is empty.  Missing char on -d option.

NNoottiicceess
       fmlcut cannot correctly process  lines  longer  than  1023
       characters, or lines with no newline character.

RReeffeerreenncceess

                                                                2

