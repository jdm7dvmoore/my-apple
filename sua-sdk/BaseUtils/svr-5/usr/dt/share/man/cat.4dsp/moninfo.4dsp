

moninfo(4dsp)                                       moninfo(4dsp)

SSyynnooppssiiss
       DESCRIPTION       =  "identification";  MON_VENDOR       =
       "vendor name"; MON_MODEL       = "model name";

       WIDTH           = monitor  width  in  millimeters;  HEIGHT
       = monitor height in millimeters;

       TYPE            = "color | mono";

DDeessccrriippttiioonn
       moninfo  files  are  ASCII  files that contain information
       about specific monitors that are supported by the  server.
       The  X server uses moninfo files to determine the physical
       dimensions of your screen so that fonts and other  records
       are displayed in the correct size.

       The  files are located under the /usr/lib/grafinfo/moninfo
       directory, with a subdirectory for each vendor.  The  sub-
       directory  name  must  match  the  value of the MON_VENDOR
       parameter in the file.  The individual moninfo  files  are
       within the vendor's subdirectory and are named using lower
       case with the value of the MON_MODEL  parameter,  suffixed
       with  .mon.   If the directory and file names do not match
       the parameter names in the file, the  Video  Configuration
       Manager  will  not  display your monitor during configura-
       tion.

       Most monitors can be  supported  by  the  generic  moninfo
       files.  You only need to create a moninfo file if you wish
       to add support for a new monitor that cannot be  supported
       by the generic files.

   PPaarraammeetteerrss
       Parameter  names  and values are case-sensitive and in the
       form: parameter_name=value;
       DESCRIPTION   text that is used to represent the monitor in the
                     supported monitor list in the Video Configuration
                     Manager.
       MON_VENDOR    name of the vendor who manufactures the  monitor.
                     This  string must be identical to the name of the
                     directory in which the moninfo file resides.
       MON_MODEL     model of the monitor.  This string must be  iden-
                     tical  to the name you used for the moninfo file,
                     excluding the .mon extension.
       WIDTH         width, in millimeters, of the monitor screen.
       HEIGHT        height, in millimeters, of the monitor screen.
       TYPE          monitor type.  Legal values are color and mono.
       Future X servers may require parameter  definitions  other
       than  those  listed  in  the  preceding  table.   Graphics
       adapter drivers ignore parameter definitions that they  do
       not  specifically  require.  This ensures that all drivers
       can use the same set of moninfo files.

&geminiREL; and &everest;                                       1

moninfo(4dsp)                                       moninfo(4dsp)

UUssaaggee
       To create a new moninfo file, copy an existing  file  into
       the  appropriate  directory and modify it.  This file must
       be   in    a    directory    for    the    vendor    under
       /usr/lib/grafinfo/moninfo  directory.   You  may  need  to
       first create a new directory for the vendor; the directory
       must be named with the same value assigned to the MON_VEN-
       DOR parameter.  The file must be named with the  value  of
       the MON_MODEL parameter with the .mon extension.

       After creating the new moninfo file, run the Video Config-
       uration Manager to add the new monitor to your  current  X
       server video configuration.

EExxaammpplleess
       This  is  the  /usr/lib/grafinfo/moninfo/8514.mon file for
       IBM 8514  monitor:  DESCRIPTION            =  "IBM  8514";
       MON_VENDOR             =  "ibm";  MON_MODEL              =
       "8514";

       WIDTH                 = 300; HEIGHT                = 220;

       TYPE                    =    "color";    This    is    the
       /usr/lib/grafinfo/moninfo/sony/1304.mon  file for the Sony
       CPD-1304 monitor.  DESCRIPTION=  "Sony  1304  (Multi  Scan
       HG)"; MON_VENDOR = "sony"; MON_MODEL  = "1304";

       WIDTH      = 274; HEIGHT     = 207;

       TYPE       =  "color";  For  more  examples, see the files
       under the /usr/lib/grafinfo/moninfo directory tree.

   VVeerrssiioonn aapppplliiccaabbiilliittyy
       moninfo files are used on &everest;, &geminiREL;, and Mon-
       terey-64  systems.   Most  monitors on these platforms are
       well supported by the generic moninfo  files  and  do  not
       need to have new files created.

   DDiiffffeerreenncceess bbeettwweeeenn vveerrssiioonnss
       Future  Xsco  implementations may use additional parameter
       definitions.  Xsco graphics adapter drivers ignore parame-
       ter  definitions  that  they  do  not require, so graphics
       drivers can use the same set of moninfo files on all plat-
       forms.

SSeeee aallssoo

&geminiREL; and &everest;                                       2

