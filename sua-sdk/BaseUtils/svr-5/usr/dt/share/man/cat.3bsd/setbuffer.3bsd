

setbuffer(3bsd)                                   setbuffer(3bsd)

SSyynnooppssiiss
       /usr/ucb/cc [flag . . . ] file . . .

       #include <stdio.h>

       setbuffer(FILE *stream, char *buf, int size);

       setlinebuf(FILE *stream);

DDeessccrriippttiioonn
       The  three  types  of  buffering available are unbuffered,
       block buffered, and line buffered.  When an output  stream
       is unbuffered, information appears on the destination file
       or terminal as soon as written; when it is block  buffered
       many  characters are saved up and written as a block; when
       it is line buffered characters are saved up until  a  NEW-
       LINE  is  encountered  or  input  is  read  from  any line
       buffered input stream.  fflush [see may be used  to  force
       the  block  out  early.   Normally  all  files  are  block
       buffered.  A buffer is obtained from upon the  first  getc
       or on the file.

       By  default, output to a terminal is line buffered, except
       for  output  to  the  standard  stream  stderr  which   is
       unbuffered,  and all other input/output is fully buffered.

       setbuffer can be used after a stream has been  opened  but
       before it is read or written.  It uses the character array
       buf whose size is determined by the size argument  instead
       of  an automatically allocated buffer.  If buf is the NULL
       pointer, input/output will be  completely  unbuffered.   A
       manifest  constant  BUFSIZ,  defined in the stdio.h header
       file, tells how big an array is needed: char buf[BUFSIZ];

       setlinebuf is used to change the  buffering  on  a  stream
       from  block  buffered  or  unbuffered  to  line  buffered.
       Unlike setbuffer, it can be used at any time that the file
       descriptor is active.

       A  file can be changed from unbuffered or line buffered to
       block buffered by using freopen [see A file can be changed
       from  block  buffered  or  line  buffered to unbuffered by
       using freopen followed by setbuffer with a buffer argument
       of NULL.

RReeffeerreenncceess
NNoottiicceess
       A  common source of error is allocating buffer space as an
       automatic variable in a code block, and  then  failing  to
       close the stream in the same block.

                     BSD System Compatibility                   1

