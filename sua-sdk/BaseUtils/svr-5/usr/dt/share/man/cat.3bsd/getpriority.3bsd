

getpriority(3bsd)                               getpriority(3bsd)

SSyynnooppssiiss
       /usr/ucb/cc [flag . . . ] file . . .

       #include <sys/time.h> #include <sys/resource.h>

       int getpriority(int which, int who);

       int setpriority(int which, int who, int prio);

DDeessccrriippttiioonn
       The  scheduling priority of the process, process group, or
       user, as indicated by which and who is obtained with  get-
       priority  and set with setpriority The default priority is
       0; lower priorities cause more favorable scheduling.

       which is one of PRIO_PROCESS, PRIO_PGRP, or PRIO_USER, and
       who is interpreted relative to which (a process identifier
       for PRIO_PROCESS, process group identifier for  PRIO_PGRP,
       and a user ID for PRIO_USER).  A zero value of who denotes
       the current process, process group, or user.

       getpriority returns the highest priority (lowest numerical
       value) enjoyed by any of the specified processes.  setpri-
       ority sets the priorities of all  of  the  specified  pro-
       cesses  to  the  value specified by prio.  If prio is less
       than -20, a value of -20 is used; if it  is  greater  than
       20,  a  value of 20 is used.  Only the privileged user may
       lower priorities.

RReettuurrnn vvaalluueess
       Since getpriority can legitimately return the value -1, it
       is necessary to clear the external variable errno prior to
       the call, then check it afterward to determine if a -1  is
       an  error  or  a  legitimate  value.  The setpriority call
       returns 0 if there is no error, or -1 if there is.

       getpriority and setpriority may return one of the  follow-
       ing errors: No process was located using the which and who
       values specified.  which  was  not  one  of  PRIO_PROCESS,
       PRIO_PGRP, or PRIO_USER.

       In addition to the errors indicated above, setpriority may
       fail with the following error: A process was located,  but
       one  of  the  following is true: Neither its effective nor
       real user ID matched the effective user ID of the  caller,
       and neither the effective nor the real user ID of the pro-
       cess executing the setpriority was  the  privileged  user.
       The call to getpriority would have changed a process' pri-
       ority to a value lower than its  current  value,  and  the
       effective  user  ID  of the process executing the call was
       not that of the privileged user.

RReeffeerreenncceess

                     BSD System Compatibility                   1

getpriority(3bsd)                               getpriority(3bsd)

NNoottiicceess
       It is not possible for the process  executing  setpriority
       to  lower  any other process down to its current priority,
       without requiring privileged user privileges.

                     BSD System Compatibility                   2

