

me(5bsd)                                                 me(5bsd)

SSyynnooppssiiss
       nroff -me [options] file . . .

       troff -me [options] file . . .

DDeessccrriippttiioonn
       This package of nroff and troff macro definitions provides
       a canned formatting facility for technical papers in vari-
       ous  formats.   When producing 2-column output on a termi-
       nal, filter the output through

       The macro requests are  defined  below.   Many  nroff  and
       troff  requests  are unsafe in conjunction with this pack-
       age, however, these requests may  be  used  with  impunity
       after the first .pp:
       -----------------------------------------------------
       .bp      begin new page
       .br      break output line here
       .sp n    insert n spacing lines
       .ls n    (line spacing) n=1 single, n=2 double space
       .na      no alignment of right margin
       .ce n    center next n lines
       .ul n    underline next n lines
       .sz +n   add n to point size

       Output  of  the  eqn,  neqn,  refer, and preprocessors for
       equations and tables is acceptable as input.

   RReeqquueessttss
       In the following  list,  "initialization"  refers  to  the
       first .pp, .lp, .ip, .np, .sh, or .uh macro.  This list is
       incomplete.
       ------------------------------------------------------------------------------
                     Initial   Cause
       Request       Value     Break   Explanation
       ------------------------------------------------------------------------------
       .(c           -         yes     Begin centered block
       .(d           -         no      Begin delayed text
       .(f           -         no      Begin footnote
       .(l           -         yes     Begin list
       .(q           -         yes     Begin major quote
       .(xx          -         no      Begin indexed item in index x
       .(z           -         no      Begin floating keep
       .)c           -         yes     End centered block
       .)d           -         yes     End delayed text
       .)f           -         yes     End footnote
       .)l           -         yes     End list
       .)q           -         yes     End major quote
       .)x           -         yes     End index item
       .)z           -         yes     End floating keep

                     BSD System Compatibility                   1

()                                                             ()

       .++ m H       -         no      Define paper section.  m defines the part  of
                                       the   paper,   and  can  be  C  (chapter),  A
                                       (appendix),  P  (preliminary,  for  instance,
                                       abstract,  table  of  contents, and so on), B
                                       (bibliography), RC (chapters renumbered  from
                                       page  one  each  chapter),  or  RA  (appendix
                                       renumbered from page one).
       .+c T         -         yes     Begin chapter (or appendix, and so on, as set
                                       by .++).  T is the chapter title.
       .1c           1         yes     One column format on a new page.
       .2c           1         yes     Two column format.
       .EN           -         yes     Space after equation produced by eqn or meqn.
       .EQ x y       -         yes     Precede  equation;  break  out and add space.
                                       Equation number is y.  The optional  argument
                                       x may be I to indent equation (default), L to
                                       left-adjust the equation, or C to center  the
                                       equation.
       .TE           -         yes     End table.
       .TH           -         yes     End heading section of table.
       .TS x         -         yes     Begin  table;  if  x  is H table has repeated
                                       heading.
       .ac A N       -         no      Set up  for  ACM  style  output.   A  is  the
                                       Author's  name(s),  N  is the total number of
                                       pages.  Must be given before the  first  ini-
                                       tialization.
       .b x          no        no      Print x in boldface; if no argument switch to
                                       boldface.
       .ba +n        0         yes     Augments the base indent by n.   This  indent
                                       is  used  to  set  the indent on regular text
                                       (like paragraphs).
       .bc           no        yes     Begin new column
       .bi x         no        no      Print x in bold italics (nofill only)
       .bx x         no        no      Print x in a box (nofill only).
       .ef  'x'y'z   '''''     no      Set even footer to x y z
       .eh 'x'y'z    '''''     no      Set even header to x y z
       .fo 'x'y'z    '''''     no      Set footer to x y z
       .hx           -         no      Suppress headers and footers on next page.
       .he 'x'y'z    '''''     no      Set header to x  y  z
       .hl           -         yes     Draw a horizontal line
       .i x          no        no      Italicize x; if x missing, italic  text  fol-
                                       lows.
       .ip x y       no        yes     Start indented paragraph, with hanging tag x.
                                       Indentation is y ens (default 5).
       .lp           yes       yes     Start left-blocked paragraph.
       .lo           -         no      Read in a file of local macros  of  the  form
                                       .*x.  Must be given before initialization.
       .np           1         yes     Start numbered paragraph.
       .of 'x'y'z    '''''     no      Set odd footer to x  y  z
       .oh 'x'y'z    '''''     no      Set odd header to x  y  z
       .pd           -         yes     Print delayed text.
       .pp           no        yes     Begin paragraph.  First line indented.
       .r            yes       no      Roman text follows.
       .re           -         no      Reset tabs to default values.

                                                                2

()                                                             ()

       .sc           no        no      Read in a file of special characters and dia-
                                       critical marks.  Must be  given  before  ini-
                                       tialization.
       .sh n x       -         yes     Section   head  follows,  font  automatically
                                       bold.  n is level of section, x is  title  of
                                       section.
       .sk           no        no      Leave  the next page blank.  Only one page is
                                       remembered ahead.
       .sz +n        10p       no      Augment the point size by n points.
       .th           no        no      Produce the paper in thesis format.  Must  be
                                       given before initialization.
       .tp           no        yes     Begin title page.
       .u x          -         no      Underline  argument (even in troff).  (Nofill
                                       only).
       .uh           -         yes     Like .sh but unnumbered.
       .xp x         -         no      Print index x.

FFiilleess
RReeffeerreenncceess

                                                                3

