

ls(1bsd)                                                 ls(1bsd)

SSyynnooppssiiss
       /usr/ucb/ls [-aAcCdfFgilLqrRstu1] file . . .

DDeessccrriippttiioonn
       For  each file which is a directory, ls lists the contents
       of the directory; for  each  file  which  is  a  file,  ls
       repeats  its name and any other information requested.  By
       default, the output is  sorted  alphabetically.   When  no
       argument  is given, the current directory is listed.  When
       several arguments  are  given,  the  arguments  are  first
       sorted  appropriately,  but  file  arguments are processed
       before directories and their contents.

   PPeerrmmiissssiioonnss ffiieelldd
       The mode printed under the -l option contains  10  charac-
       ters interpreted as follows.  If the first character is:
       entry  is a directory; entry is a block-type special file;
       entry is a character-type special file; entry  is  a  sym-
       bolic  link;  entry is a FIFO (also known as "named pipe")
       special file, or entry is a plain file.

       The next 9 characters are interpreted  as  three  sets  of
       three  bits  each.   The first set refers to owner permis-
       sions; the next refers to permissions  to  others  in  the
       same  user-group;  and  the  last  refers  to  all others.
       Within each set the three characters  indicate  permission
       respectively  to read, to write, or to execute the file as
       a program.   For  a  directory,  "execute"  permission  is
       interpreted  to  mean  permission to search the directory.
       The permissions are indicated as follows:
       the file is readable; the file is writable;  the  file  is
       executable; the indicated permission is not granted.

       The  group-execute  permission  character is given as s if
       the file has the set-group-id bit set; likewise the owner-
       execute permission character is given as s if the file has
       the set-user-id bit set.

       The last character of the mode (normally x or `-') is true
       if the 1000 bit of the mode is on.  See for the meaning of
       this mode.  The indications of set-ID and 1000 bits of the
       mode  are capitalized (S and T respectively) if the corre-
       sponding execute permission is not set.

       When the sizes of the files in a directory are  listed,  a
       total  count  of  blocks,  including  indirect  blocks  is
       printed.

   CCoommmmaanndd ooppttiioonnss
       List all entries; in the absence of this  option,  entries
       whose  names  begin with a `.'  are not listed (except for
       the privileged user, for  whom  ls  normally  prints  even
       files that begin with a `.').  Same as -a, except that `.'
       and `..'  are not listed.  Use time of last edit (or  last

                     BSD System Compatibility                   1

ls(1bsd)                                                 ls(1bsd)

       mode change) for sorting or printing.
       Force  multi-column  output,  with entries sorted down the
       columns; for ls, this is the default when output is  to  a
       terminal.   If argument is a directory, list only its name
       (not its contents); often used with -l to get  the  status
       of  a directory.  Force each argument to be interpreted as
       a directory and list the name found in  each  slot.   This
       option  turns off -l, -t, -s, and -r, and turns on -a; the
       order is the order in which entries appear in  the  direc-
       tory.   Mark directories with a trailing slash (`/'), exe-
       cutable files with a  trailing  asterisk  (`*'),  symbolic
       links  with  a  trailing  at-sign (`@').  For ls, show the
       group ownership of the file in a long  output.   For  each
       file,  print  the i-node number in the first column of the
       report.  List in  long  format,  giving  mode,  number  of
       links, owner, size in bytes, and time of last modification
       for each file.  If the file is a  special  file  the  size
       field will instead contain the major and minor device num-
       bers.  If the time of last modification  is  greater  than
       six  months  ago,  it  is  shown in the format `month date
       year'; files modified within six months show  `month  date
       time'.  If the file is a symbolic link the pathname of the
       linked-to file is printed preceded by `-->'.  If  argument
       is  a  symbolic  link, list the file or directory the link
       references rather than  the  link  itself.   Display  non-
       graphic  characters  in  filenames as the character ?; for
       ls, this is the default when  output  is  to  a  terminal.
       Reverse  the  order  of  sort to get reverse alphabetic or
       oldest first as appropriate.  Recursively list subdirecto-
       ries  encountered.   Give size of each file, including any
       indirect blocks used to map the file, in kilobytes.   Sort
       by  time  modified (latest first) instead of by name.  Use
       time of last access instead of last modification for sort-
       ing  (with  the  -t  option)  and/or printing (with the -l
       option).  Force one entry per line output format; this  is
       the default when output is not to a terminal.

FFiilleess
       to get user ID's for `ls -l' and `ls -o'.  to get group ID
       for `ls -g'

NNoottiicceess
       NEWLINE and TAB  are  considered  printing  characters  in
       filenames.

       The output device is assumed to be 80 columns wide.

       The  option setting based on whether the output is a tele-
       type is undesirable as `ls -s' is much different than  `ls
       -s  |  lpr'.   On  the  other hand, not doing this setting
       would make old shell scripts which used ls almost  certain
       losers.

       Unprintable  characters  in  file  names  may  confuse the

                     BSD System Compatibility                   2

ls(1bsd)                                                 ls(1bsd)

       columnar output options.

       The identification of sockets made possible by -l or using
       the -F option on a BSD system is not supported.

                     BSD System Compatibility                   3

