

Search_path(3C++)                               Search_path(3C++)

SSyynnooppssiiss
       #include <Search_path.h> namespace SCO_SC {

       class  Search_path  :  public List<Path>{ public: //  Con-
       structors
           Search_path();
           Search_path(const char* p); //  Copy and assign
           Search_path(const Search_path& p);
           Search_path& operator=(const Search_path& p); //  Con-
       version to and from strings
           operator const char*()const;
           operator String()const; //  Searching
           int find(
               const Path& p,
               Path& ret,
               Ksh_test::unary u = Ksh_test::x,
               Ksh_test::id id = Ksh_test::effective
           )const;
           int find_all(
               const Path& p,
               List<Path>& ret,
               Ksh_test::unary u = Ksh_test::x,
               Ksh_test::id id = Ksh_test::effective
           )const; //  Stream insertion and extraction
           friend ostream& operator<<(ostream& os,
               const Search_path& p);
           friend istream& operator>>(istream& is,
               Search_path&  p);  };  //  Global variables extern
       Search_path PATH; }

DDeessccrriippttiioonn
       A Search_path represents a UNIX search path,  that  is,  a
       list  of  zero  or more path names.  A Search_path is con-
       structed, written and read in standard  UNIX  search  path
       format,  i.e.,  as a string of colon-separated path names.
       However, Search_paths are real Lists, and as such obey all
       the semantics of Lists (see List(3C++)).

SSeeaarrcchh__ppaatthh
   CCoonnssttrruuccttoorrss
       Search_path(); The empty search path.

       Search_path(const  char*  p);  Constructs  the search path
       corresponding to the colon-separated list of paths pointed
       to  by  p.   If  p is 0, constructs the empty search path.
       (This enables usages such  as  Search_path(getenv("PATH"))
       shown below.)

   CCooppyy aanndd aassssiiggnn
       Search_path(const Search_path& p);

       Search_path&  operator=(const  Search_path&  p);  Copy and
       assignment operator.

                                                                1

Search_path(3C++)                               Search_path(3C++)

   CCoonnvveerrssiioonn ttoo aanndd ffrroomm ssttrriinnggss
       operator const char*()const;

       operator String()const; Conversion to colon-separated list
       of paths.

   SSeeaarrcchhiinngg
       int find(
           const Path& p,
           Path& ret,
           Ksh_test::unary u = Ksh_test::x,
           Ksh_test::id  id = Ksh_test::effective )const; If p is
       relative, looks for the first Path dir in this Search_path
       for  which  ksh_test(t,Path(dir,p),id) is true.  If such a
       directory exists, sets  ret  to  Path(dir,p)  and  returns
       true, otherwise returns false without affecting ret.  If p
       is absolute, returns ksh_test(t,p,id), setting ret to p if
       true.

       int find_all(
           const Path& p,
           List<Path>& ret,
           Ksh_test::unary u = Ksh_test::x,
           Ksh_test::id  id = Ksh_test::effective )const; Similar
       to the above, except that in the case p is relative,  sets
       ret  to  the  list consisting of all the satisfying Paths,
       and in the case p is absolute, sets ret to the  list  con-
       sisting  of  p  or  the  empty  list, depending on whether
       ksh_test(t,p,id) is true.  The current position of ret  is
       set  to  the  beginning of the List.  Returns true just if
       the returned value of ret is non-empty.

   SSttrreeaamm iinnsseerrttiioonn aanndd eexxttrraaccttiioonn
       friend ostream& operator<<(ostream& os,"
           const Search_path& p);

       friend istream& operator>>(istream& is,
           Search_path& p); Representation is  as  a  colon-sepa-
       rated list of Paths.

GGlloobbaall vvaarriiaabblleess
       extern  Search_path  PATH;  The  value  of the environment
       variable PATH at the time of program  start-up.   PATH  is
       intentionally  not  declared  const  in  order  to  enable
       assignment to it in case  the  value  of  the  environment
       variable  PATH  changes  while  the  program is executing.
       Assignment   can   be   done   as    follows:    PATH    =
       Search_path(getenv("PATH")).

RReeffeerreenncceess

                                                                2

