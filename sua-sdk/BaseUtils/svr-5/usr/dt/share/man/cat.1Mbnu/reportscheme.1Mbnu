

reportscheme(1Mbnu)                           reportscheme(1Mbnu)

SSyynnooppssiiss
       /usr/sbin/reportscheme [-d]

DDeessccrriippttiioonn
       The reportscheme command is a non-standing network service
       that tells client machine applications what authentication
       scheme  to  use  for  a  specified  network  service.  The
       reportscheme service must exist on each port monitor  that
       offers  network  services  if the server wishes to enforce
       authentication scheme invocation.

       The Connection Server invokes the reportscheme service  on
       the  server  machine  on behalf of client applications and
       sends the name of the requested network service to be exe-
       cuted.  On the server side, reportscheme receives the name
       of the network service, searches the  current  transport's
       _pmtab  database  and finds the first entry for the speci-
       fied network service.  If the service is not listed in the
       _pmtab  file,  reportscheme returns an error.  The Connec-
       tion Server will then fail the connection request for  the
       network  service  over the current transport.  If the ser-
       vice is found in the _pmtab  file  but  no  authentication
       information  is  listed,  a  null authentication scheme is
       assumed.

   OOppttiioonnss
       Turn on debugging.  All debugging information  is  written
       to   /var/adm/log/cs.debug.    The  debugging  information
       related to reportscheme begins with an rs: tag.

FFiilleess
UUssaaggee
       Since  reportscheme  is  used  to  report   authentication
       schemes,  there can be no authentication scheme associated
       with reportscheme itself.  If there were, client  machines
       would not be able to invoke the reportscheme service.

RReettuurrnn vvaalluueess
       reportscheme  returns  the following information: the name
       of the authentication scheme  for  the  requested  network
       service  on  the  current  port  monitor  an indication of
       whether the network service will invoke the authentication
       scheme as the imposer or the responder

EExxaammpplleess
       The  following  shows  a  reportscheme entry from a sample
       _pmtab file.  reportscheme itself may not have an  authen-
       tication  scheme  name  in  the scheme field of the _pmtab
       file, that is, it is always executed with a NULL authenti-
       cation  scheme.  A line similar to this must appear in the
       _pmtab file of every  port  monitor  that  the  Connection
       Server expects to connect to.  If the reportscheme service
       is not entered in a port monitor's  _pmtab  file,  a  NULL
       authentication  scheme  is  assumed for all services under

                                                                1

reportscheme(1Mbnu)                           reportscheme(1Mbnu)

       that port monitor.  The following  represents  a  one-line
       entry   in  the  _pmtab  file  (broken  here  with  a  \):
       reportscheme::root:reserved:reserved::\x00020b02c00b6c180000000000000000
       \
        /usr/sbin/reportscheme#

RReeffeerreenncceess

                                                                2

