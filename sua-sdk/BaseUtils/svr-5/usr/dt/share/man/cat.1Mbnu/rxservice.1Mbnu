

rxservice(1Mbnu)                                 rxservice(1Mbnu)

SSyynnooppssiiss
       rxservice -a servicename [-d description] [-u] servicedef
       rxservice -r servicename...
       rxservice -l

DDeessccrriippttiioonn
       The rxservice command provides an administrator with a way
       of adding and removing REXEC services.

   OOppttiioonnss
       The following options are available to the rxservice  com-
       mand:  Add  an  REXEC service.  servicename is the name of
       the REXEC  service  being  defined.   Users  on  a  client
       machine invoke the service via the service name.  The ser-
       vice name is restricted  to  14  characters  and  must  be
       alphanumeric.  A text description (up to 256 characters in
       length) that describes the service.  Specifies that a utmp
       entry  is  to  be  made  on the remote host containing the
       mapped user's login name.  A service definition,  consist-
       ing  of  a  character string composed of the command (with
       parameters) that is executed when the service is  invoked.
       The  command  must be a full pathname.  The parameters are
       parsed as tokens separated by white space, tokens enclosed
       in double quotes, or tokens enclosed in single quotes.  If
       the double or single quote is to be interpreted literally,
       it  must  be preceded with a backslash (\).  If a token is
       enclosed in single quote characters, then every  character
       of  that  token  is interpreted literally.  If you need to
       use a quote in the service definition, use a different one
       from  that  used in the service name.  For example, if you
       use double quotes in the service name, use  single  quotes
       in  the  service definition.  The character string is lim-
       ited to 256 characters.  The command line is first  inter-
       preted  by  the  shell, then by rxservice, and then by the
       remote shell.  Remove one or more REXEC services.   servi-
       cename  is  the  name  of  the  service  or services to be
       deleted.  Displays the contents of the services file.

FFiilleess
       REXEC services database

UUssaaggee
       The following rules apply when creating a service  defini-
       tion:  The service definition can take parameters from the
       client machine; these parameters  are  referenced  in  the
       service definition via the macros %0 for the service name,
       %1 for the first parameter, %2 for the second, and so  on,
       up  to  %9  for the ninth parameter.  The parameter macros
       are parsed as tokens consisting of the %  character,  fol-
       lowed  immediately  by a single digit integer representing
       the position of the parameter.  The special  macro  %*  is
       used  to  reference  all parameters (except %0).  If the %
       character is to be interpreted literally, it must be  pre-
       ceded by a backslash (\), be contained in a token enclosed

                                                                1

rxservice(1Mbnu)                                 rxservice(1Mbnu)

       within single quotes, or  be  contained  in  an  undefined
       macro  name.   If a service definition must reference more
       than nine parameters, the %* macro can be used to pass all
       the  parameters  to  a  shell  script.  The REXEC facility
       defines the following macros, which can  be  used  by  any
       service:  The  address of the client machine.  The name of
       the transport provider used to connect to the remote host.
       The   shell   for  the  mapped  user,  obtained  from  the
       /etc/passwd file.  Service  definitions  are  required  to
       begin with a slash (/) or a percent (%) character.

EExxaammpplleess
       The  following  defines  a  service  called rlookup, which
       accesses a local database via  a  command  called  dblook:
       rxservice   -a   rlookup   -d   'Remote  database  lookup'
       '/usr/bin/dblook  %*'  The  following  defines  a  service
       called  rsetup, which modifies database tables via a local
       command called setdb.  The setdb command takes the address
       of the client machine as a parameter.  rxservice -a rsetdb
       -d 'remote setup service' '/usr/bin/setdb %m'

RReeffeerreenncceess

                                                                2

