

flock(1tcl)                                           flock(1tcl)

SSyynnooppssiiss
       flock options fileId [start] [length] [origin]
       funlock fileId [start] [length] [origin]

DDeessccrriippttiioonn
       flock  places  a lock on all or part of the file specified
       by fileId. funlock removes a lock from  a  file  that  was
       previously placed with flock.

       The lock is either advisory or mandatory, depending on the
       mode bits of the file.  The lock is  placed  beginning  at
       relative  byte offset start for length bytes.  If start or
       length is omitted or empty, zero is assumed.  If length is
       zero, then the lock always extends to end-of-file, even if
       the file grows.  If origin is start, then  the  offset  is
       relative  to  the beginning of the file. If it is current,
       it is relative to the current access position in the file.
       If  it  is  end, then it is relative to the end-of-file (a
       negative is before the EOF, a positive is after).  If ori-
       gin is omitted, start is assumed.

       The  following  options are recognized: Places a read lock
       on the file.  Multiple processes can access  a  file  with
       read-locks.   Places  a  write lock on the file.  Only one
       process can access a file with write-lock.  If  specified,
       then  the  process  will  not block if the lock can not be
       obtained.  With this option, the command returns 1 if  the
       lock is obtained and 0 if it is not.

       See your system's fcntl system call documentation for full
       details of the behavior of file locking.   If  locking  is
       being  done  on  ranges  of  a  file,  it  is  best to use
       unbuffered file access (see the fcntl command).

                                                                1

