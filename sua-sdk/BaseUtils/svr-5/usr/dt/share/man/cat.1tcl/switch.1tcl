

switch(1tcl)                                         switch(1tcl)

SSyynnooppssiiss
       switch [options] string pattern body [pattern body ...]

       switch [options] string {pattern body [pattern body ...]}

DDeessccrriippttiioonn
       The  switch  command  matches  its string argument against
       each of the pattern arguments in order.   As  soon  as  it
       finds  a pattern that matches string it evaluates the fol-
       lowing body argument by passing it recursively to the  Tcl
       interpreter and returns the result of that evaluation.  If
       the last pattern argument is default then it matches  any-
       thing.   If  no  pattern  argument  matches  string and no
       default is given, then the switch command returns an empty
       string.

       If  the initial arguments to switch start with - then they
       are treated as options.  The following  options  are  cur-
       rently supported: Use exact matching when comparing string
       to a pattern.  This is the default.  When matching  string
       to the patterns, use glob-style matching (i.e. the same as
       implemented by the string match command).   When  matching
       string  to  the  patterns, use regular expression matching
       (i.e. the same as  implemented  by  the  regexp  command).
       Marks the end of options.  The argument following this one
       will be treated as string even if it starts with  -.   Two
       syntaxes  are provided for the pattern and body arguments.
       The first uses a separate argument for each  of  the  pat-
       terns  and  commands; this form is convenient if substitu-
       tions are desired on some of  the  patterns  or  commands.
       The  second  form  places all of the patterns and commands
       together into a single argument; the  argument  must  have
       proper list structure, with the elements of the list being
       the patterns and commands.  The second form makes it  easy
       to  construct multi-line switch commands, since the braces
       around the whole list make it  unnecessary  to  include  a
       backslash  at  the  end  of  each line.  Since the pattern
       arguments are in braces in the second form, no command  or
       variable  substitutions are performed on them;  this makes
       the behavior of the second form different than  the  first
       form in some cases.

       If a body is specified as - it means that the body for the
       next pattern should also be used as the body for this pat-
       tern  (if  the  next pattern also has a body of - then the
       body after that is used, and so on).  This  feature  makes
       it possible to share a single body among several patterns.

       Below are some examples of switch commands: switch abc a -
       b {format 1} abc {format 2} default {format 3} will return
       2, switch -regexp aaab {
              ^a.*b$ -
              b {format 1}
              a* {format 2}

                                                                1

switch(1tcl)                                         switch(1tcl)

              default {format 3} } will return 1, and switch  xyz
       {
               a
                       -
               b
                       {format 1}
               a*
                       {format 2}
               default
                       {format 3} } will return 3.

                                                                2

