

getmany(1Msnmp)                                   getmany(1Msnmp)

SSyynnooppssiiss
       getmany  [-f  defn_file]  [-T  timeout] entity_addr commu-
       nity_string object_name ...

DDeessccrriippttiioonn
       The getmany command is an SNMP application that  retrieves
       multiple objects from an SNMP entity.

       The  arguments  are  the  entity's  address, the community
       string for access to  the  SNMP  entity,  and  the  object
       name(s).

       The  entity  address can be either an IP address or entity
       name (if name-to-address resolution is enabled).

       The community string used  must  be  valid  on  the  given
       entity.   On  &gemini;,  community  strings  are  kept  in
       /etc/netmgt/snmpd.comm.

       Object names must refer to a valid SNMP object or class of
       objects.  Object names can be in the form of an identifier
       or the equivalent SNMP dot-notation. Identifiers for &gem-
       ini;  SNMP objects are listed in Equivalent SNMP dot-nota-
       tion for these objects is defined in the  files  /etc/net-
       mgt/nwumpsd.defs,  /etc/netmgt/snmpd.defs,  and  /etc/net-
       mgt/unixwared.defs. For a general explanation of SNMP dot-
       notation, see RFC 1213.

       getmany retrieves a object class by first calling the SNMP
       entity with the object class name to get the first  object
       in  the  class. Utilizing the GET_NEXT capability, it then
       calls the entity again using the object name  returned  in
       the  previous  call  to  retrieve  the  next object in the
       class.

       For instance, running the following: getmany suzzy  public
       ipRouteDest will traverse the network entity's ipRouteDest
       object class. The traversal of the object space stops when
       all of the classes being polled return a object of a class
       different than what was  requested.   A  network  entity's
       entire object tree can be traversed with a call of getmany
       suzzy public iso

   OOppttiioonnss
       The -f option is used to specify a file containing the and
       compiled  list  of  object  names and their numeric Object
       Identifiers, in addition to the objects  in  the  standard
       MIB.  This allows the user to dynamically augment the num-
       ber of objects that can be recognized  by  the  management
       utility.

       The -T option can be used to specify the number of seconds
       the command will wait for a response from the entity spec-
       ified  by  entity_addr. If not specified, this defaults to

                                                                1

getmany(1Msnmp)                                   getmany(1Msnmp)

       20 seconds.

RReeffeerreenncceess
       RFC 1155, RFC 1157, RFC 1213

                                                                2

