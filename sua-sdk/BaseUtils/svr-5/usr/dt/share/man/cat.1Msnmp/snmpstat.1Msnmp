

snmpstat(1Msnmp)                                 snmpstat(1Msnmp)

SSyynnooppssiiss
       snmpstat  [-a  | -i | -r | -s | -S | -t] [-n] [-T timeout]
       [entity_addr] [community_string]

DDeessccrriippttiioonn
       The snmpstat command displays the contents of various net-
       work-related  Simple  Network  Management  Protocol (SNMP)
       data structures.

       Each of the -airsSt options displays  a  different  struc-
       ture;  these  options cannot be used together. By default,
       snmpstat displays address information  symbolically.  Com-
       bine  the -n option with another option to display address
       information numerically.

       The entity_addr can be either an IP address or  an  entity
       name  (if  name-to-address  resolution is enabled). If not
       supplied, this defaults to the  symbolic  name  localhost,
       which  refers  to the machine on which the command is exe-
       cuted.

       The community_string is a string used to  gain  access  to
       the  SNMP agent on the given entity_addr. If not supplied,
       this defaults to the community string public. On &gemini;,
       community strings are kept in /etc/netmgt/snmpd.comm.

       If  no options are specified on the command line, then the
       transport endpoint table is displayed (the -t option), but
       with no server endpoints in the displayed table.

   OOppttiioonnss
       The  options have the following meanings: show the address
       translation table show the  status  of  active  interfaces
       display  addresses and port numbers numerically instead of
       symbolically show the routing  table  show  the  variables
       comprising  the system group of the MIB show the SNMP sta-
       tus show the complete transport endpoint table  specify  a
       different  number of seconds for the command to wait for a
       response By default, getid waits 20 seconds for a response
       from the entity specified by entity_addr. Use the -T time-
       out option to specify a different number  of  seconds  for
       the command to wait for a response.

       There  are  a  number of display formats, depending on the
       information presented.

   AAddddrreessss ttrraannssllaattiioonn ddiissppllaayy ((--aa))
       The address  translation  display  indicates  the  current
       knowledge  regarding address translations for remote hosts
       with which communication has recently occurred. Entries in
       the  address  translation table consist of a host address,
       its physical address (typically an Ethernet address),  and
       the  name  of  the interface for which this translation is

                                                                1

snmpstat(1Msnmp)                                 snmpstat(1Msnmp)

       valid.

       Using this option to snmpstat displays the  same  informa-
       tion   as  getmany  entity_addr  community_string  atTable
       though in a more understandable format.

   IInntteerrffaaccee ddiissppllaayy ((--ii))
       The interface  display  provides  a  table  of  cumulative
       statistics  regarding  packets  transferred,  errors,  and
       queue lengths. The name address and mtu (maximum transmis-
       sion unit) of the interface are also displayed.

       Using  this  option to snmpstat displays the same informa-
       tion  as  getmany  entity_addr  community_string   ifTable
       though in a more understandable format.

   RRoouuttiinngg ttaabbllee ddiissppllaayy ((--rr))
       The  routing  table display indicates the available routes
       and their status.  Each route consists  of  a  destination
       host  or  network and a gateway to use in forwarding pack-
       ets. The metric field shows the metric associated with the
       route. The type field displays what kind of route this is,
       whether for a directly connected network or a remote  net-
       work,  and  so on. The proto field indicates the mechanism
       by which the route was learned. The interface field  shows
       the name of the interface with which this route is associ-
       ated. The type and proto  fields  are  displayed  symboli-
       cally.

       Using  this  option to snmpstat displays the same informa-
       tion as getmany entity_addr community_string  ipRouteTable
       though in a more understandable format.

   SSyysstteemm ddiissppllaayy ((--ss))
       The  system display contains the description of the entity
       being managed, the object identifier describing  the  man-
       agement  subsystem on the entity, and the duration of time
       since the management  subsystem  was  re-initialized,  and
       other system group statistics.

       Using  this  option to snmpstat displays the same informa-
       tion as getmany entity_addr community_string system though
       in a more understandable format.

   SSNNMMPP ssttaattiissttiiccss ((--SS))
       The SNMP display contains a number of statistics for pack-
       ets received and sent by  the  SNMP  agent.  For  incoming
       packets,  this  includes  the  number  of  packets, packet
       errors,  commands  received  within  packets,  and   traps
       received.  For  outgoing packets, this includes the number
       of packets sent, packet errors, commands sent within pack-
       ets,  traps sent, and whether or not sending SNMP traps is
       currently enabled.

                                                                2

snmpstat(1Msnmp)                                 snmpstat(1Msnmp)

       Using this option to snmpstat displays the  same  informa-
       tion  as  getmany entity_addr community_string snmp though
       in a more understandable format.

   TTrraannssppoorrtt eennddppooiinntt ddiissppllaayy ((--tt))
       By default, active transport endpoints are displayed.  The
       -t flag is used to display all transport endpoints includ-
       ing servers. Active transport endpoints  are  those  whose
       local  address  portions  have  been  set  to  a  specific
       address. The protocol, local and remote address,  and  the
       internal  state of the protocol (if applicable) are shown.

       Address formats are of the form host.port or  network.port
       if  an  endpoint's address specifies a network but no spe-
       cific host address.  If the -n flag is not used, the  host
       and  network addresses and port numbers are displayed sym-
       bolically  according  to  the   data   bases   /etc/hosts,
       /etc/networks,  and /etc/services, respectively. If a sym-
       bolic name for an address is unknown or if the -n flag has
       been  used,  the  address  is  printed in the Internet dot
       notation. Unspecified or ``wildcard'' addresses and  ports
       appear as *.

       Using  this  option to snmpstat displays the same informa-
       tion as getmany entity_addr community_string  tcpConnTable
       getmany  entity_addr community_string udpTable though in a
       more understandable format.

RReeffeerreenncceess
       RFC 1155, RFC 1157, RFC 1213

                                                                3

