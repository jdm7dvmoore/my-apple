

mrinfo(1Mtcp)                                       mrinfo(1Mtcp)

SSyynnooppssiiss
       /usr/sbin/mrinfo  [  -d  debug_level ] [ -r retries ] [ -t
       timeout ] multicast_router

DDeessccrriippttiioonn
       mrinfo attempts to display configuration information  from
       the multicast router multicast_router.

       mrinfo  sends the IGMP message ASK_NEIGHBORS to the speci-
       fied multicast router. If the router responds, it includes
       the  version  number  and  a list of neighboring multicast
       router addresses  in  the  response.   If  the  responding
       router  has  a  recent  multicast  version  number, mrinfo
       requests additional information such as  metrics,  thresh-
       olds  and flags from the multicast router. Once the multi-
       cast router responds, mrinfo displays its configuration on
       the standard output.

       Only root can run mrinfo.

   OOppttiioonnss
       mrinfo  understands  the  following options: Set the debug
       level. If debug_level is greater than the default value of
       0, mrinfo displays additional debugging messages.  Regard-
       less of the debug level,  mrinfo  will  always  output  an
       error  message  and  terminate  if  an error occurs. Other
       debug levels are: Print packet  warnings  to  stderr.   As
       level 1 with the addition of printing notification of down
       networks to stderr.  As levels 1 and 2 with  the  addition
       of printing notification of all packet timeouts to stderr.
       Set the limit on the number of times  to  try  querying  a
       neighbor.  The default limit is 3 retries.  Set the number
       of seconds to wait for a neighbor  to  reply  to  a  query
       before retrying. The default timeout is 4 seconds.

RReeffeerreenncceess
NNoottiicceess
   AAuutthhoorr
       Van Jacobson

EExxaammpplleess
       The  following  is  example output from the command mrinfo
       mbone.phony.dom.net: 127.148.176.10  (mbone.phony.dom.net)
       [version 3.3]: 127.148.176.10 -> 0.0.0.0 (?) [1/1/querier]
       127.148.176.10   ->    127.0.8.4    (mbone2.phony.dom.net)
       [1/45/tunnel]  127.148.176.10  -> 105.1.41.9 (momoney.com)
       [1/32/tunnel/down]   127.148.176.10   ->   143.192.152.119
       (mbone.dipu.edu)  [1/32/tunnel]  For  each neighbor of the
       queried multicast router, mrinfo displays the  IP  address
       of the queried router, followed by the IP address and name
       of its neighbor. The information in square brackets  shows
       the metric (cost of connection), and the threshold (multi-
       cast TTL). If the queried multicast  router  has  a  newer
       version  number,  mrinfo  also  displays the type of route

                                                                1

mrinfo(1Mtcp)                                       mrinfo(1Mtcp)

       (tunnel or srcrt) and status of the  connection  (disabled
       or down).

                                                                2

