

slattach(1Mtcp)                                   slattach(1Mtcp)

SSyynnooppssiiss
       slattach [ +c | -c ] [ -d ] [ +e | -e ] [ +f | -f ] [ +i |
       -i ] [ -m mtu ] [ +v | -v ] device source_address destina-
       tion_address [baud_rate]

DDeessccrriippttiioonn
       The  slattach  command  attaches  serial  lines as network
       interfaces using the Serial Line IP (SLIP) protocol.   For
       outgoing  connections  over dedicated serial lines and for
       all incoming connections, the slattach command assigns the
       tty  line  specified by device to a network interface.  Do
       not use the -d flag for these connections.  If device does
       not  begin  with  /dev/,  /dev/  will  be  prepended.  The
       optional baud_rate parameter sets the speed of the connec-
       tion.  If not specified, the default value of 9600 will be
       used.

       For outgoing connections over a modem, the -d option  must
       be  used  to  specify  that  the device parameter is to be
       interpreted as an entry in the file that should be used to
       dial the remote system.

       source_address  specifies the IP address of the local side
       of the connection.

       destination_address specifies the IP address of the remote
       side of the connection.

       To  detach  a  SLIP  network interface, mark the interface
       down using kill the slattach process, and  use  to  remove
       the route from the routing table.

   OOppttiioonnss
       The following optional arguments can be used to adjust the
       behavior of the network interface: Turns  header  compres-
       sion  on  or  off (the default setting is off).  Specifies
       that the device parameter should be read as a
        sitename rather than a tty line  name.  Use  this  option
       only for outgoing SLIP connections over modems.  Turns the
       automatic detection and the use of  header compression  on
       or  off  (the  default setting is off).  If the flag +c is
       given, then this flag (either +e or  -e)  has  no  effect.
       When  the  flag +e is given, the SLIP module will not send
       any compressed  headers until it has received and success-
       fully uncompressed a compressed
        packet.

       NOTE:  If  both ends of the connection use the flag +e and
       if neither end uses +c, then the  header compression  mode
       will  never be turned on because neither end will take the
       initiative to send a compressed packet.  Enables  or  dis-
       ables  hardware  flow  control.   Turns the suppression of
       ICMP packets on or off (the default setting is off).  Sets
       the   maximum  transmission  unit  (MTU)  of  the  network

                                                                1

slattach(1Mtcp)                                   slattach(1Mtcp)

       interface to mtu bytes. (The default MTU is 296 bytes.)

       NOTE: It is suggested that the mtu value for  the   packet
       header  be 40 plus some power of 2  (for example, 296 = 40
       + 2**8).  Specifies  whether  to  print  various  messages
       about the interface as it is being brought up (the default
       setting is to not print messages).

RReeffeerreenncceess
       RFC 1055, RFC 1144

EExxaammpplleess
       slattach    /dev/term/00     128.211.8.4     128.211.8.186
       255.255.255.0  slattach +c /dev/term/01 percival zapranoth
       255.255.0.0 19200 slattach -d uu_zapranoth percival zapra-
       noth 255.255.255.0

                                                                2

