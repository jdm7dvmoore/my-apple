

fseeko(3S)                                             fseeko(3S)

SSyynnooppssiiss
       #include <stdio.h>

       int fseeko(FILE *stream, off_t offset, int ptrname);

       int fseeko64(FILE *stream, off64_t offset, int ptrname);

       off_t ftello(FILE *stream);

       off64_t ftello64(FILE *stream);

DDeessccrriippttiioonn
       fseeko sets the position of the next input or output oper-
       ation on the stream (see The new position is at the signed
       distance offset bytes from the beginning, from the current
       position, or from the end of the file, according to a ptr-
       name  value of SEEK_SET, SEEK_CUR, or SEEK_END (defined in
       stdio.h) as follows:

       set position equal to offset bytes.  set position to  cur-
       rent  location plus offset.  set position to EOF plus off-
       set.

       fseeko allows the file position indicator to be set beyond
       the  end  of  the  existing  data in the file.  If data is
       later written at this point, subsequent reads of  data  in
       the  gap  will  return zero until data is actually written
       into the gap.  fseeko, by itself, does not extend the size
       of the file.

       fseek  clears  the EOF indicator and undoes any effects of
       ungetc on stream.  After fseek, the next  operation  on  a
       file opened for update may be either input or output.

       If stream is writable and buffered data has not been writ-
       ten to the underlying file,  fseek  causes  the  unwritten
       data to be written to the file.

       ftello  returns the offset of the current byte relative to
       the beginning  of  the  file  associated  with  the  named
       stream.

   EErrrroorrss
       fseeko  and  fseeko64 return -1 for improper seeks, other-
       wise zero.  An improper  seek  can  be,  for  example,  an
       fseeko  or  an  fseeko64  done on a file that has not been
       opened via fopen or fopen64;  in  particular,  fseeko  and
       fseeko64 may not be used on a terminal or on a file opened
       via popen.  After a stream is closed,  no  further  opera-
       tions are defined on that stream.

       fseeko  and  ftello  fail  when  the resulting file offset
       would be a value which cannot be represented correctly  in
       an object of type off_t.

                                                                1

fseeko(3S)                                             fseeko(3S)

RReeffeerreenncceess
NNoottiicceess
       Although on the

       system  an offset returned by ftello is measured in bytes,
       and it is permissible to seek  to  positions  relative  to
       that  offset, portability to non- systems requires that an
       offset be used by fseeko  directly.   Arithmetic  may  not
       meaningfully  be performed on such an offset, which is not
       necessarily measured in bytes.

   CCoonnssiiddeerraattiioonnss ffoorr llaarrggee ffiillee ssuuppppoorrtt
       fseeko64 and ftello64 support large files, but are  other-
       wise  identical  to  fseeko and ftello, respectively.  For
       details on programming for  large  file  capable  applica-
       tions, see on intro(2).

                                                                2

