

getwc(3S)                                               getwc(3S)

SSyynnooppssiiss
       #include <stdio.h> #include <wchar.h>

       wint_t getwc(FILE *stream);

       wint_t getwchar(void);

       wint_t fgetwc(FILE *stream);

DDeessccrriippttiioonn
       fgetwc  transforms  the  next multibyte character from the
       named input stream into a wide character, and returns  it.
       It  also  increments  the file pointer, if defined, by one
       multibyte character.  getwchar is defined as getwc(stdin).

       getwc behaves like fgetwc, except that getwc may be imple-
       mented as a macro which evaluates stream more than once.

   EErrrroorrss
       These functions return  the  constant  WEOF  and  set  the
       stream's  end-of-file  indicator at the end-of-file.  They
       return WEOF if an error is found.  If the error is an  I/O
       error,  the  error  indicator  is set.  If it is due to an
       invalid or incomplete multibyte character, errno is set to
       EILSEQ.

       These  functions fail if the file is a regular file and an
       attempt was made to read at or beyond the  offset  maximum
       associated with the corresponding stream.

RReeffeerreenncceess
NNoottiicceess
       If  the  value  returned  by getwc, getwchar, or fgetwc is
       compared with the integer constant WEOF after being stored
       in a wchar_t object, the comparison may not succeed.

                                                                1

