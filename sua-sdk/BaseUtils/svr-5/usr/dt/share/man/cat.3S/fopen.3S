

fopen(3S)                                               fopen(3S)

SSyynnooppssiiss
       #include <stdio.h>

       FILE *fopen(const char *filename, const char *type);

       FILE *fopen64(const char *filename, const char *type);

       FILE *freopen(const char *filename, const char *type, FILE
       *stream);

       FILE *freopen64(const char *filename,  const  char  *type,
       FILE *stream);

       FILE *fdopen(int fildes, const char *type);

DDeessccrriippttiioonn
       fopen  opens  the  file named by filename and associates a
       stream with it.  fopen  returns  a  pointer  to  the  FILE
       structure associated with the stream.

       filename  points  to  a character string that contains the
       name of the file to be opened.

       type is a character string beginning with one of the  fol-
       lowing sequences: open for reading truncate to zero length
       or create for writing append; open for writing at  end  of
       file,  or  create for writing open for update (reading and
       writing) truncate or create for  update  append;  open  or
       create for update at end-of-file

       The  ``b''  has  no  effect in the above types.  The ``b''
       exists to distinguish binary files from text files.  There
       is  no  distinction  between  these  types  of  files on a
       system.

       freopen substitutes the named file in place  of  the  open
       stream.  A flush is first attempted, and then the original
       stream is closed, regardless of  whether  the  open  ulti-
       mately  succeeds.   Failure  to flush or close stream suc-
       cessfully is ignored.  freopen returns a  pointer  to  the
       FILE structure associated with stream.

       freopen  is typically used to attach the preopened streams
       associated with stdin, stdout, and stderr to other  files.
       stderr  is  by  default unbuffered, but the use of freopen
       will cause it to become buffered or line-buffered.

       fdopen associates a stream with a file  descriptor.   File
       descriptors  are  obtained from open, dup, creat, or pipe,
       which open files but do not  return  pointers  to  a  FILE
       structure  stream  Streams  are necessary input for almost
       all of the Section  3S  library  routines.   The  type  of
       stream  must  agree  with  the mode of the open file.  The
       file position indicator associated with stream is  set  to

                                                                1

fopen(3S)                                               fopen(3S)

       the  position indicated by the file offset associated with
       fildes.

       When a file is opened for update, both  input  and  output
       may  be done on the resulting stream.  However, output may
       not be directly followed by input without  an  intervening
       fflush,  fseek,  fsetpos,  or rewind, and input may not be
       directly followed by output without an intervening  fseek,
       fsetpos,  or rewind, or an input operation that encounters
       end-of-file.

       When a file is opened for append (i.e., when  type  is  a,
       ab, a+, or ab+), it is impossible to overwrite information
       already in the file.  fseek may be used to reposition  the
       file  pointer to any position in the file, but when output
       is written to the file, the current file pointer is disre-
       garded.   All output is written at the end of the file and
       causes the file pointer to be repositioned at the  end  of
       the  output.  If two separate processes open the same file
       for append, each process may  write  freely  to  the  file
       without  fear  of  destroying  output being written by the
       other.  The output from the two processes will  be  inter-
       mixed in the file in the order in which it is written.

       When  opened, a stream is fully buffered if and only if it
       can be determined not to refer to an  interactive  device.
       The  error  and end-of-file indicators are cleared for the
       stream.

       fopen64 and freopen64 support large files, but are  other-
       wise  identical to their counterparts.  Large file support
       is filesystem-dependent, supported only on vxfs file  sys-
       tem  types  [see For details on programming for large file
       capable applications, see on intro(2) and in  the  discus-
       sion of the vxfs filesystem type in Understanding filesys-
       tem types.

   RReettuurrnn vvaalluueess
       The  functions  fopen,  fopen64,  freopen,  and  freopen64
       return  a  null  pointer if path cannot be accessed, or if
       type is invalid, or if the file cannot be opened.

       fopen and freopen fail and set errno when  filename  is  a
       regular  file and its size cannot be represented correctly
       in an object of type off_t.

       fdopen returns a null pointer if fildes  is  not  an  open
       file  descriptor,  or  if  type is invalid, or if the file
       cannot be opened.

   NNoottiicceess
       There is no upper bound to the integer value  provided  in
       the the fildes argument to fdopen.

                                                                2

fopen(3S)                                               fopen(3S)

RReeffeerreenncceess
NNoottiicceess
   CCoonnssiiddeerraattiioonnss ffoorr llaarrggee ffiillee ssuuppppoorrtt
       For details on programming for large file capable applica-
       tions, see on intro(2) and in the discussion of  the  vxfs
       filesystem type in Understanding filesystem types.

                                                                3

