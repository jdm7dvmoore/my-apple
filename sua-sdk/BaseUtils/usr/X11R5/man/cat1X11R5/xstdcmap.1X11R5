xstdcmap(1X11R5)                                       xstdcmap(1X11R5)

  xxssttddccmmaapp

  NNAAMMEE

    xstdcmap - X standard colormap utility

  SSYYNNOOPPSSIISS

    xstdcmap [[-all]] [[-best]] [[-blue]] [[-default]]
             [[-delete map]] [[-display display]]
             [[-gray]] [[-green]] [[-red]] [[-verbose]]

  DDEESSCCRRIIPPTTIIOONN

    The xxssttddccmmaapp(1X11R5) utility can be used to selectively define standard
    colormap properties. It is intended to be run from a user's X startup
    script to create standard colormap definitions in order to facilitate
    sharing of scarce colormap resources among clients. Where at all possible,
    colormaps are created with read-only allocations.

  OOPPTTIIOONNSS

    The following options can be used with xxssttddccmmaapp(1X11R5):
        This option indicates that all six standard colormap properties should
        be defined on each screen of the display. Not all screens will support
        visuals under which all six standard colormap properties are
        meaningful. The xxssttddccmmaapp(1X11R5) utility will determine the best
        allocations and visuals for the colormap properties of a screen. Any
        previously existing standard colormap properties will be replaced.

    --bbeesstt
        This option indicates that the RGB_BEST_MAP should be defined.

    --bblluuee
        This option indicates that the RGB_BLUE_MAP should be defined.

    --ddeeffaauulltt
        This option indicates that the RGB_DEFAULT_MAP should be defined.
    --ddeelleettee map
        This option specifies that a standard colormap property should be
        removed. map can be one of the following: ddeeffaauulltt, bbeesstt, rreedd, ggrreeeenn,
        bblluuee, or ggrraayy..
    --ddiissppllaayy display
        This option specifies the host and display to use; see XX(5X11R5).

    --ggrraayy
        This option indicates that the RGB_GRAY_MAP should be defined.

    --ggrreeeenn
        This option indicates that the RGB_GREEN_MAP should be defined.

    --rreedd
        This option indicates that the RGB_RED_MAP should be defined.

    --vveerrbboossee
        This option indicates that xxssttddccmmaapp(1X11R5) should print logging
        information as it parses its input and defines the standard colormap
        properties.

  EENNVVIIRROONNMMEENNTT VVAARRIIAABBLLEESS

    DISPLAY
        Contains default host and display number.

  CCOOPPYYRRIIGGHHTT

    Copyright 1989, Massachusetts Institute of Technology.
    See _X(5X11R5) for a full statement of rights and permissions.

  AAUUTTHHOORR

    Donna Converse, MIT X Consortium

  SSEEEE AALLSSOO

    _X(5X11R5)

