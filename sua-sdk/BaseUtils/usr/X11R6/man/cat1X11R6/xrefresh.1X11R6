xrefresh(1X11R6)                                       xrefresh(1X11R6)

  XXRREEFFRREESSHH

  NNAAMMEE

    xrefresh - refresh all or part of an X screen

  SSYYNNOOPPSSIISS

    xrefresh [[-option......]]

  DDEESSCCRRIIPPTTIIOONN

    Xrefresh is a simple X program that causes all or part of your screen to
    be repainted. This is useful when system messages have messed up your
    screen. Xrefresh maps a window on top of the desired area of the screen
    and then immediately unmaps it, causing refresh events to be sent to all
    applications. By default, a window with no background is used, causing all
    applications to repaint "smoothly." However, the various options can be
    used to indicate that a solid background (of any color) or the root window
    background should be used instead.

  AARRGGUUMMEENNTTSS

    --wwhhiittee
        Use a white background. The screen just appears to flash quickly, and
        then repaint.

    --bbllaacckk
        Use a black background (in effect, turning off all of the electron
        guns to the tube). This can be somewhat disorienting as everything
        goes black for a moment.

    --ssoolliidd color
        Use a solid background of the specified color. Try green.

    --rroooott
        Use the root window background.

    --nnoonnee
        This is the default. All of the windows simply repaint.

    --ggeeoommeettrryy WxH+X+Y
        Specifies the portion of the screen to be repainted; see _X(5X11R5).

    --ddiissppllaayy display
        This argument allows you to specify the server and screen to refresh;
        see _X(5X11R5).

  XX DDEEFFAAUULLTTSS

    The xrefresh program uses the routine XXGGeettDDeeffaauulltt() to read defaults, so
    its resource names are all capitalized.

    BBllaacckk, WWhhiittee, SSoolliidd, NNoonnee, RRoooott
        Determines what sort of window background to use.

    GGeeoommeettrryy
        Determines the area to refresh. Not very useful.

  EENNVVIIRROONNMMEENNTT

    DISPLAY - To get default host and display number.

  SSEEEE AALLSSOO

    _X(5X11R5)

  BBUUGGSS

    It should have just one default type for the background.

  AAUUTTHHOORRSS

    Jim Gettys, Digital Equipment Corp., MIT Project Athena

