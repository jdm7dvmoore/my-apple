xkbcomp(1X11R6)                                         xkbcomp(1X11R6)

  XXKKBBCCOOMMPP

  NNAAMMEE

    xkbcomp - compile XKB keyboard description

  SSYYNNOOPPSSIISS

    xkbcomp [[option]] source [[destination]]

  DDEESSCCRRIIPPTTIIOONN

    The xkbcomp keymap compiler converts a description of an XKB keymap into
    one of several output formats. The most common use for xkbcomp is to
    create a compiled keymap file (.xkm extension) which can be read directly
    by XKB-capable X servers or utilities. The keymap compiler can also
    produce C header files or XKB source files. The C header files produced by
    xkbcomp can be included by X servers or utilities that need a built-in
    default keymap. The XKB source files produced by xkbcomp are fully
    resolved and can be used to verify that the files which typically make up
    an XKB keymap are merged correctly or to create a single file which
    contains a complete description of the keymap.

    The source may specify an X display, or an .xkb or .xkm file; unless
    explicitly specified, the format of destination depends on the format of
    the source. Compiling a .xkb (keymap source) file generates a .xkm
    (compiled keymap file) by default. If the source is a .xkm file or an X
    display, xkbcomp generates a keymap source file by default.

    If the destination is an X display, the keymap for the display is updated
    with the compiled keymap.

    The name of the destination is usually computed from the name of the
    source, with the extension replaced as appropriate. When compiling a
    single map from a file which contains several maps, xkbcom constructs the
    destination file name by appending an appropriate extension to the name of
    the map to be used.

  OOPPTTIIOONNSS

    --aa
        Show all keyboard information, reporting implicit or dervied
        information as a comment. Only affects .xkb format output.

    --CC
        Produce a C header file as output (.h extension).

    --ddffllttss
        Compute defaults for any missing components, such as key names.
    --IIdir
        Specifies top-level directories to be searched for files included by
        the keymap description.

    --ll
        List maps that specify the map pattern in any files listed on the
        command line (not implemented yet).
    --mm name
        Specifies a map to be compiled from an file with multiple entries.

    --mmeerrggee
        Merge the compiled information with the map from the server (not
        implemented yet).
    --oo name
        Specifies a name for the generated output file. The default is the
        name of the source file with an appropriate extension for the output
        format.
    --oopptt parts
        Specifies a list of optional parts. Compilation errors in any optional
        parts are not fatal. Parts may consist of any combination of the
        letters c, g,k,s,t which specify the compatibility map, geometry,
        keycodes, symbols and types, respectively.
    --RRdir
        Specifies the root directory for relative path names.

    --ssyynncchh
        Force synchonization for X requests.
    --ww lvl
        Controls the reporting of warnings during compilation. A warning level
        of 0 disables all warnings; a warning level of 10 enables them all.

    --xxkkbb
        Generate a source description of the keyboard as output (.xkb
        extension).

    --xxkkmm
        Generate a compiled keymap file as output (.xkm extension).

  SSEEEE AALLSSOO

    _X(5X11R5)

  CCOOPPYYRRIIGGHHTT

    Copyright 1994, Silicon Graphics Computer Systems and X Consortium, Inc.
    See _X(5X11R5) for a full statement of rights and permissions.

  AAUUTTHHOORR

    Erik Fortune, Silicon Graphics

