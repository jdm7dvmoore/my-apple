fsinfo(1X11R6)                                           fsinfo(1X11R6)

  FFSSIINNFFOO

  NNAAMMEE

    fsinfo - X font server information utility

  SSYYNNOOPPSSIISS

    fsinfo [[-server servername]]

  DDEESSCCRRIIPPTTIIOONN

    Fsinfo is a utility for displaying information about an X font server. It
    is used to examine the capabilities of a server, the predefined values for
    various parameters used in communicating between clients and the server,
    and the font catalogues and alternate servers that are available.

  EEXXAAMMPPLLEE

    The following shows a sample produced by fsinfo.

    name of server: hansen:7100
    version number: 1
    vendor string:  Font Server Prototype
    vendor release number:  17
    maximum request size:   16384 longwords (65536 bytes)
    number of catalogues:   1
            all
    Number of alternate servers: 2
        #0  hansen:7101
        #1  hansen:7102
    number of extensions:   0

  EENNVVIIRROONNMMEENNTT

    FFOONNTTSSEERRVVEERR
        To get the default fontserver.

  SSEEEE AALLSSOO

    _x_f_s(1X11R6), _f_s_l_s_f_o_n_t_s(1X11R6)

  AAUUTTHHOORR

    Dave Lemke, Network Computing Devices, Inc

