��    �      L  �   |
      �  -   �  1   '     Y  �   n     J  E   W  4   �  H   �  J     M   f  A   �  J   �  N   A  K   �  Q   �  7   .  F   f  A   �  C   �  D   3  ?   x  =   �  8   �  E   /  D   u  B   �  P   �  M   N  �   �  N   ,  Q   {  <   �  O   
  L   Z  A   �  H   �  B   2  5  u     �  	   �     �     �     �     �                         ,     ?     N     _     r     �     �     �     �  ,   �     �       ,        8     A     N     [     h     u     �     �     �     �     �     �     �     �     �          
  2     2   R     �     �     �     �     �     �  
   �     �  	     "        3     E  $   b     �     �     �     �  &   �  %   �  6        I     \     e  (   n     �  	   �     �     �     �     �  	   �     �     �  &   �     $     -  /   6     f     v  &   �  %   �     �     �     �     �           #      7      R   
   a      l       t      �   	   �      �      �      �   
   �      �      �      �      !     !  %   %!     K!  	   P!  %   Z!     �!     �!     �!     �!      �!  +   �!     "  	   "     "     &"  3   A"     u"  0   �"     �"     �"     �"  	   �"     �"  )  #  5   -$  :   c$     �$  �   �$     �%  N   �%  :   �%  R   6&  `   �&  Z   �&  I   E'  Y   �'  X   �'  R   B(  [   �(  ;   �(  [   -)  K   �)  M   �)  ^   #*  B   �*  K   �*  >   +  M   P+  D   �+  T   �+  `   8,  R   �,  �   �,  ]   �-  ^   .  @   e.  h   �.  O   /  K   _/  P   �/  H   �/  V  E0     �1     �1     �1     �1     �1     �1      2     2     2     2     )2     B2     S2     f2     {2     �2     �2     �2     �2  0   �2     3     "3  >   )3     h3     q3     �3     �3     �3     �3     �3     �3     �3     �3     �3     4     4     :4     K4     \4     _4  >   v4  >   �4     �4     5     #5     35     A5     U5     o5     }5  
   �5  &   �5     �5     �5  (   �5     6     .6     ?6     W6  )   q6  ,   �6  8   �6     7     7     %7  .   .7     ]7  	   o7     y7     �7     �7     �7  	   �7     �7     �7  .   �7     �7     8  /   
8     :8     I8  (   X8  '   �8     �8     �8     �8     �8     �8     9  "   9     <9  
   M9     X9  (   ]9     �9  	   �9     �9     �9     �9     �9  	   �9     �9  '   �9     :  $   +:  4   P:     �:     �:  (   �:     �:     �:  	   �:  .   �:  -   ;  4   @;     u;     z;     ;  "   �;  2   �;     �;  <   <     B<     H<     f<     �<     �<             u      o   E              W       �   K   �   w   �       2            R       ,                     "          }   z   �   �       /   �   C       f   P   I   �   N   �   �       �   �   (       l       >       F       [       `   @          D          ~   $       �   S   ^       �   8   �              b   <   �   �      \       A               �   �      n      x       i          q   �   g   �      k   s   7   �   �       r   �       V   O   T      {   Z       p       y   a   �   -   !   c   U       G      4   ;   �   :                    +   �      �   X   �   %       	      M   d       ?                   5   �          Q   _          Y       #   t   �   B       j   =   �   )   
   H   h           L           |   .          0   �   �   �   6   �       ]   *   v   �          &      '   9       �      1       m   �   J   e       �   3    
For bug reporting instructions, please see:
 
For bug reporting instructions, please see:
%s.
 
Go ahead? (y or n)  
Options starting with -g, -f, -m, -O, -W, or --param are automatically
 passed on to the various sub-processes invoked by %s.  In order to pass
 other options on to these processes the -W<letter> options must be used.
         `%D'   (Use '-v --help' to display command line options of sub-processes)
   --help                   Display this information
   --target-help            Display target specific command line options
   -B <directory>           Add <directory> to the compiler's search paths
   -E                       Preprocess only; do not compile, assemble or link
   -S                       Compile only; do not assemble or link
   -V <version>             Run gcc version number <version>, if installed
   -Wa,<options>            Pass comma-separated <options> on to the assembler
   -Wl,<options>            Pass comma-separated <options> on to the linker
   -Wp,<options>            Pass comma-separated <options> on to the preprocessor
   -Xlinker <arg>           Pass <arg> on to the linker
   -b <machine>             Run gcc for target <machine>, if installed
   -c                       Compile and assemble, but do not link
   -dumpmachine             Display the compiler's target processor
   -dumpspecs               Display all of the built in spec strings
   -dumpversion             Display the version of the compiler
   -h, --help                      Print this help, then exit
   -o <file>                Place the output into <file>
   -pass-exit-codes         Exit with highest error code from a phase
   -pipe                    Use pipes rather than intermediate files
   -print-file-name=<lib>   Display the full path to library <lib>
   -print-libgcc-file-name  Display the name of the compiler's companion library
   -print-multi-directory   Display the root directory for versions of libgcc
   -print-multi-lib         Display the mapping between command line options and
                           multiple library search directories
   -print-prog-name=<prog>  Display the full path to compiler component <prog>
   -print-search-dirs       Display the directories in the compiler's search path
   -save-temps              Do not delete intermediate files
   -specs=<file>            Override built-in specs with the contents of <file>
   -std=<standard>          Assume that the input sources are for <standard>
   -time                    Time the execution of each subprocess
   -v                       Display the programs invoked by the compiler
   -v, --version                   Print version number, then exit
   -x <language>            Specify the language of the following input files
                           Permissable languages include: c c++ assembler none
                           'none' means revert to the default behavior of
                           guessing the language based on the file's extension
   but %d required   by `%D'  %s  TOTAL                 : # %s %.2f %.2f
 #elif after #else #else after #else %s %s  %s (GCC) %s
 %s at end of input %s before "%s" %s before %s'%c' %s before %s'\x%x' %s before '%s' token %s before `%c' %s before `%s' %s before `\%o' %s before numeric constant %s makes pointer from integer without a cast %s.
%s %s: %s %s: %s compiler not installed on this system %s: %s:  '(' expected ')' expected '*' expected ':' expected ';' expected '[' expected ']' expected '{' expected ((anonymous)) (continued): -pipe is not supported -pipe not supported 31 bit mode 64 bit mode :
 Configured with: %s
 Copyright (C) 2001 Free Software Foundation, Inc.
 Copyright (C) 2002 Free Software Foundation, Inc.
 Creating %s.
 In block-data unit In common block In function In function `%s': In member function `%s': In program In subroutine Options:
 Print code coverage information.

 Thread model: %s
 Usage: %s [options] file...
 Usage: gcov [OPTION]... SOURCEFILE

 Use 32-bit ABI Use 64-bit ABI [REPORT BUG!!] [REPORT BUG!!] % `%s' is normally a non-static function `%s' takes only zero or two arguments `return' with no value, in function returning non-void bad header version close %d close %s control reaches end of non-void function division by zero dup2 %d 1 dynamic dependencies.
 execv %s fatal: fclose fclose %s fdopen field width first argument of `%s' should be `int' fopen %s fstat %s gcc driver version %s executing gcc version %s
 gcc version %s
 gcov (GCC) %s
 implicit declaration of function `%#D' implicit declaration of function `%s' initialization install: %s%s
 internal error internal error:  internal gcc abort invalid option `%s' ld returned %d exit status libraries: %s
 lseek %s 0 members missing terminating %c character msync %s munmap %s named members no arguments no input files not found
 note: open %s parameter name omitted parse error passing arg %d of `%s' passing arg %d of pointer to function pipe precision previous implicit declaration of `%s' programs: %s
 read %s return return type defaults to `int' return type of `%s' is not `int' second argument of `%s' should be `char **' struct structure syntax error syntax error at '%s' token third argument of `%s' should probably be `char **' too many input files type mismatch with previous implicit declaration union unrecognized option `-%s' unused variable `%s' warning:  write %s Project-Id-Version: gcc 3.2
POT-Creation-Date: 2002-12-30 18:56+0000
PO-Revision-Date: 2002-08-18 15:46+0100
Last-Translator: Simos Xenitellis <simos@hellug.gr>
Language-Team: Greek <nls@tux.hellug.gr>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-7
Content-Transfer-Encoding: 8bit
 
��� ������� ��� �������� ���������. �������� �����:
 
��� ������� ���� �������� ���������, �������� �����:
%s.
 
�� ����������; (y � n)  
�� �������� ��� �������� �� -g, -f, -m, -O, -W � --param ������� ��������
 ���� �������� ������������� ��� ����� �� %s.  ��� �� �������� �����
 �������� �� ����� ��� ����������, ������ �� ��������������� ��� ��������
 -W<������>.
         `%D'   (����� '-v --help' ��� ��� �������� �������� ������� ������� �������������)
   --help                   �������� ����� ��� �����������
   --target-help            �������� �������� ������� ������� �������� �� �� �����
   -B <���������>           �������� ��� <���������> ���� ��������� ���������� ��� �������������
   -E                       �������������� ���� ��� ������������, ������������� � �������
   -S                       ������������ ���� ��� ������������� ��������
   -V <������>              �������� ��� ������� <������> ��� gcc, �� ����� �������������
   -Wa,<��������>            ������� <��������> ������������� �� ����� ��� �������������
   -Wl,<��������>            ������� <��������> ������������� �� ����� ��� �������
   -Wp,<��������>            ������� <��������> ������������� �� ����� ���� ���-�����������
   -Xlinker <������>        ������� <���������> ��� �������
   -b <������>              �������� ��� gcc ��� �� ����� <������>, �� ����� ��������������
   -c                       ������������ ��� �������������, ������� �������
   -dumpmachine             �������� ��� ����������� ������ ��� �������������
   -dumpspecs               �������� ���� ��� ��������������� �������������� ��� �������������
   -dumpversion             �������� ��� ������� ��� �������������
   -h, --help                      �������� ����� ��� ��������, ���� ������
   -o <������>              ���������� ��� ������ ��� <������>
   -pass-exit-codes         ������ �� �� ��������� ������ ��������� ��� �����
   -pipe                    ����� ���������� ���� ���������� �������
   -print-file-name=<�������> �������� ��� ������ ��������� ��� ���������� <�������>
   -print-libgcc-file-name  �������� ��� �������� ��� ������������ ����������� ��� �������������
   -print-multi-directory   �������� ��� ������� ��������� ��� �������� ��� libgcc
   -print-multi-lib         �������� ��� ������������� ������ �������� �������
                           ������� ��� ��������� ���������� ���������
                           �����������
   -print-prog-name=<�����> �������� ��� ������ ��������� ��� ��������� ������������� <�����>
   -print-search-dirs       �������� ��� ��������� ���� ��������� ���������� ��� �������������
   -save-temps              �� �� ���������� �� ��������� ������
   -specs=<������>           ����� ��� ������������ ��� <�������> ���� ��� ��������������� �������������
   -std=<�������>           ������� ��� �� ����� ������ ����� ��� �� <�������>

   -time                    ������� ��� ������ ��������� ���� �������������
   -v                       �������� ��� ������������ ��� ����� � ��������������
   -v, --version                   �������� ������� �������, ���� ������
   -x <������>              ���������� ��� ������� ��� �������� ������� �������
                           ��������� ������� �����: c c++ assembler none
                           �� 'none' �������� ��������� ���� �� �������
                           ����������� ������� ��� ������� ����� ��� ���������
                           ��� �������
   ���� %d �����������   ���� `%D'  %s  ��������              : # %s %.2f %.2f
 #elif ���� ��� #else #else ���� ��� #else %s %s  %s (GCC) %s
 %s ��� ����� ��� ������� %s ���� ��� "%s" %s ���� ��� %s'%c' %s ���� ��� %s'\x%x' %s ���� ��� �� ������� '%s' %s ���� ��� `%c' %s ���� ��� `%s' %s ���� ��� `\%o' %s ���� ��� ���������� ������� %s ���������� ������� �� ������ ����� ���������� %s.
%s %s: %s %s: � �������������� %s ��� ���� ������������ ��� ������� ���� %s: %s:  ���������� '(' ���������� '(' ���������� '*' ���������� ':' ���������� ';' ���������� '[' ���������� ']' ���������� '{' ((�������)) (�����������): �� -pipe ��� ������������� �� -pipe ��� ������������� ��������� 31 bit ��������� 64 bit :
 ���� ��������� ��: %s
 ���������� ���������� (C) 2001 Free Software Foundation, Inc.
 ���������� ���������� (C) 2002 Free Software Foundation, Inc.
 ���������� ��� %s.
 ��� ������ �����-��������� ��� ����� ����� ��� ��������� ��� ��������� `%s': ��� ��������� ����� `%s': ��� ��������� ���� ���������� ��������:
 E������� ����������� ������� ������.

 ������� �������: %s
 �����: %s [��������] ������...
 �����: gcov [�������]... [���ź������]

 ����� ABI 32-bit ����� ABI 64-bit [���ָ���� �� ������!!] [���ָ���� �� �ֶ���!!] % � `%s' ����� ������� ��-������� ��������� � `%s' ������� ���� ������ ���� ��� �������� `return' ����� ����, �� ��������� ��� ���������� ��-���� ��������� ������ ��������� close %d close %s � ������� ������ ��� ����� ��-����� ���������� �������� �� ����� dup2 %d 1 ��������� ����������.
 execv %s �������: fclose fclose %s fdopen ������� ������ �� ����� ������ ��� `%s' ������ �� ����� `int' fopen %s fstat %s ������ ������ gcc %s ������� ��� ������ gcc %s
 ������ gcc %s
 gcov (GCC) %s
 ��������� ��������� ��� ���������� `%#D' ��������� ��������� ��� ���������� `%s' ������������ �����������: %s%s
 ��������� ������ ��������� ������:  ��������� ������ gcc �� ������ ������� `%s' � ld ��������� ��������� ������ %d �����������: %s
 lseek %s 0 ���� ���� ���������� ���������� ���������� %c msync %s munmap %s ������� ���� ����� �������� ��� �������� ������ ������� �� �������
 ��������: open %s ���� ���������� �� ����� ��� ���������� ������ ������������ �� ������� ��� ��������� %d ��� `%s' �� ������� ��� ��������� %d ��� ������ ��� ��������� �������� �������� ����������� ��������� ��������� ��� `%s' �����������: %s
 read %s ��������� � �������������� ����� �������������� �� `int' � ������������� ���� ��� `%s' ��� ����� `int' �� ������� ������ ��� `%s' ������ �� ����� `char **' ���� ���� ������ �������� ���������� ������ ��� ������� '%s' �� ����� ������ ��� `%s' ������ �� ����� `char **' ���������� ����� ������ ������� ������ ������������ ����� �� ����������� ��������� ��������� ����� �� ������������ ������� `-%s' �������������� ��������� `%s' �������������:  write %s 