XwcLookupString(3X11R5)                         XwcLookupString(3X11R5)

  XXmmbbLLooookkuuppSSttrriinngg(())

  NNAAMMEE

    XmbLookupString(), XwcLookupString() - obtain composed input from an input
    method

  SSYYNNOOPPSSIISS

    int XmbLookupString (XIC ic, XKeyPressedEvent *event,
                         char *buffer_return, int bytes_buffer,
                         KeySym *keysym_return, Status *status_return)
    int XwcLookupString (XIC ic, XKeyPressedEvent *event,
                         wchar_t *buffer_return, int wchars_buffer,
                         KeySym *keysym_return, Status *status_return)

  AARRGGUUMMEENNTTSS

    buffer_return
        Returns a multibyte string or wide character string (if any) from the
        input method.

    bytes_buffer

    wchars_buffer
        Specifies space available in return buffer.

    event
        Specifies the key event to be used.

    ic
        Specifies the input context.

    keysym_return
        Returns the KeySym computed from the event if this argument is not
        NULL.

    status_return
        Returns a value indicating what kind of data is returned.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_mm_bb_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) and _XX_ww_cc_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) functions return
    the string from the input method specified in the buffer_return argument.
    If no string is returned, the buffer_return argument is unchanged.

    The KeySym into which the KeyCode from the event was mapped is returned in
    the keysym_return argument if it is non-NULL and the status_return
    argument indicates that a KeySym was returned. If both a string and a
    KeySym are returned, the KeySym value does not necessarily correspond to
    the string returned.

    Note that _XX_mm_bb_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) returns the length of the string in
    bytes and that _XX_ww_cc_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) returns the length of the string in
    characters.

    _XX_mm_bb_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) and _XX_ww_cc_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) return text in the
    encoding of the locale bound to the input method of the specified input
    context.

    Note that each string returned by _XX_mm_bb_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) and
    _XX_ww_cc_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) begins in the initial state of the encoding of the
    locale (if the encoding of the locale is state-dependent).

    NNoottee

    In order to insure proper input processing, it is essential that the
    client pass only KeyPress events to _XX_mm_bb_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) and
    _XX_ww_cc_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5). Their behavior when a client passes a KeyRelease
    event is undefined.

    Clients should check the status_return argument before using the other
    returned values. These two functions both return a value to status_return
    that indicates what has been returned in the other arguments. The possible
    values returned are:
    XXBBuuffffeerrOOvveerrffllooww()
        The input string to be returned is too large for the supplied
        buffer_return. The required size (XXmmbbLLooookkuuppSSttrriinngg() in bytes;
        XXwwccLLooookkuuppSSttrriinngg() in characters) is returned as the value of the
        function, and the contents of buffer_return and keysym_return are not
        modified. The client should recall the function with the same event
        and a buffer of adequate size in order to obtain the string.
    XXLLooookkuuppNNoonnee()
        No consistent input has been composed so far. The contents of
        buffer_return and keysym_return are not modified, and the function
        returns zero.
    XXLLooookkuuppCChhaarrss()
        Some input characters have been composed. They are placed in the
        buffer_return argument, and the string length is returned as the value
        of the function. The string is encoded in the locale bound to the
        input context. The contents of the keysym_return argument is not
        modified.
    XXLLooookkuuppKKeeyySSyymm()
        A KeySym has been returned instead of a string and is returned in
        keysym_return. The contents of the buffer_return argument is not
        modified, and the function returns zero.
    XXLLooookkuuppBBootthh()
        Both a KeySym and a string are returned; XLookupChars and
        _XX_LL_oo_oo_kk_uu_pp_KK_ee_yy_SS_yy_mm(3X11R5) occur simultaneously.

    It does not make any difference if the input context passed as an argument
    to _XX_mm_bb_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) and _XX_ww_cc_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) is the one
    currently in possession of the focus or not. Input may have been composed
    within an input context before it lost the focus, and that input may be
    returned on subsequent calls to _XX_mm_bb_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) or
    _XX_ww_cc_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5), even though it does not have any more keyboard
    focus.

  SSEEEE AALLSSOO

    _X_L_o_o_k_u_p_K_e_y_s_y_m()

    Xlib

