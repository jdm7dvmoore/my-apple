XtMergeArgLists(3X11R5)                         XtMergeArgLists(3X11R5)

  XXttSSeettAArrgg(())

  NNAAMMEE

    XtSetArg(), XtMergeArgLists() - set and merge ArgLists

  SSYYNNOOPPSSIISS

    XtSetArg (Arg arg, String name, XtArgVal value)
    ArgList XtMergeArgLists (ArgList args1, Cardinal num_args1,
                             ArgList args2, Cardinal num_args2)

  AARRGGUUMMEENNTTSS

    arg
        Specifies the name-value pair to set.

    args1
        Specifies the first AArrggLLiisstt.

    args2
        Specifies the second AArrggLLiisstt.

    num_args1
        Specifies the number of arguments in the first argument list.

    num_args2
        Specifies the number of arguments in the second argument list.

    name
        Specifies the name of the resource.

    value
        Specifies the value of the resource if it will fit in an XXttAArrggVVaall or
        the address.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_SS_ee_tt_AA_rr_gg(3X11R5) function is usually used in a highly stylized manner
    to minimize the probability of making a mistake; for example:

    Arg args[20];
    int n;

    n = 0;
    XtSetArg(args[n], XtNheight, 100);n++;
    XtSetArg(args[n], XtNwidth, 200);n++;
    XtSetValues(widget, args, n);

    Alternatively, an application can statically declare the argument list and
    use _XX_tt_NN_uu_mm_bb_ee_rr(3X11R5):

    static Args args[] = {
         {XtNheight, (XtArgVal) 100},
         {XtNwidth, (XtArgVal) 200},
    };
    XtSetValues(Widget, args, XtNumber(args));

    Note that you should not use auto-increment or auto-decrement within the
    first argument to _XX_tt_SS_ee_tt_AA_rr_gg(3X11R5). _XX_tt_SS_ee_tt_AA_rr_gg(3X11R5) can be implemented as
    a macro that dereferences the first argument twice.

    The _XX_tt_MM_ee_rr_gg_ee_AA_rr_gg_LL_ii_ss_tt_ss(3X11R5) function allocates enough storage to hold the
    combined AArrggLLiisstt structures and copies them into it. Note that it does not
    check for duplicate entries. When it is no longer needed, free the
    returned storage by using XXttFFrreeee.

  SSEEEE AALLSSOO

    _X_t_O_f_f_s_e_t()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

