XcmsQueryBlack(3X11R5)                           XcmsQueryBlack(3X11R5)

  XXccmmssQQuueerryyBBllaacckk(())

  NNAAMMEE

    XcmsQueryBlack(), XcmsQueryBlue(), XcmsQueryGreen(), XcmsQueryRed(),
    XcmsQueryWhite() - obtain black, blue, green, red, and white CCC color
    specifications

  SSYYNNOOPPSSIISS

    Status XcmsQueryBlack (XcmsCCC ccc, XcmsColorFormat target_format,
                           XcmsColor *color_return)
    Status XcmsQueryBlue (XcmsCCC ccc, XcmsColorFormat target_format,
                          XcmsColor *color_return)
    Status XcmsQueryGreen (XcmsCCC ccc, XcmsColorFormat target_format,
                           XcmsColor *color_return)
    Status XcmsQueryRed (XcmsCCC ccc, XcmsColorFormat target_format,
                         XcmsColor *color_return)
    Status XcmsQueryWhite (XcmsCCC ccc, XcmsColorFormat target_format,
                           XcmsColor *color_return)

  AARRGGUUMMEENNTTSS

    ccc
        Specifies the CCC. Note that the CCC's Client White Point and White
        Point Adjustment procedures are ignored.

    color_return
        Returns the color specification in the specified target format. The
        white point associated with the returned color specification is the
        Screen White Point. The value returned in the pixel member is
        undefined.

    target_format
        Specifies the target color specification format.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_cc_mm_ss_QQ_uu_ee_rr_yy_BB_ll_aa_cc_kk(3X11R5) function returns the color specification in the
    specified target format for zero intensity red, green, and blue.

    The _XX_cc_mm_ss_QQ_uu_ee_rr_yy_BB_ll_uu_ee(3X11R5) function returns the color specification in the
    specified target format for full intensity blue while red and green are
    zero.

    The _XX_cc_mm_ss_QQ_uu_ee_rr_yy_GG_rr_ee_ee_nn(3X11R5) function returns the color specification in the
    specified target format for full intensity green while red and blue are
    zero.

    The _XX_cc_mm_ss_QQ_uu_ee_rr_yy_RR_ee_dd(3X11R5) function returns the color specification in the
    specified target format for full intensity red while green and blue are
    zero.

    The _XX_cc_mm_ss_QQ_uu_ee_rr_yy_WW_hh_ii_tt_ee(3X11R5) function returns the color specification in the
    specified target format for full intensity red, green, and blue.

  SSEEEE AALLSSOO

    _X_c_m_s_C_I_E_L_a_b_Q_u_e_r_y_M_a_x_C()

    _X_c_m_s_C_I_E_L_u_v_Q_u_e_r_y_M_a_x_C()

    _X_c_m_s_T_e_k_H_V_C_Q_u_e_r_y_M_a_x_C()

    Xlib

