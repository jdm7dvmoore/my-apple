XtMalloc(3X11R5)                                       XtMalloc(3X11R5)

  XXttMMaalllloocc(())

  NNAAMMEE

    XtMalloc(), XtCalloc(), XtRealloc(), XtFree(), XtNew(), XtNewString() -
    memory management functions

  SSYYNNOOPPSSIISS

    char *XtMalloc (Cardinal size)
    char *XtCalloc (Cardinal num, Cardinal size)
    char *XtRealloc (char *ptr, Cardinal num)
    void XtFree (char *ptr)
    type *XtNew (type)
    String XtNewString (String string)

  AARRGGUUMMEENNTTSS

    num
        Specifies the number of bytes or array elements.

    ptr
        Specifies a pointer to the old storage or to the block of storage that
        is to be freed.

    size
        Specifies the size of an array element (in bytes) or the number of
        bytes desired.

    string
        Specifies a previously declared string.

    type
        Specifies a previously declared data type.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_MM_aa_ll_ll_oo_cc(3X11R5) functions returns a pointer to a block of storage of
    at least the specified size bytes. If there is insufficient memory to
    allocate the new block, _XX_tt_MM_aa_ll_ll_oo_cc(3X11R5) calls XXttEErrrroorrMMssgg().

    The _XX_tt_CC_aa_ll_ll_oo_cc(3X11R5) function allocates space for the specified number of
    array elements of the specified size and initializes the space to zero. If
    there is insufficient memory to allocate the new block, _XX_tt_CC_aa_ll_ll_oo_cc(3X11R5)
    calls XXttEErrrroorrMMssgg().

    The _XX_tt_RR_ee_aa_ll_ll_oo_cc(3X11R5) function changes the size of a block of storage
    (possibly moving it). Then, it copies the old contents (or as much as will
    fit) into the new block and frees the old block. If there is insufficient
    memory to allocate the new block, _XX_tt_RR_ee_aa_ll_ll_oo_cc(3X11R5) calls XXttEErrrroorrMMssgg(). If
    ptr is NULL, _XX_tt_RR_ee_aa_ll_ll_oo_cc(3X11R5) allocates the new storage without copying
    the old contents; that is, it simply calls _XX_tt_MM_aa_ll_ll_oo_cc(3X11R5).

    The _XX_tt_FF_rr_ee_ee(3X11R5) function returns storage and allows it to be reused. If
    ptr is NULL, _XX_tt_FF_rr_ee_ee(3X11R5) returns immediately.

    _XX_tt_NN_ee_ww(3X11R5) returns a pointer to the allocated storage. If there is
    insufficient memory to allocate the new block, _XX_tt_NN_ee_ww(3X11R5) calls
    XXttEErrrroorrMMssgg(). _XX_tt_NN_ee_ww(3X11R5) is a convenience macro that calls
    _XX_tt_MM_aa_ll_ll_oo_cc(3X11R5) with the following arguments specified:

    ((type *) XtMalloc((unsigned) sizeof(type))

    _XX_tt_NN_ee_ww_SS_tt_rr_ii_nn_gg(3X11R5) returns a pointer to the allocated storage. If there
    is insufficient memory to allocate the new block, _XX_tt_NN_ee_ww_SS_tt_rr_ii_nn_gg(3X11R5)
    calls XXttEErrrroorrMMssgg(). _XX_tt_NN_ee_ww_SS_tt_rr_ii_nn_gg(3X11R5) is a convenience macro that calls
    _XX_tt_MM_aa_ll_ll_oo_cc(3X11R5) with the following arguments specified:

    (strcpy(XtMalloc((unsigned) strlen(str) + 1), str))

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

