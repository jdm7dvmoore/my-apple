XAllowDeviceEvents(3X11R5)                   XAllowDeviceEvents(3X11R5)

  XXAAlllloowwDDeevviicceeEEvveennttss(())

  NNAAMMEE

    XAllowDeviceEvents() - release queued events

  SSYYNNOOPPSSIISS

    XAllowDeviceEvents (Display *display, XDevice *device,
                        int event_mode, Time time)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    device
        Specifies the device from which events are to be allowed.

    event_mode
        Specifies the event mode. You can pass AsyncThisDevice,
        SyncThisDevice, ReplayThisDevice, AsyncOtherDevices, SyncAll, or
        AsyncAll.

    time
        Specifies the time. You can pass either a timestamp or CurrentTime.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_AA_ll_ll_oo_ww_DD_ee_vv_ii_cc_ee_EE_vv_ee_nn_tt_ss(3X11R5) function releases some queued events if the
    client has caused a device to freeze. It has no effect if the specified
    time is earlier than the last-grab time of the most recent active grab for
    the client and device, or if the specified time is later than the current
    X server time.

    The following describes the processing that occurs depending on what
    constant you pass to the event_mode argument.

    AsyncThisDevice
        If the specified device is frozen by the client, event processing for
        that device continues as usual. If the device is frozen multiple times
        by the client on behalf of multiple separate grabs, AsyncThisDevice
        thaws for all. AsyncThisDevice has no effect if the specified device
        is not frozen by the client, but the device need not be grabbed by the
        client.

    SyncThisDevice
        If the specified device is frozen and actively grabbed by the client,
        event processing for that device continues normally until the next key
        or button event is reported to the client. At this time, the specified
        device again appears to freeze. However, if the reported event causes
        the grab to be released, the specified device does not freeze.
        SyncThisDevice has no effect if the specified device is not frozen by
        the client or is not grabbed by the client.

    ReplayThisDevice
        If the specified device is actively grabbed by the client and is
        frozen as the result of an event having been sent to the client
        (either from the activation of a GrabDeviceButton or from a previous
        AllowDeviceEvents with mode SyncThisDevice, but not from a
        GrabDevice), the grab is released and that event is completely
        reprocessed. This time, however, the request ignores any passive grabs
        at or above (toward the root) that the grab-window of the grab just
        released. The request has no effect if the specified device is not
        grabbed by the client or if it is not frozen as the result of an
        event.

    AsyncOtherDevices
        If the remaining devices are frozen by the client, event processing
        for them continues as usual. If the other devices are frozen multiple
        times by the client on behalf of multiple grabs, AsyncOtherDevices
        "thaws" for all. AsyncOtherDevices has no effect if the devices are
        not frozen by the client.

    SyncAll
        If all devices are frozen by the client, event processing (for all
        devices) continues normally until the next button or key event is
        reported to the client for a grabbed device, at which time all devices
        again appear to freeze. However, if the reported event causes the grab
        to be released, then the devices do not freeze. If any device is still
        grabbed, then a subsequent event for it will still cause all devices
        to freeze. SyncAll has no effect unless all devices are frozen by the
        client. If any device is frozen twice by the client on behalf of two
        separate grabs, SyncAll thaws for both. A subsequent freeze for
        SyncAll will only freeze each device once.

    AsyncAll
        If all devices are frozen by the client, event processing for all
        devices continues normally. If any device is frozen multiple times by
        the client on behalf of multiple separate grabs, AsyncAll thaws for
        all. AsyncAll has no effect unless all devices are frozen by the
        client.
        AsyncThisDevice, SyncThisDevice, and ReplayThisDevice have no effect
        on the processing of events from the remaining devices.
        AsyncOtherDevices has no effect on the processing of events from the
        specified device. When the event_mode is SyncAll or AsyncAll, the
        device parameter is ignored.
        It is possible for several grabs of different devices by the same or
        different clients to be active simultaneously. If a device is frozen
        on behalf of any grab, no event processing is performed for the
        device. It is possible for a single device to be frozen because of
        several grabs. In this case, the freeze must be released on behalf of
        each grab before events can again be processed.
        XAllowDeviceEvents can generate a BadDevice or BadValue error.

  DDIIAAGGNNOOSSTTIICCSS

    BadDevice
        An invalid device was specified. The specified device does not exist
        or has not been opened by this client through XXOOppeennIInnppuuttDDeevviiccee(). This
        error might also occur if the specified device is the X keyboard or X
        pointer device.

    BadValue
        An invalid mode was specified on the request.

  SSEEEE AALLSSOO

    _X_G_r_a_b_D_e_v_i_c_e()

    Programming With Xlib

