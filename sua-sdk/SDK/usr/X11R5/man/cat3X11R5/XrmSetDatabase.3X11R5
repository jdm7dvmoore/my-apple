XrmSetDatabase(3X11R5)                           XrmSetDatabase(3X11R5)

  XXrrmmGGeettFFiilleeDDaattaabbaassee(())

  NNAAMMEE

    XrmGetFileDatabase(), XrmPutFileDatabase(), XrmGetStringDatabase(),
    XrmLocaleOfDatabase(), XrmGetDatabase(), XrmSetDatabase(),
    XrmDestroyDatabase() - retrieve and store resource databases

  SSYYNNOOPPSSIISS

    XrmDatabase XrmGetFileDatabase (char *filename)
    void XrmPutFileDatabase (XrmDatabase database, char *stored_db)
    XrmDatabase XrmGetStringDatabase (char *data)
    char *XrmLocaleOfDatabase (XrmDatabase database)
    XrmDatabase XrmGetDatabase (Display *display)
    void XrmSetDatabase (Display *display, XrmDatabase database)
    void XrmDestroyDatabase (XrmDatabase database)

  AARRGGUUMMEENNTTSS

    filename
        Specifies the resource database file name.

    database
        Specifies the database that is to be used.

    stored_db
        Specifies the file name for the stored database.

    data
        Specifies the database contents using a string.

    database
        Specifies the resource database.

    display
        Specifies the connection to the X server.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_rr_mm_GG_ee_tt_FF_ii_ll_ee_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) function opens the specified file, creates
    a new resource database, and loads it with the specifications read in from
    the specified file. The specified file must contain a sequence of entries
    in valid ResourceLine format (see section 15.1). The file is parsed in the
    current locale, and the database is created in the current locale. If it
    cannot open the specified file, _XX_rr_mm_GG_ee_tt_FF_ii_ll_ee_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) returns NULL.

    The _XX_rr_mm_PP_uu_tt_FF_ii_ll_ee_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) function stores a copy of the specified
    database in the specified file. Text is written to the file as a sequence
    of entries in valid ResourceLine format (see section 15.1). The file is
    written in the locale of the database. Entries containing resource names
    that are not in the Host Portable Character Encoding, or containing values
    that are not in the encoding of the database locale, are written in an
    implementation-dependent manner. The order in which entries are written is
    implementation dependent. Entries with representation types other than
    ``String'' are ignored.

    The _XX_rr_mm_GG_ee_tt_SS_tt_rr_ii_nn_gg_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) function creates a new database and
    stores the resources specified in the specified null-terminated string.
    _XX_rr_mm_GG_ee_tt_SS_tt_rr_ii_nn_gg_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) is similar to _XX_rr_mm_GG_ee_tt_FF_ii_ll_ee_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5)
    except that it reads the information out of a string instead of out of a
    file. The string must contain a sequence of entries in valid ResourceLine
    format (see section 15.1). The string is parsed in the current locale, and
    the database is created in the current locale.

    If database is NULL, _XX_rr_mm_DD_ee_ss_tt_rr_oo_yy_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) returns immediately.

    The _XX_rr_mm_LL_oo_cc_aa_ll_ee_OO_ff_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) function returns the name of the locale
    bound to the specified database, as a null-terminated string. The returned
    locale name string is owned by Xlib and should not be modified or freed by
    the client. Xlib is not permitted to free the string until the database is
    destroyed. Until the string is freed, it will not be modified by Xlib.

    The _XX_rr_mm_GG_ee_tt_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) function returns the database associated with
    the specified display. It returns NULL if a database has not yet been set.

    The _XX_rr_mm_SS_ee_tt_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) function associates the specified resource
    database (or NULL) with the specified display. The database previously
    associated with the display (if any) is not destroyed. A client or toolkit
    might find this function convenient for retaining a database once it is
    constructed.

  FFIILLEE SSYYNNOOPPSSIISS

    The syntax of a resource file is a sequence of resource lines terminated
    by newline characters or end of file. The syntax of an individual resource
    line is:

    Elements separated by vertical bar (|) are alternatives. Curly braces
    ({...}) indicate zero or more repetitions of the enclosed elements. Square
    brackets ([...]) indicate that the enclosed element is optional. Quotes
    ("...") are used around literal characters.

    IncludeFile lines are interpreted by replacing the line with the contents
    of the specified file. The word "include" must be in lowercase. The file
    name is interpreted relative to the directory of the file in which the
    line occurs (for example, if the file name contains no directory or
    contains a relative directory specification).

    If a ResourceName contains a contiguous sequence of two or more Binding
    characters, the sequence will be replaced with single "." character if the
    sequence contains only "." characters, otherwise the sequence will be
    replaced with a single "*" character.

    A resource database never contains more than one entry for a given
    ResourceName. If a resource file contains multiple lines with the same
    ResourceName, the last line in the file is used.

    Any whitespace character before or after the name or colon in a
    ResourceSpec are ignored. To allow a Value to begin with whitespace, the
    two-character sequence ``\space'' (backslash followed by space) is
    recognized and replaced by a space character, and the two-character
    sequence ``\tab'' (backslash followed by horizontal tab) is recognized and
    replaced by a horizontal tab character. To allow a Value to contain
    embedded newline characters, the two-character sequence ``\n'' is
    recognized and replaced by a newline character. To allow a Value to be
    broken across multiple lines in a text file, the two-character sequence
    ``\newline'' (backslash followed by newline) is recognized and removed
    from the value. To allow a Value to contain arbitrary character codes, the
    four-character sequence ``\nnn'', where each n is a digit character in the
    range of ``0''-``7'', is recognized and replaced with a single byte that
    contains the octal value specified by the sequence. Finally, the two-
    character sequence ``\\'' is recognized and replaced with a single
    backslash.

  SSEEEE AALLSSOO

    _X_r_m_G_e_t_R_e_s_o_u_r_c_e()

    _X_r_m_I_n_i_t_i_a_l_i_z_e()

    _X_r_m_P_u_t_R_e_s_o_u_r_c_e()

    Xlib

