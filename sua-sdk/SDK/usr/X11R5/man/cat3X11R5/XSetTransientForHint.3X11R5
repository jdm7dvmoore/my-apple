XSetTransientForHint(3X11R5)               XSetTransientForHint(3X11R5)

  XXSSeettTTrraannssiieennttFFoorrHHiinntt(())

  NNAAMMEE

    XSetTransientForHint(), XGetTransientForHint() - set or read a window's
    WM_TRANSIENT_FOR property

  SSYYNNOOPPSSIISS

    XSetTransientForHint (Display *display, Window w,
                          Window prop_window)
    Status XGetTransientForHint (Display *display, Window w,
                                 Window *prop_window_return)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    w
        Specifies the window.

    prop_window
        Specifies the window that the WM_TRANSIENT_FOR property is to be set
        to.

    prop_window_return
        Returns the WM_TRANSIENT_FOR property of the specified window.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_tt_TT_rr_aa_nn_ss_ii_ee_nn_tt_FF_oo_rr_HH_ii_nn_tt(3X11R5) function sets the WM_TRANSIENT_FOR
    property of the specified window to the specified prop_window.

    _XX_SS_ee_tt_TT_rr_aa_nn_ss_ii_ee_nn_tt_FF_oo_rr_HH_ii_nn_tt(3X11R5) can generate BadAlloc and BadWindow errors.

    The _XX_GG_ee_tt_TT_rr_aa_nn_ss_ii_ee_nn_tt_FF_oo_rr_HH_ii_nn_tt(3X11R5) function returns the WM_TRANSIENT_FOR
    property for the specified window. It returns nonzero status on success;
    otherwise it returns a zero status.

    _XX_GG_ee_tt_TT_rr_aa_nn_ss_ii_ee_nn_tt_FF_oo_rr_HH_ii_nn_tt(3X11R5) can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_TRANSIENT_FOR
        Set by application programs to indicate to the window manager that a
        transient top-level window, such as a dialog box.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t()

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e()

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s()

    _X_A_l_l_o_c_W_M_H_i_n_t_s()

    _X_S_e_t_C_o_m_m_a_n_d()

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y()

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e()

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s()

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e()

    _X_S_e_t_W_M_N_a_m_e()

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s()

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s()

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y()

    Xlib

