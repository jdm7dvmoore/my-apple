XMaskEvent(3X11R5)                                   XMaskEvent(3X11R5)

  XXNNeexxttEEvveenntt(())

  NNAAMMEE

    XNextEvent(), XPeekEvent(), XWindowEvent(), XCheckWindowEvent(),
    XMaskEvent(), XCheckMaskEvent(), XCheckTypedEvent(),
    XCheckTypedWindowEvent() - select events by type

  SSYYNNOOPPSSIISS

    XNextEvent (Display *display, XEvent *event_return)
    XPeekEvent (Display *display, XEvent *event_return)
    XWindowEvent (Display *display, Window w, long event_mask,
                  XEvent *event_return)
    Bool XCheckWindowEvent (Display *display, Window w,
                            long event_mask, XEvent *event_return)
    XMaskEvent (Display *display, long event_mask,
                XEvent *event_return)
    Bool XCheckMaskEvent (Display *display, long event_mask,
                          XEvent *event_return)
    Bool XCheckTypedEvent (Display *display, int event_type,
                           XEvent *event_return)
    Bool XCheckTypedWindowEvent (Display *display, Window w,
                                 int event_type, XEvent *event_return)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    event_mask
        Specifies the event mask.

    event_return
        Returns the matched event's associated structure.

    event_return
        Returns the next event in the queue.

    event_return
        Returns a copy of the matched event's associated structure.

    event_type
        Specifies the event type to be compared.

    w
        Specifies the window whose event you are interested in.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_NN_ee_xx_tt_EE_vv_ee_nn_tt(3X11R5) function copies the first event from the event
    queue into the specified XXEEvveenntt structure and then removes it from the
    queue. If the event queue is empty, _XX_NN_ee_xx_tt_EE_vv_ee_nn_tt(3X11R5) flushes the output
    buffer and blocks until an event is received.

    The _XX_PP_ee_ee_kk_EE_vv_ee_nn_tt(3X11R5) function returns the first event from the event
    queue, but it does not remove the event from the queue. If the queue is
    empty, _XX_PP_ee_ee_kk_EE_vv_ee_nn_tt(3X11R5) flushes the output buffer and blocks until an
    event is received. It then copies the event into the client-supplied
    XXEEvveenntt structure without removing it from the event queue.

    The _XX_WW_ii_nn_dd_oo_ww_EE_vv_ee_nn_tt(3X11R5) function searches the event queue for an event
    that matches both the specified window and event mask. When it finds a
    match, _XX_WW_ii_nn_dd_oo_ww_EE_vv_ee_nn_tt(3X11R5) removes that event from the queue and copies
    it into the specified XXEEvveenntt structure. The other events stored in the
    queue are not discarded. If a matching event is not in the queue,
    XXWWiinnddoowwEEvveenntt flushes the output buffer and blocks until one is received.

    The _XX_CC_hh_ee_cc_kk_WW_ii_nn_dd_oo_ww_EE_vv_ee_nn_tt(3X11R5) function searches the event queue and then
    the events available on the server connection for the first event that
    matches the specified window and event mask. If it finds a match,
    _XX_CC_hh_ee_cc_kk_WW_ii_nn_dd_oo_ww_EE_vv_ee_nn_tt(3X11R5) removes that event, copies it into the specified
    XXEEvveenntt structure, and returns True. The other events stored in the queue
    are not discarded. If the event you requested is not available,
    _XX_CC_hh_ee_cc_kk_WW_ii_nn_dd_oo_ww_EE_vv_ee_nn_tt(3X11R5) returns False, and the output buffer will have
    been flushed.

    The _XX_MM_aa_ss_kk_EE_vv_ee_nn_tt(3X11R5) function searches the event queue for the events
    associated with the specified mask. When it finds a match,
    _XX_MM_aa_ss_kk_EE_vv_ee_nn_tt(3X11R5) removes that event and copies it into the specified
    XXEEvveenntt structure. The other events stored in the queue are not discarded.
    If the event you requested is not in the queue, _XX_MM_aa_ss_kk_EE_vv_ee_nn_tt(3X11R5) flushes
    the output buffer and blocks until one is received.

    The _XX_CC_hh_ee_cc_kk_MM_aa_ss_kk_EE_vv_ee_nn_tt(3X11R5) function searches the event queue and then any
    events available on the server connection for the first event that matches
    the specified mask. If it finds a match, _XX_CC_hh_ee_cc_kk_MM_aa_ss_kk_EE_vv_ee_nn_tt(3X11R5) removes
    that event, copies it into the specified XXEEvveenntt structure, and returns
    True. The other events stored in the queue are not discarded. If the event
    you requested is not available, _XX_CC_hh_ee_cc_kk_MM_aa_ss_kk_EE_vv_ee_nn_tt(3X11R5) returns False, and
    the output buffer will have been flushed.

    The _XX_CC_hh_ee_cc_kk_TT_yy_pp_ee_dd_EE_vv_ee_nn_tt(3X11R5) function searches the event queue and then
    any events available on the server connection for the first event that
    matches the specified type. If it finds a match, _XX_CC_hh_ee_cc_kk_TT_yy_pp_ee_dd_EE_vv_ee_nn_tt(3X11R5)
    removes that event, copies it into the specified XXEEvveenntt structure, and
    returns True. The other events in the queue are not discarded. If the
    event is not available, _XX_CC_hh_ee_cc_kk_TT_yy_pp_ee_dd_EE_vv_ee_nn_tt(3X11R5) returns False, and the
    output buffer will have been flushed.

    The _XX_CC_hh_ee_cc_kk_TT_yy_pp_ee_dd_WW_ii_nn_dd_oo_ww_EE_vv_ee_nn_tt(3X11R5) function searches the event queue and
    then any events available on the server connection for the first event
    that matches the specified type and window. If it finds a match,
    _XX_CC_hh_ee_cc_kk_TT_yy_pp_ee_dd_WW_ii_nn_dd_oo_ww_EE_vv_ee_nn_tt(3X11R5) removes the event from the queue, copies it
    into the specified XXEEvveenntt structure, and returns True. The other events in
    the queue are not discarded. If the event is not available,
    _XX_CC_hh_ee_cc_kk_TT_yy_pp_ee_dd_WW_ii_nn_dd_oo_ww_EE_vv_ee_nn_tt(3X11R5) returns False, and the output buffer will
    have been flushed.

  SSEEEE AALLSSOO

    _X_A_n_y_E_v_e_n_t()

    _X_I_f_E_v_e_n_t()

    _X_P_u_t_B_a_c_k_E_v_e_n_t()

    _X_S_e_n_d_E_v_e_n_t()

    Xlib

