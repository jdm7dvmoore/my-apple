XFetchBytes(3X11R5)                                 XFetchBytes(3X11R5)

  XXSSttoorreeBByytteess(())

  NNAAMMEE

    XStoreBytes(), XStoreBuffer(), XFetchBytes(), XFetchBuffer(),
    XRotateBuffers() - manipulate cut and paste buffers

  SSYYNNOOPPSSIISS

    XStoreBytes (Display *display, char *bytes, int  nbytes)
    XStoreBuffer (Display *display, char *bytes, int  nbytes,
                  int buffer)
    char *XFetchBytes (Display *display, int *nbytes_return)
    char *XFetchBuffer (Display *display, int *nbytes_return,
                        int buffer)
    XRotateBuffers (Display *display, int rotate)

  AARRGGUUMMEENNTTSS

    buffer
        Specifies the buffer in which you want to store the bytes or from
        which you want the stored data returned.

    bytes
        Specifies the bytes, which are not necessarily ASCII or null-
        terminated.

    display
        Specifies the connection to the X server.

    nbytes
        Specifies the number of bytes to be stored.

    nbytes_return
        Returns the number of bytes in the buffer.

    rotate
        Specifies how much to rotate the cut buffers.

  DDEESSCCRRIIPPTTIIOONN

    Note that the data can have embedded null characters, and need not be null
    terminated. The cut buffer's contents can be retrieved later by any client
    calling _XX_FF_ee_tt_cc_hh_BB_yy_tt_ee_ss(3X11R5).

    _XX_SS_tt_oo_rr_ee_BB_yy_tt_ee_ss(3X11R5) can generate a BadAlloc error.

    If an invalid buffer is specified, the call has no effect. Note that the
    data can have embedded null characters, and need not be null terminated.

    _XX_SS_tt_oo_rr_ee_BB_uu_ff_ff_ee_rr(3X11R5) can generate a BadAlloc error.

    The _XX_FF_ee_tt_cc_hh_BB_yy_tt_ee_ss(3X11R5) function returns the number of bytes in the
    nbytes_return argument, if the buffer contains data. Otherwise, the
    function returns NULL and sets nbytes to 0. The appropriate amount of
    storage is allocated and the pointer returned. The client must free this
    storage when finished with it by calling _XX_FF_rr_ee_ee(3X11R5).

    The _XX_FF_ee_tt_cc_hh_BB_uu_ff_ff_ee_rr(3X11R5) function returns zero to the nbytes_return
    argument if there is no data in the buffer or if an invalid buffer is
    specified.

    _XX_FF_ee_tt_cc_hh_BB_uu_ff_ff_ee_rr(3X11R5) can generate a BadValue error.

    The _XX_RR_oo_tt_aa_tt_ee_BB_uu_ff_ff_ee_rr_ss(3X11R5) function rotates the cut buffers, such that
    buffer 0 becomes buffer n, buffer 1 becomes n + 1 mod 8, and so on. This
    cut buffer numbering is global to the display. Note that
    _XX_RR_oo_tt_aa_tt_ee_BB_uu_ff_ff_ee_rr_ss(3X11R5) generates BadMatch errors if any of the eight
    buffers have not been created.

    _XX_RR_oo_tt_aa_tt_ee_BB_uu_ff_ff_ee_rr_ss(3X11R5) can generate a BadMatch error.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadAtom
        A value for an Atom argument does not name a defined Atom.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_F_r_e_e()

    Xlib

