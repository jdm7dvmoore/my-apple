XGetVisualInfo(3X11R5)                           XGetVisualInfo(3X11R5)

  XXGGeettVViissuuaallIInnffoo(())

  NNAAMMEE

    XGetVisualInfo(), XMatchVisualInfo(), XVisualIDFromVisual(), XVisualInfo()
    - obtain visual information and visual structure

  SSYYNNOOPPSSIISS

    XVisualInfo *XGetVisualInfo (Display *display, long vinfo_mask,
                                 XVisualInfo *vinfo_template,
                                 int *nitems_return)
    Status XMatchVisualInfo (Display *display, int screen, int depth,
                             int class, XVisualInfo *vinfo_return)
    VisualID XVisualIDFromVisual (Visual * visual)

  AARRGGUUMMEENNTTSS

    class
        Specifies the class of the screen.

    depth
        Specifies the depth of the screen.

    display
        Specifies the connection to the X server.

    nitems_return
        Returns the number of matching visual structures.

    screen
        Specifies the screen.

    visual
        Specifies the visual type.

    vinfo_mask
        Specifies the visual mask value.

    vinfo_return
        Returns the matched visual information.

    vinfo_template
        Specifies the visual attributes that are to be used in matching the
        visual structures.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_GG_ee_tt_VV_ii_ss_uu_aa_ll_II_nn_ff_oo(3X11R5) function returns a list of visual structures
    that have attributes equal to the attributes specified by vinfo_template.
    If no visual structures match the template using the specified vinfo_mask,
    _XX_GG_ee_tt_VV_ii_ss_uu_aa_ll_II_nn_ff_oo(3X11R5) returns a NULL. To free the data returned by this
    function, use _XX_FF_rr_ee_ee(3X11R5).

    The _XX_MM_aa_tt_cc_hh_VV_ii_ss_uu_aa_ll_II_nn_ff_oo(3X11R5) function returns the visual information for a
    visual that matches the specified depth and class for a screen. Because
    multiple visuals that match the specified depth and class can exist, the
    exact visual chosen is undefined. If a visual is found,
    _XX_MM_aa_tt_cc_hh_VV_ii_ss_uu_aa_ll_II_nn_ff_oo(3X11R5) returns nonzero and the information on the visual
    to vinfo_return. Otherwise, when a visual is not found,
    _XX_MM_aa_tt_cc_hh_VV_ii_ss_uu_aa_ll_II_nn_ff_oo(3X11R5) returns zero.

    The _XX_VV_ii_ss_uu_aa_ll_II_DD_FF_rr_oo_mm_VV_ii_ss_uu_aa_ll(3X11R5) function returns the visual ID for the
    specified visual type.

  SSTTRRUUCCTTUURREESS

    The XXVViissuuaallIInnffoo structure contains:

    Visual information mask bits

    Values

    typedef struct {
         Visual *visual;
         VisualID visualid;
         int screen;
         unsigned int depth;
         int class;
         unsigned long red_mask;
         unsigned long green_mask;
         unsigned long blue_mask;
         int colormap_size;
         int bits_per_rgb;
    } XVisualInfo;

  SSEEEE AALLSSOO

    _X_F_r_e_e()

    Xlib

