XOpenDisplay(3X11R5)                               XOpenDisplay(3X11R5)

  XXOOppeennDDiissppllaayy(())

  NNAAMMEE

    XOpenDisplay(), XCloseDisplay() - connect or disconnect to X server

  SSYYNNOOPPSSIISS

    Display *XOpenDisplay (char *display_name)
    XCloseDisplay (Display *display)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    display_name
        Specifies the hardware display name, which determines the display and
        communications domain to be used. On a POSIX-conformant system, if the
        display_name is NULL, it defaults to the value of the DISPLAY
        environment variable.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) function returns a DDiissppllaayy structure that serves
    as the connection to the X server and that contains all the information
    about that X server. _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) connects your application to the
    X server through TCP or DECnet communications protocols, or through some
    local inter-process communication protocol. If the hostname is a host
    computer name and a single colon (:) separates the hostname and display
    number, _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) connects using TCP streams. If the hostname
    is not specified, Xlib uses whatever it believes is the fastest transport.
    If the hostname is a host computer name and a double colon (::) separates
    the hostname and display number, _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) connects using
    DECnet. A single X server can support any or all of these transport
    mechanisms simultaneously. A particular Xlib implementation can support
    many more of these transport mechanisms.

    If successful, _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) returns a pointer to a DDiissppllaayy
    structure, which is defined in <<XX1111//XXlliibb..hh>>. If _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) does
    not succeed, it returns NULL. After a successful call to
    _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5), all of the screens in the display can be used by the
    client. The screen number specified in the display_name argument is
    returned by the _DD_ee_ff_aa_uu_ll_tt_SS_cc_rr_ee_ee_nn(3X11R5) macro (or the XXDDeeffaauullttSSccrreeeenn()
    function). You can access elements of the DDiissppllaayy and SSccrreeeenn structures
    only by using the information macros or functions. For information about
    using macros and functions to obtain information from the DDiissppllaayy
    structure, see section 2.2.1.

    The _XX_CC_ll_oo_ss_ee_DD_ii_ss_pp_ll_aa_yy(3X11R5) function closes the connection to the X server
    for the display specified in the DDiissppllaayy structure and destroys all
    windows, resource IDs (Window , Font, Pixmap, Colormap, Cursor, and
    GContext), or other resources that the client has created on this display,
    unless the close-down mode of the resource has been changed (see
    _XX_SS_ee_tt_CC_ll_oo_ss_ee_DD_oo_ww_nn_MM_oo_dd_ee(3X11R5)). Therefore, these windows, resource IDs, and
    other resources should never be referenced again or an error will be
    generated. Before exiting, you should call _XX_CC_ll_oo_ss_ee_DD_ii_ss_pp_ll_aa_yy(3X11R5)
    explicitly so that any pending errors are reported as
    _XX_CC_ll_oo_ss_ee_DD_ii_ss_pp_ll_aa_yy(3X11R5) performs a final _XX_SS_yy_nn_cc(3X11R5) operation.

    _XX_CC_ll_oo_ss_ee_DD_ii_ss_pp_ll_aa_yy(3X11R5) can generate a BadGC error.

  SSEEEE AALLSSOO

    _A_l_l_P_l_a_n_e_s()

    _X_F_l_u_s_h()

    _X_S_e_t_C_l_o_s_e_D_o_w_n_M_o_d_e()

    Xlib

