XXorRegion(3X11R5)                                   XXorRegion(3X11R5)

  XXIInntteerrsseeccttRReeggiioonn(())

  NNAAMMEE

    XIntersectRegion(), XUnionRegion(), XUnionRectWithRegion(),
    XSubtractRegion(), XXorRegion(), XOffsetRegion(), XShrinkRegion() - region
    arithmetic

  SSYYNNOOPPSSIISS

    XIntersectRegion (Region sra, srb, dr_return)
    XUnionRegion (Region sra, srb, dr_return)
    XUnionRectWithRegion (XRectangle *rectangle, Region src_region,
                          Region dest_region_return)
    XSubtractRegion (Region sra, srb, dr_return)
    XXorRegion (Region sra, srb, dr_return)
    XOffsetRegion (Region r, int dx, dy)
    XShrinkRegion (Region r, int dx, dy)

  AARRGGUUMMEENNTTSS

    dest_region_return
        Returns the destination region.

    dr_return
        Returns the result of the computation.

    dx

    dy
        Specify the x and y coordinates, which define the amount you want to
        move or shrink the specified region.

    r
        Specifies the region.

    rectangle
        Specifies the rectangle.

    sra

    srb
        Specify the two regions with which you want to perform the
        computation.

    src_region
        Specifies the source region to be used.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_II_nn_tt_ee_rr_ss_ee_cc_tt_RR_ee_gg_ii_oo_nn(3X11R5) function computes the intersection of two
    regions.

    The _XX_UU_nn_ii_oo_nn_RR_ee_gg_ii_oo_nn(3X11R5) function computes the union of two regions.

    The _XX_UU_nn_ii_oo_nn_RR_ee_cc_tt_WW_ii_tt_hh_RR_ee_gg_ii_oo_nn(3X11R5) function updates the destination region
    from a union of the specified rectangle and the specified source region.

    The _XX_SS_uu_bb_tt_rr_aa_cc_tt_RR_ee_gg_ii_oo_nn(3X11R5) function subtracts srb from sra and stores the
    results in dr_return.

    The _XX_XX_oo_rr_RR_ee_gg_ii_oo_nn(3X11R5) function calculates the difference between the
    union and intersection of two regions.

    The _XX_OO_ff_ff_ss_ee_tt_RR_ee_gg_ii_oo_nn(3X11R5) function moves the specified region by a
    specified amount.

    The _XX_SS_hh_rr_ii_nn_kk_RR_ee_gg_ii_oo_nn(3X11R5) function reduces the specified region by a
    specified amount. Positive values shrink the size of the region, and
    negative values expand the region.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_R_e_g_i_o_n()

    _X_D_r_a_w_R_e_c_t_a_n_g_l_e()

    _X_E_m_p_t_y_R_e_g_i_o_n()

    Xlib

