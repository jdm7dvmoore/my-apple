XGetRGBColormaps(3X11R5)                       XGetRGBColormaps(3X11R5)

  XXAAllllooccSSttaannddaarrddCCoolloorrmmaapp(())

  NNAAMMEE

    XAllocStandardColormap(), XSetRGBColormaps(), XGetRGBColormaps(),
    XStandardColormap() - allocate, set, or read a standard colormap structure

  SSYYNNOOPPSSIISS

    XStandardColormap *XAllocStandardColormap ()
    void XSetRGBColormaps (Display *display, Window w,
                           XStandardColormap *std_colormap, int count,
                           Atom property)
    Status XGetRGBColormaps (Display *display, Window w,
                             XStandardColormap **std_colormap_return,
                             int *count_return, Atom property)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    count
        Specifies the number of colormaps.

    count_return
        Returns the number of colormaps.

    property
        Specifies the property name.

    std_colormap
        Specifies the XXSSttaannddaarrddCCoolloorrmmaapp structure to be used.

    std_colormap_return
        Returns the XXSSttaannddaarrddCCoolloorrmmaapp structure.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_AA_ll_ll_oo_cc_SS_tt_aa_nn_dd_aa_rr_dd_CC_oo_ll_oo_rr_mm_aa_pp(3X11R5) function allocates and returns a
    pointer to a XXSSttaannddaarrddCCoolloorrmmaapp structure. Note that all fields in the
    XXSSttaannddaarrddCCoolloorrmmaapp structure are initially set to zero. If insufficient
    memory is available, _XX_AA_ll_ll_oo_cc_SS_tt_aa_nn_dd_aa_rr_dd_CC_oo_ll_oo_rr_mm_aa_pp(3X11R5) returns NULL. To free
    the memory allocated to this structure, use _XX_FF_rr_ee_ee(3X11R5).

    The _XX_SS_ee_tt_RR_GG_BB_CC_oo_ll_oo_rr_mm_aa_pp_ss(3X11R5) function replaces the RGB colormap definition
    in the specified property on the named window. If the property does not
    already exist, _XX_SS_ee_tt_RR_GG_BB_CC_oo_ll_oo_rr_mm_aa_pp_ss(3X11R5) sets the RGB colormap definition
    in the specified property on the named window. The property is stored with
    a type of RGB_COLOR_MAP and a format of 32. Note that it is the caller's
    responsibility to honor the ICCCM restriction that only RGB_DEFAULT_MAP
    contain more than one definition.

    The _XX_SS_ee_tt_RR_GG_BB_CC_oo_ll_oo_rr_mm_aa_pp_ss(3X11R5) function usually is only used by window or
    session managers. To create a standard colormap, follow this procedure:
   1.     Open a new connection to the same server.
   2.     Grab the server.
   3.     See if the property is on the property list of the root window for
          the screen.
   4.     If the desired property is not present:
          *     Create a colormap (unless using the default colormap of the
                screen).
          *     Determine the color characteristics of the visual.
          *     Call _XX_AA_ll_ll_oo_cc_CC_oo_ll_oo_rr_PP_ll_aa_nn_ee_ss(3X11R5) or _XX_AA_ll_ll_oo_cc_CC_oo_ll_oo_rr_CC_ee_ll_ll_ss(3X11R5) to
                allocate cells in the colormap.
          *     Call _XX_SS_tt_oo_rr_ee_CC_oo_ll_oo_rr_ss(3X11R5) to store appropriate color values in
                the colormap.
          *     Fill in the descriptive members in the XXSSttaannddaarrddCCoolloorrmmaapp
                structure.
          *     Attach the property to the root window.
          *     Use _XX_SS_ee_tt_CC_ll_oo_ss_ee_DD_oo_ww_nn_MM_oo_dd_ee(3X11R5) to make the resource permanent.
   5.     Ungrab the server.

    _XX_SS_ee_tt_RR_GG_BB_CC_oo_ll_oo_rr_mm_aa_pp_ss(3X11R5) can generate BadAlloc, BadAtom, and BadWindow
    errors.

    The _XX_GG_ee_tt_RR_GG_BB_CC_oo_ll_oo_rr_mm_aa_pp_ss(3X11R5) function returns the RGB colormap definitions
    stored in the specified property on the named window. If the property
    exists, is of type RGB_COLOR_MAP, is of format 32, and is long enough to
    contain a colormap definition, _XX_GG_ee_tt_RR_GG_BB_CC_oo_ll_oo_rr_mm_aa_pp_ss(3X11R5) allocates and
    fills in space for the returned colormaps and returns a nonzero status. If
    the visualid is not present, _XX_GG_ee_tt_RR_GG_BB_CC_oo_ll_oo_rr_mm_aa_pp_ss(3X11R5) assumes the default
    visual for the screen on which the window is located; if the killid is not
    present, NNoonnee is assumed, which indicates that the resources cannot be
    released. Otherwise, none of the fields are set, and
    _XX_GG_ee_tt_RR_GG_BB_CC_oo_ll_oo_rr_mm_aa_pp_ss(3X11R5) returns a zero status. Note that it is the
    caller's responsibility to honor the ICCCM restriction that only
    RGB_DEFAULT_MAP contain more than one definition.

    _XX_GG_ee_tt_RR_GG_BB_CC_oo_ll_oo_rr_mm_aa_pp_ss(3X11R5) can generate BadAtom and BadWindow errors.

  SSTTRRUUCCTTUURREESS

    The XXSSttaannddaarrddCCoolloorrmmaapp structure contains:

    Hints
    #define ReleaseByFreeingColormap     ( (XID) 1L)

    Values

    typedef struct {
         Colormap colormap;
         unsigned long red_max;
         unsigned long red_mult;
         unsigned long green_max;
         unsigned long green_mult;
         unsigned long blue_max;
         unsigned long blue_mult;
         unsigned long base_pixel;
         VisualID visualid;
         XID killid;
    } XStandardColormap;

    The colormap member is the colormap created by the _XX_CC_rr_ee_aa_tt_ee_CC_oo_ll_oo_rr_mm_aa_pp(3X11R5)
    function. The red_max, green_max, and blue_max members give the maximum
    red, green, and blue values, respectively. Each color coefficient ranges
    from zero to its max, inclusive. For example, a common colormap allocation
    is 3/3/2 (3 planes for red, 3 planes for green, and 2 planes for blue).
    This colormap would have red_max = 7, green_max = 7, and blue_max = 3. An
    alternate allocation that uses only 216 colors is red_max = 5, green_max =
    5, and blue_max = 5.

    The red_mult, green_mult, and blue_mult members give the scale factors
    used to compose a full pixel value. (See the discussion of the base_pixel
    members for further information.) For a 3/3/2 allocation, red_mult might
    be 32, green_mult might be 4, and blue_mult might be 1. For a 6-colors-
    each allocation, red_mult might be 36, green_mult might be 6, and
    blue_mult might be 1.

    The base_pixel member gives the base pixel value used to compose a full
    pixel value. Usually, the base_pixel is obtained from a call to the
    _XX_AA_ll_ll_oo_cc_CC_oo_ll_oo_rr_PP_ll_aa_nn_ee_ss(3X11R5) function. Given integer red, green, and blue
    coefficients in their appropriate ranges, one then can compute a
    corresponding pixel value by using the following expression:

    (r * red_mult + g * green_mult + b * blue_mult + base_pixel) & 0xFFFFFFFF

    For GrayScale colormaps, only the colormap, red_max, red_mult, and
    base_pixel members are defined. The other members are ignored. To compute
    a GrayScale pixel value, use the following expression:

    (gray * red_mult + base_pixel) & 0xFFFFFFFF

    Negative multipliers can be represented by converting the 2's complement
    representation of the multiplier into an unsigned long and storing the
    result in the appropriate _mult field. The step of masking by 0xFFFFFFFF
    effectively converts the resulting positive multiplier into a negative
    one. The masking step will take place automatically on many computer
    architectures, depending on the size of the integer type used to do the
    computation,

    The visualid member gives the ID number of the visual from which the
    colormap was created. The killid member gives a resource ID that indicates
    whether the cells held by this standard colormap are to be released by
    freeing the colormap ID or by calling the _XX_KK_ii_ll_ll_CC_ll_ii_ee_nn_tt(3X11R5) function on
    the indicated resource. (Note that this method is necessary for allocating
    out of an existing colormap.)

    The properties containing the _XX_SS_tt_aa_nn_dd_aa_rr_dd_CC_oo_ll_oo_rr_mm_aa_pp(3X11R5) information have
    the type RGB_COLOR_MAP.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc

        The server failed to allocate the requested resource or server memory.

    BadAtom

        A value for an Atom argument does not name a defined Atom.

    BadWindow

        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_o_l_o_r()

    _X_C_r_e_a_t_e_C_o_l_o_r_m_a_p()

    _X_F_r_e_e()

    _X_S_e_t_C_l_o_s_e_D_o_w_n_M_o_d_e()

    Xlib

