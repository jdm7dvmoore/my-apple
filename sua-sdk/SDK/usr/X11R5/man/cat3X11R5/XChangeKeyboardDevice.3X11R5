XChangeKeyboardDevice(3X11R5)             XChangeKeyboardDevice(3X11R5)

  XXCChhaannggeeKKeeyybbooaarrddDDeevviiccee(())

  NNAAMMEE

    XChangeKeyboardDevice() - change which device is used as the X keyboard

  SSYYNNOOPPSSIISS

    Status XChangeKeyboardDevice (Display *display, XDevice *device)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    device
        Specifies the device to be used as the X keyboard.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_CC_hh_aa_nn_gg_ee_KK_ee_yy_bb_oo_aa_rr_dd_DD_ee_vv_ii_cc_ee(3X11R5) request causes the server to use the
    specified device as the X keyboard. The device must have been previously
    opened by the requesting client through _XX_OO_pp_ee_nn_DD_ee_vv_ii_cc_ee(3X11R5) or a BadDevice
    error will result. The device must support input class Keys, or a BadMatch
    error will result. If the server implementation does not support using the
    requested device as the X keyboard, a BadDevice error will result.

    If the specified device is grabbed by another client, AlreadyGrabbed is
    returned. If the specified device is frozen by a grab on another device,
    GrabFrozen is returned. If the request is successful, Success is returned.

    If the request succeeds, a ChangeDeviceNotify event is sent to all clients
    that have selected that event. A MappingNotify event with request =
    MappingKeyboard is sent to all clients. The specified device becomes the X
    keyboard and the old X keyboard becomes accessible through the input
    extension protocol requests.

    _XX_CC_hh_aa_nn_gg_ee_KK_ee_yy_bb_oo_aa_rr_dd_DD_ee_vv_ii_cc_ee(3X11R5) can generate a BadDevice or a BadMatch
    error.

  DDIIAAGGNNOOSSTTIICCSS

    BadDevice
        An invalid device was specified. The specified device does not exist,
        has not been opened by this client with XXOOppeennIInnppuuttDDeevviiccee(), or is
        already one of the core X device (pointer or keyboard). This error
        might also occur if the server implementation does not support using
        the specified device as the X keyboard.

    BadMatch
        This error might occur if an _XX_CC_hh_aa_nn_gg_ee_KK_ee_yy_bb_oo_aa_rr_dd_DD_ee_vv_ii_cc_ee(3X11R5) request was
        made specifying a device that has no keys.

  SSEEEE AALLSSOO

    _X_C_h_a_n_g_e_P_o_i_n_t_e_r_D_e_v_i_c_e()

    Programming With Xlib

