XLowerWindow(3X11R5)                               XLowerWindow(3X11R5)

  XXRRaaiisseeWWiinnddooww(())

  NNAAMMEE

    XRaiseWindow(), XLowerWindow(), XCirculateSubwindows(),
    XCirculateSubwindowsUp(), XCirculateSubwindowsDown(), XRestackWindows() -
    change window stacking order

  SSYYNNOOPPSSIISS

    XRaiseWindow (Display *display, Window w)
    XLowerWindow (Display *display, Window w)
    XCirculateSubwindows (Display *display, Window w,
                          int direction)
    XCirculateSubwindowsUp (Display *display, Window w)
    XCirculateSubwindowsDown (Display *display, Window w)
    XRestackWindows (Display *display, Window windows[],
                     int nwindows)

  AARRGGUUMMEENNTTSS

    direction
        Specifies the direction (up or down) that you want to circulate the
        window. You can pass RaiseLowest or LowerHighest.

    display
        Specifies the connection to the X server.

    nwindows
        Specifies the number of windows to be restacked.

    w
        Specifies the window.

    windows
        Specifies an array containing the windows to be restacked.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_RR_aa_ii_ss_ee_WW_ii_nn_dd_oo_ww(3X11R5) function raises the specified window to the top
    of the stack so that no sibling window obscures it. If the windows are
    regarded as overlapping sheets of paper stacked on a desk, then raising a
    window is analogous to moving the sheet to the top of the stack but
    leaving its x and y location on the desk constant. Raising a mapped window
    can generate Expose events for the window and any mapped subwindows that
    were formerly obscured.

    If the override-redirect attribute of the window is False and some other
    client has selected SubstructureRedirectMask on the parent, the X server
    generates a ConfigureRequest event, and no processing is performed.
    Otherwise, the window is raised.

    _XX_RR_aa_ii_ss_ee_WW_ii_nn_dd_oo_ww(3X11R5) can generate a BadWindow error.

    The _XX_LL_oo_ww_ee_rr_WW_ii_nn_dd_oo_ww(3X11R5) function lowers the specified window to the
    bottom of the stack so that it does not obscure any sibling windows. If
    the windows are regarded as overlapping sheets of paper stacked on a desk,
    then lowering a window is analogous to moving the sheet to the bottom of
    the stack but leaving its x and y location on the desk constant. Lowering
    a mapped window will generate Expose events on any windows it formerly
    obscured.

    If the override-redirect attribute of the window is False and some other
    client has selected SubstructureRedirectMask on the parent, the X server
    generates a ConfigureRequest event, and no processing is performed.
    Otherwise, the window is lowered to the bottom of the stack.

    _XX_LL_oo_ww_ee_rr_WW_ii_nn_dd_oo_ww(3X11R5) can generate a BadWindow error.

    The _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss(3X11R5) function circulates children of the
    specified window in the specified direction. If you specify RaiseLowest,
    _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss(3X11R5) raises the lowest mapped child (if any) that
    is occluded by another child to the top of the stack. If you specify
    LowerHighest, _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss(3X11R5) lowers the highest mapped child
    (if any) that occludes another child to the bottom of the stack. Exposure
    processing is then performed on formerly obscured windows. If some other
    client has selected SubstructureRedirectMask on the window, the X server
    generates a CirculateRequest event, and no further processing is
    performed. If a child is actually restacked, the X server generates a
    CirculateNotify event.

    _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss(3X11R5) can generate BadValue and BadWindow errors.

    The _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss_UU_pp(3X11R5) function raises the lowest mapped child
    of the specified window that is partially or completely occluded by
    another child. Completely unobscured children are not affected. This is a
    convenience function equivalent to _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss(3X11R5) with
    RaiseLowest specified.

    _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss_UU_pp(3X11R5) can generate a BadWindow error.

    The _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss_DD_oo_ww_nn(3X11R5) function lowers the highest mapped
    child of the specified window that partially or completely occludes
    another child. Completely unobscured children are not affected. This is a
    convenience function equivalent to _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss(3X11R5) with
    LowerHighest specified.

    _XX_CC_ii_rr_cc_uu_ll_aa_tt_ee_SS_uu_bb_ww_ii_nn_dd_oo_ww_ss_DD_oo_ww_nn(3X11R5) can generate a BadWindow error.

    The _XX_RR_ee_ss_tt_aa_cc_kk_WW_ii_nn_dd_oo_ww_ss(3X11R5) function restacks the windows in the order
    specified, from top to bottom. The stacking order of the first window in
    the windows array is unaffected, but the other windows in the array are
    stacked underneath the first window, in the order of the array. The
    stacking order of the other windows is not affected. For each window in
    the window array that is not a child of the specified window, a BadMatch
    error results.

    If the override-redirect attribute of a window is False and some other
    client has selected SubstructureRedirectMask on the parent, the X server
    generates ConfigureRequest events for each window whose override-redirect
    flag is not set, and no further processing is performed. Otherwise, the
    windows will be restacked in top to bottom order.

    _XX_RR_ee_ss_tt_aa_cc_kk_WW_ii_nn_dd_oo_ww_ss(3X11R5) can generate BadWindow error.

  DDIIAAGGNNOOSSTTIICCSS

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_C_h_a_n_g_e_W_i_n_d_o_w_A_t_t_r_i_b_u_t_e_s()

    _X_C_o_n_f_i_g_u_r_e_W_i_n_d_o_w()

    _X_C_r_e_a_t_e_W_i_n_d_o_w()

    _X_D_e_s_t_r_o_y_W_i_n_d_o_w()

    _X_M_a_p_W_i_n_d_o_w()

    _X_U_n_m_a_p_W_i_n_d_o_w()

    Xlib

