XrmCombineFileDatabase(3X11R5)           XrmCombineFileDatabase(3X11R5)

  XXrrmmMMeerrggeeDDaattaabbaasseess(())

  NNAAMMEE

    XrmMergeDatabases(), XrmCombineDatabase(), XrmCombineFileDatabase() -
    merge resource databases

  SSYYNNOOPPSSIISS

    void XrmMergeDatabases (XrmDatabase source_db, *target_db)
    void XrmCombineDatabase (XrmDatabase source_db, *target_db,
                             Bool override)
    void XrmCombineFileDatabase (char *filename,
                                 XrmDatabase *target_db,
                                 Bool override)

  AARRGGUUMMEENNTTSS

    source_db
        Specifies the resource database that is to be merged into the target
        database.

    target_db
        Specifies the resource database into which the source database is to
        be merged.

    filename
        Specifies the resource database file name.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_rr_mm_MM_ee_rr_gg_ee_DD_aa_tt_aa_bb_aa_ss_ee_ss(3X11R5) function merges the contents of one database
    into another. If the same specifier is used for an entry in both
    databases, the entry in the source_db will replace the entry in the
    target_db (that is, it overrides target_db). If target_db contains NULL,
    _XX_rr_mm_MM_ee_rr_gg_ee_DD_aa_tt_aa_bb_aa_ss_ee_ss(3X11R5) simply stores source_db in it. Otherwise,
    source_db is destroyed by the merge, but the database pointed to by
    target_db is not destroyed. The database entries are merged without
    changing values or types, regardless of the locales of the databases. The
    locale of the target database is not modified.

    The _XX_rr_mm_CC_oo_mm_bb_ii_nn_ee_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) function merges the contents of one
    database into another. If the same specifier is used for an entry in both
    databases, the entry in the source_db will replace the entry in the
    target_db if override is True; otherwise, the entry in source_db is
    discarded. If target_db contains NULL, _XX_rr_mm_CC_oo_mm_bb_ii_nn_ee_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) simply
    stores source_db in it. Otherwise, source_db is destroyed by the merge,
    but the database pointed to by target_db is not destroyed. The database
    entries are merged without changing values or types, regardless of the
    locales of the databases. The locale of the target database is not
    modified.

    The _XX_rr_mm_CC_oo_mm_bb_ii_nn_ee_FF_ii_ll_ee_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) function merges the contents of a
    resource file into a database. If the same specifier is used for an entry
    in both the file and the database, the entry in the file will replace the
    entry in the database if override is True; otherwise, the entry in file is
    discarded. The file is parsed in the current locale. If the file cannot be
    read a zero status is returned; otherwise a nonzero status is returned. If
    target_db contains NULL, _XX_rr_mm_CC_oo_mm_bb_ii_nn_ee_FF_ii_ll_ee_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) creates and
    returns a new database to it. Otherwise, the database pointed to by
    target_db is not destroyed by the merge. The database entries are merged
    without changing values or types, regardless of the locale of the
    database. The locale of the target database is not modified.

  SSEEEE AALLSSOO

    _X_r_m_G_e_t_R_e_s_o_u_r_c_e()

    _X_r_m_I_n_i_t_i_a_l_i_z_e()

    _X_r_m_P_u_t_R_e_s_o_u_r_c_e()

    Xlib

