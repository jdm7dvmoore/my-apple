XwcDrawImageString(3X11R5)                   XwcDrawImageString(3X11R5)

  XXmmbbDDrraawwIImmaaggeeSSttrriinngg(())

  NNAAMMEE

    XmbDrawImageString(), XwcDrawImageString() - draw image text using a
    single font set

  SSYYNNOOPPSSIISS

    void XmbDrawImageString (Display *display, Drawable d,
                             XFontSet font_set, GC gc, int x,
                             int y, char *string, int num_bytes)
    void XwcDrawImageString (Display *display, Drawable d,
                             XFontSet font_set, GC gc, int x,
                             int y, wchar_t *string, int num_wchars)

  AARRGGUUMMEENNTTSS

    d
        Specifies the drawable.

    display
        Specifies the connection to the X server.

    font_set
        Specifies the font set.

    gc
        Specifies the GC.

    num_bytes
        Specifies the number of bytes in the string argument.

    num_wchars
        Specifies the number of characters in the string argument.

    string
        Specifies the character string.

    x

    y
        Specify the x and y coordinates.

  DDEESSCCRRIIPPTTIIOONN

    _XX_mm_bb_DD_rr_aa_ww_II_mm_aa_gg_ee_SS_tt_rr_ii_nn_gg(3X11R5) and _XX_ww_cc_DD_rr_aa_ww_II_mm_aa_gg_ee_SS_tt_rr_ii_nn_gg(3X11R5) fill a
    destination rectangle with the background pixel defined in the GC and then
    paint the text with the foreground pixel. The filled rectangle is the
    rectangle returned to overall_logical_return by _XX_mm_bb_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) or
    _XX_ww_cc_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) for the same text and XXFFoonnttSSeett.

    When the XXFFoonnttSSeett has missing charsets, each unavailable character is
    drawn with the default string returned by _XX_CC_rr_ee_aa_tt_ee_FF_oo_nn_tt_SS_ee_tt(3X11R5). The
    behavior for an invalid codepoint is undefined.

  SSEEEE AALLSSOO

    _X_D_r_a_w_I_m_a_g_e_S_t_r_i_n_g()

    _X_D_r_a_w_S_t_r_i_n_g()

    _X_D_r_a_w_T_e_x_t()

    _X_m_b_D_r_a_w_S_t_r_i_n_g()

    _X_m_b_D_r_a_w_T_e_x_t()

    Xlib

