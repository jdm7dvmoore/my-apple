XLocaleOfIM(3X11R5)                                 XLocaleOfIM(3X11R5)

  XXOOppeennIIMM(())

  NNAAMMEE

    XOpenIM(), XCloseIM(), XGetIMValues(), XDisplayOfIM(), XLocaleOfIM() -
    open, close, and otain input method information

  SSYYNNOOPPSSIISS

    XIM XOpenIM (Display *display, XrmDataBase db, char *res_name,
                 char *res_class)
    Status XCloseIM (XIM im)
    char * XGetIMValues (XIM im, ...)
    Display * XDisplayOfIM (XIM im)
    char * XLocaleOfIM (XIM im)

  AARRGGUUMMEENNTTSS

    db
        Specifies a pointer to the resource database.

    display
        Specifies the connection to the X server.

    im
        Specifies the input method.

    res_class
        Specifies the full class name of the application.

    res_name
        Specifies the full resource name of the application.

    ...
        Specifies the variable length argument list to get XIM values.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_OO_pp_ee_nn_II_MM(3X11R5) function opens an input method, matching the current
    locale and modifiers specification. Current locale and modifiers are bound
    to the input method at opening time. The locale associated with an input
    method cannot be changed dynamically. This implies the strings returned by
    _XX_mm_bb_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5) or _XX_ww_cc_LL_oo_oo_kk_uu_pp_SS_tt_rr_ii_nn_gg(3X11R5), for any input context
    affiliated with a given input method, will be encoded in the locale
    current at the time input method is opened.

    The specific input method to which this call will be routed is identified
    on the basis of the current locale. _XX_OO_pp_ee_nn_II_MM(3X11R5) will identify a
    default input method corresponding to the current locale. That default can
    be modified using _XX_SS_ee_tt_LL_oo_cc_aa_ll_ee_MM_oo_dd_ii_ff_ii_ee_rr_ss(3X11R5) for the input method
    modifier.

    The db argument is the resource database to be used by the input method
    for looking up resources that are private to the input method. It is not
    intended that this database be used to look up values that can be set as
    IC values in an input context. If db is NULL, no data base is passed to
    the input method.

    The res_name and res_class arguments specify the resource name and class
    of the application. They are intended to be used as prefixes by the input
    method when looking up resources that are common to all input contexts
    that can be created for this input method. The characters used for
    resource names and classes must be in the X portable character set. The
    resources looked up are not fully specified if res_name or res_class is
    NULL.

    The res_name and res_class arguments are not assumed to exist beyond the
    call to _XX_OO_pp_ee_nn_II_MM(3X11R5). The specified resource database is assumed to
    exist for the lifetime of the input method.

    _XX_OO_pp_ee_nn_II_MM(3X11R5) returns NULL if no input method could be opened.

    The _XX_CC_ll_oo_ss_ee_II_MM(3X11R5) function closes the specified input method.

    The _XX_GG_ee_tt_II_MM_VV_aa_ll_uu_ee_ss(3X11R5) function presents a variable argument list
    programming interface for querying properties or features of the specified
    input method. This function returns NULL if it succeeds; otherwise, it
    returns the name of the first argument that could not be obtained.

    Only one standard argument is defined by Xlib: XNQueryInputStyle, which
    must be used to query about input styles supported by the input method.

    A client should always query the input method to determine which styles
    are supported. The client should then find an input style it is capable of
    supporting.

    If the client cannot find an input style that it can support it should
    negotiate with the user the continuation of the program (exit, choose
    another input method, and so on).

    The argument value must be a pointer to a location where the returned
    value will be stored. The returned value is a pointer to a structure of
    type XXIIMMSSttyylleess. Clients are responsible for freeing the XXIIMMSSttyylleess data
    structure. To do so, use _XX_FF_rr_ee_ee(3X11R5).

    The _XX_DD_ii_ss_pp_ll_aa_yy_OO_ff_II_MM(3X11R5) function returns the display associated with the
    specified input method.

    The _XX_LL_oo_cc_aa_ll_ee_OO_ff_II_MM(3X11R5) returns the locale associated with the specified
    input method.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_I_C()

    _X_S_e_t_I_C_F_o_c_u_s()

    _X_S_e_t_I_C_V_a_l_u_e_s()

    _X_m_b_R_e_s_e_t_I_C()

    Xlib

