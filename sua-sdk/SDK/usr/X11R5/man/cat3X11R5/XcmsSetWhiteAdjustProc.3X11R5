XcmsSetWhiteAdjustProc(3X11R5)           XcmsSetWhiteAdjustProc(3X11R5)

  XXccmmssSSeettWWhhiitteePPooiinntt(())

  NNAAMMEE

    XcmsSetWhitePoint(), XcmsSetWhiteAdjustProc() - modifying CCC attributes

  SSYYNNOOPPSSIISS

    Status XcmsSetWhitePoint (XcmsCCC ccc, XcmsColor *color)
    XcmsWhiteAdjustProc XcmsSetWhiteAdjustProc (XcmsCCC ccc,
                             XcmsWhiteAdjustProc white_adjust_proc,
                             XPointer client_data)

  AARRGGUUMMEENNTTSS

    ccc
        Specifies the CCC.

    client_data
        Specifies client data for the white point adjustment procedure or
        NULL.

    color
        Specifies the new Client White Point.

    white_adjust_proc
        Specifies the white point adjustment procedure.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_cc_mm_ss_SS_ee_tt_WW_hh_ii_tt_ee_PP_oo_ii_nn_tt(3X11R5) function changes the Client White Point in
    the specified CCC. Note that the pixel member is ignored and that the
    color specification is left unchanged upon return. The format for the new
    white point must be XcmsCIEXYZFormat, XcmsCIEuvYFormat, XcmsCIExyYFormat,
    or XcmsUndefinedFormat. If color is NULL, this function sets the format
    component of the Client White Point specification to XcmsUndefinedFormat,
    indicating that the Client White Point is assumed to be the same as the
    Screen White Point.

    The _XX_cc_mm_ss_SS_ee_tt_WW_hh_ii_tt_ee_AA_dd_jj_uu_ss_tt_PP_rr_oo_cc(3X11R5) function first sets the white point
    adjustment procedure and client data in the specified CCC with the newly
    specified procedure and client data and then returns the old procedure.

  SSEEEE AALLSSOO

    _D_i_s_p_l_a_y_O_f_C_C_C()

    _X_c_m_s_C_C_C_o_f_C_o_l_o_r_m_a_p()

    _X_c_m_s_C_o_n_v_e_r_t_C_o_l_o_r_s()

    _X_c_m_s_C_r_e_a_t_e_C_C_C()

    _X_c_m_s_D_e_f_a_u_l_t_C_C_C()

    Xlib

