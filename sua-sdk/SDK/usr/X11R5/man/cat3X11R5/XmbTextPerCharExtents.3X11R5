XmbTextPerCharExtents(3X11R5)             XmbTextPerCharExtents(3X11R5)

  XXmmbbTTeexxttPPeerrCChhaarrEExxtteennttss(())

  NNAAMMEE

    XmbTextPerCharExtents(), XwcTextPerCharExtents() - obtain per-character
    information for a text string

  SSYYNNOOPPSSIISS

    Status XmbTextPerCharExtents (XFontSet font_set, char *string,
                                 int num_bytes,
                                 XRectangle *ink_array_return,
                                 XRectangle *logical_array_return,
                                 int array_size, int *num_chars_return,
                                 XRectangle *overall_ink_return,
                                 XRectangle *overall_logical_return)
    Status XwcTextPerCharExtents (XFontSet font_set, wchar_t *string,
                                  int num_wchars,
                                  XRectangle *ink_array_return,
                                  XRectangle *logical_array_return,
                                  int array_size, int *num_chars_return,
                                  XRectangle *overall_ink_return,
                                  XRectangle *overall_logical_return)

  AARRGGUUMMEENNTTSS

    array_size
        Specifies the size of ink_array_return and logical_array_return. Note
        that the caller must pass in arrays of this size.

    font_set
        Specifies the font set.

    ink_array_return
        Returns the ink dimensions for each character.

    logical_array_return
        Returns the logical dimensions for each character.

    num_bytes
        Specifies the number of bytes in the string argument.

    num_chars_return
        Returns the number characters in the string argument.

    num_wchars
        Specifies the number of characters in the string argument.

    overall_ink_return
        Returns the overall ink extents of the entire string.

    overall_logical_return
        Returns the overall logical extents of the entire string.

    string
        Specifies the character string.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_mm_bb_TT_ee_xx_tt_PP_ee_rr_CC_hh_aa_rr_EE_xx_tt_ee_nn_tt_ss(3X11R5) and _XX_ww_cc_TT_ee_xx_tt_PP_ee_rr_CC_hh_aa_rr_EE_xx_tt_ee_nn_tt_ss(3X11R5) return
    the text dimensions of each character of the specified text, using the
    fonts loaded for the specified font set. Each successive element of
    ink_array_return and logical_array_return is set to the successive
    character's drawn metrics, relative to the drawing origin of the string,
    one XXRReeccttaannggllee for each character in the supplied text string. The number
    of elements of ink_array_return and logical_array_return that have been
    set is returned to num_chars_return.

    Each element of ink_array_return is set to the bounding box of the
    corresponding character's drawn foreground color. Each element of
    logical_array_return is set to the bounding box which provides minimum
    spacing to other graphical features for the corresponding character. Other
    graphical features should not intersect any of the logical_array_return
    rectangles.

    Note that an XXRReeccttaannggllee represents the effective drawing dimensions of the
    character, regardless of the number of font glyphs that are used to draw
    the character, or the direction in which the character is drawn. If
    multiple characters map to a single character glyph, the dimensions of all
    the XXRReeccttaannggllees of those characters are the same.

    When the XXFFoonnttSSeett has missing charsets, metrics for each unavailable
    character are taken from the default string returned by
    _XX_CC_rr_ee_aa_tt_ee_FF_oo_nn_tt_SS_ee_tt(3X11R5), so that the metrics represent the text as it will
    actually be drawn. The behavior for an invalid codepoint is undefined.

    If the array_size is too small for the number of characters in the
    supplied text, the functions return zero and num_chars_return is set to
    the number of rectangles required. Otherwise, the routines return a non-
    zero value.

    If the overall_ink_return or overall_logical_return argument is non-NULL,
    _XX_mm_bb_TT_ee_xx_tt_PP_ee_rr_CC_hh_aa_rr_EE_xx_tt_ee_nn_tt_ss(3X11R5) and _XX_ww_cc_TT_ee_xx_tt_PP_ee_rr_CC_hh_aa_rr_EE_xx_tt_ee_nn_tt_ss(3X11R5) return the
    maximum extent of the string's metrics to overall_ink_return or
    overall_logical_return, as returned by _XX_mm_bb_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) or
    _XX_ww_cc_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5).

  SSEEEE AALLSSOO

    _X_m_b_T_e_x_t_E_s_c_a_p_e_m_e_n_t()

    _X_m_b_T_e_x_t_E_x_t_e_n_t_s()

    Xlib

