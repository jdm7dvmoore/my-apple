XSetAccessControl(3X11R5)                     XSetAccessControl(3X11R5)

  XXAAddddHHoosstt(())

  NNAAMMEE

    XAddHost(), XAddHosts(), XListHosts(), XRemoveHost(), XRemoveHosts(),
    XSetAccessControl(), XEnableAccessControl(), XDisableAccessControl(),
    XHostAddress() - control host access and host control structure

  SSYYNNOOPPSSIISS

    XAddHost (Display *display, XHostAddress *host)
    XAddHosts (Display *display, XHostAddress *hosts, int num_hosts)
    XHostAddress *XListHosts (Display *display, int *nhosts_return,
                              Bool *state_return)
    XRemoveHost (Display *display, XHostAddress *host)
    XRemoveHosts (Display *display, XHostAddress *hosts, int num_hosts)
    XSetAccessControl (Display *display, int mode)
    XEnableAccessControl (Display *display)
    XDisableAccessControl (Display *display)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    host
        Specifies the host that is to be added or removed.

    hosts
        Specifies each host that is to be added or removed.

    mode
        Specifies the mode. You can pass EnableAccess or DisableAccess,
        defined in <<XX1111//XX..hh>>:
        #define EnableAccess  1
        #define DisableAccess 0

    nhosts_return
        Returns the number of hosts currently in the access control list.

    num_hosts
        Specifies the number of hosts.

    state_return
        Returns the state of the access control.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_AA_dd_dd_HH_oo_ss_tt(3X11R5) function adds the specified host to the access
    control list for that display. The server must be on the same host as the
    client issuing the command, or a BadAccess error results.

    The _XX_AA_dd_dd_HH_oo_ss_tt_ss(3X11R5) function adds each specified host to the access
    control list for that display. The server must be on the same host as the
    client issuing the command, or a BadAccess error results.

    The _XX_LL_ii_ss_tt_HH_oo_ss_tt_ss(3X11R5) function returns the current access control list as
    well as whether the use of the list at connection setup was enabled or
    disabled. _XX_LL_ii_ss_tt_HH_oo_ss_tt_ss(3X11R5) allows a program to find out what computers
    can make connections. It also returns a pointer to a list of host
    structures that were allocated by the function. When no longer needed,
    this memory should be freed by calling _XX_FF_rr_ee_ee(3X11R5).

    The _XX_RR_ee_mm_oo_vv_ee_HH_oo_ss_tt(3X11R5) function removes the specified host from the
    access control list for that display. The server must be on the same host
    as the client process, or a BadAccess error results. If you remove your
    computer from the access list, you can no longer connect to that server,
    and this operation cannot be reversed unless you reset the server.

    The _XX_RR_ee_mm_oo_vv_ee_HH_oo_ss_tt_ss(3X11R5) function removes each specified host from the
    access control list for that display. The X server must be on the same
    host as the client process, or a BadAccess error results. If you remove
    your computer from the access list, you can no longer connect to that
    server, and this operation cannot be reversed unless you reset the server.

    _XX_RR_ee_mm_oo_vv_ee_HH_oo_ss_tt_ss(3X11R5) can generate BadAccess and BadValue errors.

    The _XX_SS_ee_tt_AA_cc_cc_ee_ss_ss_CC_oo_nn_tt_rr_oo_ll(3X11R5) function either enables or disables the use
    of the access control list at each connection setup.

    The _XX_EE_nn_aa_bb_ll_ee_AA_cc_cc_ee_ss_ss_CC_oo_nn_tt_rr_oo_ll(3X11R5) function enables the use of the access
    control list at each connection setup.

    _XX_EE_nn_aa_bb_ll_ee_AA_cc_cc_ee_ss_ss_CC_oo_nn_tt_rr_oo_ll(3X11R5) can generate a BadAccess error.

    The _XX_DD_ii_ss_aa_bb_ll_ee_AA_cc_cc_ee_ss_ss_CC_oo_nn_tt_rr_oo_ll(3X11R5) function disables the use of the access
    control list at each connection setup.

    _XX_DD_ii_ss_aa_bb_ll_ee_AA_cc_cc_ee_ss_ss_CC_oo_nn_tt_rr_oo_ll(3X11R5) can generate a BadAccess error.

  SSTTRRUUCCTTUURREESS

    The XXHHoossttAAddddrreessss structure contains:

    The family member specifies which protocol address family to use (for
    example, TCP/IP or DECnet) and can be FamilyInternet, FamilyDECnet, or
    FamilyChaos. The length member specifies the length of the address in
    bytes. The address member specifies a pointer to the address.

  DDIIAAGGNNOOSSTTIICCSS

    _XX_AA_dd_dd_HH_oo_ss_tt(3X11R5) can generate BadAccess and BadValue errors.

    _XX_AA_dd_dd_HH_oo_ss_tt_ss(3X11R5) can generate BadAccess and BadValue errors.

    _XX_RR_ee_mm_oo_vv_ee_HH_oo_ss_tt(3X11R5) can generate BadAccess and BadValue errors.

    _XX_SS_ee_tt_AA_cc_cc_ee_ss_ss_CC_oo_nn_tt_rr_oo_ll(3X11R5) can generate BadAccess and BadValue errors.

    BadAccess
        A client attempted to modify the access control list from other than
        the local (or otherwise authorized) host.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_F_r_e_e()

    Xlib

