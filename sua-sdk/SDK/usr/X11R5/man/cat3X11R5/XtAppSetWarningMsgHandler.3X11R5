XtAppSetWarningMsgHandler(3X11R5)        XtAppSetWarningMsgHandler(3X11R5)

  XXttAAppppEErrrroorrMMssgg(())

  NNAAMMEE

    XtAppErrorMsg(), XtAppSetErrorMsgHandler(), XtAppSetWarningMsgHandler(),
    XtAppWarningMsg() - high-level error handlers

  SSYYNNOOPPSSIISS

    void XtAppErrorMsg (XtAppContext app_context, String name,
                        String type, String class, String default,
                        String *params, Cardinal *num_params)
    void XtAppSetErrorMsgHandler (XtAppContext app_context,
                                  XtErrorMsgHandler msg_handler)
    void XtAppSetWarningMsgHandler (XtAppContext app_context,
                                    XtErrorMsgHandler msg_handler)
    void XtAppWarningMsg (XtAppContext app_context, String name,
                          String type, String class,
                          String default, String *params,
                          Cardinal *num_params)

  AARRGGUUMMEENNTTSS

    app_context
        Specifies the application context.

    class
        Specifies the resource class.

    default
        Specifies the default message to use.

    name
        Specifies the general kind of error.

    type
        Specifies the detailed name of the error.

    msg_handler
        Specifies the new fatal error procedure, which should not return or
        the nonfatal error procedure, which usually returns.

    num_params
        Specifies the number of values in the parameter list.

    params
        Specifies a pointer to a list of values to be stored in the message.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_AA_pp_pp_EE_rr_rr_oo_rr_MM_ss_gg(3X11R5) function calls the high-level error handler and
    passes the specified information.

    The _XX_tt_AA_pp_pp_SS_ee_tt_EE_rr_rr_oo_rr_MM_ss_gg_HH_aa_nn_dd_ll_ee_rr(3X11R5) function registers the specified
    procedure, which is called when a fatal error occurs.

    The _XX_tt_AA_pp_pp_SS_ee_tt_WW_aa_rr_nn_ii_nn_gg_MM_ss_gg_HH_aa_nn_dd_ll_ee_rr(3X11R5) function registers the specified
    procedure, which is called when a nonfatal error condition occurs.

    The _XX_tt_AA_pp_pp_WW_aa_rr_nn_ii_nn_gg_MM_ss_gg(3X11R5) function calls the high-level error handler
    and passes the specified information.

  SSEEEE AALLSSOO

    _X_t_A_p_p_G_e_t_E_r_r_o_r_D_a_t_a_b_a_s_e()

    _X_t_A_p_p_E_r_r_o_r()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

