XSetWindowBackgroundPixmap(3X11R5)       XSetWindowBackgroundPixmap(3X11R5)

  XXCChhaannggeeWWiinnddoowwAAttttrriibbuutteess(())

  NNAAMMEE

    XChangeWindowAttributes(), XSetWindowBackground(),
    XSetWindowBackgroundPixmap(), XSetWindowBorder(),
    XSetWindowBorderPixmap(), XSetWindowColormap() - change window attributes

  SSYYNNOOPPSSIISS

    XChangeWindowAttributes (Display *display, Window w,
                             unsigned long valuemask,
                             XSetWindowAttributes *attributes)
    XSetWindowBackground (Display *display, Window w,
                          unsigned long background_pixel)
    XSetWindowBackgroundPixmap (Display *display, Window w,
                                Pixmap background_pixmap)
    XSetWindowBorder (Display *display, Window w,
                      unsigned long border_pixel)
    XSetWindowBorderPixmap (Display *display, Window w,
                            Pixmap border_pixmap)
    XSetWindowColormap (Display *display, Window w,
                        Colormap colormap)

  AARRGGUUMMEENNTTSS

    attributes
        Specifies the structure from which the values (as specified by the
        value mask) are to be taken. The value mask should have the
        appropriate bits set to indicate which attributes have been set in the
        structure.

    background_pixel
        Specifies the pixel that is to be used for the background.

    background_pixmap
        Specifies the background pixmap, ParentRelative, or None.

    border_pixel
        Specifies the entry in the colormap.

    border_pixmap
        Specifies the border pixmap or CopyFromParent.

    display
        Specifies the connection to the X server.

    valuemask
        Specifies which window attributes are defined in the attributes
        argument. This mask is the bitwise inclusive OR of the valid attribute
        mask bits. If valuemask is zero, the attributes are ignored and are
        not referenced.

    w
        Specifies the window.

    colormap
        Specifies the colormap.

  DDEESSCCRRIIPPTTIIOONN

    Depending on the valuemask, the _XX_CC_hh_aa_nn_gg_ee_WW_ii_nn_dd_oo_ww_AA_tt_tt_rr_ii_bb_uu_tt_ee_ss(3X11R5) function
    uses the window attributes in the XXSSeettWWiinnddoowwAAttttrriibbuutteess structure to change
    the specified window attributes. Changing the background does not cause
    the window contents to be changed. To repaint the window and its
    background, use _XX_CC_ll_ee_aa_rr_WW_ii_nn_dd_oo_ww(3X11R5). Setting the border or changing the
    background such that the border tile origin changes causes the border to
    be repainted. Changing the background of a root window to None or
    ParentRelative restores the default background pixmap. Changing the border
    of a root window to CopyFromParent restores the default border pixmap.
    Changing the win-gravity does not affect the current position of the
    window. Changing the backing-store of an obscured window to WhenMapped or
    Always, or changing the backing-planes, backing-pixel, or save-under of a
    mapped window might have no immediate effect. Changing the colormap of a
    window (that is, defining a new map, not changing the contents of the
    existing map) generates a ColormapNotify event. Changing the colormap of a
    visible window might have no immediate effect on the screen because the
    map might not be installed (see _XX_II_nn_ss_tt_aa_ll_ll_CC_oo_ll_oo_rr_mm_aa_pp(3X11R5)). Changing the
    cursor of a root window to None restores the default cursor. Whenever
    possible, you are encouraged to share colormaps.

    Multiple clients can select input on the same window. Their event masks
    are maintained separately. When an event is generated, it is reported to
    all interested clients. However, only one client at a time can select for
    SubstructureRedirectMask, ResizeRedirectMask, and ButtonPressMask. If a
    client attempts to select any of these event masks and some other client
    has already selected one, a BadAccess error results. There is only one do-
    not-propagate-mask for a window, not one per client.

    _XX_CC_hh_aa_nn_gg_ee_WW_ii_nn_dd_oo_ww_AA_tt_tt_rr_ii_bb_uu_tt_ee_ss(3X11R5) can generate BadAccess, BadColor,
    BadCursor, BadMatch, BadPixmap, BadValue, and BadWindow errors.

    The _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_aa_cc_kk_gg_rr_oo_uu_nn_dd(3X11R5) function sets the background of the
    window to the specified pixel value. Changing the background does not
    cause the window contents to be changed. _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_aa_cc_kk_gg_rr_oo_uu_nn_dd(3X11R5) uses
    a pixmap of undefined size filled with the pixel value you passed. If you
    try to change the background of an InputOnly window, a BadMatch error
    results.

    _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_aa_cc_kk_gg_rr_oo_uu_nn_dd(3X11R5) can generate BadMatch and BadWindow errors.

    The _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_aa_cc_kk_gg_rr_oo_uu_nn_dd_PP_ii_xx_mm_aa_pp(3X11R5) function sets the background pixmap
    of the window to the specified pixmap. The background pixmap can
    immediately be freed if no further explicit references to it are to be
    made. If ParentRelative is specified, the background pixmap of the
    window's parent is used, or on the root window, the default background is
    restored. If you try to change the background of an InputOnly window, a
    BadMatch error results. If the background is set to None, the window has
    no defined background.

    _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_aa_cc_kk_gg_rr_oo_uu_nn_dd_PP_ii_xx_mm_aa_pp(3X11R5) can generate BadMatch, BadPixmap, and
    BadWindow errors.

    The _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_oo_rr_dd_ee_rr(3X11R5) function sets the border of the window to the
    pixel value you specify. If you attempt to perform this on an InputOnly
    window, a BadMatch error results.

    _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_oo_rr_dd_ee_rr(3X11R5) can generate BadMatch and BadWindow errors.

    The _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_oo_rr_dd_ee_rr_PP_ii_xx_mm_aa_pp(3X11R5) function sets the border pixmap of the
    window to the pixmap you specify. The border pixmap can be freed
    immediately if no further explicit references to it are to be made. If you
    specify CopyFromParent, a copy of the parent window's border pixmap is
    used. If you attempt to perform this on an InputOnly window, a BadMatch
    error results.

    _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_oo_rr_dd_ee_rr_PP_ii_xx_mm_aa_pp(3X11R5) can generate BadMatch, BadPixmap, and
    BadWindow errors.

    The _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_CC_oo_ll_oo_rr_mm_aa_pp(3X11R5) function sets the specified colormap of the
    specified window. The colormap must have the same visual type as the
    window, or a BadMatch error results.

    _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_CC_oo_ll_oo_rr_mm_aa_pp(3X11R5) can generate BadColor, BadMatch, and BadWindow
    errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadAccess
        A client attempted to free a color map entry that it did not already
        allocate.

    BadAccess
        A client attempted to store into a read-only color map entry.

    BadColor
        A value for a Colormap argument does not name a defined Colormap.

    BadCursor
        A value for a Cursor argument does not name a defined Cursor.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadMatch
        An InputOnly window locks this attribute.

    BadPixmap
        A value for a Pixmap argument does not name a defined Pixmap.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_C_o_n_f_i_g_u_r_e_W_i_n_d_o_w()

    _X_C_r_e_a_t_e_W_i_n_d_o_w()

    _X_D_e_s_t_r_o_y_W_i_n_d_o_w()

    _X_I_n_s_t_a_l_l_C_o_l_o_r_m_a_p()

    _X_M_a_p_W_i_n_d_o_w()

    _X_R_a_i_s_e_W_i_n_d_o_w()

    _X_U_n_m_a_p_W_i_n_d_o_w()

    Xlib

