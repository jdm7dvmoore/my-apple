XSetTile(3X11R5)                                       XSetTile(3X11R5)

  XXSSeettTTiillee(())

  NNAAMMEE

    XSetTile(), XSetStipple(), XSetTSOrigin() - GC convenience routines

  SSYYNNOOPPSSIISS

    XSetTile (Display *display, GC gc, Pixmap tile)
    XSetStipple (Display *display, GC gc, Pixmap stipple)
    XSetTSOrigin (Display *display, GC gc, int ts_x_origin,
                  int ts_y_origin)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    gc
        Specifies the GC.

    stipple
        Specifies the stipple you want to set for the specified GC.

    tile
        Specifies the fill tile you want to set for the specified GC.

    ts_x_origin

    ts_y_origin
        Specify the x and y coordinates of the tile and stipple origin.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_tt_TT_ii_ll_ee(3X11R5) function sets the fill tile in the specified GC. The
    tile and GC must have the same depth, or a BadMatch error results.

    _XX_SS_ee_tt_TT_ii_ll_ee(3X11R5) can generate BadAlloc, BadGC, BadMatch, and BadPixmap
    errors.

    The _XX_SS_ee_tt_SS_tt_ii_pp_pp_ll_ee(3X11R5) function sets the stipple in the specified GC. The
    stipple must have a depth of one, or a BadMatch error results.

    _XX_SS_ee_tt_SS_tt_ii_pp_pp_ll_ee(3X11R5) can generate BadAlloc, BadGC, BadMatch, and BadPixmap
    errors.

    The _XX_SS_ee_tt_TT_SS_OO_rr_ii_gg_ii_nn(3X11R5) function sets the tile/stipple origin in the
    specified GC. When graphics requests call for tiling or stippling, the
    parent's origin will be interpreted relative to whatever destination
    drawable is specified in the graphics request.

    _XX_SS_ee_tt_TT_SS_OO_rr_ii_gg_ii_nn(3X11R5) can generate BadAlloc and BadGC errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadGC
        A value for a GContext argument does not name a defined GContext.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadPixmap
        A value for a Pixmap argument does not name a defined Pixmap.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_G_C()

    _X_Q_u_e_r_y_B_e_s_t_S_i_z_e()

    _X_S_e_t_A_r_c_M_o_d_e()

    _X_S_e_t_C_l_i_p_O_r_i_g_i_n()

    _X_S_e_t_F_i_l_l_S_t_y_l_e()

    _X_S_e_t_F_o_n_t()

    _X_S_e_t_L_i_n_e_A_t_t_r_i_b_u_t_e_s()

    _X_S_e_t_S_t_a_t_e()

    Xlib

