XtReleaseGC(3X11R5)                                 XtReleaseGC(3X11R5)

  XXttGGeettGGCC(())

  NNAAMMEE

    XtGetGC(), XtReleaseGC() - obtain and destroy a sharable GC

  SSYYNNOOPPSSIISS

    GC XtGetGC (Widget w, XtGCMask value_mask, XGCValues *values)
    void XtReleaseGC (Widget w, GC gc)

  AARRGGUUMMEENNTTSS

    gc
        Specifies the GC to be deallocated.

    values
        Specifies the actual values for this GC.

    value_mask
        Specifies which fields of the values are specified.

    w
        Specifies the widget.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_GG_ee_tt_GG_CC(3X11R5) function returns a sharable, read-only GC. The
    parameters to this function are the same as those for _XX_CC_rr_ee_aa_tt_ee_GG_CC(3X11R5)
    except that a widget is passed instead of a display. _XX_tt_GG_ee_tt_GG_CC(3X11R5)
    shares only GCs in which all values in the GC returned by
    _XX_CC_rr_ee_aa_tt_ee_GG_CC(3X11R5) are the same. In particular, it does not use the
    value_mask provided to determine which fields of the GC a widget considers
    relevant. The value_mask is used only to tell the server which fields
    should be filled in with widget data and which it should fill in with
    default values. For further information about value_mask and values, see
    _XX_CC_rr_ee_aa_tt_ee_GG_CC(3X11R5) in the Xlib - C Language X Interface.

    The _XX_tt_RR_ee_ll_ee_aa_ss_ee_GG_CC(3X11R5) function deallocate the specified shared GC.

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

