XtDatabase(3X11R5)                                   XtDatabase(3X11R5)

  XXttDDiissppllaayyIInniittiiaalliizzee(())

  NNAAMMEE

    XtDisplayInitialize(), XtOpenDisplay(), XtDatabase(), XtCloseDisplay() -
    initialize, open, or close a display

  SSYYNNOOPPSSIISS

    void XtToolkitInitialize (void)
    void XtDisplayInitialize (XtAppContext app_context, Display *display,
                              String application_name,
                              String application_class,
                              XrmOptionDescRec *options,
                              Cardinal num_options,
                              Cardinal *argc, String *argv)
    Display *XtOpenDisplay  (XtAppContext app_context, String display_string,
                             String application_name,
                             String application_class,
                             XrmOptionDescRec *options,
                             Cardinal num_options,
                             Cardinal *argc, String *argv)
    void XtCloseDisplay (Display *display)
    XrmDatabase XtDatabase (Display *display)

  AARRGGUUMMEENNTTSS

    argc
        Specifies a pointer to the number of command line parameters.

    argv
        Specifies the command line parameters.

    app_context
        Specifies the application context.

    application_class
        Specifies the class name of this application, which usually is the
        generic name for all instances of this application.

    application_name
        Specifies the name of the application instance.

    display
        Specifies the display. Note that a display can be in at most one
        application context.

    num_options
        Specifies the number of entries in the options list.

    options
        Specifies how to parse the command line for any application-specific
        resources. The options argument is passed as a parameter to
        _XX_rr_mm_PP_aa_rr_ss_ee_CC_oo_mm_mm_aa_nn_dd(3X11R5). For further information, see Xlib - C
        Language X Interface.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_DD_ii_ss_pp_ll_aa_yy_II_nn_ii_tt_ii_aa_ll_ii_zz_ee(3X11R5) function builds the resource database,
    calls the Xlib _XX_rr_mm_PP_aa_rr_ss_ee_CC_oo_mm_mm_aa_nn_dd(3X11R5) function to parse the command line,
    and performs other per display initialization. After
    _XX_rr_mm_PP_aa_rr_ss_ee_CC_oo_mm_mm_aa_nn_dd(3X11R5) has been called, argc and argv contain only those
    parameters that were not in the standard option table or in the table
    specified by the options argument. If the modified argc is not zero, most
    applications simply print out the modified argv along with a message
    listing the allowable options. On UNIX-based systems, the application name
    is usually the final component of argv[0]. If the synchronize resource is
    True for the specified application, _XX_tt_DD_ii_ss_pp_ll_aa_yy_II_nn_ii_tt_ii_aa_ll_ii_zz_ee(3X11R5) calls the
    Xlib _XX_SS_yy_nn_cc_hh_rr_oo_nn_ii_zz_ee(3X11R5) function to put Xlib into synchronous mode for
    this display connection. If the reverseVideo resource is True, the
    Intrinsics exchange XtDefaultForeground and XtDefaultBackground for
    widgets created on this display. (See Section 9.6.1).

    The _XX_tt_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) function calls _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) the
    specified display name. If display_string is NULL, _XX_tt_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5)
    uses the current value of the --ddiissppllaayy option specified in argv and if no
    display is specified in argv, uses the user's default display (on UNIX-
    based systems, this is the value of the DISPLAY environment variable).

    If this succeeds, it then calls _XX_tt_DD_ii_ss_pp_ll_aa_yy_II_nn_ii_tt_ii_aa_ll_ii_zz_ee(3X11R5) and pass it
    the opened display and the value of the ----nnaammee option specified in argv as
    the application name. If no name option is specified, it uses the
    application name passed to _XX_tt_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5). If the application name
    is NULL, it uses the last component of argv[0]. _XX_tt_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5)
    returns the newly opened display or NULL if it failed.

    _XX_tt_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) is provided as a convenience to the application
    programmer.

    The _XX_tt_CC_ll_oo_ss_ee_DD_ii_ss_pp_ll_aa_yy(3X11R5) function closes the specified display as soon
    as it is safe to do so. If called from within an event dispatch (for
    example, a callback procedure), _XX_tt_CC_ll_oo_ss_ee_DD_ii_ss_pp_ll_aa_yy(3X11R5) does not close the
    display until the dispatch is complete. Note that applications need only
    call _XX_tt_CC_ll_oo_ss_ee_DD_ii_ss_pp_ll_aa_yy(3X11R5) if they are to continue executing after
    closing the display; otherwise, they should call
    _XX_tt_DD_ee_ss_tt_rr_oo_yy_AA_pp_pp_ll_ii_cc_aa_tt_ii_oo_nn_CC_oo_nn_tt_ee_xx_tt(3X11R5) or just exit.

    The _XX_tt_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5) function returns the fully merged resource database
    that was built by _XX_tt_DD_ii_ss_pp_ll_aa_yy_II_nn_ii_tt_ii_aa_ll_ii_zz_ee(3X11R5) associated with the display
    that was passed in. If this display has not been initialized by
    _XX_tt_DD_ii_ss_pp_ll_aa_yy_II_nn_ii_tt_ii_aa_ll_ii_zz_ee(3X11R5), the results are not defined.

  SSEEEE AALLSSOO

    _X_t_A_p_p_C_r_e_a_t_e_S_h_e_l_l()

    _X_t_C_r_e_a_t_e_A_p_p_l_i_c_a_t_i_o_n_C_o_n_t_e_x_t()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

