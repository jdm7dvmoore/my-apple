XExtentsOfFontSet(3X11R5)                     XExtentsOfFontSet(3X11R5)

  XXEExxtteennttssOOffFFoonnttSSeett(())

  NNAAMMEE

    XExtentsOfFontSet() - obtain the maximum extents structure for a font set

  SSYYNNOOPPSSIISS

    XFontSetExtents *XExtentsOfFontSet (XFontSet font_set)

  AARRGGUUMMEENNTTSS

    font_set
        Specifies the font set.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_EE_xx_tt_ee_nn_tt_ss_OO_ff_FF_oo_nn_tt_SS_ee_tt(3X11R5) function returns an XXFFoonnttSSeettEExxtteennttss
    structure for the fonts used by the Xmb and Xwc layers, for the given font
    set.

    The XXFFoonnttSSeettEExxtteennttss structure is owned by Xlib and should not be modified
    or freed by the client. It will be freed by a call to _XX_FF_rr_ee_ee_FF_oo_nn_tt_SS_ee_tt(3X11R5)
    with the associated XXFFoonnttSSeett. Until freed, its contents will not be
    modified by Xlib.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_F_o_n_t_S_e_t()

    _X_F_o_n_t_s_O_f_F_o_n_t_S_e_t()

    _X_F_o_n_t_S_e_t_E_x_t_e_n_t_s()

    Xlib

