XtGetSubvalues(3X11R5)                           XtGetSubvalues(3X11R5)

  XXttSSeettVVaalluueess(())

  NNAAMMEE

    XtSetValues(), XtSetSubvalues(), XtGetValues(), XtGetSubvalues() - obtain
    and set widget resources

  SSYYNNOOPPSSIISS

    void XtSetValues (Widget w, ArgList args, Cardinal num_args)
    void XtSetSubvalues (XtPointer base, XtResourceList resources,
                         Cardinal num_resources, ArgList args,
                         Cardinal num_args)
    void XtGetValues (Widget w, ArgList args, Cardinal num_args)
    void XtGetSubvalues (XtPointer base, XtResourceList resources,
                         Cardinal num_resources, ArgList args,
                         Cardinal num_args)

  AARRGGUUMMEENNTTSS

    args
        Specifies the argument list of name/address pairs that contain the
        resource name and either the address into which the resource value is
        to be stored or their new values.

    base
        Specifies the base address of the subpart data structure where the
        resources should be retrieved or written.

    num_args
        Specifies the number of arguments in the argument list.

    resources
        Specifies the nonwidget resource list or values.

    num_resources
        Specifies the number of resources in the resource list.

    w
        Specifies the widget.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) function starts with the resources specified for
    the CCoorree widget fields and proceeds down the subclass chain to the widget.
    At each stage, it writes the new value (if specified by one of the
    arguments) or the existing value (if no new value is specified) to a new
    widget data record. _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) then calls the set_values
    procedures for the widget in superclass-to-subclass order. If the widget
    has any non-NULL set_values_hook fields, these are called immediately
    after the corresponding set_values procedure. This procedure permits
    subclasses to set nonwidget data for _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5).

    If the widget's parent is a subclass of constraintWidgetClass,
    _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) also updates the widget's constraints. It starts with
    the constraint resources specified for constraintWidgetClass and proceeds
    down the subclass chain to the parent's class. At each stage, it writes
    the new value or the existing value to a new constraint record. It then
    calls the constraint set_values procedures from constraintWidgetClass down
    to the parent's class. The constraint set_values procedures are called
    with widget arguments, as for all set_values procedures, not just the
    constraint record arguments, so that they can make adjustments to the
    desired values based on full information about the widget.

    _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) determines if a geometry request is needed by
    comparing the current widget to the new widget. If any geometry changes
    are required, it makes the request, and the geometry manager returns
    XtGeometryYes, XtGeometryAlmost, or XtGeometryNo. If XtGeometryYes,
    _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) calls the widget's resize procedure. If XtGeometryNo,
    _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) resets the geometry fields to their original values.
    If XtGeometryAlmost, _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) calls the set_values_almost
    procedure, which determines what should be done and writes new values for
    the geometry fields into the new widget. _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) then repeats
    this process, deciding once more whether the geometry manager should be
    called.

    Finally, if any of the set_values procedures returned True, XtSetValues
    causes the widget's expose procedure to be invoked by calling the Xlib
    _XX_CC_ll_ee_aa_rr_AA_rr_ee_aa(3X11R5) function on the widget's window.

    The _XX_tt_SS_ee_tt_SS_uu_bb_vv_aa_ll_uu_ee_ss(3X11R5) function stores resources into the structure
    identified by base.

    The _XX_tt_GG_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) function starts with the resources specified for
    the core widget fields and proceeds down the subclass chain to the widget.
    The value field of a passed argument list should contain the address into
    which to store the corresponding resource value. It is the caller's
    responsibility to allocate and deallocate this storage according to the
    size of the resource representation type used within the widget.

    If the widget's parent is a subclass of constraintWidgetClass,
    _XX_tt_GG_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) then fetches the values for any constraint resources
    requested. It starts with the constraint resources specified for
    constraintWidgetClass and proceeds down to the subclass chain to the
    parent's constraint resources. If the argument list contains a resource
    name that is not found in any of the resource lists searched, the value at
    the corresponding address is not modified. Finally, if the get_values_hook
    procedures are non-NULL, they are called in superclass-to-subclass order
    after all the resource values have been fetched by _XX_tt_GG_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5).
    This permits a subclass to provide nonwidget resource data to
    _XX_tt_GG_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5).

    The _XX_tt_GG_ee_tt_SS_uu_bb_vv_aa_ll_uu_ee_ss(3X11R5) function obtains resource values from the
    structure identified by base.

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

