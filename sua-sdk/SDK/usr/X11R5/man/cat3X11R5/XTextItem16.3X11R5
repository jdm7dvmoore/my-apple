XTextItem16(3X11R5)                                 XTextItem16(3X11R5)

  XXDDrraawwTTeexxtt(())

  NNAAMMEE

    XDrawText(), XDrawText16(), XTextItem(), XTextItem16() - draw polytext
    text and text drawing structures

  SSYYNNOOPPSSIISS

    XDrawText (Display *display, Drawable d, GC gc, int x,
               int y, XTextItem *items, int nitems)
    XDrawText16 (Display *display, Drawable d, GC gc, int x,
                 int y, XTextItem16 *items, int nitems)

  AARRGGUUMMEENNTTSS

    d
        Specifies the drawable.

    display
        Specifies the connection to the X server.

    gc
        Specifies the GC.

    items
        Specifies an array of text items.

    nitems
        Specifies the number of text items in the array.

    x

    y
        Specify the x and y coordinates, which are relative to the origin of
        the specified drawable and define the origin of the first character.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_DD_rr_aa_ww_TT_ee_xx_tt_11_66(3X11R5) function is similar to _XX_DD_rr_aa_ww_TT_ee_xx_tt(3X11R5) except
    that it uses 2-byte or 16-bit characters. Both functions allow complex
    spacing and font shifts between counted strings.

    Each text item is processed in turn. A font member other than None in an
    item causes the font to be stored in the GC and used for subsequent text.
    A text element delta specifies an additional change in the position along
    the x axis before the string is drawn. The delta is always added to the
    character origin and is not dependent on any characteristics of the font.
    Each character image, as defined by the font in the GC, is treated as an
    additional mask for a fill operation on the drawable. The drawable is
    modified only where the font character has a bit set to 1. If a text item
    generates a BadFont error, the previous text items may have been drawn.

    For fonts defined with linear indexing rather than 2-byte matrix indexing,
    each XXCChhaarr22bb structure is interpreted as a 16-bit number with byte1 as the
    most-significant byte.

    Both functions use these GC components: function, plane-mask, fill-style,
    font, subwindow-mode, clip-x-origin, clip-y-origin, and clip-mask. They
    also use these GC mode-dependent components: foreground, background, tile,
    stipple, tile-stipple-x-origin, and tile-stipple-y-origin.

    _XX_DD_rr_aa_ww_TT_ee_xx_tt(3X11R5) and _XX_DD_rr_aa_ww_TT_ee_xx_tt_11_66(3X11R5) can generate BadDrawable,
    BadFont, BadGC, and BadMatch errors.

  SSTTRRUUCCTTUURREESS

    The XXTTeexxttIItteemm and XXTTeexxttIItteemm1166 structures contain:

    typedef struct {
         char *chars;             pointer to string
         int nchars;              number of characters
         int delta;               delta between strings
         Font font;               Font to print it in, None don't change
    } XTextItem;

    typedef struct {
         XChar2b *chars;          pointer to two-byte characters
         int nchars;              number of characters
         int delta;               delta between strings
         Font font;               font to print it in, None don't change
    } XTextItem16;

    If the font member is not None, the font is changed before printing and
    also is stored in the GC. If an error was generated during text drawing,
    the previous items may have been drawn. The baseline of the characters are
    drawn starting at the x and y coordinates that you pass in the text
    drawing functions.

    For example, consider the background rectangle drawn by
    _XX_DD_rr_aa_ww_II_mm_aa_gg_ee_SS_tt_rr_ii_nn_gg(3X11R5). If you want the upper-left corner of the
    background rectangle to be at pixel coordinate (x,y), pass the (x,y +
    ascent) as the baseline origin coordinates to the text functions. The
    ascent is the font ascent, as given in the XXFFoonnttSSttrruucctt structure. If you
    want the lower-left corner of the background rectangle to be at pixel
    coordinate (x,y), pass the (x,y - descent + 1) as the baseline origin
    coordinates to the text functions. The descent is the font descent, as
    given in the XXFFoonnttSSttrruucctt structure.

  DDIIAAGGNNOOSSTTIICCSS

    BadDrawable
        A value for a Drawable argument does not name a defined Window or
        Pixmap.

    BadFont
        A value for a Font or GContext argument does not name a defined Font.

    BadGC
        A value for a GContext argument does not name a defined GContext.

    BadMatch
        An InputOnly window is used as a Drawable.

  SSEEEE AALLSSOO

    _X_D_r_a_w_I_m_a_g_e_S_t_r_i_n_g()

    _X_D_r_a_w_T_e_x_t()

    _X_L_o_a_d_F_o_n_t()

    Xlib

