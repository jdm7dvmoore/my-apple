XTextExtents16(3X11R5)                           XTextExtents16(3X11R5)

  XXTTeexxttEExxtteennttss(())

  NNAAMMEE

    XTextExtents(), XTextExtents16(), XQueryTextExtents(),
    XQueryTextExtents16() - compute or query text extents

  SSYYNNOOPPSSIISS

    XTextExtents (XFontStruct *font_struct, char *string, int nchars,
                  int *direction_return, int *font_ascent_return,
                  int *font_descent_return, XCharStruct *overall_return)
    XTextExtents16 (XFontStruct *font_struct, XChar2b *string,
                    int nchars, int *direction_return,
                    int *font_ascent_return, int *font_descent_return,
                    XCharStruct *overall_return)
    XQueryTextExtents (Display *display, XID font_ID, char *string,
                       int nchars, int *direction_return,
                       int *font_ascent_return, int *font_descent_return,
                       XCharStruct *overall_return)
    XQueryTextExtents16 (Display *display, XID font_ID,
                         XChar2b *string, int nchars,
                         int *direction_return, int *font_ascent_return,
                         int *font_descent_return,
                         XCharStruct *overall_return)

  AARRGGUUMMEENNTTSS

    direction_return
        Returns the value of the direction hint (FontLeftToRight or
        FontRightToLeft).

    display
        Specifies the connection to the X server.

    font_ID
        Specifies either the font ID or the GContext ID that contains the
        font.

    font_ascent_return
        Returns the font ascent.

    font_descent_return
        Returns the font descent.

    font_struct
        Specifies the XXFFoonnttSSttrruucctt structure.

    nchars
        Specifies the number of characters in the character string.

    string
        Specifies the character string.

    overall_return
        Returns the overall size in the specified XXCChhaarrSSttrruucctt structure.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) and _XX_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss_11_66(3X11R5) functions perform the
    size computation locally and, thereby, avoid the round-trip overhead of
    _XX_QQ_uu_ee_rr_yy_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) and _XX_QQ_uu_ee_rr_yy_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss_11_66(3X11R5). Both functions
    return an XXCChhaarrSSttrruucctt structure, whose members are set to the values as
    follows.

    The ascent member is set to the maximum of the ascent metrics of all
    characters in the string. The descent member is set to the maximum of the
    descent metrics. The width member is set to the sum of the character-width
    metrics of all characters in the string. For each character in the string,
    let W be the sum of the character-width metrics of all characters
    preceding it in the string. Let L be the left-side-bearing metric of the
    character plus W. Let R be the right-side-bearing metric of the character
    plus W. The lbearing member is set to the minimum L of all characters in
    the string. The rbearing member is set to the maximum R.

    For fonts defined with linear indexing rather than 2-byte matrix indexing,
    each XXCChhaarr22bb structure is interpreted as a 16-bit number with byte1 as the
    most-significant byte. If the font has no defined default character,
    undefined characters in the string are taken to have all zero metrics.

    The _XX_QQ_uu_ee_rr_yy_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) and _XX_QQ_uu_ee_rr_yy_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss_11_66(3X11R5) functions
    return the bounding box of the specified 8-bit and 16-bit character string
    in the specified font or the font contained in the specified GC. These
    functions query the X server and, therefore, suffer the round-trip
    overhead that is avoided by _XX_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) and
    _XX_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss_11_66(3X11R5). Both functions return a XXCChhaarrSSttrruucctt structure,
    whose members are set to the values as follows.

    The ascent member is set to the maximum of the ascent metrics of all
    characters in the string. The descent member is set to the maximum of the
    descent metrics. The width member is set to the sum of the character-width
    metrics of all characters in the string. For each character in the string,
    let W be the sum of the character-width metrics of all characters
    preceding it in the string. Let L be the left-side-bearing metric of the
    character plus W. Let R be the right-side-bearing metric of the character
    plus W. The lbearing member is set to the minimum L of all characters in
    the string. The rbearing member is set to the maximum R.

    For fonts defined with linear indexing rather than 2-byte matrix indexing,
    each XXCChhaarr22bb structure is interpreted as a 16-bit number with byte1 as the
    most-significant byte. If the font has no defined default character,
    undefined characters in the string are taken to have all zero metrics.

    Characters with all zero metrics are ignored. If the font has no defined
    default_char, the undefined characters in the string are also ignored.

    _XX_QQ_uu_ee_rr_yy_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) and _XX_QQ_uu_ee_rr_yy_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss_11_66(3X11R5) can generate
    BadFont and BadGC errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadFont
        A value for a Font or GContext argument does not name a defined Font.

    BadGC
        A value for a GContext argument does not name a defined GContext.

  SSEEEE AALLSSOO

    _X_L_o_a_d_F_o_n_t()

    _X_T_e_x_t_W_i_d_t_h()

    Xlib

