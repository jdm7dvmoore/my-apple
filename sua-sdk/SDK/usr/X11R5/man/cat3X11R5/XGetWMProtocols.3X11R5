XGetWMProtocols(3X11R5)                         XGetWMProtocols(3X11R5)

  XXSSeettWWMMPPrroottooccoollss(())

  NNAAMMEE

    XSetWMProtocols(), XGetWMProtocols() - set or read a window's WM_PROTOCOLS
    property

  SSYYNNOOPPSSIISS

    Status XSetWMProtocols (Display *display, Window w,
                            Atom *protocols, int count)
    Status XGetWMProtocols (Display *display, Window w,
                            Atom **protocols_return, int *count_return)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    count
        Specifies the number of protocols in the list.

    count_return
        Returns the number of protocols in the list.

    protocols
        Specifies the list of protocols.

    protocols_return
        Returns the list of protocols.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_tt_WW_MM_PP_rr_oo_tt_oo_cc_oo_ll_ss(3X11R5) function replaces the WM_PROTOCOLS property on
    the specified window with the list of atoms specified by the protocols
    argument. If the property does not already exist, _XX_SS_ee_tt_WW_MM_PP_rr_oo_tt_oo_cc_oo_ll_ss(3X11R5)
    sets the WM_PROTOCOLS property on the specified window to the list of
    atoms specified by the protocols argument. The property is stored with a
    type of AATTOOMM and a format of 32. If it cannot intern the WM_PROTOCOLS
    atom, _XX_SS_ee_tt_WW_MM_PP_rr_oo_tt_oo_cc_oo_ll_ss(3X11R5) returns a zero status. Otherwise, it returns
    a nonzero status.

    _XX_SS_ee_tt_WW_MM_PP_rr_oo_tt_oo_cc_oo_ll_ss(3X11R5) can generate BadAlloc and BadWindow errors.

    The _XX_GG_ee_tt_WW_MM_PP_rr_oo_tt_oo_cc_oo_ll_ss(3X11R5) function returns the list of atoms stored in
    the WM_PROTOCOLS property on the specified window. These atoms describe
    window manager protocols in which the owner of this window is willing to
    participate. If the property exists, is of type AATTOOMM, is of format 32, and
    the atom WM_PROTOCOLS can be interned, _XX_GG_ee_tt_WW_MM_PP_rr_oo_tt_oo_cc_oo_ll_ss(3X11R5) sets the
    protocols_return argument to a list of atoms, sets the count_return
    argument to the number of elements in the list, and returns a nonzero
    status. Otherwise, it sets neither of the return arguments and returns a
    zero status. To release the list of atoms, use _XX_FF_rr_ee_ee(3X11R5).

    _XX_GG_ee_tt_WW_MM_PP_rr_oo_tt_oo_cc_oo_ll_ss(3X11R5) can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_PROTOCOLS
        List of atoms that identify the communications protocols between the
        client and window manager in which the client is willing to
        participate.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t()

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e()

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s()

    _X_A_l_l_o_c_W_M_H_i_n_t_s()

    _X_F_r_e_e()

    _X_S_e_t_C_o_m_m_a_n_d()

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t()

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y()

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e()

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s()

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e()

    _X_S_e_t_W_M_N_a_m_e()

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s()

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y()

    Xlib

