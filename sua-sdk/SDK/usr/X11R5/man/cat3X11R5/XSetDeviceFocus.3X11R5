XSetDeviceFocus(3X11R5)                         XSetDeviceFocus(3X11R5)

  XXSSeettDDeevviicceeFFooccuuss(())

  NNAAMMEE

    XSetDeviceFocus(), XGetDeviceFocus() - control extension input device
    focus

  SSYYNNOOPPSSIISS

    XSetDeviceFocus (Display *display, Display *device,
                     Window focus, int revert_to,  Time time)
    XGetDeviceFocus (Display *display, Display *device,
                     Window *focus_return, int *revert_to_return,
                     int *time_return)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    device
        Specifies the device whose focus is to be queried or changed.

    focus
        Specifies the window, PointerRoot, FollowKeyboard, or None.

    focus_return
        Returns the focus window, PointerRoot, FollowKeyboard, or None.

    revert_to
        Specifies where the input focus reverts to if the window becomes not
        viewable. You can pass RevertToParent, RevertToPointerRoot,
        RevertToFollowKeyboard, or RevertToNone.

    revert_to_return
        Returns the current focus state RevertToParent, RevertToPointerRoot,
        RevertToFollowKeyboard, or RevertToNone.

    time_return
        Returns the last_focus_time for the device.

    time
        Specifies the time. You can pass either a timestamp or CurrentTime.

  DDEESSCCRRIIPPTTIIOONN

    The XSetDeviceFocus request changes the focus of the specified device and
    its last-focus-change time. It has no effect if the specified time is
    earlier than the current last-focus-change time or is later than the
    current X server time. Otherwise, the last-focus-change time is set to the
    specified time CurrentTime is replaced by the current X server time).
    _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_FF_oo_cc_uu_ss(3X11R5) causes the X server to generate DeviceFocusIn and
    DeviceFocusOut events.

    Depending on the focus argument, the following occurs:
    *     If focus is None , all device events are discarded until a new focus
          window is set, and the revert_to argument is ignored.
    *     If focus is a window, it becomes the device's focus window. If a
          generated device event would normally be reported to this window or
          one of its inferiors, the event is reported as usual. Otherwise, the
          event is reported relative to the focus window.
    *     If focus is PointerRoot, the focus window is dynamically taken to be
          the root window of whatever screen the pointer is on at each event
          from the specified device. In this case, the revert_to argument is
          ignored.
    *     If focus is FollowKeyboard, the focus window is dynamically taken to
          be the window to which the X keyboard focus is set at each input
          event.

    The specified focus window must be viewable at the time
    _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_FF_oo_cc_uu_ss(3X11R5) is called, or a BadMatch error results. If the
    focus window later becomes not viewable, the X server evaluates the
    revert_to argument to determine the new focus window as follows:
    *     If revert_to is RevertToParent, the focus reverts to the parent (or
          the closest viewable ancestor), and the new revert_to value is taken
          to be RevertToNone.
    *     If revert_to is RevertToPointerRoot, RevertToFollowKeyboard, or
          RevertToNone, the focus reverts to PointerRoot, FollowKeyboard, or
          None, respectively.

    When the focus reverts, the X server generates DeviceFocusIn and
    DeviceFocusOut events, but the last-focus-change time is not affected.

    Input extension devices are not required to support the ability to be
    focused. Attempting to set the focus of a device that does not support
    this request will result in a BadMatch error. Whether or not given device
    can support this request can be determined by the information returned by
    _XX_OO_pp_ee_nn_DD_ee_vv_ii_cc_ee(3X11R5). For those devices that support focus,
    _XX_OO_pp_ee_nn_DD_ee_vv_ii_cc_ee(3X11R5) will return an XXIInnppuuttCCllaassssIInnffoo structure with the
    input_class field equal to the constant FocusClass (defined in the file
    <<XX1111//XXII..hh>>).

    _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_FF_oo_cc_uu_ss(3X11R5) can generate BadDevice, BadMatch, BadValue, and
    BadWindow errors.

    The _XX_GG_ee_tt_DD_ee_vv_ii_cc_ee_FF_oo_cc_uu_ss(3X11R5) request returns the focus window and the
    current focus state.

    Not all input extension devices can be focused. Attempting to query the
    focus state of a device that can't be focused results in a BadMatch error.
    A device that can be focused returns information for input Class Focus
    when an _XX_OO_pp_ee_nn_DD_ee_vv_ii_cc_ee(3X11R5) request is made.

    _XX_GG_ee_tt_DD_ee_vv_ii_cc_ee_FF_oo_cc_uu_ss(3X11R5) can generate BadDevice, and BadMatch errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadDevice
        An invalid device was specified. The specified device does not exist
        or has not been opened by this client with XXOOppeennIInnppuuttDDeevviiccee(). This
        error might also occur if the specified device is the X keyboard or X
        pointer device.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

    BadMatch
        This error might occur if an _XX_GG_ee_tt_DD_ee_vv_ii_cc_ee_FF_oo_cc_uu_ss(3X11R5) or
        _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_FF_oo_cc_uu_ss(3X11R5) request was made specifying a device that the
        server implementation does not allow to be focused.

  SSEEEE AALLSSOO

    Programming With Xlib

