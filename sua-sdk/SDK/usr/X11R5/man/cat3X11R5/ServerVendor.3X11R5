ServerVendor(3X11R5)                               ServerVendor(3X11R5)

  AAllllPPllaanneess(())

  NNAAMMEE

    AllPlanes(), BlackPixel(), WhitePixel(), ConnectionNumber(),
    DefaultColormap(), DefaultDepth(), XListDepths(), DefaultGC(),
    DefaultRootWindow(), DefaultScreenOfDisplay(), DefaultScreen(),
    DefaultVisual(), DisplayCells(), DisplayPlanes(), DisplayString(),
    XMaxRequestSize(), LastKnownRequestProcessed(), NextRequest(),
    ProtocolVersion(), ProtocolRevision(), QLength(), RootWindow(),
    ScreenCount(), ScreenOfDisplay(), ServerVendor(), VendorRelease() -
    Display macros

  SSYYNNOOPPSSIISS

    unsigned long AllPlanes (void)
    BlackPixel (Display *display, int screen_number)
    unsigned long WhitePixel (Display *display, int screen_number)
    int ConnectionNumber (Display *display)
    Colormap DefaultColormap (Display *display, int screen_number)
    DefaultDepth (Display *display, int screen_number)
    int *XListDepths (Display *display, int screen_number, int *count_return)
    GC DefaultGC (Screen *screen)
    DefaultRootWindow (Display *display)
    Screen *DefaultScreenOfDisplay (Display *display)
    int DefaultScreen (Display *display)
    Visual *DefaultVisual (Display *display, int screen_number)
    int DisplayCells (Display *display, int screen_number)
    int DisplayPlanes (Display *display, int screen_number)
    char *DisplayString (Display *display)
    long XMaxRequestSize (Display *display)
    unsigned long LastKnownRequestProcessed (Display *display)
    unsigned long NextRequest (Display *display)
    int ProtocolVersion (Display *display)
    int ProtocolRevision  (Display *display)
    QLength (Display *display)
    Window RootWindow (Display *display, int screen_number)
    int ScreenCount (Display *display, int screen_number)
    Screen *ScreenOfDisplay (Display *display, int screen_number)
    char *ServerVendor (Display *display)
    int VendorRelease (Display *display)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    screen_number
        Specifies the appropriate screen number on the host server.

    count_return
        Returns the number of depths.

  DDEESSCCRRIIPPTTIIOONN

    The _AA_ll_ll_PP_ll_aa_nn_ee_ss(3X11R5) macro returns a value with all bits set to 1
    suitable for use in a plane argument to a procedure.

    The _BB_ll_aa_cc_kk_PP_ii_xx_ee_ll(3X11R5) macro returns the black pixel value for the
    specified screen.

    The _WW_hh_ii_tt_ee_PP_ii_xx_ee_ll(3X11R5) macro returns the white pixel value for the
    specified screen.

    The _CC_oo_nn_nn_ee_cc_tt_ii_oo_nn_NN_uu_mm_bb_ee_rr(3X11R5) macro returns a connection number for the
    specified display.

    The _DD_ee_ff_aa_uu_ll_tt_CC_oo_ll_oo_rr_mm_aa_pp(3X11R5) macro returns the default colormap ID for
    allocation on the specified screen.

    The _DD_ee_ff_aa_uu_ll_tt_DD_ee_pp_tt_hh(3X11R5) macro returns the depth (number of planes) of the
    default root window for the specified screen.

    The _XX_LL_ii_ss_tt_DD_ee_pp_tt_hh_ss(3X11R5) function returns the array of depths that are
    available on the specified screen. If the specified screen_number is valid
    and sufficient memory for the array can be allocated, _XX_LL_ii_ss_tt_DD_ee_pp_tt_hh_ss(3X11R5)
    sets count_return to the number of available depths. Otherwise, it does
    not set count_return and returns NULL. To release the memory allocated for
    the array of depths, use _XX_FF_rr_ee_ee(3X11R5).

    The _DD_ee_ff_aa_uu_ll_tt_GG_CC(3X11R5) macro returns the default GC for the root window of
    the specified screen.

    The _DD_ee_ff_aa_uu_ll_tt_RR_oo_oo_tt_WW_ii_nn_dd_oo_ww(3X11R5) macro returns the root window for the
    default screen.

    The _DD_ee_ff_aa_uu_ll_tt_SS_cc_rr_ee_ee_nn_OO_ff_DD_ii_ss_pp_ll_aa_yy(3X11R5) macro returns the default screen of the
    specified display.

    The _DD_ee_ff_aa_uu_ll_tt_SS_cc_rr_ee_ee_nn(3X11R5) macro returns the default screen number
    referenced in the _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) routine.

    The _DD_ee_ff_aa_uu_ll_tt_VV_ii_ss_uu_aa_ll(3X11R5) macro returns the default visual type for the
    specified screen.

    The _DD_ii_ss_pp_ll_aa_yy_CC_ee_ll_ll_ss(3X11R5) macro returns the number of entries in the
    default colormap.

    The _DD_ii_ss_pp_ll_aa_yy_PP_ll_aa_nn_ee_ss(3X11R5) macro returns the depth of the root window of
    the specified screen.

    The _DD_ii_ss_pp_ll_aa_yy_SS_tt_rr_ii_nn_gg(3X11R5) macro returns the string that was passed to
    _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5) when the current display was opened.

    _XX_MM_aa_xx_RR_ee_qq_uu_ee_ss_tt_SS_ii_zz_ee(3X11R5) returns the maximum request size (in 4-byte units)
    supported by the server. Single protocol requests to the server can be no
    longer than this size. The protocol guarantees the size to be no smaller
    than 4096 units (16384 bytes). Xlib automatically breaks data up into
    multiple protocol requests as necessary for the following functions:
    _XX_DD_rr_aa_ww_PP_oo_ii_nn_tt_ss(3X11R5), _XX_DD_rr_aa_ww_RR_ee_cc_tt_aa_nn_gg_ll_ee_ss(3X11R5), _XX_DD_rr_aa_ww_SS_ee_gg_mm_ee_nn_tt_ss(3X11R5),
    _XX_FF_ii_ll_ll_AA_rr_cc_ss(3X11R5), _XX_FF_ii_ll_ll_RR_ee_cc_tt_aa_nn_gg_ll_ee_ss(3X11R5), and _XX_PP_uu_tt_II_mm_aa_gg_ee(3X11R5).

    The _LL_aa_ss_tt_KK_nn_oo_ww_nn_RR_ee_qq_uu_ee_ss_tt_PP_rr_oo_cc_ee_ss_ss_ee_dd(3X11R5) macro extracts the full serial
    number of the last request known by Xlib to have been processed by the X
    server.

    The _NN_ee_xx_tt_RR_ee_qq_uu_ee_ss_tt(3X11R5) macro extracts the full serial number that is to
    be used for the next request.

    The _PP_rr_oo_tt_oo_cc_oo_ll_VV_ee_rr_ss_ii_oo_nn(3X11R5) macro returns the major version number (11) of
    the X protocol associated with the connected display.

    The _PP_rr_oo_tt_oo_cc_oo_ll_RR_ee_vv_ii_ss_ii_oo_nn(3X11R5) macro returns the minor protocol revision
    number of the X server.

    The _QQ_LL_ee_nn_gg_tt_hh(3X11R5) macro returns the length of the event queue for the
    connected display.

    The _RR_oo_oo_tt_WW_ii_nn_dd_oo_ww(3X11R5) macro returns the root window.

    The _SS_cc_rr_ee_ee_nn_CC_oo_uu_nn_tt(3X11R5) macro returns the number of available screens.

    The _SS_cc_rr_ee_ee_nn_OO_ff_DD_ii_ss_pp_ll_aa_yy(3X11R5) macro returns a pointer to the screen of the
    specified display.

    The _SS_ee_rr_vv_ee_rr_VV_ee_nn_dd_oo_rr(3X11R5) macro returns a pointer to a null-terminated
    string that provides some identification of the owner of the X server
    implementation.

    The _VV_ee_nn_dd_oo_rr_RR_ee_ll_ee_aa_ss_ee(3X11R5) macro returns a number related to a vendor's
    release of the X server.

  SSEEEE AALLSSOO

    _B_l_a_c_k_P_i_x_e_l_O_f_S_c_r_e_e_n()

    _I_m_a_g_e_B_y_t_e_O_r_d_e_r()

    _I_s_C_u_r_s_o_r_K_e_y()

    _X_O_p_e_n_D_i_s_p_l_a_y()

    Xlib

