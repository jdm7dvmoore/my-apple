XSelectExtensionEvent(3X11R5)             XSelectExtensionEvent(3X11R5)

  XXSSeelleeccttEExxtteennssiioonnEEvveenntt(())

  NNAAMMEE

    XSelectExtensionEvent(), XGetSelectedExtensionEvents() - select extension
    events, get the list of currently selected extension events

  SSYYNNOOPPSSIISS

    XSelectExtensionEvent (Display *display, Window w,
                           XEventClass *event_list, int event_count)
    XGetSelectedExtensionEvents (Display *display, Window w,
                                 int this_client_event_count_return,
                                 XEventClass *this_client_event_list_return,
                                 int all_clients_event_count_return,
                                 XEventClass *all_clients_event_list_return)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    w
        Specifies the window whose events you are interested in.

    event_list
        Specifies the list of event classes that describe the events you are
        interested in.

    event_count
        Specifies the count of event classes in the event list.

    this_client_event_count_return
        Returns the count of event classes selected by this client.

    this_client_event_list_return
        Returns a pointer to the list of event classes selected by this
        client.

    all_clients_event_count_return
        Returns the count of event classes selected by all clients.

    all_clients_event_list_return
        Returns a pointer to the list of event classes selected by all
        clients.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_ll_ee_cc_tt_EE_xx_tt_ee_nn_ss_ii_oo_nn_EE_vv_ee_nn_tt(3X11R5) request causes the X server to report
    the events associated with the specified list of event classes. Initially,
    X will not report any of these events. Events are reported relative to a
    window. If a window is not interested in a device event, it usually
    propagates to the closest ancestor that is interested, unless the
    do_not_propagate mask prohibits it.

    Multiple clients can select for the same events on the same window with
    the following restrictions:
    *     Multiple clients can select events on the same window because their
          event masks are disjoint. When the X server generates an event, it
          reports it to all interested clients.
    *     Only one client at a time can select a DeviceButtonPress event with
          automatic passive grabbing enabled, which is associated with the
          event class DeviceButtonPressGrab. To receive DeviceButtonPress
          events without automatic passive grabbing, use event class
          DeviceButtonPress but do not specify event class
          DeviceButtonPressGrab.

    The server reports the event to all interested clients.

    Information contained in the XXDDeevviiccee structure returned by
    _XX_OO_pp_ee_nn_DD_ee_vv_ii_cc_ee(3X11R5) is used by macros to obtain the event classes that
    clients use in making _XX_SS_ee_ll_ee_cc_tt_EE_xx_tt_ee_nn_ss_ii_oo_nn_EE_vv_ee_nn_tt(3X11R5) requests. Currently
    defined macros are DDeevviicceeKKeeyyPPrreessss(), DDeevviicceeKKeeyyRReelleeaassee(),
    DDeevviicceeBBuuttttoonnPPrreessss(), DDeevviicceeBBuuttttoonnRReelleeaassee(), DDeevviicceeMMoottiioonnNNoottiiffyy(),
    DDeevviicceeFFooccuussIInn(), DDeevviicceeFFooccuussOOuutt(), PPrrooxxiimmiittyyIInn(), PPrrooxxiimmiittyyOOuutt(),
    DDeevviicceeSSttaatteeNNoottiiffyy(), DDeevviicceeMMaappppiiiinnggNNoottiiffyy(), CChhaannggeeDDeevviicceeNNoottiiffyy(),
    DDeevviicceePPooiinntteerrMMoottiioonnHHiinntt(), DDeevviicceeBBuuttttoonn11MMoottiioonn(), DDeevviicceeBBuuttttoonn22MMoottiioonn(),
    DDeevviicceeBBuuttttoonn33MMoottiioonn(), DDeevviicceeBBuuttttoonn44MMoottiioonn(), DDeevviicceeBBuuttttoonn55MMoottiioonn(),
    DDeevviicceeBBuuttttoonnMMoottiioonn(), DDeevviicceeOOwwnneerrGGrraabbBBuuttttoonn(), DDeevviicceeBBuuttttoonnPPrreessssGGrraabb(),
    and NNooEExxtteennssiioonnEEvveenntt().

    To obtain the proper event class for a particular device, one of the above
    macros is invoked using the XXDDeevviiccee structure for that device. For
    example,

    DeviceKeyPress (*device, type, eventclass);

    returns the DeviceKeyPress event type and the eventclass for selecting
    DeviceKeyPress events from this device.

    _XX_SS_ee_ll_ee_cc_tt_EE_xx_tt_ee_nn_ss_ii_oo_nn_EE_vv_ee_nn_tt(3X11R5) can generate a BadWindow or BadClass error.
    The _XX_GG_ee_tt_SS_ee_ll_ee_cc_tt_ee_dd_EE_xx_tt_ee_nn_ss_ii_oo_nn_EE_vv_ee_nn_tt_ss(3X11R5) request reports the extension
    events selected by this client and all clients for the specified window.
    This request returns pointers to two XXEEvveennttCCllaassss arrays. One lists the
    input extension events selected by this client from the specified window.
    The other lists the event classes selected by all clients from the
    specified window. You should use _XX_FF_rr_ee_ee(3X11R5) to free these two arrays.

    _XX_GG_ee_tt_SS_ee_ll_ee_cc_tt_ee_dd_EE_xx_tt_ee_nn_ss_ii_oo_nn_EE_vv_ee_nn_tt_ss(3X11R5) can generate a BadWindow error.

  DDIIAAGGNNOOSSTTIICCSS

    BadWindow
        A value for a Window argument does not name a defined Window.

    BadClass
        A value for an XEventClass argument is invalid.

  SSEEEE AALLSSOO

    Programming With Xlib

