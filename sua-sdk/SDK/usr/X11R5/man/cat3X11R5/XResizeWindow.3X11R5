XResizeWindow(3X11R5)                             XResizeWindow(3X11R5)

  XXCCoonnffiigguurreeWWiinnddooww(())

  NNAAMMEE

    XConfigureWindow(), XMoveWindow(), XResizeWindow(), XMoveResizeWindow(),
    XSetWindowBorderWidth(), XWindowChanges() - configure windows and window
    changes structure

  SSYYNNOOPPSSIISS

    XConfigureWindow (Display *display, Window w,
                      unsigned int value_mask, XWindowChanges *values)
    XMoveWindow (Display *display, Window w, int x, int y)
    XResizeWindow (Display *display, Window w, unsigned int width,
                   unsigned int height)
    XMoveResizeWindow (Display *display, Window w, int x,
                       int y, unsigned int width, unsigned int height)
    XSetWindowBorderWidth (Display *display, Window w,
                           unsigned int bwidth)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    value_mask
        Specifies which values are to be set using information in the values
        structure. This mask is the bitwise inclusive OR of the valid
        configure window values bits.

    values
        Specifies the XXWWiinnddoowwCChhaannggeess structure.

    w
        Specifies the window to be reconfigured, moved, or resized..

    bwidth
        Specifies the width of the window border.

    width

    height
        Specify the width and height, which are the interior dimensions of the
        window.

    x

    y
        Specify the x and y coordinates, which define the new location of the
        top-left pixel of the window's border or the window itself if it has
        no border or define the new position of the window relative to its
        parent.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_CC_oo_nn_ff_ii_gg_uu_rr_ee_WW_ii_nn_dd_oo_ww(3X11R5) function uses the values specified in the
    XXWWiinnddoowwCChhaannggeess structure to reconfigure a window's size, position, border,
    and stacking order. Values not specified are taken from the existing
    geometry of the window.

    If a sibling is specified without a stack_mode or if the window is not
    actually a sibling, a BadMatch error results. Note that the computations
    for BottomIf, TopIf, and Opposite are performed with respect to the
    window's final geometry (as controlled by the other arguments passed to
    _XX_CC_oo_nn_ff_ii_gg_uu_rr_ee_WW_ii_nn_dd_oo_ww(3X11R5)), not its initial geometry. Any backing store
    contents of the window, its inferiors, and other newly visible windows are
    either discarded or changed to reflect the current screen contents
    (depending on the implementation).

    _XX_CC_oo_nn_ff_ii_gg_uu_rr_ee_WW_ii_nn_dd_oo_ww(3X11R5) can generate BadMatch , BadValue , and BadWindow
    errors.

    The _XX_MM_oo_vv_ee_WW_ii_nn_dd_oo_ww(3X11R5) function moves the specified window to the
    specified x and y coordinates, but it does not change the window's size,
    raise the window, or change the mapping state of the window. Moving a
    mapped window might or might not lose the window's contents depending on
    if the window is obscured by nonchildren and if no backing store exists.
    If the contents of the window are lost, the X server generates Expose
    events. Moving a mapped window generates Expose events on any formerly
    obscured windows.

    If the override-redirect flag of the window is False and some other client
    has selected SubstructureRedirectMask on the parent, the X server
    generates a ConfigureRequest event, and no further processing is
    performed. Otherwise, the window is moved.

    _XX_MM_oo_vv_ee_WW_ii_nn_dd_oo_ww(3X11R5) can generate a BadWindow error.

    The _XX_RR_ee_ss_ii_zz_ee_WW_ii_nn_dd_oo_ww(3X11R5) function changes the inside dimensions of the
    specified window, not including its borders. This function does not change
    the window's upper-left coordinate or the origin and does not restack the
    window. Changing the size of a mapped window might lose its contents and
    generate Expose events. If a mapped window is made smaller, changing its
    size generates Expose events on windows that the mapped window formerly
    obscured.

    If the override-redirect flag of the window is False and some other client
    has selected SubstructureRedirectMask on the parent, the X server
    generates a ConfigureRequest event, and no further processing is
    performed. If either width or height is zero, a BadValue error results.

    _XX_RR_ee_ss_ii_zz_ee_WW_ii_nn_dd_oo_ww(3X11R5) can generate BadValue and BadWindow errors.

    The _XX_MM_oo_vv_ee_RR_ee_ss_ii_zz_ee_WW_ii_nn_dd_oo_ww(3X11R5) function changes the size and location of
    the specified window without raising it. Moving and resizing a mapped
    window can generate an Expose event on the window. Depending on the new
    size and location parameters, moving and resizing a window may generate
    Expose events on windows that the window formerly obscured.

    If the override-redirect flag of the window is False and some other client
    has selected SubstructureRedirectMask on the parent, the X server
    generates a ConfigureRequest event, and no further processing is
    performed. Otherwise, the window size and location are changed.

    _XX_MM_oo_vv_ee_RR_ee_ss_ii_zz_ee_WW_ii_nn_dd_oo_ww(3X11R5) can generate BadValue and BadWindow errors.

    The _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_oo_rr_dd_ee_rr_WW_ii_dd_tt_hh(3X11R5) function sets the specified window's
    border width to the specified width.

    _XX_SS_ee_tt_WW_ii_nn_dd_oo_ww_BB_oo_rr_dd_ee_rr_WW_ii_dd_tt_hh(3X11R5) can generate a BadWindow error.

  SSTTRRUUCCTTUURREESS

    The XXWWiinnddoowwCChhaannggeess structure contains:

    /* Configure window value mask bits */

    Values

    typedef struct {
         int x, y;
         int width, height;
         int border_width;
         Window sibling;
         int stack_mode;
    } XWindowChanges;

    The x and y members are used to set the window's x and y coordinates,
    which are relative to the parent's origin and indicate the position of the
    upper-left outer corner of the window. The width and height members are
    used to set the inside size of the window, not including the border, and
    must be nonzero, or a BadValue error results. Attempts to configure a root
    window have no effect.

    The border_width member is used to set the width of the border in pixels.
    Note that setting just the border width leaves the outer-left corner of
    the window in a fixed position but moves the absolute position of the
    window's origin. If you attempt to set the border-width attribute of an
    InputOnly window nonzero, a BadMatch error results.

    The sibling member is used to set the sibling window for stacking
    operations. The stack_mode member is used to set how the window is to be
    restacked and can be set to Above, Below, TopIf, BottomIf, or Opposite.

  DDIIAAGGNNOOSSTTIICCSS

    BadMatch
        An InputOnly window is used as a Drawable.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_C_h_a_n_g_e_W_i_n_d_o_w_A_t_t_r_i_b_u_t_e_s()

    _X_C_r_e_a_t_e_W_i_n_d_o_w()

    _X_D_e_s_t_r_o_y_W_i_n_d_o_w()

    _X_M_a_p_W_i_n_d_o_w()

    _X_R_a_i_s_e_W_i_n_d_o_w()

    _X_U_n_m_a_p_W_i_n_d_o_w()

    Xlib

