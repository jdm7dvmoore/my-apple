XSetPlanemask(3X11R5)                             XSetPlanemask(3X11R5)

  XXSSeettSSttaattee(())

  NNAAMMEE

    XSetState(), XSetFunction(), XSetPlanemask(), XSetForeground(),
    XSetBackground() - GC convenience routines

  SSYYNNOOPPSSIISS

    XSetState (Display *display, GC gc,
               unsigned long foreground,
               unsigned long background,
               int function, unsigned long plane_mask)
    XSetFunction (Display *display, GC gc, int function)
    XSetPlaneMask (Display *display, GC gc,
                   unsigned long plane_mask)
    XSetForeground (Display *display, GC gc,
                    unsigned long foreground)
    XSetBackground (Display *display, GC gc,
                    unsigned long background)

  AARRGGUUMMEENNTTSS

    background
        Specifies the background you want to set for the specified GC.

    display
        Specifies the connection to the X server.

    foreground
        Specifies the foreground you want to set for the specified GC.

    function
        Specifies the function you want to set for the specified GC.

    gc
        Specifies the GC.

    plane_mask
        Specifies the plane mask.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_tt_SS_tt_aa_tt_ee(3X11R5) function sets the foreground, background, plane
    mask, and function components for the specified GC.

    _XX_SS_ee_tt_SS_tt_aa_tt_ee(3X11R5) can generate BadAlloc, BadGC, and BadValue errors.

    _XX_SS_ee_tt_FF_uu_nn_cc_tt_ii_oo_nn(3X11R5) sets a specified value in the specified GC.

    _XX_SS_ee_tt_FF_uu_nn_cc_tt_ii_oo_nn(3X11R5) can generate BadAlloc, BadGC, and BadValue errors.

    The _XX_SS_ee_tt_PP_ll_aa_nn_ee_MM_aa_ss_kk(3X11R5) function sets the plane mask in the specified
    GC.

    _XX_SS_ee_tt_PP_ll_aa_nn_ee_MM_aa_ss_kk(3X11R5) can generate BadAlloc and BadGC errors.

    The _XX_SS_ee_tt_FF_oo_rr_ee_gg_rr_oo_uu_nn_dd(3X11R5) function sets the foreground in the specified
    GC.

    _XX_SS_ee_tt_FF_oo_rr_ee_gg_rr_oo_uu_nn_dd(3X11R5) can generate BadAlloc and BadGC errors.

    The _XX_SS_ee_tt_BB_aa_cc_kk_gg_rr_oo_uu_nn_dd(3X11R5) function sets the background in the specified
    GC.

    _XX_SS_ee_tt_BB_aa_cc_kk_gg_rr_oo_uu_nn_dd(3X11R5) can generate BadAlloc and BadGC errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadGC
        A value for a GContext argument does not name a defined GContext.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_G_C()

    _X_Q_u_e_r_y_B_e_s_t_S_i_z_e()

    _X_S_e_t_A_r_c_M_o_d_e()

    _X_S_e_t_C_l_i_p_O_r_i_g_i_n()

    _X_S_e_t_F_i_l_l_S_t_y_l_e()

    _X_S_e_t_F_o_n_t()

    _X_S_e_t_L_i_n_e_A_t_t_r_i_b_u_t_e_s()

    _X_S_e_t_T_i_l_e()

    Xlib

