gdb(1)                                                           gdb(1)

  ggddbb

  NNAAMMEE

    gdb - The GNU debugger

  SSYYNNOOPPSSIISS

    gdb [[-help]] [[-nx]] [[-q]] [[-batch]] [[-cd=dir]]
        [[-f]] [[-b bps]] [[-tty=dev]] [[-s symfile]]
        [[-e prog]] [[-se prog]] [[-c core]] [[-d dir]]
        [[-x file]] [[-epoch]] [[-mapped]] [[-nw || -w]]
        [[-readnow]] [[-version]]
        [[ prog [[ core || procID]]]]

  DDEESSCCRRIIPPTTIIOONN

    You can use ggddbb(1) to view what is going on inside another program while
    it executes, or to view what another program was doing at the moment it
    crashed.

    The ggddbb utility can do the following four main things (plus other things
    in support of these) to help you catch bugs while a program is executing:

    *     Start your program, specifying anything that might affect its
          behavior.
    *     Make your program stop on specified conditions.
    *     Examine what has happened, when your program has stopped.
    *     Change things in your program so you can experiment with correcting
          the effects of one bug and also learn about another.

    The ggddbb utility can be used to debug programs built by using gcc
	in C, C++, and Modula-2. For cl.exe programs, you must use the
	Visual Studio debugger. Fortran support will be added when a GNU
	Fortran compiler is available.

    *     In Subsystem for UNIX-based Applications, ggddbb is most useful with C
	      and C++ programs compiled with ggcccc(1). 

    The ggddbb debugger is invoked with the shell command ggddbb(1). Once started,
    it reads commands from the terminal until you tell it to exit with the ggddbb
    command qquuiitt. You can get online help from ggddbb itself by using the command
    hheellpp.

    You can run ggddbb with no arguments or options, but the usual way to start
    ggddbb is with one argument or two, specifying an executable program as the
    argument:

    gdb program

    You can also start with both an executable program and a core file
    specified:

    gdb program core

    You can specify a process identifier (ID) as a second argument if you want
    to debug a running process. The following code attaches ggddbb to process
    11223344 (unless you also have a file named 11223344; ggddbb checks for a core file
    first):

    gdb program 1234

  CCOOMMMMAANNDDSS

    The following is a list of commonly used ggddbb commands:
    bbrreeaakk [file::]function
        Set a breakpoint at function (in file).
    rruunn [arglist]
        Start your program (with arglist, if specified).

    bbtt
        Backtrace: display the program stack.
    pprriinntt expr
        Display the value of an expression.

    cc
        Continue running your program (after stopping, for example, at a
        breakpoint).

    nneexxtt
        Execute next program line (after stopping); step over any function
        calls in the line.

    sstteepp
        Execute next program line (after stopping); step into any function
        calls in the line.
    hheellpp [name]
        Show information about ggddbb command name or general information about
        using ggddbb.

    qquuiitt
        Exit from ggddbb.

    For full details on ggddbb, see Using GDB: A Guide to the GNU Source-Level
    Debugger by Richard M. Stallman and Roland H. Pesch. The same text is
    available online as the PPDDFF file, ggddbb__iinnffoo..ppddff.

  OOPPTTIIOONNSS

    Any arguments other than options specify an executable file and core file
    (or process ID); that is, the first argument encountered with no
    associated option flag is equivalent to a --ssee option, and the second, if
    any, is equivalent to a --cc option if it is the name of a file. Many
    options have both long and short forms; both are shown here. The long
    forms are also recognized if you truncate them, so long as enough of the
    option is present to be unambiguous. (If you prefer, you can flag option
    arguments with ++ rather than --, though we illustrate the more usual
    convention.)

    All of the options and command-line arguments you give are processed in
    sequential order. The order makes a difference when the --xx option is used.
    --ccdd==directory
        Run ggddbb using directory, instead of the current directory, as its
        working directory.
    --ffuullllnnaammee, --ff
        Emacs sets this option when it runs ggddbb as a subprocess. It tells ggddbb
        to output the full file name and line number in a standard,
        recognizable form each time a stack frame is displayed (which includes
        each time the program stops). This recognizable format looks like two
        \032 characters, followed by the file name, line number, and character
        position separated by colons, and a newline. The Emacs-to-ggddbb
        interface program uses the two \\003322 characters as a signal to display
        the source code for the frame.
    --bb bps
        Set the line speed (baud rate or bits per second) of any serial
        interface used by ggddbb for remote debugging.
    --ttttyy==device
        Run using device for your program's standard input and output.
    --mmaappppeedd, --mm
        Use mapped symbol files. This feature is not available on Interix.

    --eeppoocchh
        Output information in the format used by the Epoch emacs-ggddbb
        interface.
    --rreeaaddnnooww, --rr
        Fully read symbol files on first access.
    --vveerrssiioonn, --vv
        Display version information, and then exit.

    --nnww
        Run ggddbb as a command-line utility (default).
    --wwiinnddoowwss, --ww
        Run ggddbb as an X Windows application.

  NNOOTTEESS

    Under some circumstances, when a function is disassembled, the disassembly
    may continue until the beginning of the next function. If the next
    function is aligned to some boundary, a few instructions of "noise" may
    appear at the end of the dissassembly; these can be ignored.

    When disassembling or otherwise dealing with the range of instruction
    locations that includes the end of a function, ggddbb treats the end as the
    last byte before the beginning of the next function. On Intel processors,
    there may be a ..aalliiggnn that causes a few bytes of empty space (containing
    random data) to be included in the range that nominally includes the end.
    This can be safely ignored.

    When debugging an execution thread, if you attempt to single-step over the
    last statement of the thread-start routine, the debugger may warn about
    not being able to set a breakpoint at address 0, even though you have not
    set any breakpoints yourself. This is a warning only and does not
    interfere with normal debugging.

  SSEEEE AALLSSOO

    Appendix B in the Professional SDK User's Guide contains an introduction
    to ggddbb.

    Using GDB: A Guide to the GNU Source-Level Debugger, Richard M. Stallman
    and Roland H. Pesch, July 1991.

  CCOOPPYYIINNGG

    Copyright (c) 1991 Free Software Foundation, Inc.

    Permission is granted to make and distribute verbatim copies of this
    manual provided the copyright notice and this permission notice are
    preserved on all copies.

    Permission is granted to copy and distribute modified versions of this
    manual under the conditions for verbatim copying, provided that the entire
    resulting derived work is distributed under the terms of a permission
    notice identical to this one.

    Permission is granted to copy and distribute translations of this manual
    into another language, under the above conditions for modified versions,
    except that this permission notice may be included in translations
    approved by the Free Software Foundation instead of in the original
    English.

