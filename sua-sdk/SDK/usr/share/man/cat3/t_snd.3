t_snd(3)                                                       t_snd(3)

  tt__ssnndd(())

  NNAAMMEE

    t_snd - send data or expedited data over a connection

  SSYYNNOOPPSSIISS

    #include <xti.h>

    int t_snd(
        int fd,
        void *buf,
        unsigned int nbytes,
        int flags)

  DDEESSCCRRIIPPTTIIOONN

    The tt__ssnndd() function is used to send either normal or expedited data.

    PPaarraammeetteerrss     BBeeffoorree ccaallll     AAfftteerr ccaallll

    fd             x               /

    buf            x (x)           =

    nbytes         x               /

    flags          x               /

    This function is used to send either normal or expedited data. The
    argument fd identifies the local transport endpoint over which data should
    be sent, buf points to the user data, nbytes specifies the number of bytes
    of user data to be sent, and flags specifies any optional flags described
    below:

    T_EXPEDITED
        If set in flags, the data will be sent as expedited data and will be
        subject to the interpretations of the transport provider.

    T_MORE
        If set in flags, this indicates to the transport provider that the
        transport service data unit (TSDU) (or expedited transport service
        data unit - ETSDU) is being sent through multiple tt__ssnndd(3) calls. Each
        tt__ssnndd(3) with the T_MORE flag set indicates that another tt__ssnndd(3) will
        follow with more data for the current TSDU (or ETSDU). The end of the
        TSDU (or ETSDU) is identified by a tt__ssnndd(3) call with the T_MORE flag
        not set. Use of T_MORE enables a user to break up large logical data
        units without losing the boundaries of those units at the other end of
        the connection. The flag implies nothing about how the data is
        packaged for transfer below the transport interface. If the transport
        provider does not support the concept of a TSDU as indicated in the
        info argument on return from _tt____oo_pp_ee_nn(3) or _tt____gg_ee_tt_ii_nn_ff_oo(3), the T_MORE
        flag is not meaningful and will be ignored if set. The sending of a
        zero-length fragment of a TSDU or ETSDU is only permitted where this
        is used to indicate the end of a TSDU or ETSDU; that is, when the
        T_MORE flag is not set. Some transport providers also forbid zero-
        length TSDUs and ETSDUs. See for a fuller explanation.

    T_PUSH
        If set in flags, requests that the provider transmit all data that it
        has accumulated but not sent. The request is a local action on the
        provider and does not affect any similarly named protocol flag (for
        example, the TCP PUSH flag). This effect of setting this flag is
        protocol-dependent, and it may be ignored entirely by transport
        providers which do not support the use of this feature. Note: The
        communications provider is free to collect data in a send buffer until
        it accumulates a sufficient amount for transmission.

    By default, tt__ssnndd(3) operates in synchronous mode and may wait if flow
    control restrictions prevent the data from being accepted by the local
    transport provider at the time the call is made. However, if O_NONBLOCK is
    set (through _tt____oo_pp_ee_nn(3) or _ff_cc_nn_tt_ll(2)), tt__ssnndd(3) will execute in asynchronous
    mode, and will fail immediately if there are flow control restrictions.
    The process can arrange to be informed when the flow control restrictions
    are cleared through either _tt____ll_oo_oo_kk(3) or the EM interface.

    On successful completion, tt__ssnndd(3) returns the number of bytes (octets)
    accepted by the communications provider. Normally this will equal the
    number of octets specified in nbytes. However, if O_NONBLOCK is set or the
    function is interrupted by a signal, it is possible that only part of the
    data has actually been accepted by the communications provider. In this
    case, tt__ssnndd(3) returns a value that is less than the value of nbytes. If
    tt__ssnndd(3) is interrupted by a signal before it could transfer data to the
    communications provider, it returns -1 with t_errno set to [TSYSERR] and
    errno set to [EINTR].

    If nbytes is zero and sending of zero bytes is not supported by the
    underlying communications service, tt__ssnndd(3) returns -1 with t_errno set to
    [TBADDATA].

    The size of each TSDU or ETSDU must not exceed the limits of the transport
    provider as specified by the current values in the TSDU or ETSDU fields in
    the info argument returned by _tt____gg_ee_tt_ii_nn_ff_oo(3).

    The error [TLOOK] is returned for asynchronous events. It is required only
    for an incoming disconnect event but may be returned for other events.

  VVAALLIIDD SSTTAATTEESS

    T_DATAXFER, T_INREL

  EERRRROORRSS

    On failure, t_errno is set to one of the following:

    [TBADDATA]
        Illegal amount of data:
        *     A single send was attempted specifying a TSDU (ETSDU) or
              fragment TSDU (ETSDU) greater than that specified by the current
              values of the TSDU or ETSDU fields in the info argument.
        *     A send of a zero byte TSDU (ETSDU) or zero byte fragment of a
              TSDU (ETSDU) is not supported by the provider.
        *     Multiple sends were attempted resulting in a TSDU (ETSDU) larger
              than that specified by the current value of the TSDU or ETSDU
              fields in the info argument - the ability of an XTI
              implementation to detect such an error case is implementation-
              dependent (see CAVEATS, below).

    [TBADF]
        The specified file descriptor does not refer to a transport endpoint.

    [TBADFLAG]
        An invalid flag was specified.

    [TFLOW]
        O_NONBLOCK was set, but the flow control mechanism prevented the
        transport provider from accepting any data at this time.

    [TLOOK]
        An asynchronous event has occurred on this transport endpoint.

    [TNOTSUPPORT]
        This function is not supported by the underlying transport provider.

    [TOUTSTATE]
        The communications endpoint referenced by fd is not in one of the
        states in which a call to this function is valid.

    [TPROTO]
        This error indicates that a communication problem has been detected
        between XTI and the transport provider for which there is no other
        suitable XTI error (t_errno).

    [TSYSERR]
        A system error has occurred during execution of this function.

  RREETTUURRNN VVAALLUUEE

    On successful completion, tt__ssnndd() returns the number of bytes accepted by
    the transport provider. Otherwise, -1 is returned on failure and t_errno
    is set to indicate the error.

    If the number of bytes accepted by the communications provider is less
    than the number of bytes requested, this may either indicate that
    O_NONBLOCK is set and the communications provider is blocked due to flow
    control, or that O_NONBLOCK is clear and the function was interrupted by a
    signal.

  SSEEEE AALLSSOO

    _tt____gg_ee_tt_ii_nn_ff_oo(3)

    _tt____oo_pp_ee_nn(3)

    _tt____rr_cc_vv(3)

  CCAAVVEEAATTSS

    It is important to remember that the transport provider treats all users
    of a transport endpoint as a single user. Therefore if several processes
    issue concurrent tt__ssnndd() calls then the different data may be intermixed.

    Multiple sends which exceed the maximum TSDU or ETSDU size may not be
    discovered by XTI. In this case an implementation-dependent error will
    result (generated by the transport provider) perhaps on a subsequent XTI
    call. This error may take the form of a connection abort, a [TSYSERR], a
    [TBADDATA] or a [TPROTO] error.

    If multiple sends which exceed the maximum TSDU or ETSDU size are detected
    by XTI, tt__ssnndd() fails with [TBADDATA].

  UUSSAAGGEE NNOOTTEESS

    The tt__ssnndd function is not thread safe.

    The tt__ssnndd function is not async-signal safe.

