rcsintro(5)                                                 rcsintro(5)

  rrccssiinnttrroo

  NNAAMMEE

    rcsintro - introduction to Revision Control System (RCS) commands

  DDEESSCCRRIIPPTTIIOONN

    The Revision Control System (RCS) manages multiple revisions of files. RCS
    automates the storing, retrieval, logging, identification, and merging of
    revisions. RCS is useful for text that is revised frequently, for example
    programs, documentation, graphics, papers, and form letters. The basic
    user interface is extremely simple. The novice only needs to learn two
    commands: _cc_ii(1) and _cc_oo(1). The ccii(1) command (ci being an abbreviation for
    check in) deposits the contents of a file into an archival file called an
    RCS file. An RCS file contains all revisions of a particular file. The
    ccoo(1) command (co being an abbreviation for check out) retrieves revisions
    from an RCS file.

  FFuunnccttiioonnss ooff RRCCSS

    *     Store and retrieve multiple revisions of text. RCS saves all old
          revisions in a space-efficient way. Changes no longer destroy the
          original because the previous revisions remain accessible. Revisions
          can be retrieved according to ranges of revision numbers, symbolic
          names, dates, authors, and states.
    *     Maintain a complete history of changes. RCS logs all changes
          automatically. In addition to storing the text of each revision, RCS
          also stores the author, the date and time of check-in, and a log
          message summarizing the change. The logging makes it easy to find
          out what happened to a module without having to compare source
          listings or having to track down colleagues.
    *     Resolve access conflicts. When two or more programmers want to
          modify the same revision, RCS alerts the programmers and prevents
          one modification from corrupting the other.
    *     Maintain a tree of revisions. RCS can maintain separate lines of
          development for each module. It stores a tree structure that
          represents the ancestral relationships among revisions.
    *     Merge revisions and resolve conflicts. Two separate lines of
          development of a module can be coalesced by merging. If the
          revisions to be merged affect the same sections of code, RCS alerts
          the user about the overlapping changes.
    *     Control releases and configurations. Revisions can be assigned
          symbolic names and marked as released, stable, experimental, and so
          on. With these facilities, configurations of modules can be
          described simply and directly.
    *     Automatically identify each revision with name, revision number,
          creation time, author, and so on. The identification is like a stamp
          that can be embedded at an appropriate place in the text of a
          revision. The identification makes it simple to determine which
          revisions of which modules make up a given configuration.
    *     Minimize secondary storage. RCS needs little extra space for the
          revisions (only the differences). If intermediate revisions are
          deleted, the corresponding deltas are compressed accordingly.

  GGeettttiinngg ssttaarrtteedd wwiitthh RRCCSS

    For the purposes of illustration, suppose you have a file ff..cc that you
    want to put under control of RCS. If you have not already done so, make an
    RCS directory with the following command:

    mkdir RCS

    Then invoke the check-in command:

    ci f.c

    This command creates an RCS file in the RRCCSS directory, stores ff..cc into it
    as revision 1.1, and deletes ff..cc. It also asks you for a description. The
    description should be a synopsis of the contents of the file. All later
    check-in commands will ask you for a log entry, which should summarize the
    changes that you made.

    Files in the RCS directory are called RCS files; the others are called
    working files. To get back the working file ff..cc in the previous example,
    use the check-out command:

    co f.c

    This command extracts the latest revision from the RCS file and writes it
    into ff..cc. If you want to edit ff..cc, you must lock it as you check it out
    with the command:

    co -l f.c

    You can now edit ff..cc.

    If, after some editing, you want to see the differences between the
    changes you have just made and the previously checked-in version, use the
    command:

    rcsdiff f.c

    You can check the file back in by invoking:

    ci f.c

    This increments the revision number properly.

    If ccii gives you the message

    ccii eerrrroorr:: nnoo lloocckk sseett bbyy your_name,

    it means that you have tried to check in a file even though you did not
    lock it when you checked it out. At this point, it is too late to do the
    check-out with locking because another check-out would overwrite your
    modifications. Instead, invoke:

    rcs -l f.c

    This command will lock the latest revision for you, unless another user
    has already done so, in which case you would have to negotiate with that
    person.

    Locking ensures that only you can check in the next update and prevents
    problems that can arise if several people work on the same file. Even if a
    revision is locked, it can still be checked out for reading, compiling,
    and so on. Locking only prevents a check-in by anybody but the locker.

    If your RCS file is private (that is, if you are the only person who is
    going to deposit revisions into it) strict locking is not needed, and you
    can turn it off. If strict locking is turned off, the owner of the RCS
    file need not have a lock for check-in; all others still do. You turn
    strict locking off and on with the commands:

    rcs -U f.c

    and

    rcs -L f.c

    If you do not want to clutter your working directory with RCS files,
    create a subdirectory called RRCCSS in your working directory, and move all
    your RCS files there. RCS commands will look first into that directory to
    find needed files. All the commands discussed above will still work
    without any modification. (Actually, pairs of RCS and working files can be
    specified in three ways: (a) both are given; (b) only the working file is
    given; (c) only the RCS file is given. Both RCS and working files can have
    arbitrary path prefixes; RCS commands pair them up intelligently.)

    To avoid the deletion of the working file during check-in (in case you
    want to continue editing or compiling), invoke:

    ci -l f.c

    or

    ci -u f.c

    These commands check in ff..cc as usual, but perform an implicit check-out.
    The first form also locks the checked-in revision, the second one does
    not. Thus, these options save you one check-out operation. The first form
    is useful if you want to continue editing, and the second one is useful if
    you just want to read the file. Both update the identification markers in
    your working file (see later in this topic).

    You can give ccii the number you want assigned to a checked-in revision.
    Assume all your revisions were numbered 1.1, 1.2, 1.3, and so on, and you
    would like to start release 2. The following command:

    ci -r2 f.c

    or

    ci -r2.1 f.c

    assigns the number 2.1 to the new revision. Thereafter, ccii will number the
    subsequent revisions with 2.2, 2.3, and so on. The corresponding ccoo
    commands:

    co -r2 f.c

    and

    co -r2.1 f.c

    retrieve the latest revision numbered 2.x and the revision 2.1,
    respectively. Using ccoo without a revision number selects the latest
    revision on the trunk; that is, the highest revision with a number
    consisting of two fields. Numbers with more than two fields are needed for
    branches. For example, to start a branch at revision 1.3, invoke:

    ci -r1.3.1 f.c

    This command starts a branch numbered 1 at revision 1.3 and assigns the
    number 1.3.1.1 to the new revision. For more information about branches,
    see _r_c_s_f_i_l_e(5).

  AAuuttoommaattiicc iiddeennttiiffiiccaattiioonn

    RCS can put special strings for identification into your source and object
    code. To obtain such identification, place the marker:

    $$IIdd$$

    into your text; for instance, inside a comment. RCS will replace this
    marker with a string of the form:

    $$IIdd:: file-name revision date time author state $$

    With such a marker on the first page of each module, you can always see
    with which revision you are working. RCS keeps the markers up to date
    automatically. To propagate the markers into your object code, simply put
    them into literal character strings. In C, this is done as follows:

     static char rcsid[] = "$Id$";

    The command iiddeenntt extracts such markers from any file, even object code
    and dumps. Thus, iiddeenntt lets you find out which revisions of which modules
    were used in a given program.

    It can also be useful to put the marker $$LLoogg$$ into your text, inside a
    comment. This marker accumulates the log messages that are requested
    during check-in. Thus, you can maintain the complete history of your file
    directly inside it. There are several additional identification markers;
    see
    _cc_oo_(_1_)
     for details.

  IIDDEENNTTIIFFIICCAATTIIOONN

    Author: Walter F. Tichy.

    Manual Page Revision: 1.1; Release Date: 1996/08/12.

    Copyright (C) 1982, 1988, 1989 Walter F. Tichy.

    Copyright (C) 1990, 1991, 1992, 1993 Paul Eggert.

  SSEEEE AALLSSOO

    _r_c_s_f_i_l_e(5)

    _c_i(1)

    _c_o(1)

    _i_d_e_n_t(1)

    _r_c_s(1)

    _r_c_s_d_i_f_f(1)

    _r_c_s_m_e_r_g_e(1)

    _r_l_o_g(1)

    "RCS�A System for Version Control," Walter F. Tichy, Software�Practice &
    Experience 15, 7 (July 1985), 637-654.

