XClientMessageEvent(3X11R6)                 XClientMessageEvent(3X11R6)

  XXCClliieennttMMeessssaaggeeEEvveenntt

  NNAAMMEE

    XClientMessageEvent - ClientMessage event structure

  SSTTRRUUCCTTUURREESS

    The structure for ClientMessage events contains:

    typedef struct {
         int type;                /* ClientMessage */
         unsigned long serial;    /* # of last request processed by server */
         Bool send_event;         /* true if this came from a SendEvent
    request */
         Display *display;        /* Display the event was read from */
         Window window;
         Atom message_type;
         int format;
         union {
              char b[20];
              short s[10];
              long l[5];
                 } data;
    } XClientMessageEvent;

    When you receive this event, the structure members are set as follows.

    The type member is set to the event type constant name that uniquely
    identifies it. For example, when the X server reports a GraphicsExpose
    event to a client application, it sends an XXGGrraapphhiiccssEExxppoosseeEEvveenntt structure
    with the type member set to GraphicsExpose. The display member is set to a
    pointer to the display the event was read on. The send_event member is set
    to True if the event came from a SendEvent protocol request. The serial
    member is set from the serial number reported in the protocol but expanded
    from the 16-bit least-significant bits to a full 32-bit value. The window
    member is set to the window that is most useful to toolkit dispatchers.

    The message_type member is set to an atom that indicates how the data
    should be interpreted by the receiving client. The format member is set to
    8, 16, or 32 and specifies whether the data should be viewed as a list of
    bytes, shorts, or longs. The data member is a union that contains the
    members b, s, and l. The b, s, and l members represent data of twenty 8-
    bit values, ten 16-bit values, and five 32-bit values. Particular message
    types might not make use of all these values. The X server places no
    interpretation on the values in the window, message_type, or data members.

  SSEEEE AALLSSOO

    _X_A_n_y_E_v_e_n_t(3X11R6)

    _X_B_u_t_t_o_n_E_v_e_n_t(3X11R6)

    _X_C_r_e_a_t_e_W_i_n_d_o_w_E_v_e_n_t(3X11R6)

    _X_C_i_r_c_u_l_a_t_e_E_v_e_n_t(3X11R6)

    _X_C_i_r_c_u_l_a_t_e_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_C_o_l_o_r_m_a_p_E_v_e_n_t(3X11R6)

    _X_C_o_n_f_i_g_u_r_e_E_v_e_n_t(3X11R6)

    _X_C_o_n_f_i_g_u_r_e_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_C_r_o_s_s_i_n_g_E_v_e_n_t(3X11R6)

    _X_D_e_s_t_r_o_y_W_i_n_d_o_w_E_v_e_n_t(3X11R6)

    _X_E_r_r_o_r_E_v_e_n_t(3X11R6)

    _X_E_x_p_o_s_e_E_v_e_n_t(3X11R6)

    _X_F_o_c_u_s_C_h_a_n_g_e_E_v_e_n_t(3X11R6)

    _X_G_r_a_p_h_i_c_s_E_x_p_o_s_e_E_v_e_n_t(3X11R6)

    _X_G_r_a_v_i_t_y_E_v_e_n_t(3X11R6)

    _X_K_e_y_m_a_p_E_v_e_n_t(3X11R6)

    _X_M_a_p_E_v_e_n_t(3X11R6)

    _X_M_a_p_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_P_r_o_p_e_r_t_y_E_v_e_n_t(3X11R6)

    _X_R_e_p_a_r_e_n_t_E_v_e_n_t(3X11R6)

    _X_R_e_s_i_z_e_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_S_e_l_e_c_t_i_o_n_C_l_e_a_r_E_v_e_n_t(3X11R6)

    _X_S_e_l_e_c_t_i_o_n_E_v_e_n_t(3X11R6)

    _X_S_e_l_e_c_t_i_o_n_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_U_n_m_a_p_E_v_e_n_t(3X11R6)

    Xlib - C Language X Interface

