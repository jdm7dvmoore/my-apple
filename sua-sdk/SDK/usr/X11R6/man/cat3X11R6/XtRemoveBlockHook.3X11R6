XtRemoveBlockHook(3X11R6)                     XtRemoveBlockHook(3X11R6)

  XXttAAppppAAddddBBlloocckkHHooookk

  NNAAMMEE

    XtAppAddBlockHook, XtRemoveBlockHook - register a block hook procedure

  SSYYNNTTAAXX

    XtBlockHookId XtAppAddBlockHook(app_context, proc, client_data)
          XtAppContext app_context;
          XtBlockHookProc proc;
          XtPointer client_data;

    void XtRemoveBlockHook(idFP)
          XtBlockHookId id;

  AARRGGUUMMEENNTTSS

    app_context
        Specifies the application context.

    proc
        Specifies the block hook procedure.

    num_args
        Specifies the application-specific data to be passed to the block
        hook.

  DDEESSCCRRIIPPTTIIOONN

    XXttAAppppAAddddBBlloocckkHHooookk registers the specified procedure and returns an
    identifier for it. The hook is called at any time in the future when the
    Intrinsics are about to block pending some input.

    Block hook procedures are removed automatically and the XtBlockHookId is
    destroyed when the application context in which they were added is
    destroyed.

    XXttRReemmoovveeBBlloocckkHHooookk removes the specified block hook procedure from the list
    in which it was registered.

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

