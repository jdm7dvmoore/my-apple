XFillArc(3X11R6)                                       XFillArc(3X11R6)

  XXFFiillllRReeccttaannggllee

  NNAAMMEE

    XFillRectangle, XFillRectangles, XFillPolygon, XFillArc, XFillArcs - fill
    rectangles, polygons, or arcs

  SSYYNNTTAAXX

    XFillRectangle(display, d, gc, x, y, width, height)
          Display *display;
          Drawable d;
          GC gc;
          int x, y;
          unsigned int width, height;

    XFillRectangles(display, d, gc, rectangles, nrectangles)
          Display *display;
          Drawable d;
          GC gc;
          XRectangle *rectangles;
          int nrectangles;

    XFillPolygon(display, d, gc, points, npoints, shape, mode)
          Display *display;
          Drawable d;
          GC gc;
          XPoint *points;
          int npoints;
          int shape; 
          int mode; 

    XFillArc(display, d, gc,  x, y, width, height, angle1, angle2)
          Display *display;
          Drawable d;
          GC gc;
          int x, y;
          unsigned int width, height;
          int angle1, angle2;

    XFillArcs(display, d, gc, arcs, narcs)
          Display *display;
          Drawable d;
          GC gc;
          XArc *arcs;
          int narcs;

  AARRGGUUMMEENNTTSS

    angle1
        Specifies the start of the arc relative to the three-o'clock position
        from the center, in units of degrees * 64.

    angle2
        Specifies the path and extent of the arc relative to the start of the
        arc, in units of degrees * 64.

    arcs
        Specifies an array of arcs.

    d
        Specifies the drawable.

    display
        Specifies the connection to the X server.

    gc
        Specifies the GC.

    mode
        Specifies the coordinate mode. You can pass CoordModeOrigin or
        CoordModePrevious.

    narcs
        Specifies the number of arcs in the array.

    npoints
        Specifies the number of points in the array.

    nrectangles
        Specifies the number of rectangles in the array.

    points
        Specifies an array of points.

    rectangles
        Specifies an array of rectangles.

    shape
        Specifies a shape that helps the server to improve performance. You
        can pass Complex, Convex, or Nonconvex.
    width
    height
        Specify the width and height, which are the dimensions of the
        rectangle to be filled or the major and minor axes of the arc.
    x
    y
        Specify the x and y coordinates, which are relative to the origin of
        the drawable and specify the upper-left corner of the rectangle.

  DDEESSCCRRIIPPTTIIOONN

    The XXFFiillllRReeccttaannggllee and XXFFiillllRReeccttaanngglleess functions fill the specified
    rectangle or rectangles as if a four-point FillPolygon protocol request
    were specified for each rectangle:

    [x,y] [x+width,y] [x+width,y+height] [x,y+height]

    Each function uses the x and y coordinates, width and height dimensions,
    and GC you specify.

    XXFFiillllRReeccttaanngglleess fills the rectangles in the order listed in the array. For
    any given rectangle, XXFFiillllRReeccttaannggllee and XXFFiillllRReeccttaanngglleess do not draw a
    pixel more than once. If rectangles intersect, the intersecting pixels are
    drawn multiple times.

    Both functions use these GC components: function, plane-mask, fill-style,
    subwindow-mode, clip-x-origin, clip-y-origin, and clip-mask. They also use
    these GC mode-dependent components: foreground, background, tile, stipple,
    tile-stipple-x-origin, and tile-stipple-y-origin.

    XXFFiillllRReeccttaannggllee and XXFFiillllRReeccttaanngglleess can generate BadDrawable, BadGC, and
    BadMatch errors.

    XXFFiillllPPoollyyggoonn fills the region closed by the specified path. The path is
    closed automatically if the last point in the list does not coincide with
    the first point. XXFFiillllPPoollyyggoonn does not draw a pixel of the region more
    than once. CoordModeOrigin treats all coordinates as relative to the
    origin, and CoordModePrevious treats all coordinates after the first as
    relative to the previous point.

    Depending on the specified shape, the following occurs:

    *
        If shape is Complex, the path may self-intersect. Note that contiguous
        coincident points in the path are not treated as self-intersection.

    *
        If shape is Convex, for every pair of points inside the polygon, the
        line segment connecting them does not intersect the path. If known by
        the client, specifying Convex can improve performance. If you specify
        Convex for a path that is not convex, the graphics results are
        undefined.

    *
        If shape is Nonconvex, the path does not self-intersect, but the shape
        is not wholly convex. If known by the client, specifying Nonconvex
        instead of Complex may improve performance. If you specify Nonconvex
        for a self-intersecting path, the graphics results are undefined.

    The fill-rule of the GC controls the filling behavior of self-intersecting
    polygons.

    This function uses these GC components: function, plane-mask, fill-style,
    fill-rule, subwindow-mode, clip-x-origin, clip-y-origin, and clip-mask. It
    also uses these GC mode-dependent components: foreground, background,
    tile, stipple, tile-stipple-x-origin, and tile-stipple-y-origin.

    XXFFiillllPPoollyyggoonn can generate BadDrawable, BadGC, BadMatch, and BadValue
    errors.

    For each arc, XXFFiillllAArrcc or XXFFiillllAArrccss fills the region closed by the
    infinitely thin path described by the specified arc and, depending on the
    arc-mode specified in the GC, one or two line segments. For ArcChord, the
    single line segment joining the endpoints of the arc is used. For
    ArcPieSlice, the two line segments joining the endpoints of the arc with
    the center point are used. XXFFiillllAArrccss fills the arcs in the order listed in
    the array. For any given arc, XXFFiillllAArrcc and XXFFiillllAArrccss do not draw a pixel
    more than once. If regions intersect, the intersecting pixels are drawn
    multiple times.

    Both functions use these GC components: function, plane-mask, fill-style,
    arc-mode, subwindow-mode, clip-x-origin, clip-y-origin, and clip-mask.
    They also use these GC mode-dependent components: foreground, background,
    tile, stipple, tile-stipple-x-origin, and tile-stipple-y-origin.

    XXFFiillllAArrcc and XXFFiillllAArrccss can generate BadDrawable, BadGC, and BadMatch
    errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadDrawable
        A value for a Drawable argument does not name a defined Window or
        Pixmap.

    BadGC
        A value for a GContext argument does not name a defined GContext.

    BadMatch
        An InputOnly window is used as a Drawable.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_D_r_a_w_A_r_c(3X11R6)

    _X_D_r_a_w_P_o_i_n_t(3X11R6)

    _X_D_r_a_w_R_e_c_t_a_n_g_l_e(3X11R6)

    Xlib - C Language X Interface

