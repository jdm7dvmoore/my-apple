DisplayHeight(3X11R6)                             DisplayHeight(3X11R6)

  IImmaaggeeBByytteeOOrrddeerr

  NNAAMMEE

    ImageByteOrder, BitmapBitOrder, BitmapPad, BitmapUnit, DisplayHeight,
    DisplayHeightMM, DisplayWidth, DisplayWidthMM, XListPixmapFormats,
    XPixmapFormatValues - image format functions and macros

  SSYYNNTTAAXX

    XPixmapFormatValues *XListPixmapFormats(display, count_return)
          Display *display;
          int *count_return;

    ImageByteOrder(display)

    BitmapBitOrder(display)

    BitmapPad(display)

    BitmapUnit(display)

    DisplayHeight(display, screen_number)

    DisplayHeightMM(display, screen_number)

    DisplayWidth(display, screen_number)

    DisplayWidthMM(display, screen_number)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    count_return
        Returns the number of pixmap formats that are supported by the
        display.

    screen_number
        Specifies the appropriate screen number on the host server.

  DDEESSCCRRIIPPTTIIOONN

    The XXLLiissttPPiixxmmaappFFoorrmmaattss function returns an array of XXPPiixxmmaappFFoorrmmaattVVaalluueess
    structures that describe the types of Z format images supported by the
    specified display. If insufficient memory is available, XXLLiissttPPiixxmmaappFFoorrmmaattss
    returns NULL. To free the allocated storage for the XXPPiixxmmaappFFoorrmmaattVVaalluueess
    structures, use XXFFrreeee.

    The IImmaaggeeBByytteeOOrrddeerr macro specifies the required byte order for images for
    each scanline unit in XY format (bitmap) or for each pixel value in Z
    format.

    The BBiittmmaappBBiittOOrrddeerr macro returns LSBFirst or MSBFirst to indicate whether
    the leftmost bit in the bitmap as displayed on the screen is the least or
    most significant bit in the unit.

    The BBiittmmaappPPaadd macro returns the number of bits that each scanline must be
    padded.

    The BBiittmmaappUUnniitt macro returns the size of a bitmap's scanline unit in bits.

    The DDiissppllaayyHHeeiigghhtt macro returns the height of the specified screen in
    pixels.

    The DDiissppllaayyHHeeiigghhttMMMM macro returns the height of the specified screen in
    millimeters.

    The DDiissppllaayyWWiiddtthh macro returns the width of the screen in pixels.

    The DDiissppllaayyWWiiddtthhMMMM macro returns the width of the specified screen in
    millimeters.

  SSTTRRUUCCTTUURREESS

    The XXPPiixxmmaappFFoorrmmaattVVaalluueess structure provides an interface to the pixmap
    format information that is returned at the time of a connection setup. It
    contains:

    typedef struct {
         int depth;
         int bits_per_pixel;
         int scanline_pad;
    } XPixmapFormatValues;

  SSEEEE AALLSSOO

    _A_l_l_P_l_a_n_e_s(3X11R6)

    _B_l_a_c_k_P_i_x_e_l_O_f_S_c_r_e_e_n(3X11R6)

    _I_s_C_u_r_s_o_r_K_e_y(3X11R6)

    _X_F_r_e_e(3X11R6)

    Xlib - C Language X Interface

