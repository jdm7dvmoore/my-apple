XGetWMColormapWindows(3X11R6)             XGetWMColormapWindows(3X11R6)

  XXSSeettWWMMCCoolloorrmmaappWWiinnddoowwss

  NNAAMMEE

    XSetWMColormapWindows, XGetWMColormapWindows - set or read a window's
    WM_COLORMAP_WINDOWS property

  SSYYNNTTAAXX

    Status XSetWMColormapWindows(display, w, colormap_windows, count)
          Display *display;
          Window w;
          Window *colormap_windows;
          int count;

    Status XGetWMColormapWindows(display, w, colormap_windows_return,
    count_return)
          Display *display;
          Window w;
          Window **colormap_windows_return;
          int *count_return;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    colormap_windows
        Specifies the list of windows.

    colormap_windows_return
        Returns the list of windows.

    count
        Specifies the number of windows in the list.

    count_return
        Returns the number of windows in the list.

    w
        Specifies the window.

  DDEESSCCRRIIPPTTIIOONN

    The XXSSeettWWMMCCoolloorrmmaappWWiinnddoowwss function replaces the WM_COLORMAP_WINDOWS
    property on the specified window with the list of windows specified by the
    colormap_windows argument. It the property does not already exist,
    XXSSeettWWMMCCoolloorrmmaappWWiinnddoowwss sets the WM_COLORMAP_WINDOWS property on the
    specified window to the list of windows specified by the colormap_windows
    argument. The property is stored with a type of WINDOW and a format of 32.
    If it cannot intern the WM_COLORMAP_WINDOWS atom, XXSSeettWWMMCCoolloorrmmaappWWiinnddoowwss
    returns a zero status. Otherwise, it returns a nonzero status.

    XXSSeettWWMMCCoolloorrmmaappWWiinnddoowwss can generate BadAlloc and BadWindow errors.

    The XXGGeettWWMMCCoolloorrmmaappWWiinnddoowwss function returns the list of window identifiers
    stored in the WM_COLORMAP_WINDOWS property on the specified window. These
    identifiers indicate the colormaps that the window manager may need to
    install for this window. If the property exists, is of type WINDOW, is of
    format 32, and the atom WM_COLORMAP_WINDOWS can be interned,
    XXGGeettWWMMCCoolloorrmmaappWWiinnddoowwss sets the windows_return argument to a list of window
    identifiers, sets the count_return argument to the number of elements in
    the list, and returns a nonzero status. Otherwise, it sets neither of the
    return arguments and returns a zero status. To release the list of window
    identifiers, use XXFFrreeee.

    XXGGeettWWMMCCoolloorrmmaappWWiinnddoowwss can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_COLORMAP_WINDOWS
        The list of window IDs that may need a different colormap from that of
        their top-level window.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t(3X11R6)

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e(3X11R6)

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s(3X11R6)

    _X_A_l_l_o_c_W_M_H_i_n_t_s(3X11R6)

    _X_F_r_e_e(3X11R6)

    _X_S_e_t_C_o_m_m_a_n_d(3X11R6)

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t(3X11R6)

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e(3X11R6)

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s(3X11R6)

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s(3X11R6)

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    Xlib - C Language X Interface

