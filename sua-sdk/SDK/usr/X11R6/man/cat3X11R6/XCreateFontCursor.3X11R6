XCreateFontCursor(3X11R6)                     XCreateFontCursor(3X11R6)

  XXCCrreeaatteeFFoonnttCCuurrssoorr

  NNAAMMEE

    XCreateFontCursor, XCreatePixmapCursor, XCreateGlyphCursor - create
    cursors

  SSYYNNTTAAXX

    #include <X11/cursorfont.h>
    Cursor XCreateFontCursor(display, shape)
          Display *display;
          unsigned int shape;

    Cursor XCreatePixmapCursor(display, source, mask, foreground_color,
    background_color, x, y)
          Display *display;
          Pixmap source;
          Pixmap mask;
          XColor *foreground_color;
          XColor *background_color;
          unsigned int x, y;

    Cursor XCreateGlyphCursor(display, source_font, mask_font, source_char,
    mask_char,
                               foreground_color, background_color)
          Display *display;
          Font source_font, mask_font;
          unsigned int source_char, mask_char;
          XColor *foreground_color;
          XColor *background_color;

  AARRGGUUMMEENNTTSS

    background_color
        Specifies the RGB values for the background of the source.

    display
        Specifies the connection to the X server.

    foreground_color
        Specifies the RGB values for the foreground of the source.

    mask
        Specifies the cursor's source bits to be displayed or None.

    mask_char
        Specifies the glyph character for the mask.

    mask_font
        Specifies the font for the mask glyph or None.

    shape
        Specifies the shape of the cursor.

    source
        Specifies the shape of the source cursor.

    source_char
        Specifies the character glyph for the source.

    source_font
        Specifies the font for the source glyph.
    x
    y
        Specify the x and y coordinates, which indicate the hotspot relative
        to the source's origin.

  DDEESSCCRRIIPPTTIIOONN

    X provides a set of standard cursor shapes in a special font named cursor.
    Applications are encouraged to use this interface for their cursors
    because the font can be customized for the individual display type. The
    shape argument specifies which glyph of the standard fonts to use.

    The hotspot comes from the information stored in the cursor font. The
    initial colors of a cursor are a black foreground and a white background
    (see XXRReeccoolloorrCCuurrssoorr).

    XXCCrreeaatteeFFoonnttCCuurrssoorr can generate BadAlloc and BadValue errors.

    The XXCCrreeaatteePPiixxmmaappCCuurrssoorr function creates a cursor and returns the cursor
    ID associated with it. The foreground and background RGB values must be
    specified using foreground_color and background_color, even if the X
    server only has a StaticGray or GrayScale screen. The foreground color is
    used for the pixels set to 1 in the source, and the background color is
    used for the pixels set to 0. Both source and mask, if specified, must
    have depth one (or a BadMatch error results) but can have any root. The
    mask argument defines the shape of the cursor. The pixels set to 1 in the
    mask define which source pixels are displayed, and the pixels set to 0
    define which pixels are ignored. If no mask is given, all pixels of the
    source are displayed. The mask, if present, must be the same size as the
    pixmap defined by the source argument, or a BadMatch error results. The
    hotspot must be a point within the source, or a BadMatch error results.

    The components of the cursor can be transformed arbitrarily to meet
    display limitations. The pixmaps can be freed immediately if no further
    explicit references to them are to be made. Subsequent drawing in the
    source or mask pixmap has an undefined effect on the cursor. The X server
    might or might not make a copy of the pixmap.

    XXCCrreeaatteePPiixxmmaappCCuurrssoorr can generate BadAlloc and BadPixmap errors.

    The XXCCrreeaatteeGGllyypphhCCuurrssoorr function is similar to XXCCrreeaatteePPiixxmmaappCCuurrssoorr except
    that the source and mask bitmaps are obtained from the specified font
    glyphs. The source_char must be a defined glyph in source_font, or a
    BadValue error results. If mask_font is given, mask_char must be a defined
    glyph in mask_font, or a BadValue error results. The mask_font and
    character are optional. The origins of the source_char and mask_char (if
    defined) glyphs are positioned coincidently and define the hotspot. The
    source_char and mask_char need not have the same bounding box metrics, and
    there is no restriction on the placement of the hotspot relative to the
    bounding boxes. If no mask_char is given, all pixels of the source are
    displayed. You can free the fonts immediately by calling XXFFrreeeeFFoonntt if no
    further explicit references to them are to be made.

    For 2-byte matrix fonts, the 16-bit value should be formed with the byte1
    member in the most significant byte and the byte2 member in the least
    significant byte.

    XXCCrreeaatteeGGllyypphhCCuurrssoorr can generate BadAlloc, BadFont, and BadValue errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadFont
        A value for a Font or GContext argument does not name a defined Font.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadPixmap
        A value for a Pixmap argument does not name a defined Pixmap.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_D_e_f_i_n_e_C_u_r_s_o_r(3X11R6)

    _X_L_o_a_d_F_o_n_t(3X11R6)

    _X_R_e_c_o_l_o_r_C_u_r_s_o_r(3X11R6)

    Xlib - C Language X Interface

