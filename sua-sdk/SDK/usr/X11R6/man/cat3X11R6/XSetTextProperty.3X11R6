XSetTextProperty(3X11R6)                       XSetTextProperty(3X11R6)

  XXSSeettTTeexxttPPrrooppeerrttyy

  NNAAMMEE

    XSetTextProperty, XGetTextProperty - set and read text properties

  SSYYNNTTAAXX

    void XSetTextProperty(display, w, text_prop, property)
          Display *display;
          Window w;
          XTextProperty *text_prop;
          Atom property;

    Status XGetTextProperty(display, w, text_prop_return, property)
           Display *display;
           Window w;
           XTextProperty *text_prop_return;
           Atom property;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    property
        Specifies the property name.

    text_prop
        Specifies the XXTTeexxttPPrrooppeerrttyy structure to be used.

    text_prop_return
        Returns the XXTTeexxttPPrrooppeerrttyy structure.

  DDEESSCCRRIIPPTTIIOONN

    The XXSSeettTTeexxttPPrrooppeerrttyy function replaces the existing specified property for
    the named window with the data, type, format, and number of items
    determined by the value field, the encoding field, the format field, and
    the nitems field, respectively, of the specified XXTTeexxttPPrrooppeerrttyy structure.
    If the property does not already exist, XXSSeettTTeexxttPPrrooppeerrttyy sets it for the
    specified window.

    XXSSeettTTeexxttPPrrooppeerrttyy can generate BadAlloc, BadAtom, BadValue, and BadWindow
    errors.

    The XXGGeettTTeexxttPPrrooppeerrttyy function reads the specified property from the window
    and stores the data in the returned XXTTeexxttPPrrooppeerrttyy structure. It stores the
    data in the value field, the type of the data in the encoding field, the
    format of the data in the format field, and the number of items of data in
    the nitems field. An extra byte containing null (which is not included in
    the nitems member) is stored at the end of the value field of
    text_prop_return. The particular interpretation of the property's encoding
    and data as text is left to the calling application. If the specified
    property does not exist on the window, XXGGeettTTeexxttPPrrooppeerrttyy sets the value
    field to NULL, the encoding field to None, the format field to zero, and
    the nitems field to zero.

    If it was able to read and store the data in the XXTTeexxttPPrrooppeerrttyy structure,
    XXGGeettTTeexxttPPrrooppeerrttyy returns a nonzero status; otherwise, it returns a zero
    status.

    XXGGeettTTeexxttPPrrooppeerrttyy can generate BadAtom and BadWindow errors.

  PPRROOPPEERRTTIIEESS

    WM_CLIENT_MACHINE
        The string name of the machine on which the client application is
        running.

    WM_COMMAND
        The command and arguments, null-separated, used to invoke the
        application.

    WM_ICON_NAME
        The name to be used in an icon.

    WM_NAME
        The name of the application.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadAtom
        A value for an Atom argument does not name a defined Atom.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t(3X11R6)

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e(3X11R6)

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s(3X11R6)

    _X_A_l_l_o_c_W_M_H_i_n_t_s(3X11R6)

    _X_S_e_t_C_o_m_m_a_n_d(3X11R6)

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t(3X11R6)

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e(3X11R6)

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s(3X11R6)

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s(3X11R6)

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s(3X11R6)

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    Xlib - C Language X Interface

