HeightOfScreen(3X11R6)                           HeightOfScreen(3X11R6)

  BBllaacckkPPiixxeellOOffSSccrreeeenn

  NNAAMMEE

    BlackPixelOfScreen, WhitePixelOfScreen, CellsOfScreen,
    DefaultColormapOfScreen, DefaultDepthOfScreen, DefaultGCOfScreen,
    DefaultVisualOfScreen, DoesBackingStore, DoesSaveUnders, DisplayOfScreen,
    XScreenNumberOfScreen, EventMaskOfScreen, HeightOfScreen,
    HeightMMOfScreen, MaxCmapsOfScreen, MinCmapsOfScreen, PlanesOfScreen,
    RootWindowOfScreen, WidthOfScreen, WidthMMOfScreen - screen information
    functions and macros

  SSYYNNTTAAXX

    BlackPixelOfScreen(screen)

    WhitePixelOfScreen(screen)

    CellsOfScreen(screen)

    DefaultColormapOfScreen(screen)

    DefaultDepthOfScreen(screen)

    DefaultGCOfScreen(screen)

    DefaultVisualOfScreen(screen)

    DoesBackingStore(screen)

    DoesSaveUnders(screen)

    DisplayOfScreen(screen)

    int XScreenNumberOfScreen(screen)
          Screen *screen;

    EventMaskOfScreen(screen)

    HeightOfScreen(screen)

    HeightMMOfScreen(screen)

    MaxCmapsOfScreen(screen)

    MinCmapsOfScreen(screen)

    PlanesOfScreen(screen)

    RootWindowOfScreen(screen)

    WidthOfScreen(screen)

    WidthMMOfScreen(screen)

  AARRGGUUMMEENNTTSS

    screen
        Specifies the appropriate Screen structure.

  DDEESSCCRRIIPPTTIIOONN

    The BBllaacckkPPiixxeellOOffSSccrreeeenn macro returns the black pixel value of the
    specified screen.

    The WWhhiitteePPiixxeellOOffSSccrreeeenn macro returns the white pixel value of the
    specified screen.

    The CCeellllssOOffSSccrreeeenn macro returns the number of colormap cells in the
    default colormap of the specified screen.

    The DDeeffaauullttCCoolloorrmmaappOOffSSccrreeeenn macro returns the default colormap of the
    specified screen.

    The DDeeffaauullttDDeepptthhOOffSSccrreeeenn macro returns the default depth of the root
    window of the specified screen.

    The DDeeffaauullttGGCCOOffSSccrreeeenn macro returns the default GC of the specified
    screen, which has the same depth as the root window of the screen.

    The DDeeffaauullttVViissuuaallOOffSSccrreeeenn macro returns the default visual of the
    specified screen.

    The DDooeessBBaacckkiinnggSSttoorree macro returns WhenMapped, NotUseful, or Always, which
    indicate whether the screen supports backing stores.

    The DDooeessSSaavveeUUnnddeerrss macro returns a Boolean value indicating whether the
    screen supports save unders.

    The DDiissppllaayyOOffSSccrreeeenn macro returns the display of the specified screen.

    The XXSSccrreeeennNNuummbbeerrOOffSSccrreeeenn function returns the screen index number of the
    specified screen.

    The EEvveennttMMaasskkOOffSSccrreeeenn macro returns the root event mask of the root window
    for the specified screen at connection setup.

    The HHeeiigghhttOOffSSccrreeeenn macro returns the height of the specified screen.

    The HHeeiigghhttMMMMOOffSSccrreeeenn macro returns the height of the specified screen in
    millimeters.

    The MMaaxxCCmmaappssOOffSSccrreeeenn macro returns the maximum number of installed
    colormaps supported by the specified screen.

    The MMiinnCCmmaappssOOffSSccrreeeenn macro returns the minimum number of installed
    colormaps supported by the specified screen.

    The PPllaanneessOOffSSccrreeeenn macro returns the number of planes in the root window
    of the specified screen.

    The RRoooottWWiinnddoowwOOffSSccrreeeenn macro returns the root window of the specified
    screen.

    The WWiiddtthhOOffSSccrreeeenn macro returns the width of the specified screen.

    The WWiiddtthhMMMMOOffSSccrreeeenn macro returns the width of the specified screen in
    millimeters.

  SSEEEE AALLSSOO

    _A_l_l_P_l_a_n_e_s(3X11R6)

    _I_m_a_g_e_B_y_t_e_O_r_d_e_r(3X11R6)

    _I_s_C_u_r_s_o_r_K_e_y(3X11R6)

    Xlib - C Language X Interface

