XmbDrawImageString(3X11R6)                   XmbDrawImageString(3X11R6)

  XXmmbbDDrraawwIImmaaggeeSSttrriinngg

  NNAAMMEE

    XmbDrawImageString, XwcDrawImageString - draw image text using a single
    font set

  SSYYNNTTAAXX

    void XmbDrawImageString(display, d, font_set, gc, x, y, string, num_bytes)
          Display *display;
          Drawable d;
          XFontSet font_set;
          GC gc;
          int x, y;
          char *string;
          int num_bytes;

    void XwcDrawImageString(display, d, font_set, gc, x, y, string,
    num_wchars)
          Display *display;
          Drawable d;
          XFontSet font_set;
          GC gc;
          int x, y;
          wchar_t *string;
          int num_wchars;

  AARRGGUUMMEENNTTSS

    d
        Specifies the drawable.

    display
        Specifies the connection to the X server.

    font_set
        Specifies the font set.

    gc
        Specifies the GC.

    num_bytes
        Specifies the number of bytes in the string argument.

    num_wchars
        Specifies the number of characters in the string argument.

    string
        Specifies the character string.
    x
    y
        Specify the x and y coordinates.

  DDEESSCCRRIIPPTTIIOONN

    The XXmmbbDDrraawwIImmaaggeeSSttrriinngg and XXwwccDDrraawwIImmaaggeeSSttrriinngg functions fill a destination
    rectangle with the background pixel defined in the GC and then paint the
    text with the foreground pixel. The filled rectangle is the rectangle
    returned to overall_logical_return by XXmmbbTTeexxttEExxtteennttss or XXwwccTTeexxttEExxtteennttss for
    the same text and XFontSet.

    When the XFontSet has missing charsets, each unavailable character is
    drawn with the default string returned by XXCCrreeaatteeFFoonnttSSeett. The behavior for
    an invalid codepoint is undefined.

  SSEEEE AALLSSOO

    _X_D_r_a_w_I_m_a_g_e_S_t_r_i_n_g(3X11R6)

    _X_D_r_a_w_S_t_r_i_n_g(3X11R6)

    _X_D_r_a_w_T_e_x_t(3X11R6)

    _X_m_b_D_r_a_w_S_t_r_i_n_g(3X11R6)

    _X_m_b_D_r_a_w_T_e_x_t(3X11R6)

    Xlib - C Language X Interface

