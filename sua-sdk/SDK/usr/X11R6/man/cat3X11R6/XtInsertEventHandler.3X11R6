XtInsertEventHandler(3X11R6)               XtInsertEventHandler(3X11R6)

  XXttAAddddEEvveennttHHaannddlleerr

  NNAAMMEE

    XtAddEventHandler, XtAddRawEventHandler, XtRemoveEventHandler,
    XtRemoveRawEventHandler, XtInsertEventHandler, XtInsertRawEventHandler -
    add and remove event handlers

  SSYYNNTTAAXX

    void XtAddEventHandler(w, event_mask, nonmaskable, proc, client_data)
          Widget w;
          EventMask event_mask;
          Boolean nonmaskable;
          XtEventHandler proc;
          XtPointer client_data;

    void XtAddRawEventHandler(w, event_mask, nonmaskable, proc, client_data)
          Widget w;
          EventMask event_mask;
          Boolean nonmaskable;
          XtEventHandler proc;
          XtPointer client_data;

    void XtRemoveEventHandler(w, event_mask, nonmaskable, proc, client_data)
          Widget w;
          EventMask event_mask;
          Boolean nonmaskable;
          XtEventHandler proc;
          XtPointer client_data;

    void XtRemoveRawEventHandler(w, event_mask, nonmaskable, proc,
    client_data)
          Widget w;
          EventMask event_mask;
          Boolean nonmaskable;
          XtEventHandler proc;
          XtPointer client_data;

    void XtInsertEventHandler(w, event_mask, nonmaskable, proc, client_data,
    position)
          Widget w;
          EventMask event_mask;
          Boolean nonmaskable;
          XtEventHandler proc;
          XtPointer client_data;
          XtListPosition position;

    void XtInsertRawEventHandler(w, event_mask, nonmaskable, proc,
    client_data, position)
          Widget w;
          EventMask event_mask;
          Boolean nonmaskable;
          XtEventHandler proc;
          XtPointer client_data;
          XtListPosition position;

    typedef enum { XtListHead, XtListTail } XtListPosition;

  AARRGGUUMMEENNTTSS

    client_data
        Specifies additional data to be passed to the client's event handler.

    event_mask
        Specifies the event mask for which to call or unregister this
        procedure.

    nonmaskable
        Specifies a Boolean value that indicates whether this procedure should
        be called or removed on the nonmaskable events (GraphicsExpose,
        NoExpose, SelectionClear, SelectionRequest, SelectionNotify,
        ClientMessage, and MappingNotify).

    proc
        Specifies the procedure that is to be added or removed.

    w
        Specifies the widget for which this event handler is being registered.

    position
        Specifies when the event handler is to be called relative to other
        previously registered handlers.

  DDEESSCCRRIIPPTTIIOONN

    The XXttAAddddEEvveennttHHaannddlleerr function registers a procedure with the dispatch
    mechanism that is to be called when an event that matches the mask occurs
    on the specified widget. If the procedure is already registered with the
    same client_data, the specified mask is ORed into the existing mask. If
    the widget is realized, XXttAAddddEEvveennttHHaannddlleerr calls XXSSeelleeccttIInnppuutt, if
    necessary.

    The XXttAAddddRRaawwEEvveennttHHaannddlleerr function is similar to XXttAAddddEEvveennttHHaannddlleerr except
    that it does not affect the widget's mask and never causes an XXSSeelleeccttIInnppuutt
    for its events. Note that the widget might already have those mask bits
    set because of other nonraw event handlers registered on it.

    The XXttRReemmoovveeRRaawwEEvveennttHHaannddlleerr function stops the specified procedure from
    receiving the specified events. Because the procedure is a raw event
    handler, this does not affect the widget's mask and never causes a call on
    XXSSeelleeccttIInnppuutt.

    XXttIInnsseerrttEEvveennttHHaannddlleerr is identical to XXttAAddddEEvveennttHHaannddlleerr with the additional
    position argument. if position is XtListHead, the event handler is
    registered to that it will be called before any event handlers that were
    previously registered for the same widget. If position is XtListTail, the
    event handler is registered to be called after any previously registered
    event handlers. If the procedure is already registered with the same
    client_data value, the specified mask augments the existing mask and the
    procedure is repositioned in the list.

    XXttIInnsseerrttRRaawwEEvveennttHHaannddlleerr is similar to XXttIInnsseerrttEEvveennttHHaannddlleerr except that it
    does not modify the widget's event mask and never causes an XXSSeelleeccttIInnppuutt
    for the specified events. If the procedure is already registered with the
    same client_data value, the specified mask augments the existing mask and
    the procedure is repositioned in the list.

  SSEEEE AALLSSOO

    _X_t_A_p_p_N_e_x_t_E_v_e_n_t(3X11R6)

    _X_t_B_u_i_l_d_E_v_e_n_t_M_a_s_k(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

