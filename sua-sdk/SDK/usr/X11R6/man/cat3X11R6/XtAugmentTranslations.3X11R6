XtAugmentTranslations(3X11R6)             XtAugmentTranslations(3X11R6)

  XXttPPaarrsseeTTrraannssllaattiioonnTTaabbllee

  NNAAMMEE

    XtParseTranslationTable, XtAugmentTranslations, XtOverrideTranslations,
    XtUninstallTranslations - manage translation tables

  SSYYNNTTAAXX

    XtTranslations XtParseTranslationTable(table)
          String table;

    void XtAugmentTranslations(w, translations)
          Widget w;
          XtTranslations translations;

    void XtOverrideTranslations(w, translations)
          Widget w;
          XtTranslations translations;

    void XtUninstallTranslations(w)
          Widget w;

  AARRGGUUMMEENNTTSS

    table
        Specifies the translation table to compile.

    translations
        Specifies the compiled translation table to merge in (must not be
        NULL).

    w
        Specifies the widget into which the new translations are to be merged
        or removed.

  DDEESSCCRRIIPPTTIIOONN

    The XXttPPaarrsseeTTrraannssllaattiioonnTTaabbllee function compiles the translation table into
    the opaque internal representation of type XtTranslations. Note that if an
    empty translation table is required for any purpose, one can be obtained
    by calling XXttPPaarrsseeTTrraannssllaattiioonnTTaabbllee and passing an empty string.

    The XXttAAuuggmmeennttTTrraannssllaattiioonnss function nondestructively merges the new
    translations into the existing widget translations. If the new
    translations contain an event or event sequence that already exists in the
    widget's translations, the new translation is ignored.

    The XXttOOvveerrrriiddeeTTrraannssllaattiioonnss function destructively merges the new
    translations into the existing widget translations. If the new
    translations contain an event or event sequence that already exists in the
    widget's translations, the new translation is merged in and override the
    widget's translation.

    To replace a widget's translations completely, use XXttSSeettVVaalluueess on the
    XtNtranslations resource and specify a compiled translation table as the
    value.

    The XXttUUnniinnssttaallllTTrraannssllaattiioonnss function causes the entire translation table
    for widget to be removed.

  SSEEEE AALLSSOO

    _X_t_A_p_p_A_d_d_A_c_t_i_o_n_s(3X11R6)

    _X_t_C_r_e_a_t_e_P_o_p_u_p_S_h_e_l_l(3X11R6)

    _X_t_P_a_r_s_e_A_c_c_e_l_e_r_a_t_o_r_T_a_b_l_e(3X11R6)

    _X_t_P_o_p_u_p(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

