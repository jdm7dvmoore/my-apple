XLookupString(3X11R6)                             XLookupString(3X11R6)

  XXLLooookkuuppKKeeyyssyymm

  NNAAMMEE

    XLookupKeysym, XRefreshKeyboardMapping, XLookupString, XRebindKeySym -
    handle keyboard input events in Latin-1

  SSYYNNTTAAXX

    KeySym XLookupKeysym(key_event, index)
          XKeyEvent *key_event;
          int index;

    XRefreshKeyboardMapping(event_map)
          XMappingEvent *event_map;

    int XLookupString(event_struct,
    buffer_return, bytes_buffer, keysym_return, status_in_out)
          XKeyEvent *event_struct;
          char *buffer_return;
          int bytes_buffer;
          KeySym *keysym_return;
          XComposeStatus *status_in_out;

    XRebindKeysym(display, keysym, list, mod_count, string, num_bytes)
          Display *display;
          KeySym keysym;
          KeySym list[];
          int mod_count;
          unsigned char *string;
          int num_bytes;

  AARRGGUUMMEENNTTSS

    buffer_return
        Returns the translated characters.

    bytes_buffer
        Specifies the length of the buffer. No more than bytes_buffer of
        translation are returned.

    num_bytes
        Specifies the number of bytes in the string argument.

    display
        Specifies the connection to the X server.

    event_map
        Specifies the mapping event that is to be used.

    event_struct
        Specifies the key event structure to be used. You can pass
        XKeyPressedEvent or XKeyReleasedEvent.

    index
        Specifies the index into the KeySyms list for the event's KeyCode.

    key_event
        Specifies the KeyPress or KeyRelease event.

    keysym
        Specifies the KeySym that is to be .

    keysym_return
        Returns the KeySym computed from the event if this argument is not
        NULL.

    list
        Specifies the KeySyms to be used as modifiers.

    mod_count
        Specifies the number of modifiers in the modifier list.

    status_in_out
        Specifies or returns the XComposeStatus structure or NULL.

    string
        Specifies the string that is copied and will be returned by
        XXLLooookkuuppSSttrriinngg.

  DDEESSCCRRIIPPTTIIOONN

    The XXLLooookkuuppKKeeyyssyymm function uses a given keyboard event and the index you
    specified to return the KeySym from the list that corresponds to the
    KeyCode member in the XKeyPressedEvent or XKeyReleasedEvent structure. If
    no KeySym is defined for the KeyCode of the event, XXLLooookkuuppKKeeyyssyymm returns
    NoSymbol.

    The XXRReeffrreesshhKKeeyybbooaarrddMMaappppiinngg function refreshes the stored modifier and
    keymap information. You usually call this function when a MappingNotify
    event with a request member of MappingKeyboard or MappingModifier occurs.
    The result is to update Xlib's knowledge of the keyboard.

    The XXLLooookkuuppSSttrriinngg function translates a key event to a KeySym and a
    string. The KeySym is obtained by using the standard interpretation of the
    Shift, Lock, group, and numlock modifiers as defined in the X Protocol
    specification. If the KeySym has been rebound (see XXRReebbiinnddKKeeyySSyymm), the
    bound string will be stored in the buffer. Otherwise, the KeySym is
    mapped, if possible, to an ISO Latin-1 character or (if the Control
    modifier is on) to an ASCII control character, and that character is
    stored in the buffer. XXLLooookkuuppSSttrriinngg returns the number of characters that
    are stored in the buffer.

    If present (non-NULL), the XComposeStatus structure records the state,
    which is private to Xlib, that needs preservation across calls to
    XXLLooookkuuppSSttrriinngg to implement compose processing. The creation of
    XComposeStatus structures is implementation-dependent; a portable program
    must pass NULL for this argument.

    The XXRReebbiinnddKKeeyySSyymm function can be used to rebind the meaning of a KeySym
    for the client. It does not redefine any key in the X server but merely
    provides an easy way for long strings to be attached to keys.
    XXLLooookkuuppSSttrriinngg returns this string when the appropriate set of modifier
    keys are pressed and when the KeySym would have been used for the
    translation. No text conversions are performed; the client is responsible
    for supplying appropriately encoded strings. Note that you can rebind a
    KeySym that may not exist.

  SSEEEE AALLSSOO

    _X_B_u_t_t_o_n_E_v_e_n_t(3X11R6)

    _X_M_a_p_E_v_e_n_t(3X11R6)

    _X_S_t_r_i_n_g_T_o_K_e_y_s_y_m(3X11R6)

    Xlib - C Language X Interface

