XUnmapWindow(3X11R6)                               XUnmapWindow(3X11R6)

  XXUUnnmmaappWWiinnddooww

  NNAAMMEE

    XUnmapWindow, XUnmapSubwindows - unmap windows

  SSYYNNTTAAXX

    XUnmapWindow(display, w)
          Display *display;
          Window w;

    XUnmapSubwindows(display, w)
          Display *display;
          Window w;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    w
        Specifies the window.

  DDEESSCCRRIIPPTTIIOONN

    The XXUUnnmmaappWWiinnddooww function unmaps the specified window and causes the X
    server to generate an UnmapNotify event. If the specified window is
    already unmapped, XXUUnnmmaappWWiinnddooww has no effect. Normal exposure processing
    on formerly obscured windows is performed. Any child window will no longer
    be visible until another map call is made on the parent. In other words,
    the subwindows are still mapped but are not visible until the parent is
    mapped. Unmapping a window will generate Expose events on windows that
    were formerly obscured by it.

    XXUUnnmmaappWWiinnddooww can generate a BadWindow error.

    The XXUUnnmmaappSSuubbwwiinnddoowwss function unmaps all subwindows for the specified
    window in bottom-to-top stacking order. It causes the X server to generate
    an UnmapNotify event on each subwindow and Expose events on formerly
    obscured windows. Using this function is much more efficient than
    unmapping multiple windows one at a time because the server needs to
    perform much of the work only once, for all of the windows, rather than
    for each window.

    XXUUnnmmaappSSuubbwwiinnddoowwss can generate a BadWindow error.

  DDIIAAGGNNOOSSTTIICCSS

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_C_h_a_n_g_e_W_i_n_d_o_w_A_t_t_r_i_b_u_t_e_s(3X11R6)

    _X_C_o_n_f_i_g_u_r_e_W_i_n_d_o_w(3X11R6)

    _X_C_r_e_a_t_e_W_i_n_d_o_w(3X11R6)

    _X_D_e_s_t_r_o_y_W_i_n_d_o_w(3X11R6)

    _X_M_a_p_W_i_n_d_o_w(3X11R6)

    _X_R_a_i_s_e_W_i_n_d_o_w(3X11R6)

    Xlib - C Language X Interface

