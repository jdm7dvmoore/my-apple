XSetFont(3X11R6)                                       XSetFont(3X11R6)

  XXSSeettFFoonntt

  NNAAMMEE

    XSetFont - GC convenience routines

  SSYYNNTTAAXX

    XSetFont(display, gc, font)
          Display *display;
          GC gc;
          Font font;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    font
        Specifies the font.

    gc
        Specifies the GC.

  DDEESSCCRRIIPPTTIIOONN

    The XXSSeettFFoonntt function sets the current font in the specified GC.

    XXSSeettFFoonntt can generate BadAlloc, BadFont, and BadGC errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadFont
        A value for a Font or GContext argument does not name a defined Font.

    BadGC
        A value for a GContext argument does not name a defined GContext.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_G_C(3X11R6)

    _X_Q_u_e_r_y_B_e_s_t_S_i_z_e(3X11R6)

    _X_S_e_t_A_r_c_M_o_d_e(3X11R6)

    _X_S_e_t_C_l_i_p_O_r_i_g_i_n(3X11R6)

    _X_S_e_t_F_i_l_l_S_t_y_l_e(3X11R6)

    _X_S_e_t_L_i_n_e_A_t_t_r_i_b_u_t_e_s(3X11R6)

    _X_S_e_t_S_t_a_t_e(3X11R6)

    _X_S_e_t_T_i_l_e(3X11R6)

    Xlib - C Language X Interface

