XSetTransientForHint(3X11R6)               XSetTransientForHint(3X11R6)

  XXSSeettTTrraannssiieennttFFoorrHHiinntt

  NNAAMMEE

    XSetTransientForHint, XGetTransientForHint - set or read a window's
    WM_TRANSIENT_FOR property

  SSYYNNTTAAXX

    XSetTransientForHint(display, w, prop_window)
          Display *display;
          Window w;
          Window prop_window;

    Status XGetTransientForHint(display, w, prop_window_return)
          Display *display;
          Window w;
          Window *prop_window_return;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    w
        Specifies the window.

    prop_window
        Specifies the window that the WM_TRANSIENT_FOR property is to be set
        to.

    prop_window_return
        Returns the WM_TRANSIENT_FOR property of the specified window.

  DDEESSCCRRIIPPTTIIOONN

    The XXSSeettTTrraannssiieennttFFoorrHHiinntt function sets the WM_TRANSIENT_FOR property of
    the specified window to the specified prop_window.

    XXSSeettTTrraannssiieennttFFoorrHHiinntt can generate BadAlloc and BadWindow errors.

    The XXGGeettTTrraannssiieennttFFoorrHHiinntt function returns the WM_TRANSIENT_FOR property
    for the specified window. It returns a nonzero status on success;
    otherwise, it returns a zero status.

    XXGGeettTTrraannssiieennttFFoorrHHiinntt can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_TRANSIENT_FOR
        Set by application programs to indicate to the window manager that a
        transient top-level window, such as a dialog box.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t(3X11R6)

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e(3X11R6)

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s(3X11R6)

    _X_A_l_l_o_c_W_M_H_i_n_t_s(3X11R6)

    _X_S_e_t_C_o_m_m_a_n_d(3X11R6)

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e(3X11R6)

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s(3X11R6)

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s(3X11R6)

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s(3X11R6)

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    Xlib - C Language X Interface

