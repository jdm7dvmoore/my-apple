XcmsStoreColor(3X11R6)                           XcmsStoreColor(3X11R6)

  XXccmmssSSttoorreeCCoolloorr

  NNAAMMEE

    XcmsStoreColor, XcmsStoreColors - set colors

  SSYYNNTTAAXX

    Status XcmsStoreColor(display, colormap, color)
          Display *display;
          Colormap colormap;
          XcmsColor *color;

    Status XcmsStoreColors(display, colormap, colors, ncolors,
    compression_flags_return)
          Display *display;
          Colormap colormap;
          XcmsColor colors[];
          int ncolors;
          Bool compression_flags_return[];

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    color
        Specifies the color cell and the color to store. Values specified in
        this XXccmmssCCoolloorr structure remain unchanged on return.

    colors
        Specifies the color specification array of XXccmmssCCoolloorr structures, each
        specifying a color cell and the color to store in that cell. Values
        specified in the array remain unchanged upon return.

    colormap
        Specifies the colormap.

    compression_flags_return
        Returns an array of Boolean values indicating compression status. If a
        non-NULL pointer is supplied, each element of the array is set to True
        if the corresponding color was compressed and False otherwise. Pass
        NULL if the compression status is not useful.

    ncolors
        Specifies the number of XXccmmssCCoolloorr structures in the color-
        specification array.

  DDEESSCCRRIIPPTTIIOONN

    The XXccmmssSSttoorreeCCoolloorr function converts the color specified in the XXccmmssCCoolloorr
    structure into RGB values. It then uses this RGB specification in an
    XXCCoolloorr structure, whose three flags (DoRed, DoGreen, and DoBlue) are set,
    in a call to XXSSttoorreeCCoolloorr to change the color cell specified by the pixel
    member of the XXccmmssCCoolloorr structure. This pixel value must be a valid index
    for the specified colormap, and the color cell specified by the pixel
    value must be a read/write cell. If the pixel value is not a valid index,
    a BadValue error results. If the color cell is unallocated or is allocated
    read-only, a BadAccess error results. If the colormap is an installed map
    for its screen, the changes are visible immediately.

    Note that XXSSttoorreeCCoolloorr has no return value; therefore, an XcmsSuccess
    return value from this function indicates that the conversion to RGB
    succeeded and the call to XXSSttoorreeCCoolloorr was made. To obtain the actual color
    stored, use XXccmmssQQuueerryyCCoolloorr. Because of the screen's hardware limitations
    or gamut compression, the color stored in the colormap may not be
    identical to the color specified.

    XXccmmssSSttoorreeCCoolloorr can generate BadAccess, BadColor, and BadValue errors.

    The XXccmmssSSttoorreeCCoolloorrss function converts the colors specified in the array of
    XXccmmssCCoolloorr structures into RGB values and then uses these RGB
    specifications in XXCCoolloorr structures, whose three flags (DoRed, DoGreen,
    and DoBlue) are set, in a call to XXSSttoorreeCCoolloorrss to change the color cells
    specified by the pixel member of the corresponding XXccmmssCCoolloorr structure.
    Each pixel value must be a valid index for the specified colormap, and the
    color cell specified by each pixel value must be a read/write cell. If a
    pixel value is not a valid index, a BadValue error results. If a color
    cell is unallocated or is allocated read-only, a BadAccess error results.
    If more than one pixel is in error, the one that gets reported is
    arbitrary. If the colormap is an installed map for its screen, the changes
    are visible immediately.

    Note that XXSSttoorreeCCoolloorrss has no return value; therefore, an XcmsSuccess
    return value from this function indicates that conversions to RGB
    succeeded and the call to XXSSttoorreeCCoolloorrss was made. To obtain the actual
    colors stored, use XXccmmssQQuueerryyCCoolloorrss. Because of the screen's hardware
    limitations or gamut compression, the colors stored in the colormap may
    not be identical to the colors specified.

    XXccmmssSSttoorreeCCoolloorrss can generate BadAccess, BadColor, and BadValue errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadAccess
        A client attempted to free a color map entry that it did not already
        allocate.

    BadAccess
        A client attempted to store into a read-only color map entry.

    BadColor
        A value for a Colormap argument does not name a defined Colormap.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_c_m_s_A_l_l_o_c_C_o_l_o_r(3X11R6)

    _X_c_m_s_Q_u_e_r_y_C_o_l_o_r(3X11R6)

    Xlib - C Language X Interface

