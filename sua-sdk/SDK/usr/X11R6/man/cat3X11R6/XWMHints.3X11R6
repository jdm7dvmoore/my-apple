XWMHints(3X11R6)                                       XWMHints(3X11R6)

  XXAAllllooccWWMMHHiinnttss

  NNAAMMEE

    XAllocWMHints, XSetWMHints, XGetWMHints, XWMHints - allocate window
    manager hints structure and set or read a window's WM_HINTS property

  SSYYNNTTAAXX

    XWMHints *XAllocWMHints()

    XSetWMHints(display, w, wmhints)
          Display *display;
          Window w;
          XWMHints *wmhints;

    XWMHints *XGetWMHints(display, w)
          Display *display;
          Window w;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    w
        Specifies the window.

    wmhints
        Specifies the XXWWMMHHiinnttss structure to be used.

  DDEESSCCRRIIPPTTIIOONN

    The XXAAllllooccWWMMHHiinnttss function allocates and returns a pointer to a XXWWMMHHiinnttss
    structure. Note that all fields in the XXWWMMHHiinnttss structure are initially
    set to zero. If insufficient memory is available, XXAAllllooccWWMMHHiinnttss returns
    NULL. To free the memory allocated to this structure, use XXFFrreeee.

    The XXSSeettWWMMHHiinnttss function sets the window manager hints that include icon
    information and location, the initial state of the window, and whether the
    application relies on the window manager to get keyboard input.

    XXSSeettWWMMHHiinnttss can generate BadAlloc and BadWindow errors.

    The XXGGeettWWMMHHiinnttss function reads the window manager hints and returns NULL
    if no WM_HINTS property was set on the window or returns a pointer to a
    XXWWMMHHiinnttss structure if it succeeds. When finished with the data, free the
    space used for it by calling XXFFrreeee.

    XXGGeettWWMMHHiinnttss can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_HINTS
        Additional hints set by the client for use by the window manager. The
        C type of this property is XXWWMMHHiinnttss.

  SSTTRRUUCCTTUURREESS

    The XXWWMMHHiinnttss structure contains:

    /* Window manager hints mask bits */
    #define     InputHint            (1L << 0)

    #define     StateHint            (1L << 1)

    #define     IconPixmapHint       (1L << 2)

    #define     IconWindowHint       (1L << 3)

    #define     IconPositionHint     (1L << 4)

    #define     IconMaskHint         (1L << 5)

    #define     WindowGroupHint      (1L << 6)

    #define     UrgencyHint          (1L << 8)

    #define     AllHints             (InputHint|StateHint|IconPixmapHint|
                                     IconWindowHint|IconPositionHint|
                                     IconMaskHint|WindowGroupHint)

    /* Values */

    typedef struct {
         long flags;         /* marks which fields in this structure are
    defined */
         Bool input;         /* does this application rely on the window
    manager to
                             get keyboard input? */
         int initial_state;  /* see below */
         Pixmap icon_pixmap; /* pixmap to be used as icon */
         Window icon_window; /* window to be used as icon */
         int icon_x, icon_y; /* initial position of icon */
         Pixmap icon_mask;   /* pixmap to be used as mask for icon_pixmap */
         XID window_group;   /* id of related window group */
         /* this structure may be extended in the future */
    } XWMHints;

    The input member is used to communicate to the window manager the input
    focus model used by the application. Applications that expect input but
    never explicitly set focus to any of their subwindows (that is, use the
    push model of focus management), such as X Version 10 style applications
    that use real-estate driven focus, should set this member to True.
    Similarly, applications that set input focus to their subwindows only when
    it is given to their top-level window by a window manager should also set
    this member to True. Applications that manage their own input focus by
    explicitly setting focus to one of their subwindows whenever they want
    keyboard input (that is, use the pull model of focus management) should
    set this member to False. Applications that never expect any keyboard
    input also should set this member to False.

    Pull model window managers should make it possible for push model
    applications to get input by setting input focus to the top-level windows
    of applications whose input member is True. Push model window managers
    should make sure that pull model applications do not break them by
    resetting input focus to PointerRoot when it is appropriate (for example,
    whenever an application whose input member is False sets input focus to
    one of its subwindows).

    The definitions for the initial_state flag are:
    #define     WithdrawnState     0

    #define     NormalState        1     /* most applications start this way
                                         */

    #define     IconicState        3     /* application wants to start as an
                                         icon */
    The icon_mask specifies which pixels of the icon_pixmap should be used as
    the icon. This allows for nonrectangular icons. Both icon_pixmap and
    icon_mask must be bitmaps. The icon_window lets an application provide a
    window for use as an icon for window managers that support such use. The
    window_group lets you specify that this window belongs to a group of other
    windows. For example, if a single application manipulates multiple top-
    level windows, this allows you to provide enough information that a window
    manager can iconify all of the windows rather than just the one window.

    The UrgencyHint flag, if set in the flags field, indicates that the client
    deems the window contents to be urgent, requiring the timely response of
    the user. The window manager will make some effort to draw the user's
    attention to this window while this flag is set. The client must provide
    some means by which the user can cause the urgency flag to be cleared
    (either mitigating the condition that made the window urgent or merely
    shutting off the alarm) or the window to be withdrawn.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t(3X11R6)

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e(3X11R6)

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s(3X11R6)

    _X_F_r_e_e(3X11R6)

    _X_S_e_t_C_o_m_m_a_n_d(3X11R6)

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t(3X11R6)

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e(3X11R6)

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s(3X11R6)

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s(3X11R6)

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s(3X11R6)

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    Xlib - C Language X Interface

