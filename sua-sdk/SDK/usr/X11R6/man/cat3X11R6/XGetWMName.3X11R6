XGetWMName(3X11R6)                                   XGetWMName(3X11R6)

  XXSSeettWWMMNNaammee

  NNAAMMEE

    XSetWMName, XGetWMName, XStoreName, XFetchName - set or read a window's
    WM_NAME property

  SSYYNNTTAAXX

    void XSetWMName(display, w, text_prop)
          Display *display;
          Window w;
          XTextProperty *text_prop;

    Status XGetWMName(display, w, text_prop_return)
          Display *display;
          Window w;
          XTextProperty *text_prop_return;

    XStoreName(display, w, window_name)
          Display *display;
          Window w;
          char *window_name;

    Status XFetchName(display, w, window_name_return)
          Display *display;
          Window w;
          char **window_name_return;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    text_prop
        Specifies the XXTTeexxttPPrrooppeerrttyy structure to be used.

    text_prop_return
        Returns the XXTTeexxttPPrrooppeerrttyy structure.

    w
        Specifies the window.

    window_name
        Specifies the window name, which should be a null-terminated string.

    window_name_return
        Returns the window name, which is a null-terminated string.

  DDEESSCCRRIIPPTTIIOONN

    The XXSSeettWWMMNNaammee convenience function calls XXSSeettTTeexxttPPrrooppeerrttyy to set the
    WM_NAME property.

    The XXGGeettWWMMNNaammee convenience function calls XXGGeettTTeexxttPPrrooppeerrttyy to obtain the
    WM_NAME property. It returns a nonzero status on success; otherwise, it
    returns a zero status.

    The XXSSttoorreeNNaammee function assigns the name passed to window_name to the
    specified window. A window manager can display the window name in some
    prominent place, such as the title bar, to allow users to identify windows
    easily. Some window managers may display a window's name in the window's
    icon, although they are encouraged to use the window's icon name if one is
    provided by the application. If the string is not in the Host Portable
    Character Encoding, the result is implementation-dependent.

    XXSSttoorreeNNaammee can generate BadAlloc and BadWindow errors.

    The XXFFeettcchhNNaammee function returns the name of the specified window. If it
    succeeds, it returns a nonzero status; otherwise, no name has been set for
    the window, and it returns zero. If the WM_NAME property has not been set
    for this window, XXFFeettcchhNNaammee sets window_name_return to NULL. If the data
    returned by the server is in the Latin Portable Character Encoding, then
    the returned string is in the Host Portable Character Encoding. Otherwise,
    the result is implementation-dependent. When finished with it, a client
    must free the window name string using XXFFrreeee.

    XXFFeettcchhNNaammee can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_NAME
        The name of the application.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t(3X11R6)

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e(3X11R6)

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s(3X11R6)

    _X_A_l_l_o_c_W_M_H_i_n_t_s(3X11R6)

    _X_F_r_e_e(3X11R6)

    _X_S_e_t_C_o_m_m_a_n_d(3X11R6)

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t(3X11R6)

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e(3X11R6)

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s(3X11R6)

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s(3X11R6)

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s(3X11R6)

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    Xlib - C Language X Interface

