XwcTextEscapement(3X11R6)                     XwcTextEscapement(3X11R6)

  XXmmbbTTeexxttEEssccaappeemmeenntt

  NNAAMMEE

    XmbTextEscapement, XwcTextEscapement - obtain the escapement of text

  SSYYNNTTAAXX

    int XmbTextEscapement(font_set, string, num_bytes)
          XFontSet font_set;
          char *string;
          int num_bytes;

    int XwcTextEscapement(font_set, string, num_wchars)
          XFontSet font_set;
          wchar_t *string;
          int num_wchars;

  AARRGGUUMMEENNTTSS

    font_set
        Specifies the font set.

    num_bytes
        Specifies the number of bytes in the string argument.

    num_wchars
        Specifies the number of characters in the string argument.

    string
        Specifies the character string.

  DDEESSCCRRIIPPTTIIOONN

    The XXmmbbTTeexxttEEssccaappeemmeenntt and XXwwccTTeexxttEEssccaappeemmeenntt functions return the
    escapement in pixels of the specified string as a value, using the fonts
    loaded for the specified font set. The escapement is the distance in
    pixels in the primary draw direction from the drawing origin to the origin
    of the next character to be drawn, assuming that the rendering of the next
    character is not dependent on the supplied string.

    Regardless of the character rendering order, the escapement is always
    positive.

  SSEEEE AALLSSOO

    _X_m_b_T_e_x_t_E_x_t_e_n_t_s(3X11R6)

    _X_m_b_T_e_x_t_P_e_r_C_h_a_r_E_x_t_e_n_t_s(3X11R6)

    Xlib - C Language X Interface

