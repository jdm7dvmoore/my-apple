XtErrorMsg(3X11R6)                                   XtErrorMsg(3X11R6)

  XXttEErrrroorrMMssgg

  NNAAMMEE

    XtErrorMsg, XtSetErrorMsgHandler, XtWarningMsg, XtSetWarningMsgHandler -
    high-level error handlers

  SSYYNNTTAAXX

    void XtErrorMsg(name, type, class, default, params, num_params)
        String name;
        String type;
        String class;
        String default;
        String *params;
        Cardinal *num_params;

    void XtSetErrorMsgHandler(msg_handler)
          XtErrorMsgHandler msg_handler;

    void XtSetWarningMsgHandler(msg_handler)
          XtErrorMsgHandler msg_handler;

    void XtWarningMsg(name, type, class, default, params, num_params)
        String name;
        String type;
        String class;
        String default;
        String *params;
        Cardinal *num_params;

  AARRGGUUMMEENNTTSS

    class
        Specifies the resource class.

    default
        Specifies the default message to use.

    name
        Specifies the general kind of error.

    type
        Specifies the detailed name of the error.

    msg_handler
        Specifies the new fatal error procedure, which should not return or
        the nonfatal error procedure, which usually returns.

    num_params
        Specifies the number of values in the parameter list.

    params
        Specifies a pointer to a list of values to be stored in the message.

  DDEESSCCRRIIPPTTIIOONN

    The XXttEErrrroorrMMssgg function has been superceded by XXttAAppppEErrrroorrMMssgg.

    The XXttSSeettEErrrroorrMMssggHHaannddlleerr function has been superceded by
    XXttAAppppSSeettEErrrroorrMMssggHHaannddlleerr.

    The XXttSSeettWWaarrnniinnggMMssggHHaannddlleerr function has been superceded by
    XXttAAppppSSeettWWaarrnniinnggMMssggHHaannddlleerr.

    The XXttWWaarrnniinnggMMssgg function has been superceded by XXttAAppppWWaarrnniinnggMMssgg

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

