XmbufChangeBufferAttributes(3X11R6)      XmbufChangeBufferAttributes(3X11R6)

  XXMMBBUUFF

  NNAAMMEE

    XmbufQueryExtension, XmbufGetVersion, XmbufCreateBuffers,
    XmbufDestroyBuffers, XmbufDisplayBuffers, XmbufGetWindowAttributes,
    XmbufChangeWindowAttributes, XmbufGetBufferAttributes,
    XmbufChangeBufferAttributes, XmbufGetScreenInfo, XmbufCreateStereoWindow -
    X multibuffering functions

  SSYYNNTTAAXX

    #include <X11/extensions/multibuf.h>

    Bool XmbufQueryExtension(
        Display *dpy,
        Display *dpy,
        int *event_base_return,
        int *error_base_return);

    Status XmbufGetVersion(
        Display *dpy,
        int *major_version_return,
        int *minor_version_return);

    int XmbufCreateBuffers(
        Display *dpy,
        Window window,
        int count,
        int update_action,
        int update_hint,
        Multibuffer *buffers_update);

    void XmbufDestroyBuffers(
        Display *dpy,
        Window window);

    void XmbufDisplayBuffers(
        Display *dpy,
        int count,
        Multibuffer *buffers,
        int min_delay,
        int max_delay);

    Status XmbufGetWindowAttributes(
        Display *dpy,
        Window window,
        XmbufWindowAttributes *attributes);

    void XmbufChangeWindowAttributes(
        Display *dpy,
        Window window,
        unsigned long valuemask,
        XmbufSetWindowAttributes *attributes);

    Status XmbufGetBufferAttributes(
        Display *dpy,
        Multibuffer buffer,
        XmbufBufferAttributes *attributes);

    void XmbufChangeBufferAttributes(
        Display *dpy,
        Multibuffer buffer,
        unsigned long valuemask,
        XmbufSetBufferAttributes *attributes);

    Status XmbufGetScreenInfo(
        Display *dpy,
        Drawable drawable,
        int *nmono_return,
        XmbufBufferInfo **mono_info_return,
        int *nstereo_return,
        XmbufBufferInfo **stereo_info_return);

    Window XmbufCreateStereoWindow(
        Display *dpy,
        Window parent,
        int x,
        int y,
        unsigned int width,
        unsigned int height,
        unsigned int border_width,
        int depth,
        unsigned int class,                 /* InputOutput, InputOnly*/
        Visual *visual,
        unsigned long valuemask,
        XSetWindowAttributes *attributes,
        Multibuffer *left_return,
        Multibuffer *right_return);

  SSTTRRUUCCTTUURREESS

    Events:
    typedef struct {
        int type;                 /* of event */
        unsigned long serial;     /* # of last request processed by server */
        int send_event;           /* true if this came frome a SendEvent
    request */
        Display *display;         /* Display the event was read from */
        Multibuffer buffer;       /* buffer of event */
        int state;                /* see Clobbered constants above */
    } XmbufClobberNotifyEvent;

    typedef struct {
        int type;                 /* of event */
        unsigned long serial;     /* # of last request processed by server */
        int send_event;           /* true if this came frome a SendEvent
    request */
        Display *display;         /* Display the event was read from */
        Multibuffer buffer;       /* buffer of event */
    } XmbufUpdateNotifyEvent;

    Per-window attributes that can be got:
    typedef struct {
        int displayed_index;      /* which buffer is being displayed */
        int update_action;        /* Undefined, Background, Untouched, Copied
    */
        int update_hint;          /* Frequent, Intermittent, Static */
        int window_mode;          /* Mono, Stereo */
        int nbuffers;             /* Number of buffers */
        Multibuffer *buffers;     /* Buffers */
    } XmbufWindowAttributes;

    Per-window attributes that can be set:
    typedef struct {
        int update_hint;          /* Frequent, Intermittent, Static */
    } XmbufSetWindowAttributes;

    Per-buffer attributes that can be got:
    typedef struct {
        Window window;            /* which window this belongs to */
        unsigned long event_mask; /* events that have been selected */
        int buffer_index;         /* which buffer is this */
        int side;                 /* Mono, Left, Right */
    } XmbufBufferAttributes;

    Per-buffer attributes that can be set:
    typedef struct {
        unsigned long event_mask; /* events that have been selected */
    } XmbufSetBufferAttributes;

    Per-screen buffer info (there will be lists of them):
    typedef struct {
        VisualID visualid;        /* visual usuable at this depth */
        int max_buffers;          /* most buffers for this visual */
        int depth;                /* depth of buffers to be created */
    } XmbufBufferInfo;

  DDEESSCCRRIIPPTTIIOONN

    The application programming library for the X11 Double-Buffering, Multi-
    Buffering, and Stereo Extension contains the interfaces described below.
    With the exception of XXmmbbuuffQQuueerryyEExxtteennssiioonn, if any of these routines are
    called with a display that does not support the extension, the
    ExtensionErrorHandler (which can be set with XSetExtensionErrorHandler and
    functions the same way as XXSSeettEErrrroorrHHaannddlleerr) will be called and the
    function will then return.

    XXmmbbuuffQQuueerryyEExxtteennssiioonn returns True if the multibuffering/stereo extension is
    available on the given display. If the extension exists, the value of the
    first event code (which should be added to the event type constants
    MultibufferClobberNotify and MultibufferUpdateNotify to get the actual
    values) is stored into event_base_return and the value of the first error
    code (which should be added to the error type constant
    MultibufferBadBuffer to get the actual value) is stored into
    error_base_return.

    XXmmbbuuffGGeettVVeerrssiioonn gets the major and minor version numbers of the extension.
    The return value is zero if an error occurs or non-zero if no error
    happens.

    XXmmbbuuffCCrreeaatteeBBuuffffeerrss requests that "count" buffers be created with the given
    update_action and update_hint and be associated with the indicated window.
    The number of buffers created is returned (zero if an error occurred) and
    buffers_update is filled in with that many Multibuffer identifiers.

    XXmmbbuuffDDeessttrrooyyBBuuffffeerrss destroys the buffers associated with the given window.

    XXmmbbuuffDDiissppllaayyBBuuffffeerrss displays the indicated buffers their appropriate
    windows within max_delay milliseconds after min_delay milliseconds have
    passed. No two buffers may be associated with the same window or else a
    Matc error is generated.

    XXmmbbuuffGGeettWWiinnddoowwAAttttrriibbuutteess gets the multibuffering attributes that apply to
    all buffers associated with the given window. The list of buffers returns
    may be freed with XXFFrreeee. Returns non-zero on success and zero if an error
    occurs.

    XXmmbbuuffCChhaannggeeWWiinnddoowwAAttttrriibbuutteess sets the multibuffering attributes that apply
    to all buffers associated with the given window. This is currently limited
    to the update_hint.

    XXmmbbuuffGGeettBBuuffffeerrAAttttrriibbuutteess gets the attributes for the indicated buffer.
    Returns non-zero on success and zero if an error occurs.

    XXmmbbuuffCChhaannggeeBBuuffffeerrAAttttrriibbuutteess sets the attributes for the indicated buffer.
    This is currently limited to the event_mask.

    XXmmbbuuffGGeettSSccrreeeennIInnffoo gets the parameters controlling how mono and stereo
    windows may be created on the screen of the given drawable. The numbers of
    sets of visual and depths are returned in nmono_return and nstereo_return.
    If nmono_return is greater than zero, then mono_info_return is set to the
    address of an array of XmbufBufferInfo structures describing the various
    visuals and depths that may be used. Otherwise, mono_info_return is set to
    NULL. Similarly, stereo_info_return is set according to nstereo_return.
    The storage returned in mono_info_return and stereo_info_return may be
    released by XXFFrreeee. If no errors are encounted, non-zero will be returned.

    XXmmbbuuffCCrreeaatteeSStteerreeooWWiinnddooww creates a stereo window in the same way that
    XXCCrreeaatteeWWiinnddooww creates a mono window. The buffer ids for the left and right
    buffers are returned in left_return and right_return, respectively. If an
    extension error handler that returns is installed, None will be returned
    if the extension is not available on this display.

  PPRREEDDEEFFIINNEEDD VVAALLUUEESS

    Update_action field:

    MultibufferUpdateActionUndefined
    MultibufferUpdateActionBackground
    MultibufferUpdateActionUntouched
    MultibufferUpdateActionCopied

    Update_hint field:

    MultibufferUpdateHintFrequent
    MultibufferUpdateHintIntermittent
    MultibufferUpdateHintStatic

    Valuemask fields:

    MultibufferWindowUpdateHint
    MultibufferBufferEventMask

    Mono vs. stereo and left vs. right:

    MultibufferModeMono
    MultibufferModeStereo
    MultibufferSideMono
    MultibufferSideLeft
    MultibufferSideRight

    Clobber state:

    MultibufferUnclobbered
    MultibufferPartiallyClobbered
    MultibufferFullyClobbered

    Event stuff:

    MultibufferClobberNotifyMask
    MultibufferUpdateNotifyMask
    MultibufferClobberNotify
    MultibufferUpdateNotify
    MultibufferNumberEvents
    MultibufferBadBuffer
    MultibufferNumberErrors

  BBUUGGSS

    This manual page needs more work.

  SSEEEE AALLSSOO

    Extending X for Double Buffering, Multi-Buffering, and Stereo

