XtAppGetErrorDatabaseText(3X11R6)        XtAppGetErrorDatabaseText(3X11R6)

  XXttAAppppGGeettEErrrroorrDDaattaabbaassee

  NNAAMMEE

    XtAppGetErrorDatabase, XtAppGetErrorDatabaseText - obtain error database

  SSYYNNTTAAXX

    XrmDatabase *XtAppGetErrorDatabase(app_context)
         XtAppContext app_context;

    void XtAppGetErrorDatabaseText(app_context, name, type, class, default,
    buffer_return, nbytes, database)
           XtAppContext app_context;
           char *name, *type, *class;
           char *default;
           char *buffer_return;
           int nbytes;
           XrmDatabase database;

  AARRGGUUMMEENNTTSS

    app_context
        Specifies the application context.

    buffer_return
        Specifies the buffer into which the error message is to be returned.

    class
        Specifies the resource class of the error message.

    database
        Specifies the name of the alternative database that is to be used or
        NULL if the application's database is to be used.

    default
        Specifies the default message to use.
    name
    type
        Specifies the name and type that are concatenated to form the resource
        name of the error message.

    nbytes
        Specifies the size of the buffer in bytes.

  DDEESSCCRRIIPPTTIIOONN

    The XXttAAppppGGeettEErrrroorrDDaattaabbaassee function returns the address of the error
    database. The Intrinsics do a lazy binding of the error database and do
    not merge in the database file until the first call to
    XtAppGetErrorDatbaseText.

    The XXttAAppppGGeettEErrrroorrDDaattaabbaasseeTTeexxtt returns the appropriate message from the
    error database or returns the specified default message if one is not
    found in the error database.

  SSEEEE AALLSSOO

    _X_t_A_p_p_E_r_r_o_r(3X11R6)

    _X_t_A_p_p_E_r_r_o_r_M_s_g(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

