XConvertSelection(3X11R6)                     XConvertSelection(3X11R6)

  XXSSeettSSeelleeccttiioonnOOwwnneerr

  NNAAMMEE

    XSetSelectionOwner, XGetSelectionOwner, XConvertSelection - manipulate
    window selection

  SSYYNNTTAAXX

    XSetSelectionOwner(display, selection, owner, time)
          Display *display;
          Atom selection;
          Window owner; 
          Time time;

    Window XGetSelectionOwner(display, selection)
          Display *display;
          Atom selection;

    XConvertSelection(display, selection, target, property, requestor, time)
          Display *display;
          Atom selection, target;
          Atom property;
          Window requestor;
          Time time;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    owner
        Specifies the owner of the specified selection atom. You can pass a
        window or None.

    property
        Specifies the property name. You also can pass None.

    requestor
        Specifies the requestor.

    selection
        Specifies the selection atom.

    target
        Specifies the target atom.

    time
        Specifies the time. You can pass either a timestamp or CurrentTime.

  DDEESSCCRRIIPPTTIIOONN

    The XXSSeettSSeelleeccttiioonnOOwwnneerr function changes the owner and last-change time for
    the specified selection and has no effect if the specified time is earlier
    than the current last-change time of the specified selection or is later
    than the current X server time. Otherwise, the last-change time is set to
    the specified time, with CurrentTime replaced by the current server time.
    If the owner window is specified as None, then the owner of the selection
    becomes None (that is, no owner). Otherwise, the owner of the selection
    becomes the client executing the request.

    If the new owner (whether a client or None) is not the same as the current
    owner of the selection and the current owner is not None, the current
    owner is sent a SelectionClear event. If the client that is the owner of a
    selection is later terminated (that is, its connection is closed) or if
    the owner window it has specified in the request is later destroyed, the
    owner of the selection automatically reverts to None, but the last-change
    time is not affected. The selection atom is uninterpreted by the X server.
    XXGGeettSSeelleeccttiioonnOOwwnneerr returns the owner window, which is reported in
    SelectionRequest and SelectionClear events. Selections are global to the X
    server.

    XXSSeettSSeelleeccttiioonnOOwwnneerr can generate BadAtom and BadWindow errors.

    The XXGGeettSSeelleeccttiioonnOOwwnneerr function returns the window ID associated with the
    window that currently owns the specified selection. If no selection was
    specified, the function returns the constant None. If None is returned,
    there is no owner for the selection.

    XXGGeettSSeelleeccttiioonnOOwwnneerr can generate a BadAtom error.

    XXCCoonnvveerrttSSeelleeccttiioonn requests that the specified selection be converted to
    the specified target type:
    *     If the specified selection has an owner, the X server sends a
          SelectionRequest event to that owner.
    *     If no owner for the specified selection exists, the X server
          generates a SelectionNotify event to the requestor with property
          None.

    The arguments are passed on unchanged in either of the events. There are
    two predefined selection atoms: PRIMARY and SECONDARY.

    XXCCoonnvveerrttSSeelleeccttiioonn can generate BadAtom and BadWindow errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadAtom
        A value for an Atom argument does not name a defined Atom.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    Xlib - C Language X Interface

