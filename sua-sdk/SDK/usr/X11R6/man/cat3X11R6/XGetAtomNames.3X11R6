XGetAtomNames(3X11R6)                             XGetAtomNames(3X11R6)

  XXIInntteerrnnAAttoomm

  NNAAMMEE

    XInternAtom, XInternAtoms, XGetAtomName, XGetAtomNames - create or return
    atom names

  SSYYNNTTAAXX

    Atom XInternAtom(display, atom_name, only_if_exists)
          Display *display;
          char *atom_name;
          Bool only_if_exists;

    Status XInternAtoms(display, names, count, only_if_exists, atoms_return)
          Display *display;
          char **names;
          int count;
          Bool only_if_exists;
          Atom *atoms_return;

    char *XGetAtomName(display, atom)
          Display *display;
          Atom atom;

    Status XGetAtomNames(display, atoms, count, names_return)
          Display *display;
          Atom *atoms;
          int count;
          char **names_return;

  AARRGGUUMMEENNTTSS

    atom
        Specifies the atom for the property name you want returned.

    atoms
        Specifies the array of atoms.

    atom_name
        Specifies the name associated with the atom you want returned.

    atoms_return
        Returns the atoms.

    count
        Specifies the number of atom names in the array.

    count
        Specifies the number of atoms in the array.

    display
        Specifies the connection to the X server.

    names
        Specifies the array of atom names.

    names_return
        Returns the atom names.

    only_if_exists
        Specifies a Boolean value that indicates whether the atom must be
        created.

  DDEESSCCRRIIPPTTIIOONN

    The XXIInntteerrnnAAttoomm function returns the atom identifier associated with the
    specified atom_name string. If only_if_exists is False, the atom is
    created if it does not exist. Therefore, XXIInntteerrnnAAttoomm can return None. If
    the atom name is not in the Host Portable Character Encoding, the result
    is implementation-dependent. Uppercase and lowercase matter; the strings
    "thing", "Thing", and "thinG" all designate different atoms. The atom will
    remain defined even after the client's connection closes. It will become
    undefined only when the last connection to the X server closes.

    XXIInntteerrnnAAttoomm can generate BadAlloc and BadValue errors.

    The XXIInntteerrnnAAttoommss function returns the atom identifiers associated with the
    specified names. The atoms are stored in the atoms_return array supplied
    by the caller. Calling this function is equivalent to calling XXIInntteerrnnAAttoomm
    for each of the names in turn with the specified value of only_if_exists,
    but this function minimizes the number of round-trip protocol exchanges
    between the client and the X server.

    This function returns a nonzero status if atoms are returned for all of
    the names; otherwise, it returns zero.

    XXIInntteerrnnAAttoommss can generate BadAlloc and BadValue errors.

    The XXGGeettAAttoommNNaammee function returns the name associated with the specified
    atom. If the data returned by the server is in the Latin Portable
    Character Encoding, then the returned string is in the Host Portable
    Character Encoding. Otherwise, the result is implementation-dependent. To
    free the resulting string, call XXFFrreeee.

    XXGGeettAAttoommNNaammee can generate a BadAtom error.

    The XXGGeettAAttoommNNaammeess function returns the names associated with the specified
    atoms. The names are stored in the names_return array supplied by the
    caller. Calling this function is equivalent to calling XXGGeettAAttoommNNaammee for
    each of the atoms in turn, but this function minimizes the number of
    round-trip protocol exchanges between the client and the X server.

    This function returns a nonzero status if names are returned for all of
    the atoms; otherwise, it returns zero.

    XXGGeettAAttoommNNaammeess can generate a BadAtom error.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadAtom
        A value for an Atom argument does not name a defined Atom.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_F_r_e_e(3X11R6)

    _X_G_e_t_W_i_n_d_o_w_P_r_o_p_e_r_t_y(3X11R6)

    Xlib - C Language X Interface

