XcmsTekHVCQueryMaxV(3X11R6)                 XcmsTekHVCQueryMaxV(3X11R6)

  XXccmmssTTeekkHHVVCCQQuueerryyMMaaxxCC

  NNAAMMEE

    XcmsTekHVCQueryMaxC, XcmsTekHVCQueryMaxV, XcmsTekHVCQueryMaxVC,
    XcmsTekHVCQueryMaxVSamples, XcmsTekHVCQueryMinV - obtain the TekHVC
    coordinates

  SSYYNNTTAAXX

    Status XcmsTekHVCQueryMaxC(ccc, hue, value, color_return)
          XcmsCCC ccc;
          XcmsFloat hue;
          XcmsFloat value;
          XcmsColor *color_return;

    Status XcmsTekHVCQueryMaxV(ccc, hue, chroma, color_return)
          XcmsCCC ccc;
          XcmsFloat hue;
          XcmsFloat chroma;
          XcmsColor *color_return;

    Status XcmsTekHVCQueryMaxVC(ccc, hue, color_return)
          XcmsCCC ccc;
          XcmsFloat hue;
          XcmsColor *color_return;

    Status XcmsTekHVCQueryMaxVSamples(ccc, hue, colors_return, nsamples)
          XcmsCCC ccc;
          XcmsFloat hue;
          XcmsColor colors_return[];
          unsigned int nsamples;

    Status XcmsTekHVCQueryMinV(ccc, hue, chroma, color_return)
          XcmsCCC ccc;
          XcmsFloat hue;
          XcmsFloat chroma;
          XcmsColor *color_return;

  AARRGGUUMMEENNTTSS

    ccc
        Specifies the CCC. Note that the CCC's Client White Point and White
        Point Adjustment procedures are ignored.

    chroma
        Specifies the chroma at which to find maximum Value (MaxV).

    colors_return
        Returns nsamples of color specifications in XcmsTekHVC such that the
        Chroma is the maximum attainable for the Value and Hue. The white
        point associated with the returned color specification is the Screen
        White Point. The value returned in the pixel member is undefined.

    color_return
        Returns the maximum Chroma along with the actual Hue and Value (MaxC),
        maximum Value along with the Hue and Chroma (MaxV), color
        specification in XcmsTekHVC for the maximum Chroma, the Value at which
        that maximum Chroma is reached and actual Hue (MaxVC) or minimum Value
        and the actual Hue and Chroma (MinL) at which the maximum Chroma (MaxC
        and MaxVC), maximum Value (MaxV), or minimum Value (MinL) was found.
        The white point associated with the returned color specification is
        the Screen White Point. The value returned in the pixel member is
        undefined.

    hue
        Specifies the Hue in which to find the maximum Chroma (MaxC and
        MaxVC), maximum Value (MaxV), the maximum Chroma/Value samples
        (MaxVSamples), or the minimum Value (MinL).

    nsamples
        Specifies the number of samples.

    value
        Specifies the Value in which to find the maximum Chroma (MaxC) or
        minimum Value (MinL).

  DDEESSCCRRIIPPTTIIOONN

    The XXccmmssTTeekkHHVVCCQQuueerryyMMaaxxCC function, given a Hue and Value, determines the
    maximum Chroma in TekHVC color space displayable by the screen. It returns
    the maximum Chroma along with the actual Hue and Value at which the
    maximum Chroma was found.

    The XXccmmssTTeekkHHVVCCQQuueerryyMMaaxxVV function, given a Hue and Chroma, determines the
    maximum Value in TekHVC color space displayable by the screen. It returns
    the maximum Value and the actual Hue and Chroma at which the maximum Value
    was found.

    The XXccmmssTTeekkHHVVCCQQuueerryyMMaaxxVVCC function, given a Hue, determines the maximum
    Chroma in TekHVC color space displayable by the screen and the Value at
    which that maximum Chroma is reached. It returns the maximum Chroma, the
    Value at which that maximum Chroma is reached, and the actual Hue for
    which the maximum Chroma was found.

    The XXccmmssTTeekkHHVVCCQQuueerryyMMaaxxVVSSaammpplleess returns nsamples of maximum Value, the
    Chroma at which that maximum Value is reached, and the actual Hue for
    which the maximum Chroma was found. These sample points may then be used
    to plot the maximum Value/Chroma boundary of the screen's color gamut for
    the specified Hue in TekHVC color space.

    The XXccmmssTTeekkHHVVCCQQuueerryyMMiinnVV function, given a Hue and Chroma, determines the
    minimum Value in TekHVC color space displayable by the screen. It returns
    the minimum Value and the actual Hue and Chroma at which the minimum Value
    was found.

  SSEEEE AALLSSOO

    _X_c_m_s_C_I_E_L_a_b_Q_u_e_r_y_M_a_x_C(3X11R6)

    _X_c_m_s_C_I_E_L_u_v_Q_u_e_r_y_M_a_x_C(3X11R6)

    _X_c_m_s_Q_u_e_r_y_B_l_a_c_k(3X11R6)

    Xlib - C Language X Interface

