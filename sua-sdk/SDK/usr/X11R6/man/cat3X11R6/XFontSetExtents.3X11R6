XFontSetExtents(3X11R6)                         XFontSetExtents(3X11R6)

  XXFFoonnttSSeettEExxtteennttss

  NNAAMMEE

    XFontSetExtents - XFontSetExtents structure

  SSTTRRUUCCTTUURREESS

    The XXFFoonnttSSeettEExxtteennttss structure contains:

    typedef struct {
         XRectangle max_ink_extent;/* over all drawable characters */
         XRectangle max_logical_extent;/* over all drawable characters */
    } XFontSetExtents;

    The XXRReeccttaannggllee structures used to return font set metrics are the usual
    Xlib screen-oriented rectangles with x, y giving the upper left corner,
    and width and height always positive.

    The max_ink_extent member gives the maximum extent, over all drawable
    characters, of the rectangles that bound the character glyph image drawn
    in the foreground color, relative to a constant origin. See XXmmbbTTeexxttEExxtteennttss
    and XXwwccTTeexxttEExxtteennttss for detailed semantics.

    The max_logical_extent member gives the maximum extent, over all drawable
    characters, of the rectangles that specify minimum spacing to other
    graphical features, relative to a constant origin. Other graphical
    features drawn by the client, for example, a border surrounding the text,
    should not intersect this rectangle. The max_logical_extent member should
    be used to compute minimum interline spacing and the minimum area that
    must be allowed in a text field to draw a given number of arbitrary
    characters.

    Due to context-dependent rendering, appending a given character to a
    string may change the string's extent by an amount other than that
    character's individual extent.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_F_o_n_t_S_e_t(3X11R6)

    _X_E_x_t_e_n_t_s_O_f_F_o_n_t_S_e_t(3X11R6)

    _X_F_o_n_t_s_O_f_F_o_n_t_S_e_t(3X11R6)

    Xlib - C Language X Interface

