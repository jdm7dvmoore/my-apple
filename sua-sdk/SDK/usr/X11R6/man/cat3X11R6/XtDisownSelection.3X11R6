XtDisownSelection(3X11R6)                     XtDisownSelection(3X11R6)

  XXttOOwwnnSSeelleeccttiioonn

  NNAAMMEE

    XtOwnSelection, XtOwnSelectionIncremental, XtDisownSelection - set
    selection owner

  SSYYNNTTAAXX

    Boolean XtOwnSelection(w, selection, time, convert_proc, lose_selection,
    done_proc)
          Widget w;
          Atom selection;
          Time time;
          XtConvertSelectionProc convert_proc;
          XtLoseSelectionProc lose_selection;
          XtSelectionDoneProc done_proc;

    Boolean XtOwnSelectionIncremental(w, selection, time, convert_callback,
    lose_callback, done_callback, cancel_callback, client_data)
          Widget w;
          Atom selection;
          Time time;
          XtConvertSelectionIncrProc convert_callback;
          XtLoseSelectionIncrProc lose_callback;
          XtSelectionDoneIncrProc done_callback;
          XtCancelConvertSelectionProc cancel_callback;
          XtPointer client_data;

    void XtDisownSelection(w, selection, time)
          Widget w;
          Atom selection;
          Time time;

  AARRGGUUMMEENNTTSS

    convert_proc
        Specifies the procedure that is to be called whenever someone requests
        the current value of the selection.

    done_proc
        Specifies the procedure that is called after the requestor has
        received the selection or NULL if the owner is not interested in being
        called back.

    lose_selection
        Specifies the procedure that is to be called whenever the widget has
        lost selection ownership or NULL if the owner is not interested in
        being called back.

    selection
        Specifies an atom that describes the type of the selection (for
        example, XA_PRIMARY, XA_SECONDARY, or XA_CLIPBOARD).

    time
        Specifies the timestamp that indicates when the selection ownership
        should commence or is to be relinquished.

    w
        Specifies the widget that wishes to become the owner or to relinquish
        ownership.

  DDEESSCCRRIIPPTTIIOONN

    The XXttOOwwnnSSeelleeccttiioonn function informs the Intrinsics selection mechanism
    that a widget believes it owns a selection. It returns True if the widget
    has successfully become the owner and False otherwise. The widget may fail
    to become the owner if some other widget has asserted ownership at a time
    later than this widget. Note that widgets can lose selection ownership
    either because someone else asserted later ownership of the selection or
    because the widget voluntarily gave up ownership of the selection. Also
    note that the lose_selection procedure is not called if the widget fails
    to obtain selection ownership in the first place.

    The XtOwnSelectionInrcremental procedure informs the Intrinsics
    incremental selection mechanism that the specified widget wishes to own
    the selection. It returns True if the specified widget successfully
    becomes the selection owner or False otherwise. For more information about
    selection, target, and time, see Section 2.6 of the Inter-Client
    Communication Conventions Manual.

    A widget that becomes the selection owner using XXttOOwwnnSSeelleeccttiioonnIInnccrreemmeennttaall
    may use XXttDDiissoowwnnSSeelleeccttiioonn to relinquish selection ownership.

    The XXttDDiissoowwnnSSeelleeccttiioonn function informs the Intrinsics selection mechanism
    that the specified widget is to lose ownership of the selection. If the
    widget does not currently own the selection either because it lost the
    selection or because it never had the selection to begin with,
    XXttDDiissoowwnnSSeelleeccttiioonn does nothing.

    After a widget has called XXttDDiissoowwnnSSeelleeccttiioonn, its convert procedure is not
    called even if a request arrives later with a timestamp during the period
    that this widget owned the selection. However, its done procedure will be
    called if a conversion that started before the call to XXttDDiissoowwnnSSeelleeccttiioonn
    finishes after the call to XXttDDiissoowwnnSSeelleeccttiioonn.

  SSEEEE AALLSSOO

    _X_t_A_p_p_G_e_t_S_e_l_e_c_t_i_o_n_T_i_m_e_o_u_t(3X11R6)

    _X_t_G_e_t_S_e_l_e_c_t_i_o_n_V_a_l_u_e(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

