XContextualDrawing(3X11R6)                   XContextualDrawing(3X11R6)

  XXFFoonnttssOOffFFoonnttSSeett

  NNAAMMEE

    XFontsOfFontSet, XBaseFontNameListOfFontSet, XLocaleOfFontSet,
    XContextDependentDrawing, XContextualDrawing, XDirectionalDependentDrawing
    - obtain fontset information

  SSYYNNTTAAXX

    int XFontsOfFontSet(font_set, font_struct_list_return,
    font_name_list_return)
           XFontSet font_set;
           XFontStruct ***font_struct_list_return;
           char ***font_name_list_return;

    char *XBaseFontNameListOfFontSet(font_set)
          XFontSet font_set;

    char *XLocaleOfFontSet(font_set)
          XFontSet font_set;

    Bool XContextDependentDrawing(font_set)
          XFontSet font_set;

    Bool XContextualDrawing(font_set)
          XFontSet font_set;

    Bool XDirectionalDependentDrawing(font_set)
          XFontSet font_set;

  AARRGGUUMMEENNTTSS

    font_set
        Specifies the font set.

    font_name_list_return
        Returns the list of font names.

    font_struct_list_return
        Returns the list of font structs.

  DDEESSCCRRIIPPTTIIOONN

    The XXFFoonnttssOOffFFoonnttSSeett function returns a list of one or more XFontStructs
    and font names for the fonts used by the Xmb and Xwc layers for the given
    font set. A list of pointers to the XXFFoonnttSSttrruucctt structures is returned to
    font_struct_list_return. A list of pointers to null-terminated, fully
    specified font name strings in the locale of the font set is returned to
    font_name_list_return. The font_name_list order corresponds to the
    font_struct_list order. The number of XXFFoonnttSSttrruucctt structures and font
    names is returned as the value of the function.

    Because it is not guaranteed that a given character will be imaged using a
    single font glyph, there is no provision for mapping a character or
    default string to the font properties, font ID, or direction hint for the
    font for the character. The client may access the XXFFoonnttSSttrruucctt list to
    obtain these values for all the fonts currently in use.

    Xlib does not guarantee that fonts are loaded from the server at the
    creation of an XFontSet. Xlib may choose to cache font data, loading it
    only as needed to draw text or compute text dimensions. Therefore,
    existence of the per_char metrics in the XXFFoonnttSSttrruucctt structures in the
    XFontStructSet is undefined. Also, note that all properties in the
    XXFFoonnttSSttrruucctt structures are in the STRING encoding.

    The XXFFoonnttSSttrruucctt and font name lists are owned by Xlib and should not be
    modified or freed by the client. They will be freed by a call to
    XXFFrreeeeFFoonnttSSeett with the associated XFontSet. Until freed, their contents
    will not be modified by Xlib.

    The XXBBaasseeFFoonnttNNaammeeLLiissttOOffFFoonnttSSeett function returns the original base font
    name list supplied by the client when the XFontSet was created. A null-
    terminated string containing a list of comma-separated font names is
    returned as the value of the function. White space may appear immediately
    on either side of separating commas.

    If XXCCrreeaatteeFFoonnttSSeett obtained an XLFD name from the font properties for the
    font specified by a non-XLFD base name, the XXBBaasseeFFoonnttNNaammeeLLiissttOOffFFoonnttSSeett
    function will return the XLFD name instead of the non-XLFD base name.

    The base font name list is owned by Xlib and should not be modified or
    freed by the client. It will be freed by a call to XXFFrreeeeFFoonnttSSeett with the
    associated XFontSet. Until freed, its contents will not be modified by
    Xlib.

    The XXLLooccaalleeOOffFFoonnttSSeett function returns the name of the locale bound to the
    specified XFontSet, as a null-terminated string.

    The returned locale name string is owned by Xlib and should not be
    modified or freed by the client. It may be freed by a call to XXFFrreeeeFFoonnttSSeett
    with the associated XFontSet. Until freed, it will not be modified by
    Xlib.

    The XXCCoonntteexxttDDeeppeennddeennttDDrraawwiinngg function returns True if the drawing
    functions implement implicit text directionality or if text drawn with the
    font_set might include context-dependent drawing; otherwise, it returns
    False.

    The XXCCoonntteexxttuuaallDDrraawwiinngg function returns True if text drawn with the font
    set might include context-dependent drawing; otherwise, it returns False.

    The XXDDiirreeccttiioonnaallDDeeppeennddeennttDDrraawwiinngg function returns True if the drawing
    functions implement implicit text directionality; otherwise, it returns
    False.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_F_o_n_t_S_e_t(3X11R6)

    _X_C_r_e_a_t_e_O_C(3X11R6)

    _X_E_x_t_e_n_t_s_O_f_F_o_n_t_S_e_t(3X11R6)

    _X_F_o_n_t_S_e_t_E_x_t_e_n_t_s(3X11R6)

    Xlib - C Language X Interface

