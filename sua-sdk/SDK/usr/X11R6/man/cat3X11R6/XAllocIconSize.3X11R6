XAllocIconSize(3X11R6)                           XAllocIconSize(3X11R6)

  XXAAllllooccIIccoonnSSiizzee

  NNAAMMEE

    XAllocIconSize, XSetIconSizes, XGetIconSizes, XIconSize - allocate icon
    size structure and set or read a window's WM_ICON_SIZES property

  SSYYNNTTAAXX

    XIconSize *XAllocIconSize()

    XSetIconSizes(display, w, size_list, count)
          Display *display;
          Window w;
          XIconSize *size_list;
          int count;

    Status XGetIconSizes(display, w, size_list_return, count_return)
          Display *display;
          Window w;
          XIconSize **size_list_return;
          int *count_return;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    count
        Specifies the number of items in the size list.

    count_return
        Returns the number of items in the size list.

    size_list
        Specifies the size list.

    size_list_return
        Returns the size list.

    w
        Specifies the window.

  DDEESSCCRRIIPPTTIIOONN

    The XXAAllllooccIIccoonnSSiizzee function allocates and returns a pointer to a XXIIccoonnSSiizzee
    structure. Note that all fields in the XXIIccoonnSSiizzee structure are initially
    set to zero. If insufficient memory is available, XXAAllllooccIIccoonnSSiizzee returns
    NULL. To free the memory allocated to this structure, use XXFFrreeee.

    The XXSSeettIIccoonnSSiizzeess function is used only by window managers to set the
    supported icon sizes.

    XXSSeettIIccoonnSSiizzeess can generate BadAlloc and BadWindow errors.

    The XXGGeettIIccoonnSSiizzeess function returns zero if a window manager has not set
    icon sizes; otherwise, it return nonzero. XXGGeettIIccoonnSSiizzeess should be called
    by an application that wants to find out what icon sizes would be most
    appreciated by the window manager under which the application is running.
    The application should then use XXSSeettWWMMHHiinnttss to supply the window manager
    with an icon pixmap or window in one of the supported sizes. To free the
    data allocated in size_list_return, use XXFFrreeee.

    XXGGeettIIccoonnSSiizzeess can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_ICON_SIZES
        The window manager may set this property on the root window to specify
        the icon sizes it supports. The C type of this property is XXIIccoonnSSiizzee.

  SSTTRRUUCCTTUURREESS

    The

    XXIIccoonnSSiizzee structure contains:

    typedef struct {
         int min_width, min_height;
         int max_width, max_height;
         int width_inc, height_inc;
    } XIconSize;

    The width_inc and height_inc members define an arithmetic progression of
    sizes (minimum to maximum) that represent the supported icon sizes.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t(3X11R6)

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s(3X11R6)

    _X_A_l_l_o_c_W_M_H_i_n_t_s(3X11R6)

    _X_F_r_e_e(3X11R6)

    _X_S_e_t_C_o_m_m_a_n_d(3X11R6)

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t(3X11R6)

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e(3X11R6)

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s(3X11R6)

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s(3X11R6)

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s(3X11R6)

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    Xlib - C Language X Interface

