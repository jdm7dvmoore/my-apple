XrmCombineFileDatabase(3X11R6)           XrmCombineFileDatabase(3X11R6)

  XXrrmmMMeerrggeeDDaattaabbaasseess

  NNAAMMEE

    XrmMergeDatabases, XrmCombineDatabase, XrmCombineFileDatabase - merge
    resource databases

  SSYYNNTTAAXX

    void XrmMergeDatabases(source_db, target_db)
          XrmDatabase source_db, *target_db;

    void XrmCombineDatabase(source_db, target_db, override)
          XrmDatabase source_db, *target_db;
          Bool override;

    Status XrmCombineFileDatabase(filename, target_db, override)
          char *filename;
          XrmDatabase *target_db;
          Bool override;

  AARRGGUUMMEENNTTSS

    source_db
        Specifies the resource database that is to be merged into the target
        database.

    target_db
        Specifies the resource database into which the source database is to
        be merged.

    filename
        Specifies the resource database file name.

    override
        Specifies whether source entries override target ones.

  DDEESSCCRRIIPPTTIIOONN

    Calling the XXrrmmMMeerrggeeDDaattaabbaasseess function is equivalent to calling the
    XXrrmmCCoommbbiinneeDDaattaabbaassee function with an override argument of True.

    The XXrrmmCCoommbbiinneeDDaattaabbaassee function merges the contents of one database into
    another. If the same specifier is used for an entry in both databases, the
    entry in the source_db will replace the entry in the target_db if override
    is True; otherwise, the entry in source_db is discarded. If target_db
    contains NULL, XXrrmmCCoommbbiinneeDDaattaabbaassee simply stores source_db in it.
    Otherwise, source_db is destroyed by the merge, but the database pointed
    to by target_db is not destroyed. The database entries are merged without
    changing values or types, regardless of the locales of the databases. The
    locale of the target database is not modified.

    The XXrrmmCCoommbbiinneeFFiilleeDDaattaabbaassee function merges the contents of a resource file
    into a database. If the same specifier is used for an entry in both the
    file and the database, the entry in the file will replace the entry in the
    database if override is True; otherwise, the entry in the file is
    discarded. The file is parsed in the current locale. If the file cannot be
    read, a zero status is returned; otherwise, a nonzero status is returned.
    If target_db contains NULL, XXrrmmCCoommbbiinneeFFiilleeDDaattaabbaassee creates and returns a
    new database to it. Otherwise, the database pointed to by target_db is not
    destroyed by the merge. The database entries are merged without changing
    values or types, regardless of the locale of the database. The locale of
    the target database is not modified.

  SSEEEE AALLSSOO

    _X_r_m_G_e_t_R_e_s_o_u_r_c_e(3X11R6)

    _X_r_m_I_n_i_t_i_a_l_i_z_e(3X11R6)

    _X_r_m_P_u_t_R_e_s_o_u_r_c_e(3X11R6)

    Xlib - C Language X Interface

