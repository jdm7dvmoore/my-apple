XGetModifierMapping(3X11R6)                 XGetModifierMapping(3X11R6)

  XXCChhaannggeeKKeeyybbooaarrddMMaappppiinngg

  NNAAMMEE

    XChangeKeyboardMapping, XGetKeyboardMapping, XDisplayKeycodes,
    XSetModifierMapping, XGetModifierMapping, XNewModifiermap,
    XInsertModifiermapEntry, XDeleteModifiermapEntry, XFreeModifierMap
    XModifierKeymap - manipulate keyboard encoding and keyboard encoding
    structure

  SSYYNNTTAAXX

    XChangeKeyboardMapping(display, first_keycode, keysyms_per_keycode,
    keysyms, num_codes)
          Display *display;
          int first_keycode;
          int keysyms_per_keycode;
          KeySym *keysyms;
          int num_codes;

    KeySym *XGetKeyboardMapping(display, first_keycode, keycode_count,
                                keysyms_per_keycode_return)
          Display *display;
          KeyCode first_keycode;
          int keycode_count;
          int *keysyms_per_keycode_return;

    XDisplayKeycodes(display, min_keycodes_return, max_keycodes_return)
            Display *display;
            int *min_keycodes_return, *max_keycodes_return;

    int XSetModifierMapping(display, modmap)
            Display *display;
            XModifierKeymap *modmap;

    XModifierKeymap *XGetModifierMapping(display)
          Display *display;

    XModifierKeymap *XNewModifiermap(max_keys_per_mod)
            int max_keys_per_mod;

    XModifierKeymap *XInsertModifiermapEntry(modmap, keycode_entry, modifier)
         XModifierKeymap *modmap;
         KeyCode keycode_entry;
         int modifier;

    XModifierKeymap *XDeleteModifiermapEntry(modmap, keycode_entry, modifier)
         XModifierKeymap *modmap;
         KeyCode keycode_entry;
         int modifier;

    XFreeModifiermap(modmap)
            XModifierKeymap *modmap;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    first_keycode
        Specifies the first KeyCode that is to be changed or returned.

    keycode_count
        Specifies the number of KeyCodes that are to be returned.

    keycode_entry
        Specifies the KeyCode.

    keysyms
        Specifies an array of KeySyms.

    keysyms_per_keycode
        Specifies the number of KeySyms per KeyCode.

    keysyms_per_keycode_return
        Returns the number of KeySyms per KeyCode.

    max_keys_per_mod
        Specifies the number of KeyCode entries preallocated to the modifiers
        in the map.

    max_keycodes_return
        Returns the maximum number of KeyCodes.

    min_keycodes_return
        Returns the minimum number of KeyCodes.

    modifier
        Specifies the modifier.

    modmap
        Specifies the XXMMooddiiffiieerrKKeeyymmaapp structure.

    num_codes
        Specifies the number of KeyCodes that are to be changed.

  DDEESSCCRRIIPPTTIIOONN

    The XXCChhaannggeeKKeeyybbooaarrddMMaappppiinngg function defines the symbols for the specified
    number of KeyCodes starting with first_keycode. The symbols for KeyCodes
    outside this range remain unchanged. The number of elements in keysyms
    must be:

    num_codes * keysyms_per_keycode

    The specified first_keycode must be greater than or equal to min_keycode
    returned by XXDDiissppllaayyKKeeyyccooddeess, or a BadValue error results. In addition,
    the following expression must be less than or equal to max_keycode as
    returned by XXDDiissppllaayyKKeeyyccooddeess, or a BadValue error results:

    first_keycode + num_codes - 1

    KeySym number N, counting from zero, for KeyCode K has the following index
    in keysyms, counting from zero:

    (K - first_keycode) * keysyms_per_keycode + N

    The specified keysyms_per_keycode can be chosen arbitrarily by the client
    to be large enough to hold all desired symbols. A special KeySym value of
    NoSymbol should be used to fill in unused elements for individual
    KeyCodes. It is legal for NoSymbol to appear in nontrailing positions of
    the effective list for a KeyCode. XXCChhaannggeeKKeeyybbooaarrddMMaappppiinngg generates a
    MappingNotify event.

    There is no requirement that the X server interpret this mapping. It is
    merely stored for reading and writing by clients.

    XXCChhaannggeeKKeeyybbooaarrddMMaappppiinngg can generate BadAlloc and BadValue errors.

    The XXGGeettKKeeyybbooaarrddMMaappppiinngg function returns the symbols for the specified
    number of KeyCodes starting with first_keycode. The value specified in
    first_keycode must be greater than or equal to min_keycode as returned by
    XXDDiissppllaayyKKeeyyccooddeess, or a BadValue error results. In addition, the following
    expression must be less than or equal to max_keycode as returned by
    XXDDiissppllaayyKKeeyyccooddeess:

    first_keycode + keycode_count - 1

    If this is not the case, a BadValue error results. The number of elements
    in the KeySyms list is:

    keycode_count * keysyms_per_keycode_return

    KeySym number N, counting from zero, for KeyCode K has the following index
    in the list, counting from zero:

    (K - first_code) * keysyms_per_code_return + N

    The X server arbitrarily chooses the keysyms_per_keycode_return value to
    be large enough to report all requested symbols. A special KeySym value of
    NoSymbol is used to fill in unused elements for individual KeyCodes. To
    free the storage returned by XXGGeettKKeeyybbooaarrddMMaappppiinngg, use XXFFrreeee.

    XXGGeettKKeeyybbooaarrddMMaappppiinngg can generate a BadValue error.

    The XXDDiissppllaayyKKeeyyccooddeess function returns the min-keycodes and max-keycodes
    supported by the specified display. The minimum number of KeyCodes
    returned is never less than 8, and the maximum number of KeyCodes returned
    is never greater than 255. Not all KeyCodes in this range are required to
    have corresponding keys.

    The XXSSeettMMooddiiffiieerrMMaappppiinngg function specifies the KeyCodes of the keys (if
    any) that are to be used as modifiers. If it succeeds, the X server
    generates a MappingNotify event, and XXSSeettMMooddiiffiieerrMMaappppiinngg returns
    MappingSuccess. X permits at most 8 modifier keys. If more than 8 are
    specified in the XXMMooddiiffiieerrKKeeyymmaapp structure, a BadLength error results.

    The modifiermap member of the XXMMooddiiffiieerrKKeeyymmaapp structure contains 8 sets of
    max_keypermod KeyCodes, one for each modifier in the order Shift, Lock,
    Control, Mod1, Mod2, Mod3, Mod4, and Mod5. Only nonzero KeyCodes have
    meaning in each set, and zero KeyCodes are ignored. In addition, all of
    the nonzero KeyCodes must be in the range specified by min_keycode and
    max_keycode in the Display structure, or a BadValue error results.

    An X server can impose restrictions on how modifiers can be changed, for
    example, if certain keys do not generate up transitions in hardware, if
    auto-repeat cannot be disabled on certain keys, or if multiple modifier
    keys are not supported. If some such restriction is violated, the status
    reply is MappingFailed, and none of the modifiers are changed. If the new
    KeyCodes specified for a modifier differ from those currently defined and
    any (current or new) keys for that modifier are in the logically down
    state, XXSSeettMMooddiiffiieerrMMaappppiinngg returns MappingBusy, and none of the modifiers
    is changed.

    XXSSeettMMooddiiffiieerrMMaappppiinngg can generate BadAlloc and BadValue errors.

    The XXGGeettMMooddiiffiieerrMMaappppiinngg function returns a pointer to a newly created
    XXMMooddiiffiieerrKKeeyymmaapp structure that contains the keys being used as modifiers.
    The structure should be freed after use by calling XXFFrreeeeMMooddiiffiieerrMMaapp. If
    only zero values appear in the set for any modifier, that modifier is
    disabled.

    The XXNNeewwMMooddiiffiieerrmmaapp function returns a pointer to XXMMooddiiffiieerrKKeeyymmaapp
    structure for later use.

    The XXIInnsseerrttMMooddiiffiieerrmmaappEEnnttrryy function adds the specified KeyCode to the set
    that controls the specified modifier and returns the resulting
    XXMMooddiiffiieerrKKeeyymmaapp structure (expanded as needed).

    The XXDDeelleetteeMMooddiiffiieerrmmaappEEnnttrryy function deletes the specified KeyCode from
    the set that controls the specified modifier and returns a pointer to the
    resulting XXMMooddiiffiieerrKKeeyymmaapp structure.

    The XXFFrreeeeMMooddiiffiieerrMMaapp function frees the specified XXMMooddiiffiieerrKKeeyymmaapp
    structure.

  SSTTRRUUCCTTUURREESS

    The XXMMooddiiffiieerrKKeeyymmaapp structure contains:

    typedef struct {
         int max_keypermod;  /* This server's max number of keys per modifier
    */
         KeyCode *modifiermap;/* An 8 by max_keypermod array of the modifiers
    */
    } XModifierKeymap;

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_F_r_e_e(3X11R6)

    _X_S_e_t_P_o_i_n_t_e_r_M_a_p_p_i_n_g(3X11R6)

    Xlib - C Language X Interface

