XErrorEvent(3X11R6)                                 XErrorEvent(3X11R6)

  XXEErrrroorrEEvveenntt

  NNAAMMEE

    XErrorEvent - X error event structure

  SSTTRRUUCCTTUURREESS

    The XXEErrrroorrEEvveenntt structure contains:

    typedef struct {
         int type;
         Display *display;   /* Display the event was read from */
         unsigned long serial;/* serial number of failed request */
         unsigned char error_code;/* error code of failed request */
         unsigned char request_code;/* Major op-code of failed request */
         unsigned char minor_code;/* Minor op-code of failed request */
         XID resourceid;     /* resource id */
    } XErrorEvent;

    When you receive this event, the structure members are set as follows.

    The serial member is the number of requests, starting from one, sent over
    the network connection since it was opened. It is the number that was the
    value of NNeexxttRReeqquueesstt immediately before the failing call was made. The
    request_code member is a protocol request of the procedure that failed, as
    defined in <X11/Xproto.h>.

  SSEEEE AALLSSOO

    _A_l_l_P_l_a_n_e_s(3X11R6)

    _X_A_n_y_E_v_e_n_t(3X11R6)

    _X_B_u_t_t_o_n_E_v_e_n_t(3X11R6)

    _X_C_r_e_a_t_e_W_i_n_d_o_w_E_v_e_n_t(3X11R6)

    _X_C_i_r_c_u_l_a_t_e_E_v_e_n_t(3X11R6)

    _X_C_i_r_c_u_l_a_t_e_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_C_o_l_o_r_m_a_p_E_v_e_n_t(3X11R6)

    _X_C_o_n_f_i_g_u_r_e_E_v_e_n_t(3X11R6)

    _X_C_o_n_f_i_g_u_r_e_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_C_r_o_s_s_i_n_g_E_v_e_n_t(3X11R6)

    _X_D_e_s_t_r_o_y_W_i_n_d_o_w_E_v_e_n_t(3X11R6)

    _X_E_x_p_o_s_e_E_v_e_n_t(3X11R6)

    _X_F_o_c_u_s_C_h_a_n_g_e_E_v_e_n_t(3X11R6)

    _X_G_r_a_p_h_i_c_s_E_x_p_o_s_e_E_v_e_n_t(3X11R6)

    _X_G_r_a_v_i_t_y_E_v_e_n_t(3X11R6)

    _X_K_e_y_m_a_p_E_v_e_n_t(3X11R6)

    _X_M_a_p_E_v_e_n_t(3X11R6)

    _X_M_a_p_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_P_r_o_p_e_r_t_y_E_v_e_n_t(3X11R6)

    _X_R_e_p_a_r_e_n_t_E_v_e_n_t(3X11R6)

    _X_R_e_s_i_z_e_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_S_e_l_e_c_t_i_o_n_C_l_e_a_r_E_v_e_n_t(3X11R6)

    _X_S_e_l_e_c_t_i_o_n_E_v_e_n_t(3X11R6)

    _X_S_e_l_e_c_t_i_o_n_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_U_n_m_a_p_E_v_e_n_t(3X11R6)

    Xlib - C Language X Interface

