XtOpenApplication(3X11R6)                     XtOpenApplication(3X11R6)

  XXttOOppeennAApppplliiccaattiioonn

  NNAAMMEE

    XtOpenApplication, XtVaOpenApplication - initialize, open, or close a
    display

  SSYYNNTTAAXX

    Widget XtOpenApplication(app_context_return, application_class, options,
    num_options, argc_in_out, argv_in_out, fallback_resources, widget_class,
    args, num_args)
          XtAppContext* app_context_return;
          String application_class;
          XrmOptionDescRec* options;
          Cardinal num_options;
          int* argc_in_out;
          String* argv_in_out;
          String* fallback_resources;
          WidgetClass widget_class;
          ArgList args;
          Cardinal num_args;

    Widget XtVaOpenApplication(app_context_return, application_class, options,
    num_options, argc_in_out, argv_in_out, fallback_resources, widget_class,
    ...)
          XtAppContext* app_context_return;
          String application_class;
          XrmOptionDescRec* options;
          Cardinal num_options;
          int* argc_in_out;
          String* argv_in_out;
          String* fallback_resources;
          WidgetClass widget_class;

  AARRGGUUMMEENNTTSS

    app_context_return
        Specifies the application context.

    application_class
        Specifies the class name of this application, which usually is the
        generic name for all instances of this application.

    options
        Specifies how to parse the command line for any application-specific
        resources. The options argument is passed as a parameter to
        XXrrmmPPaarrsseeCCoommmmaanndd. For further information, see Xlib - C Language X
        Interface.

    num_options
        Specifies the number of entries in the options list.

    argc_in_out
        Specifies a pointer to the number of command line parameters.

    argv_in_out
        Specifies the command line parameters.

    fallback_resources
        Specifies resource values to be used if the application class resource
        file cannot be opened or read, or NULL.

    widget_class
        Specifies the widget class of the shell to be created.

    args
        Specifies the argument list to override any other resource
        specification for the created shell widget.

    num_args
        Specifies the number of entries in the argument list.

    ...
        Specifies the variable argument list to override any other resource
        specification for the created shell widget.

  DDEESSCCRRIIPPTTIIOONN

    The XXttOOppeennAApppplliiccaattiioonn function calls XXttTToooollkkiittIInniittiiaalliizzee followed by
    XXttCCrreeaatteeAApppplliiccaattiioonnCCoonntteexxtt, then calls XXttOOppeennDDiissppllaayy with display_string
    NULL and application_name NULL, and finally calls XXttAAppppCCrreeaatteeSShheellll with
    appcation_name NULL, widget_class applicationShellWidgetClass, and the
    specified args and num_args and returns the created shell. The modified
    argc and argv returned by XXttDDiissppllaayyIInniittiiaalliizzee are returned in argc_in_out
    and argv_in_out. If app_context_return is not NULL, the created
    application context is also returned. If the display specified by the
    command line cannot be opened, an error message is issued and
    XtOpenApplication terminates the application. If fallback_resources is
    non-NULL, XXttAAppppSSeettFFaallllbbaacckkRReessoouurrcceess is called with the value prior to
    calling XXttOOppeennDDiissppllaayy.

    XtAppInitialize and XtVaAppInitialize have been superceded by
    XXttOOppeennAApppplliiccaattiioonn and XXttVVaaOOppeennAApppplliiccaattiioonn respectively.

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

