XDrawImageString16(3X11R6)                   XDrawImageString16(3X11R6)

  XXDDrraawwIImmaaggeeSSttrriinngg

  NNAAMMEE

    XDrawImageString, XDrawImageString16 - draw image text

  SSYYNNTTAAXX

    XDrawImageString(display, d, gc, x, y, string, length)
          Display *display;
          Drawable d;
          GC gc;
          int x, y;
          char *string;
          int length; 

    XDrawImageString16(display, d, gc, x, y, string, length)
          Display *display;
          Drawable d;
          GC gc;
          int x, y;
          XChar2b *string;
          int length; 

  AARRGGUUMMEENNTTSS

    d
        Specifies the drawable.

    display
        Specifies the connection to the X server.

    gc
        Specifies the GC.

    length
        Specifies the number of characters in the string argument.

    string
        Specifies the character string.
    x
    y
        Specify the x and y coordinates, which are relative to the origin of
        the specified drawable and define the origin of the first character.

  DDEESSCCRRIIPPTTIIOONN

    The XXDDrraawwIImmaaggeeSSttrriinngg1166 function is similar to XXDDrraawwIImmaaggeeSSttrriinngg except that
    it uses 2-byte or 16-bit characters. Both functions also use both the
    foreground and background pixels of the GC in the destination.

    The effect is first to fill a destination rectangle with the background
    pixel defined in the GC and then to paint the text with the foreground
    pixel. The upper-left corner of the filled rectangle is at:

    [x, y - font-ascent]

    The width is:

    overall-width

    The height is:

    font-ascent + font-descent

    The overall-width, font-ascent, and font-descent are as would be returned
    by XXQQuueerryyTTeexxttEExxtteennttss using gc and string. The function and fill-style
    defined in the GC are ignored for these functions. The effective function
    is GXcopy, and the effective fill-style is FillSolid.

    For fonts defined with 2-byte matrix indexing and used with
    XXDDrraawwIImmaaggeeSSttrriinngg, each byte is used as a byte2 with a byte1 of zero.

    Both functions use these GC components: plane-mask, foreground,
    background, font, subwindow-mode, clip-x-origin, clip-y-origin, and clip-
    mask.

    XXDDrraawwIImmaaggeeSSttrriinngg and XXDDrraawwIImmaaggeeSSttrriinngg1166 can generate BadDrawable, BadGC,
    and BadMatch errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadDrawable
        A value for a Drawable argument does not name a defined Window or
        Pixmap.

    BadGC
        A value for a GContext argument does not name a defined GContext.

    BadMatch
        An InputOnly window is used as a Drawable.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

  SSEEEE AALLSSOO

    _X_D_r_a_w_S_t_r_i_n_g(3X11R6)

    _X_D_r_a_w_T_e_x_t(3X11R6)

    _X_L_o_a_d_F_o_n_t(3X11R6)

    _X_T_e_x_t_E_x_t_e_n_t_s(3X11R6)

    Xlib - C Language X Interface

