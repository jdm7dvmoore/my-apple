XIconifyWindow(3X11R6)                           XIconifyWindow(3X11R6)

  XXIIccoonniiffyyWWiinnddooww

  NNAAMMEE

    XIconifyWindow, XWithdrawWindow, XReconfigureWMWindow - manipulate top-
    level windows

  SSYYNNTTAAXX

    Status XIconifyWindow(display, w, screen_number)
          Display *display;
          Window w;
          int screen_number;

    Status XWithdrawWindow(display, w, screen_number)
          Display *display;
          Window w;
          int screen_number;

    Status XReconfigureWMWindow(display, w, screen_number, value_mask, values)
          Display *display;
          Window w;
          int screen_number;
          unsigned int value_mask;
          XWindowChanges *values;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    screen_number
        Specifies the appropriate screen number on the host server.

    value_mask
        Specifies which values are to be set using information in the values
        structure. This mask is the bitwise inclusive OR of the valid
        configure window values bits.

    values
        Specifies the XXWWiinnddoowwCChhaannggeess structure.

    w
        Specifies the window.

  DDEESSCCRRIIPPTTIIOONN

    The XXIIccoonniiffyyWWiinnddooww function sends a WM_CHANGE_STATE ClientMessage event
    with a format of 32 and a first data element of IconicState (as described
    in section 4.1.4 of the Inter-Client Communication Conventions Manual) and
    a window of w to the root window of the specified screen with an event
    mask set to SubstructureNotifyMask| SubstructureRedirectMask. Window
    managers may elect to receive this message and if the window is in its
    normal state, may treat it as a request to change the window's state from
    normal to iconic. If the WM_CHANGE_STATE property cannot be interned,
    XXIIccoonniiffyyWWiinnddooww does not send a message and returns a zero status. It
    returns a nonzero status if the client message is sent successfully;
    otherwise, it returns a zero status.

    The XXWWiitthhddrraawwWWiinnddooww function unmaps the specified window and sends a
    synthetic UnmapNotify event to the root window of the specified screen.
    Window managers may elect to receive this message and may treat it as a
    request to change the window's state to withdrawn. When a window is in the
    withdrawn state, neither its normal nor its iconic representations is
    visible. It returns a nonzero status if the UnmapNotify event is
    successfully sent; otherwise, it returns a zero status.

    XXWWiitthhddrraawwWWiinnddooww can generate a BadWindow error.

    The XXRReeccoonnffiigguurreeWWMMWWiinnddooww function issues a ConfigureWindow request on the
    specified top-level window. If the stacking mode is changed and the
    request fails with a BadMatch error, the error is trapped by Xlib and a
    synthetic ConfigureRequestEvent containing the same configuration
    parameters is sent to the root of the specified window. Window managers
    may elect to receive this event and treat it as a request to reconfigure
    the indicated window. It returns a nonzero status if the request or event
    is successfully sent; otherwise, it returns a zero status.

    XXRReeccoonnffiigguurreeWWMMWWiinnddooww can generate BadValue and BadWindow errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_C_h_a_n_g_e_W_i_n_d_o_w_A_t_t_r_i_b_u_t_e_s(3X11R6)

    _X_C_o_n_f_i_g_u_r_e_W_i_n_d_o_w(3X11R6)

    _X_C_r_e_a_t_e_W_i_n_d_o_w(3X11R6)

    _X_D_e_s_t_r_o_y_W_i_n_d_o_w(3X11R6)

    _X_R_a_i_s_e_W_i_n_d_o_w(3X11R6)

    _X_M_a_p_W_i_n_d_o_w(3X11R6)

    _X_U_n_m_a_p_W_i_n_d_o_w(3X11R6)

    Xlib - C Language X Interface

